<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// VIEW AUTH
Route::get('/profile', function () {
    return view('auth.profile');
})->middleware('auth');

Route::group(['middleware' => 'auth'], function () {
    Route::get('profile', 'ProfileController@edit')->name('profile.edit');

    Route::patch('profile', 'ProfileController@update')->name('profile.update');

});



//Test apakah masuk?

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

## P L A C E (Pengenalan Tempat)
Route::get('/place', 'PlaceController@index')->name('place.index')->middleware('auth');
Route::get('/place/tambah', 'PlaceController@create')->name('place.create')->middleware('auth');
Route::post('/place', 'PlaceController@store')->name('place.store')->middleware('auth');
Route::get('/place/{places_id}', 'PlaceController@show')->name('place.show')->middleware('auth');
Route::delete('/place/{places_id}', 'PlaceController@destroy')->name('place.destroy')->middleware('auth');
Route::get('/place/print', 'PlaceController@print')->name('place.print')->middleware('auth');


## C O O R D I N A T O R (Keterangan Petugas bagian Koordinator)
Route::get('/coordinator', 'CoordinatorController@index')->name('coordinator.index')->middleware('auth');
Route::get('/coordinator/tambah', 'CoordinatorController@create')->name('coordinator.create')->middleware('auth');
Route::post('/coordinator', 'CoordinatorController@store')->name('coordinator.store')->middleware('auth');
Route::get('/coordinator/{coordinators_id}', 'CoordinatorController@show')->name('coordinator.show')->middleware('auth');
Route::get('/coordinator/{coordinator_id}/edit', 'CoordinatorController@edit')->name('coordinator.edit')->middleware('auth');
Route::put('/coordinator/{coordinator_id}', 'CoordinatorController@update')->name('coordinator.update')->middleware('auth');
Route::delete('/coordinator/{coordinators_id}', 'CoordinatorController@destroy')->name('coordinator.destroy')->middleware('auth');
Route::get('/coordinator/print', 'CoordinatorController@print')->name('coordinator.print')->middleware('auth');

## O P E R A T O R (Keterangan Petugas bagian Pendata)
Route::get('/operator', 'OperatorController@index')->name('operator.index')->middleware('auth');
Route::get('/operator/tambah', 'OperatorController@create')->name('operator.create')->middleware('auth');
Route::post('/operator', 'OperatorController@store')->name('operator.store')->middleware('auth');
Route::get('/operator/{operators_id}', 'OperatorController@show')->name('operator.show')->middleware('auth');
Route::get('/operator/{operator_id}/edit', 'OperatorController@edit')->name('operator.edit')->middleware('auth');
Route::put('/operator/{operator_id}', 'OperatorController@update')->name('operator.update')->middleware('auth');
Route::delete('/operator/{operators_id}', 'OperatorController@destroy')->name('operator.destroy')->middleware('auth');

## I D E N T I T Y (Identitas Lembaga)
Route::get('/identity', 'IdentityController@index')->name('identity.index')->middleware('auth');
Route::get('/identity/tambah', 'IdentityController@create')->name('identity.create')->middleware('auth');
Route::post('/identity', 'IdentityController@store')->name('identity.store')->middleware('auth');
Route::get('/identity/{identities_id}', 'IdentityController@show')->name('identity.show')->middleware('auth');
Route::get('/identity/{identities_id}/edit', 'IdentityController@edit')->name('identity.edit')->middleware('auth');
Route::put('/identity/{identities_id}', 'IdentityController@update')->name('identity.update')->middleware('auth');
Route::delete('/identity/{identities_id}', 'IdentityController@destroy')->name('identity.destroy')->middleware('auth');

# L E M B A G A  K O N S U L T A S I  K E S E J A H T E R A A N  K E L U A R G A (LK3)
Route::get('/lkkk', 'LkkkController@index')->name('lkkk.index')->middleware('auth');
Route::get('/lkkk/tambah', 'LkkkController@create')->name('lkkk.create')->middleware('auth');
Route::post('/lkkk', 'LkkkController@store')->name('lkkk.store')->middleware('auth');
Route::get('/lkkk/{lkkks_id}', 'LkkkController@show')->name('lkkk.show')->middleware('auth');
Route::get('/lkkk/{lkkks_id}/edit', 'LkkkController@edit')->name('lkkk.edit')->middleware('auth');
Route::put('/lkkk/{lkkks_id}', 'LkkkController@update')->name('lkkk.update')->middleware('auth');
Route::delete('/lkkk/{lkkks_id}', 'LkkkController@destroy')->name('lkkk.destroy')->middleware('auth');

# L E M B A G A  K E S E J A H T E R A A N  S O S I A L (LKS)
Route::get('/lks', 'LksController@index')->name('lks.index')->middleware('auth');
Route::get('/lks/tambah', 'LksController@create')->name('lks.create')->middleware('auth');
Route::post('/lks', 'LksController@store')->name('lks.store')->middleware('auth');
Route::get('/lks/{lkses_id}', 'LksController@show')->name('lks.show')->middleware('auth');
Route::get('/lks/{lkses_id}/edit', 'LksController@edit')->name('lks.edit')->middleware('auth');
Route::put('/lks/{lkses_id}', 'LksController@update')->name('lks.update')->middleware('auth');
Route::delete('/lks/{lkses_id}', 'LksController@destroy')->name('lks.destroy')->middleware('auth');

# K E L U A R G A  P I O N E R (families)
Route::get('/family', 'FamilyController@index')->name('family.index')->middleware('auth');
Route::get('/family/tambah', 'FamilyController@create')->name('family.create')->middleware('auth');
Route::post('/family', 'FamilyController@store')->name('family.store')->middleware('auth');
Route::get('/family/{families_id}', 'FamilyController@show')->name('family.show')->middleware('auth');
Route::get('/family/{families_id}/edit', 'FamilyController@edit')->name('family.edit')->middleware('auth');
Route::put('/family/{families_id}', 'FamilyController@update')->name('family.update')->middleware('auth');
Route::delete('/family/{families_id}', 'FamilyController@destroy')->name('family.destroy')->middleware('auth');
Route::get('/print', 'FamilyController@print')->name('family.print')->middleware('auth');

# E X T E N S I O N
Route::get('/extension', 'ExtensionController@index')->name('extension.index')->middleware('auth');
Route::get('/extension/tambah', 'ExtensionController@create')->name('extension.create')->middleware('auth');
Route::post('/extension', 'ExtensionController@store')->name('extension.store')->middleware('auth');
Route::get('/extension/{extensions_id}', 'ExtensionController@show')->name('extension.show')->middleware('auth');
Route::get('/extension/{extensions_id}/edit', 'ExtensionController@edit')->name('extension.edit')->middleware('auth');
Route::put('/extension/{extensions_id}', 'ExtensionController@update')->name('extension.update')->middleware('auth');
Route::delete('/extension/{extensions_id}', 'ExtensionController@destroy')->name('extension.destroy')->middleware('auth');

# P E K E R J A  S O S I A L  P R O F E S I O N A L (profesionals)
Route::get('/profesional', 'ProfesionalController@index')->name('profesional.index')->middleware('auth');
Route::get('/profesional/tambah', 'ProfesionalController@create')->name('profesional.create')->middleware('auth');
Route::post('/profesional', 'ProfesionalController@store')->name('profesional.store')->middleware('auth');
Route::get('/profesional/{profesionals_id}', 'ProfesionalController@show')->name('profesional.show')->middleware('auth');
Route::get('/profesional/{profesionals_id}/edit', 'ProfesionalController@edit')->name('profesional.edit')->middleware('auth');
Route::put('/profesional/{profesionals_id}', 'ProfesionalController@update')->name('profesional.update')->middleware('auth');
Route::delete('/profesional/{profesionals_id}', 'ProfesionalController@destroy')->name('profesional.destroy')->middleware('auth');

# P S M ( P E K E R J A  S O S I A L  M A S Y A R A K A T)
Route::get('/psm', 'PsmController@index')->name('psm.index')->middleware('auth');
Route::get('/psm/tambah', 'PsmController@create')->name('psm.create')->middleware('auth');
Route::post('/psm', 'PsmController@store')->name('psm.store')->middleware('auth');
Route::get('/psm/{psms_id}', 'PsmController@show')->name('psm.show')->middleware('auth');
Route::get('/psm/{psms_id}/edit', 'PsmController@edit')->name('psm.edit')->middleware('auth');
Route::put('/psm/{psms_id}', 'PsmController@update')->name('psm.update')->middleware('auth');
Route::delete('/psm/{psms_id}', 'PsmController@destroy')->name('psm.destroy')->middleware('auth');


# T A R U N A  S I A G A  B E N C A N A (Cadets)
Route::get('/cadet', 'CadetController@index')->name('cadet.index')->middleware('auth');
Route::get('/cadet/tambah', 'CadetController@create')->name('cadet.create')->middleware('auth');
Route::post('/cadet', 'CadetController@store')->name('cadet.store')->middleware('auth');
Route::get('/cadet/{cadets_id}', 'CadetController@show')->name('cadet.show')->middleware('auth');
Route::get('/cadet/{cadets_id}/edit', 'CadetController@edit')->name('cadet.edit')->middleware('auth');
Route::put('/cadet/{cadets_id}', 'CadetController@update')->name('cadet.update')->middleware('auth');
Route::delete('/cadet/{cadets_id}', 'CadetController@destroy')->name('cadet.destroy')->middleware('auth');


# T K S K ( T E N A G A  K E S E J A H T E R A A N  S O S I A L  K E C A M A T A N)
Route::get('/tksk', 'TkskController@index')->name('tksk.index')->middleware('auth');
Route::get('/tksk/tambah', 'TkskController@create')->name('tksk.create')->middleware('auth');
Route::post('/tksk', 'TkskController@store')->name('tksk.store')->middleware('auth');
Route::get('/tksk/{tksks_id}', 'TkskController@show')->name('tksk.show')->middleware('auth');
Route::get('/tksk/{tksks_id}/edit', 'TkskController@edit')->name('tksk.edit')->middleware('auth');
Route::put('/tksk/{tksks_id}', 'TkskController@update')->name('tksk.update')->middleware('auth');
Route::delete('/tksk/{tksks_id}', 'TkskController@destroy')->name('tksk.destroy')->middleware('auth');

# T K S K ( T E N A G A  K E S E J A H T E R A A N  S O S I A L  K E C A M A T A N)
Route::get('/wpks', 'WpksController@index')->name('wpks.index')->middleware('auth');
Route::get('/wpks/tambah', 'WpksController@create')->name('wpks.create')->middleware('auth');
Route::post('/wpks', 'WpksController@store')->name('wpks.store')->middleware('auth');
Route::get('/wpks/{wpkss_id}', 'WpksController@show')->name('wpks.show')->middleware('auth');
Route::get('/wpks/{wpkss_id}/edit', 'WpksController@edit')->name('wpks.edit')->middleware('auth');
Route::put('/wpks/{wpkss_id}', 'WpksController@update')->name('wpks.update')->middleware('auth');
Route::delete('/wpks/{wpkss_id}', 'WpksController@destroy')->name('wpks.destroy')->middleware('auth');

# K A R A N G  T A R U N A (youths)
Route::get('/youth', 'YouthController@index')->name('youth.index')->middleware('auth');
Route::get('/youth/tambah', 'YouthController@create')->name('youth.create')->middleware('auth');
Route::post('/youth', 'YouthController@store')->name('youth.store')->middleware('auth');
Route::get('/youth/{youths_id}', 'YouthController@show')->name('youth.show')->middleware('auth');
Route::get('/youth/{youths_id}/edit', 'YouthController@edit')->name('youth.edit')->middleware('auth');
Route::put('/youth/{youths_id}', 'YouthController@update')->name('youth.update')->middleware('auth');
Route::delete('/youth/{youths_id}', 'YouthController@destroy')->name('youth.destroy')->middleware('auth');

# D U N I A  U S A H A  Y A N G  M E L A K U K A N  U S A H A   K E S E J A H T E R A A N (world)
Route::get('/world', 'WorldController@index')->name('world.index')->middleware('auth');
Route::get('/world/tambah', 'WorldController@create')->name('world.create')->middleware('auth');
Route::post('/world', 'WorldController@store')->name('world.store')->middleware('auth');
Route::get('/world/{worlds_id}', 'WorldController@show')->name('world.show')->middleware('auth');
Route::get('/world/{worlds_id}/edit', 'WorldController@edit')->name('world.edit')->middleware('auth');
Route::put('/world/{worlds_id}', 'WorldController@update')->name('world.update')->middleware('auth');
Route::delete('/world/{worlds_id}', 'WorldController@destroy')->name('world.destroy')->middleware('auth');

# W A H A N A  K E S E J A H T E R A A N  S O S I A L  M A S Y A R A K A T (wahana)
Route::get('/wksbm', 'WksbmController@index')->name('wksbm.index')->middleware('auth');
Route::get('/wksbm/tambah', 'WksbmController@create')->name('wksbm.create')->middleware('auth');
Route::post('/wksbm', 'WksbmController@store')->name('wksbm.store')->middleware('auth');
Route::get('/wksbm/{wksbms_id}', 'WksbmController@show')->name('wksbm.show')->middleware('auth');
Route::get('/wksbm/{wksbms_id}/edit', 'WksbmController@edit')->name('wksbm.edit')->middleware('auth');
Route::put('/wksbm/{wksbms_id}', 'WksbmController@update')->name('wksbm.update')->middleware('auth');
Route::delete('/wksbm/{wksbms_id}', 'WksbmController@destroy')->name('wksbm.destroy')->middleware('auth');


# E X P O R T  S E C T I O N
Route::get('cadet-excel','CadetController@export')->name('cadet.export')->middleware('auth');
Route::get('extension-excel','ExtensionController@export')->name('extension.export')->middleware('auth');
Route::get('family-excel','FamilyController@export')->name('family.export')->middleware('auth');
Route::get('identity-excel','IdentityController@export')->name('identity.export')->middleware('auth');
Route::get('lkkk-excel','LkkkController@export')->name('lkkk.export')->middleware('auth');
Route::get('lks-excel','LksController@export')->name('lks.export')->middleware('auth');
Route::get('profesional-excel','ProfesionalController@export')->name('profesional.export')->middleware('auth');
Route::get('psm-excel','PsmController@export')->name('psm.export')->middleware('auth');
Route::get('tksk-excel','TkskController@export')->name('tksk.export')->middleware('auth');
Route::get('wksbm-excel','WksbmController@export')->name('wksbm.export')->middleware('auth');
Route::get('world-excel','WorldController@export')->name('world.export')->middleware('auth');
Route::get('wpks-excel','WpksController@export')->name('wpks.export')->middleware('auth');
Route::get('youth-excel','YouthController@export')->name('youth.export')->middleware('auth');

