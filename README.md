<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Final project


## Anggota Kelompok
1. Dikdik Musfar Pribadi
2. Fitri Siti Nurjanah

## Tema Project
Sistem Pendataan PSKS karang taruna haur galur
Ini projek dibuat oleh Dikdik dan Fitri untuk memenuhi kebutuhan pendataan PSKS di Kota Bandung sekaligus memenuhi tugas akhir dari Jawa Barat Coding Camp. Kami berharap kedepannya sistem ini dapat menjadi bayangan bagi pendataan digital.

## ERD

## Link Demo Video
https://www.youtube.com/watch?v=Opi_7qieU1A

https://youtu.be/xri8OSdMCFQ (Versi Lengkap)

liblary
https://youtu.be/aK30kN9F4SI

## Link Deploy
http://psks.abercode-software.com/

## TREE
📦project akhir
 ┗ 📂psks
 ┃ ┣ 📂.git
 ┃ ┃ ┣ 📂hooks
 ┃ ┃ ┃ ┣ 📜applypatch-msg.sample
 ┃ ┃ ┃ ┣ 📜commit-msg.sample
 ┃ ┃ ┃ ┣ 📜fsmonitor-watchman.sample
 ┃ ┃ ┃ ┣ 📜post-update.sample
 ┃ ┃ ┃ ┣ 📜pre-applypatch.sample
 ┃ ┃ ┃ ┣ 📜pre-commit.sample
 ┃ ┃ ┃ ┣ 📜pre-merge-commit.sample
 ┃ ┃ ┃ ┣ 📜pre-push.sample
 ┃ ┃ ┃ ┣ 📜pre-rebase.sample
 ┃ ┃ ┃ ┣ 📜pre-receive.sample
 ┃ ┃ ┃ ┣ 📜prepare-commit-msg.sample
 ┃ ┃ ┃ ┣ 📜push-to-checkout.sample
 ┃ ┃ ┃ ┗ 📜update.sample
 ┃ ┃ ┣ 📂info
 ┃ ┃ ┃ ┗ 📜exclude
 ┃ ┃ ┣ 📂logs
 ┃ ┃ ┃ ┣ 📂refs
 ┃ ┃ ┃ ┃ ┣ 📂heads
 ┃ ┃ ┃ ┃ ┃ ┗ 📜master
 ┃ ┃ ┃ ┃ ┗ 📂remotes
 ┃ ┃ ┃ ┃ ┃ ┗ 📂origin
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜master
 ┃ ┃ ┃ ┗ 📜HEAD
 ┃ ┃ ┣ 📂objects
 ┃ ┃ ┃ ┣ 📂00
 ┃ ┃ ┃ ┃ ┣ 📜1676f41b1b52fdb4efec5ed7f65496f4c7be2b
 ┃ ┃ ┃ ┃ ┣ 📜17546f17447d134efd7a45587bd1a4e503700d
 ┃ ┃ ┃ ┃ ┣ 📜183830099859cc66c7db04c53275edae797f55
 ┃ ┃ ┃ ┃ ┣ 📜1e479b0ac43de05bed5dd647b86cca47fb4eb2
 ┃ ┃ ┃ ┃ ┣ 📜356105d58501aaa7f6c68b06b5565125c57726
 ┃ ┃ ┃ ┃ ┣ 📜d782639a408482734104288ca26159099a9c32
 ┃ ┃ ┃ ┃ ┗ 📜e8eb353bb939f18c023cea089058317cc0b394
 ┃ ┃ ┃ ┣ 📂01
 ┃ ┃ ┃ ┃ ┣ 📜150654f7b43fd1cbc93dd963786e17f64108fd
 ┃ ┃ ┃ ┃ ┣ 📜4d9cef8109834e0085f429d9c0878ba676f35f
 ┃ ┃ ┃ ┃ ┣ 📜a42a51338d22cb7cf85992107e39068d97ac92
 ┃ ┃ ┃ ┃ ┗ 📜e4a6cda9eb380973b23a40d562bca8a3a198b4
 ┃ ┃ ┃ ┣ 📂02
 ┃ ┃ ┃ ┃ ┣ 📜35932654ab382f9001e05882679ef2edd723f6
 ┃ ┃ ┃ ┃ ┣ 📜5cf7db9d0a9ac9a42286f310131c623a95da22
 ┃ ┃ ┃ ┃ ┣ 📜78eeb2fb82577d7994c193df4995bd51e439a2
 ┃ ┃ ┃ ┃ ┣ 📜dea62759db460f98efb391baef4b6e90ab8ed8
 ┃ ┃ ┃ ┃ ┣ 📜ee8eaa0dcd1b233b46e873cd57d30c6cdac4a3
 ┃ ┃ ┃ ┃ ┗ 📜ffed82ef425d72dfd9fa3946deecf75cc8e65d
 ┃ ┃ ┃ ┣ 📂03
 ┃ ┃ ┃ ┃ ┣ 📜27b088e8a92fb445bfc04a41cc223c3b9a2cc5
 ┃ ┃ ┃ ┃ ┣ 📜2b7804a56bfb79cc53fda4bc991ba6777c09e5
 ┃ ┃ ┃ ┃ ┣ 📜3136ad128b7687558bc41b03cc92f39cf69149
 ┃ ┃ ┃ ┃ ┣ 📜7e17df03b0598d7bbd27ed333312e8e337fb1b
 ┃ ┃ ┃ ┃ ┣ 📜881b1344c5b5360566075ebf83688f8de9d60d
 ┃ ┃ ┃ ┃ ┣ 📜9216bceebd7158463a49d2b34cd74f70cf7a12
 ┃ ┃ ┃ ┃ ┣ 📜b9d5fcbe9864e868d689539fb259e048571567
 ┃ ┃ ┃ ┃ ┣ 📜c71906dc035d01552867a9ff0b75fd3652d40d
 ┃ ┃ ┃ ┃ ┣ 📜ceb70fc53912029ca8efcf2a2564cf9d4ae060
 ┃ ┃ ┃ ┃ ┗ 📜da7c6cebf624dd3b89c2210e8628ffded0d859
 ┃ ┃ ┃ ┣ 📂04
 ┃ ┃ ┃ ┃ ┣ 📜07ab577327b92faf6bbb6cb6391587d9db6d1a
 ┃ ┃ ┃ ┃ ┣ 📜13c581a037d26f17af1730680c72259377d064
 ┃ ┃ ┃ ┃ ┣ 📜1c3d955a522f80b0343ff7814af338b5c1320f
 ┃ ┃ ┃ ┃ ┣ 📜1d64e2540b08c9c18b615908917d19ea90d221
 ┃ ┃ ┃ ┃ ┣ 📜45e0e7b44f134840101442c096e1030949c74a
 ┃ ┃ ┃ ┃ ┣ 📜505af3d993c5d11f9442d225bc95d1f941479a
 ┃ ┃ ┃ ┃ ┗ 📜65f68888ca8b7bc289fc2618a805b1828b4623
 ┃ ┃ ┃ ┣ 📂05
 ┃ ┃ ┃ ┃ ┣ 📜167efaff521dd17ed43d14b11dd671c637af38
 ┃ ┃ ┃ ┃ ┣ 📜24e43e29d2b194fa7469c69be939cee2d8f145
 ┃ ┃ ┃ ┃ ┣ 📜37bdd05a85c5f29299a80a4228155036bed3bd
 ┃ ┃ ┃ ┃ ┣ 📜433ef53e537ff6e0c6fcf63b581d5b616b1a26
 ┃ ┃ ┃ ┃ ┣ 📜6453ea1a1e8967ab3153afa093b1195794abbd
 ┃ ┃ ┃ ┃ ┗ 📜dfca9228d9a3150d9236434e6a7e0c771f35f3
 ┃ ┃ ┃ ┣ 📂06
 ┃ ┃ ┃ ┃ ┣ 📜42291578db7211710517bad35c8e8ce8255f9c
 ┃ ┃ ┃ ┃ ┣ 📜7f947c32a5e9d57306af5a8fedc491b705fbcd
 ┃ ┃ ┃ ┃ ┣ 📜da1a3cc8365ada209123d73ee7f8485d095acd
 ┃ ┃ ┃ ┃ ┣ 📜e70f701c959a26137e3046f413ef47f31e96f3
 ┃ ┃ ┃ ┃ ┣ 📜f5ed90a8311499c3b41a8c396960636838f024
 ┃ ┃ ┃ ┃ ┗ 📜fddfbb7df1effd2da16ceac96104c35dc96557
 ┃ ┃ ┃ ┣ 📂07
 ┃ ┃ ┃ ┃ ┣ 📜89ad8e8787ffbd1da23ecd1825a63ac675661c
 ┃ ┃ ┃ ┃ ┗ 📜ff74c90607c34520cc516420bcc63f88b1d1dd
 ┃ ┃ ┃ ┣ 📂08
 ┃ ┃ ┃ ┃ ┣ 📜29d6b8923e5e24ae430ea729212ff28a6ce524
 ┃ ┃ ┃ ┃ ┣ 📜53c4d4cf15c867d814ecd7e1f9ba3baf10a654
 ┃ ┃ ┃ ┃ ┣ 📜8c204e29942a941c9cbfb8dadb44671a5b1814
 ┃ ┃ ┃ ┃ ┣ 📜91d68c1058036b91ae7e77554ee8807466d9f6
 ┃ ┃ ┃ ┃ ┣ 📜ab3dd5b5d66bfadfb6bd292a557dbee2348819
 ┃ ┃ ┃ ┃ ┣ 📜b97c9f6a613abd92d7f2310c7d1db60d62767e
 ┃ ┃ ┃ ┃ ┣ 📜d488af6249d63b3e3bed5cb38b066f7bfe1b0c
 ┃ ┃ ┃ ┃ ┣ 📜df1b84a7e625d4f0995d111ff179da692df20a
 ┃ ┃ ┃ ┃ ┣ 📜f7e88df34ffb29d30833be72ed9f3dce56ba49
 ┃ ┃ ┃ ┃ ┗ 📜f894a3a4f19d0d0f376fb442576cfb7f47ff8e
 ┃ ┃ ┃ ┣ 📂09
 ┃ ┃ ┃ ┃ ┣ 📜369bec9f8bf86b4ea6444e94c488567ee99562
 ┃ ┃ ┃ ┃ ┣ 📜6af9c0385dd3e9e7698ddf8d9467f49a0c8e42
 ┃ ┃ ┃ ┃ ┣ 📜b4d5fa51e98b51ba866dcc37664b3e7139c80b
 ┃ ┃ ┃ ┃ ┣ 📜cbb9cd99c683b7f6808eb490419283776c36a7
 ┃ ┃ ┃ ┃ ┗ 📜ed439abcbf2df4923a27be2605fc11f6d1449a
 ┃ ┃ ┃ ┣ 📂0a
 ┃ ┃ ┃ ┃ ┣ 📜1691d543b04119c60fc7051e81d8e0da5bc346
 ┃ ┃ ┃ ┃ ┣ 📜4515a1cc4c25d7e63c737b88b993c1df94a0ba
 ┃ ┃ ┃ ┃ ┣ 📜6073a306584a55ebf8b966deb0e6f34cb58c6a
 ┃ ┃ ┃ ┃ ┣ 📜ad36645ec70c3b04f446f3c6abaeead3bf74f1
 ┃ ┃ ┃ ┃ ┗ 📜b9af3f4a76040008e7a3e7d5594efd4dfd8bf4
 ┃ ┃ ┃ ┣ 📂0b
 ┃ ┃ ┃ ┃ ┣ 📜202f502fd21002eb0baf9aba936b0ebea76e23
 ┃ ┃ ┃ ┃ ┣ 📜599e12467b080acf73d9e83fee8955ce15ab12
 ┃ ┃ ┃ ┃ ┣ 📜840deda976d1c57a97fc135bb78d7bfdbe6b66
 ┃ ┃ ┃ ┃ ┗ 📜fed25383c1271321063021b72c99f2f86848ba
 ┃ ┃ ┃ ┣ 📂0c
 ┃ ┃ ┃ ┃ ┣ 📜6b7edf7909880fe4f776a87ddceb5e0383bda8
 ┃ ┃ ┃ ┃ ┣ 📜707f4500ffdb8e3d0d0a0af15401da483d1395
 ┃ ┃ ┃ ┃ ┣ 📜8439c62126235a4b13230869276ff316925297
 ┃ ┃ ┃ ┃ ┗ 📜c570b35ee6afa58a9bb764ab8f1f1c323f9618
 ┃ ┃ ┃ ┣ 📂0d
 ┃ ┃ ┃ ┃ ┣ 📜059a8db3b96df9ab41588092878de96e0ed4f2
 ┃ ┃ ┃ ┃ ┣ 📜4ef1749c73146905690a3be4a3c97af1f05e83
 ┃ ┃ ┃ ┃ ┗ 📜605ff595e3092eb638062d3b0dacc1e8595afb
 ┃ ┃ ┃ ┣ 📂0e
 ┃ ┃ ┃ ┃ ┣ 📜0d37f9618c7966c9178d2a385c5725a5a2f43b
 ┃ ┃ ┃ ┃ ┣ 📜2066746c8812c9b79658d1f660b3cdf2190fb8
 ┃ ┃ ┃ ┃ ┣ 📜436ffa56f65e490def16149ad6409ca69a04aa
 ┃ ┃ ┃ ┃ ┣ 📜62585eea0adf671f8765ee3f3728eb520fd93e
 ┃ ┃ ┃ ┃ ┣ 📜64c48197ee22d82ec3dfe4a1aabd6b54876033
 ┃ ┃ ┃ ┃ ┗ 📜e0a36a4f87c2f914288abfc133573d6a6324b7
 ┃ ┃ ┃ ┣ 📂0f
 ┃ ┃ ┃ ┃ ┣ 📜4389f91fd735990f8fa3d5f8e3fd8ba6d582ca
 ┃ ┃ ┃ ┃ ┣ 📜44d1ab8c84a5d54a1319ee6a8486c7b1f5b2ad
 ┃ ┃ ┃ ┃ ┣ 📜5d56c5ac5fd475b5dccdafe01f0231b9c303f5
 ┃ ┃ ┃ ┃ ┣ 📜7df0fbef7c3ea7cf8299cd2601b54e9de6eb9e
 ┃ ┃ ┃ ┃ ┣ 📜85b64e231e521658311c07343ec211c435f044
 ┃ ┃ ┃ ┃ ┣ 📜c1ef118a07a93721e30030e8e08cc4d2aea490
 ┃ ┃ ┃ ┃ ┣ 📜d2f0dab64c50b2e1fce128d02080621e3ee00f
 ┃ ┃ ┃ ┃ ┗ 📜f17450368f59e85cafd12748cad806414f6291
 ┃ ┃ ┃ ┣ 📂10
 ┃ ┃ ┃ ┃ ┣ 📜18203523e44564dbb5ba9378f477328f567f75
 ┃ ┃ ┃ ┃ ┣ 📜e659ca90d8d09c4ad2d94e819319fe3fb6e8ac
 ┃ ┃ ┃ ┃ ┗ 📜f370be86ea8c5df09d03db120a4b8bdf91fdc8
 ┃ ┃ ┃ ┣ 📂11
 ┃ ┃ ┃ ┃ ┣ 📜89a434c2ce755c23824a9835776ba38721090f
 ┃ ┃ ┃ ┃ ┣ 📜ebe906da96e96a256f34057fce73884ab2d6db
 ┃ ┃ ┃ ┃ ┣ 📜ee576be1888d5fd3f76e0522801c560a226719
 ┃ ┃ ┃ ┃ ┗ 📜fb11ba2ce8c2f59b7ce9cc18376821c25c3064
 ┃ ┃ ┃ ┣ 📂12
 ┃ ┃ ┃ ┃ ┣ 📜1f5a9e59d669d3046e735c1add82c2070a9b55
 ┃ ┃ ┃ ┃ ┣ 📜67bf928911661bb9434b54c74a45030efb8193
 ┃ ┃ ┃ ┃ ┣ 📜976f8f923f1be0efa6be937e1e2fe960476ffc
 ┃ ┃ ┃ ┃ ┣ 📜9f79c0f8debd3e0e71a217d3247f3401324dbe
 ┃ ┃ ┃ ┃ ┗ 📜c9d2541f219a04db54d1eb4335a4064bf0c318
 ┃ ┃ ┃ ┣ 📂13
 ┃ ┃ ┃ ┃ ┣ 📜7472015150c6de43ae58193518856f641063e7
 ┃ ┃ ┃ ┃ ┣ 📜8c1f08a2b2d32764fffe6ae261180dbcc5deec
 ┃ ┃ ┃ ┃ ┣ 📜aa0569a50710789a992d2e4cb3591587501b1b
 ┃ ┃ ┃ ┃ ┣ 📜b7ea310f4c24e52b2d7b9bdafd536568a2c3a4
 ┃ ┃ ┃ ┃ ┣ 📜bf3328633813487d8f9a2977009b723521ad21
 ┃ ┃ ┃ ┃ ┗ 📜fabd958a49ea362fda16f562f8d063cc05340c
 ┃ ┃ ┃ ┣ 📂14
 ┃ ┃ ┃ ┃ ┣ 📜0052936cbb7d02405f3b774fceefe142ea2952
 ┃ ┃ ┃ ┃ ┣ 📜10c68dd3c1b656bdf73023f20cef3888ba38bf
 ┃ ┃ ┃ ┃ ┣ 📜36918523c4cdeba2ffb87b5196ace9d359bbf8
 ┃ ┃ ┃ ┃ ┣ 📜3f18d76e634e32cff8091ba4c2691ed854e06a
 ┃ ┃ ┃ ┃ ┣ 📜983d8e923aa963b004de5029b470c9bd58b072
 ┃ ┃ ┃ ┃ ┗ 📜d6cedbfb686b685662c84912e60d7ab6c71c86
 ┃ ┃ ┃ ┣ 📂15
 ┃ ┃ ┃ ┃ ┣ 📜4640640569d16a38de1c7273e1dcd154e7077a
 ┃ ┃ ┃ ┃ ┣ 📜7ff393bb9d066ed3a0f8e616379836669db0e6
 ┃ ┃ ┃ ┃ ┣ 📜8ce938fd72a072271493a0c8a0c865d9ca59b3
 ┃ ┃ ┃ ┃ ┣ 📜8dbfa365d6215bdefbe34cb7e49287838048cc
 ┃ ┃ ┃ ┃ ┣ 📜bf80a55ca00eae60ac881194fcbdc2fc328c14
 ┃ ┃ ┃ ┃ ┣ 📜d5061ad6d2abc613fe9e78497dcc6b04a05aa2
 ┃ ┃ ┃ ┃ ┗ 📜eddf7c9d4cc5b2cdd65e6f82398afbc9e56fd3
 ┃ ┃ ┃ ┣ 📂16
 ┃ ┃ ┃ ┃ ┣ 📜8bfa41435b91dbba13d9b7365aa79c12ea208d
 ┃ ┃ ┃ ┃ ┣ 📜d2837f5f78df3d605b14ad8590b7ef24f03ada
 ┃ ┃ ┃ ┃ ┣ 📜e454ec8d72adb04f9b2cdbadcfa51ae47417a4
 ┃ ┃ ┃ ┃ ┗ 📜f7e9df46132b0a7dd51b7aa21ae6a3bd1689d7
 ┃ ┃ ┃ ┣ 📂17
 ┃ ┃ ┃ ┃ ┣ 📜96ac23a7e3ce6d378a06c5ea6f881f5a953198
 ┃ ┃ ┃ ┃ ┣ 📜c7ee56280b69a0f3e8aa432689389ecc4913ab
 ┃ ┃ ┃ ┃ ┗ 📜dc352c66cf70e186409b8233372a3ba34ee4a3
 ┃ ┃ ┃ ┣ 📂18
 ┃ ┃ ┃ ┃ ┣ 📜1e44f1c3501f63b7492e4ca92ce5f99f233481
 ┃ ┃ ┃ ┃ ┣ 📜3b61c4e6ac7ae26f80285f5ef82c92fcb26ad7
 ┃ ┃ ┃ ┃ ┣ 📜764c477f26311706d94d08e0c5ba0362d4ab64
 ┃ ┃ ┃ ┃ ┣ 📜a0d088ae8c17960926578f539876764cfea417
 ┃ ┃ ┃ ┃ ┣ 📜bd3e5d4b642acaa6a7f6c043bc375c031372b0
 ┃ ┃ ┃ ┃ ┣ 📜cf9e296a9935f6842543a7297ff8d441cffbff
 ┃ ┃ ┃ ┃ ┣ 📜de852e1a2d4ba74e2f964ff5ec4e5e3dac2843
 ┃ ┃ ┃ ┃ ┗ 📜e934a5f6594125396faaca42c76fd2c4e05a38
 ┃ ┃ ┃ ┣ 📂19
 ┃ ┃ ┃ ┃ ┣ 📜0ec2c9a39b55026b6f528c5fd36d08a1401203
 ┃ ┃ ┃ ┃ ┣ 📜4cc3818d86a93f4d1d06a0889e7bb504ee9bba
 ┃ ┃ ┃ ┃ ┣ 📜56f418b4e410492f9672897ec3d9dc458959e9
 ┃ ┃ ┃ ┃ ┣ 📜683cb5bd4a4be3397af828ecc0957cf458d7b6
 ┃ ┃ ┃ ┃ ┣ 📜a436572b62540519acae015220060796e2e36d
 ┃ ┃ ┃ ┃ ┗ 📜a48fa1314868e3836bcd59721e3b6e50386dac
 ┃ ┃ ┃ ┣ 📂1a
 ┃ ┃ ┃ ┃ ┣ 📜0eafcc438ce569eb695860f0838f83f67e642f
 ┃ ┃ ┃ ┃ ┣ 📜446860b96a5d0f04d69ea8aab53a26a90de2eb
 ┃ ┃ ┃ ┃ ┣ 📜53c6b5519bf36b09c85bec7f528d40c107263c
 ┃ ┃ ┃ ┃ ┣ 📜64e6715148ea2ae4e5c287e3e0cde586ced460
 ┃ ┃ ┃ ┃ ┣ 📜70bc1611111e24243262e35f2483c9cd23231e
 ┃ ┃ ┃ ┃ ┣ 📜81695f0d36f5d70b784a212d0555651d6b83f5
 ┃ ┃ ┃ ┃ ┣ 📜c55ac48ba07d8aa33e3738a5215097bcbf3a43
 ┃ ┃ ┃ ┃ ┗ 📜d568e04152cc9efb264f4480750032f591ee17
 ┃ ┃ ┃ ┣ 📂1b
 ┃ ┃ ┃ ┃ ┣ 📜2493e9dc9eee9e5ad71c9afc4f730c1ad95b14
 ┃ ┃ ┃ ┃ ┣ 📜489ea061f7b8be74fd437dec3aacbfdf87e6f3
 ┃ ┃ ┃ ┃ ┣ 📜51316fb4ad0c0786dd062f814e5189a4b15a12
 ┃ ┃ ┃ ┃ ┗ 📜9070c4c2b24ec05209139090c9227d38789bfa
 ┃ ┃ ┃ ┣ 📂1c
 ┃ ┃ ┃ ┃ ┣ 📜21a464161296443ba77feba50c9b41bd3aba68
 ┃ ┃ ┃ ┃ ┣ 📜3ee3423e71f4104efc10a66548427f8ed56832
 ┃ ┃ ┃ ┃ ┣ 📜a67dbe4ec17b79be77c5696abb0fb23c90af90
 ┃ ┃ ┃ ┃ ┗ 📜a7e2f78316fd9cbb78b00d6cfe4618a7b16ef2
 ┃ ┃ ┃ ┣ 📂1d
 ┃ ┃ ┃ ┃ ┣ 📜06ee3eb76d69d7a7c3c160173a03ef8675a750
 ┃ ┃ ┃ ┃ ┣ 📜207ad3d3660cbb4aca8ffe4e1484c21ac07c54
 ┃ ┃ ┃ ┃ ┣ 📜22f1d749e0025a511ebcdaa100985775d7ad1e
 ┃ ┃ ┃ ┃ ┣ 📜7fb7c24d14be7c94f894724d469761a309e3d0
 ┃ ┃ ┃ ┃ ┣ 📜b573a990f16681c431ee192b93a53b6266dab3
 ┃ ┃ ┃ ┃ ┣ 📜b61d96e7561f2e9848c246bb0816dc20c26225
 ┃ ┃ ┃ ┃ ┣ 📜c2a866c15f2409a4f6b564c36148ff7444f928
 ┃ ┃ ┃ ┃ ┣ 📜d32aea1f5f5b631b9f665fe9f5e9a0f0aa4845
 ┃ ┃ ┃ ┃ ┗ 📜fa2230c12d4a38b05ee79106e70314aaf13c80
 ┃ ┃ ┃ ┣ 📂1e
 ┃ ┃ ┃ ┃ ┣ 📜17606f41b87b96d8b7ffee074d768e92e5b19c
 ┃ ┃ ┃ ┃ ┣ 📜4a78a8f379e35a24406ababb784720ee7168ae
 ┃ ┃ ┃ ┃ ┣ 📜79e03adecd83496d25f13eee07e4c2159dae9d
 ┃ ┃ ┃ ┃ ┣ 📜a684d6bcae3e80757e8f3ba60d187826162375
 ┃ ┃ ┃ ┃ ┣ 📜a7ad62caed6eb4227ff208bbc8d5f7cdd295aa
 ┃ ┃ ┃ ┃ ┣ 📜afe643745488d2a41a3a588c7f9a4c36a213c1
 ┃ ┃ ┃ ┃ ┣ 📜ba7d56634675887ce6c6a5222910ed43df88fc
 ┃ ┃ ┃ ┃ ┣ 📜c53854dd9273b7ac5ff93746413cd10737d3ab
 ┃ ┃ ┃ ┃ ┗ 📜ce042ac23a93193597167153b7373b7ff28e62
 ┃ ┃ ┃ ┣ 📂1f
 ┃ ┃ ┃ ┃ ┣ 📜0508f33a14f457c0bb64742bbc463a415d82cd
 ┃ ┃ ┃ ┃ ┣ 📜16d8a3d78e28569fcce8cef1df8276c68dbea4
 ┃ ┃ ┃ ┃ ┣ 📜2b4c17526e18b526b368ca8d120041264b4d5f
 ┃ ┃ ┃ ┃ ┣ 📜343a83abb90ae86da4707263ca1515f251cdce
 ┃ ┃ ┃ ┃ ┣ 📜4a4de4d9bd3c98bce38f6fe24f7e3bc9e89172
 ┃ ┃ ┃ ┃ ┣ 📜6a990f93057c7fc33b83732fa03a1d7b6394c8
 ┃ ┃ ┃ ┃ ┣ 📜9cd0478173133a7fcf5395c257f27e446fc2fe
 ┃ ┃ ┃ ┃ ┣ 📜ded23c8ad132581435589381a774eead5861f0
 ┃ ┃ ┃ ┃ ┗ 📜ea98456d8393846b031bf4508913a862431bd9
 ┃ ┃ ┃ ┣ 📂20
 ┃ ┃ ┃ ┃ ┣ 📜04cda12dc36fa6eea968502a0be90a086dcf4d
 ┃ ┃ ┃ ┃ ┣ 📜10004183de975821d7b32e6e29bcc9d592c904
 ┃ ┃ ┃ ┃ ┣ 📜12344b448da22bfb7339916e032204df79c49d
 ┃ ┃ ┃ ┃ ┣ 📜193c5513416347e018dfb21ca43790b4cea241
 ┃ ┃ ┃ ┃ ┣ 📜3e6bd95a2da28effb7d37c9129e5b4f08e0aa0
 ┃ ┃ ┃ ┃ ┣ 📜66ed5caa421dcc4a490a0c0b96cb7cbb9a2f53
 ┃ ┃ ┃ ┃ ┣ 📜7cc772677e83d205676ff3448dfcbd5981a122
 ┃ ┃ ┃ ┃ ┗ 📜937f32b46c27f9b2109a300674edfa26def47d
 ┃ ┃ ┃ ┣ 📂21
 ┃ ┃ ┃ ┃ ┣ 📜6ca5ee514c299dd168c27617931aaf5e6c483a
 ┃ ┃ ┃ ┃ ┗ 📜dc13a9d4e4a13c0c34e2bca8c85704f059d4e2
 ┃ ┃ ┃ ┣ 📂22
 ┃ ┃ ┃ ┃ ┣ 📜0c01048ce8b333820367bbfdc2514cd5035e78
 ┃ ┃ ┃ ┃ ┣ 📜36c6c4709dad4c3fc070f61f7b519aaaff86c3
 ┃ ┃ ┃ ┃ ┣ 📜8c036902d6a847ebda71745452d875ae0f0b95
 ┃ ┃ ┃ ┃ ┣ 📜b8a18d325814f221fb0481fa7ab320b612d601
 ┃ ┃ ┃ ┃ ┣ 📜c158e65f2b07c7a3e9a754213437ffb01ea21d
 ┃ ┃ ┃ ┃ ┗ 📜f83ee153a404d7bbb6b7ef22eaaa6d90385025
 ┃ ┃ ┃ ┣ 📂23
 ┃ ┃ ┃ ┃ ┣ 📜086839489780a52c9af5cdf956e23a2f889bb7
 ┃ ┃ ┃ ┃ ┣ 📜1b127ffd7e2a5486baa08b220185eee1b764e2
 ┃ ┃ ┃ ┃ ┣ 📜7f591680ed20bf5fc888b5be81395f563c56ee
 ┃ ┃ ┃ ┃ ┣ 📜90ee339481729e13d0994351c2ad0f5b687ef5
 ┃ ┃ ┃ ┃ ┣ 📜95ddccf9e278a719bac028ce90d5be0c2ebd8e
 ┃ ┃ ┃ ┃ ┣ 📜9c7360e8b8f8133e0cab292bc1ea1733322f33
 ┃ ┃ ┃ ┃ ┣ 📜d47ee553786789d94ce9d8bfc55ab356d1f239
 ┃ ┃ ┃ ┃ ┣ 📜f3f119cbda91885edfa7686f29325da8c65ae3
 ┃ ┃ ┃ ┃ ┗ 📜fa38b403e0c9218d8036eb6af6c27579d080ff
 ┃ ┃ ┃ ┣ 📂24
 ┃ ┃ ┃ ┃ ┣ 📜49d3f5d320e6a7d1c6624a82c140e254818959
 ┃ ┃ ┃ ┃ ┣ 📜6b0dc76b0ad7f67fa9e0e94f6c3fe2f8cfcf99
 ┃ ┃ ┃ ┃ ┣ 📜a40b41c617def9429cbf836c84ca4770a449c2
 ┃ ┃ ┃ ┃ ┣ 📜b413830eef1126d182ce1bed6bcf920195cd37
 ┃ ┃ ┃ ┃ ┣ 📜d4342da4006cfd08a230f2007b743a0afbfed1
 ┃ ┃ ┃ ┃ ┗ 📜e0eac5c6daf9ff7a203791f31c8caa6224eab8
 ┃ ┃ ┃ ┣ 📂25
 ┃ ┃ ┃ ┃ ┣ 📜00915289051604ed0708a38d5cc266671564e9
 ┃ ┃ ┃ ┃ ┣ 📜185626698393b1365199f93aadd8d3350dc9d5
 ┃ ┃ ┃ ┃ ┣ 📜330d9f231db930bd52c9b563c8aa1a704bdaea
 ┃ ┃ ┃ ┃ ┗ 📜cd8a80b1e5b0f13a33c58ac3c1e94da9656129
 ┃ ┃ ┃ ┣ 📂26
 ┃ ┃ ┃ ┃ ┣ 📜063d774bbf1824f342455e6517ed258e176838
 ┃ ┃ ┃ ┃ ┣ 📜50b5cc3a6634dcca7d6f2edbc41b55018d72bc
 ┃ ┃ ┃ ┃ ┣ 📜5c656e1d1b4f83a836276e3629ae5e70ecd36e
 ┃ ┃ ┃ ┃ ┣ 📜cbcb83393ad2ba886ee273621a5d50b6da9062
 ┃ ┃ ┃ ┃ ┗ 📜de346d3b4d5416d4dddf9e6179e822aa7fc2cc
 ┃ ┃ ┃ ┣ 📂27
 ┃ ┃ ┃ ┃ ┣ 📜26d2445519831ee66fedaff6b0ffa4feeac210
 ┃ ┃ ┃ ┃ ┗ 📜d8777057f7ef35458cb2163489f3c5909d8aa2
 ┃ ┃ ┃ ┣ 📂28
 ┃ ┃ ┃ ┃ ┣ 📜057a1ecc364a97cf32fcab74764e2c42cb7b34
 ┃ ┃ ┃ ┃ ┣ 📜3462fd56010cefaa7354f9fac9ee588e7ad829
 ┃ ┃ ┃ ┃ ┣ 📜3b35832223304188f967a2dfe011d8d3a8dd4d
 ┃ ┃ ┃ ┃ ┣ 📜748a8b9f68a39622cec7df8ae1020e0b79df19
 ┃ ┃ ┃ ┃ ┣ 📜862f204f28d8a5778701bbd2a23034ef43effe
 ┃ ┃ ┃ ┃ ┣ 📜892221614eaa3015e1cb8c3de4a4adb4eb72da
 ┃ ┃ ┃ ┃ ┗ 📜a9ec0b6e382255c6441bc1008db12f9fead37e
 ┃ ┃ ┃ ┣ 📂29
 ┃ ┃ ┃ ┃ ┣ 📜05df45cd14bea9571999fbecb64732d9572d61
 ┃ ┃ ┃ ┃ ┣ 📜114fc8168f2659f5ab2e657dff8792efd8c459
 ┃ ┃ ┃ ┃ ┣ 📜18fae827e3f536b77230b4233d9fb4894e62c9
 ┃ ┃ ┃ ┃ ┣ 📜32d4a69d6554cec4dea94e3194351710bd659e
 ┃ ┃ ┃ ┃ ┣ 📜41376fdd3a94a24f20816d5a84623781916a20
 ┃ ┃ ┃ ┃ ┣ 📜4bbddc750e64f7fc5be0cb7c234399d3442385
 ┃ ┃ ┃ ┃ ┣ 📜714f594e66d4da7a6410df3fc53075be7e937b
 ┃ ┃ ┃ ┃ ┣ 📜7e6ea6b6931e65f409ae6842d71c87d449a26b
 ┃ ┃ ┃ ┃ ┣ 📜93bf7dedd65a6ac97441616a946f1c1f512871
 ┃ ┃ ┃ ┃ ┣ 📜a1c7cff0736a5df5e3f3340fc7dd8e852c54a1
 ┃ ┃ ┃ ┃ ┗ 📜ebb979097007d62efd076cfa8bf3c0e9c0227c
 ┃ ┃ ┃ ┣ 📂2a
 ┃ ┃ ┃ ┃ ┣ 📜1d616c77464de6df0a2e6b90d5dba529dbd856
 ┃ ┃ ┃ ┃ ┣ 📜9cc467f25d08a45a51d8019fcb0af4a802909b
 ┃ ┃ ┃ ┃ ┣ 📜cdbc67033541eff3c1f65dffc26376be298dfe
 ┃ ┃ ┃ ┃ ┗ 📜d7056b08cd74c6037caaae943bb3d7a444ea4c
 ┃ ┃ ┃ ┣ 📂2b
 ┃ ┃ ┃ ┃ ┣ 📜12ae9646932f6e9d0e7497eebd5988967559a5
 ┃ ┃ ┃ ┃ ┣ 📜171acb254647bfc00c24603881306d8dbb1030
 ┃ ┃ ┃ ┃ ┣ 📜809f7864ece8a51b9aadb8e10375d20038806b
 ┃ ┃ ┃ ┃ ┣ 📜853e7d69bb9980eb474e017abd5cbbbd299aca
 ┃ ┃ ┃ ┃ ┗ 📜dec757771d5bd00f65c41adbf996ffa5439328
 ┃ ┃ ┃ ┣ 📂2c
 ┃ ┃ ┃ ┃ ┣ 📜520e5d2a764b1580bbfaf628fa52502700cb21
 ┃ ┃ ┃ ┃ ┣ 📜54a4e7661c74d334d02baf874d1ede6eb2415d
 ┃ ┃ ┃ ┃ ┣ 📜71fe5a88b91cde91c55828e58478af431e4e73
 ┃ ┃ ┃ ┃ ┗ 📜cef11b7c27cccbd261fe2facfb39ff071002c4
 ┃ ┃ ┃ ┣ 📂2d
 ┃ ┃ ┃ ┃ ┣ 📜15bb87ba4308f0f688103be34d0a5ae61c20b2
 ┃ ┃ ┃ ┃ ┣ 📜288aafc0e4ac10e8fda08bf48533a7435c0796
 ┃ ┃ ┃ ┃ ┣ 📜4a127385172803f62c35687fac8b32295981e7
 ┃ ┃ ┃ ┃ ┣ 📜60117130c3d9e573bbc64a6574de2bc9dee19d
 ┃ ┃ ┃ ┃ ┗ 📜fe4ef5b10cd2002553075bdee881d4e0616af1
 ┃ ┃ ┃ ┣ 📂2e
 ┃ ┃ ┃ ┃ ┣ 📜0306b8da4d3a3e14bd9e77551c22dfc84238c4
 ┃ ┃ ┃ ┃ ┣ 📜8ef6f29de12ba924d57ddf803d68142fcfe3c4
 ┃ ┃ ┃ ┃ ┣ 📜c5267d3e27585c5666ec313ea2f7f7263e6d75
 ┃ ┃ ┃ ┃ ┗ 📜df6b99481d1725eedb45df211f2b8fad87b4c9
 ┃ ┃ ┃ ┣ 📂2f
 ┃ ┃ ┃ ┃ ┣ 📜335e7d67fd0d9ae3e06ccc556b815baa5177ed
 ┃ ┃ ┃ ┃ ┣ 📜5b4671246fbe8fcb5f7ab7bdeff72f8c952ea3
 ┃ ┃ ┃ ┃ ┣ 📜73bcc4636fda710a5aeb985eb9723f9359c647
 ┃ ┃ ┃ ┃ ┗ 📜b02de5253f2c9ec4c68bab04c3b5bf5edef351
 ┃ ┃ ┃ ┣ 📂30
 ┃ ┃ ┃ ┃ ┣ 📜05d4e44a57e8070fe57ffc54aa13839170ac3e
 ┃ ┃ ┃ ┃ ┣ 📜490683b9eb0b302ec899af306ff9b29102fd17
 ┃ ┃ ┃ ┃ ┣ 📜7724baf53a9c7ad63d4d93e593e8e29ff4e10c
 ┃ ┃ ┃ ┃ ┣ 📜d0f372d4bffb9332aefcd24896cdc738415025
 ┃ ┃ ┃ ┃ ┣ 📜d326bb1b8e9b43552b312ed1936b6837653ca7
 ┃ ┃ ┃ ┃ ┣ 📜e8a7e3007a04994138668eae61754008eb0fb0
 ┃ ┃ ┃ ┃ ┗ 📜f1d630cd14cb499fadfae4a700c24dea1c72f9
 ┃ ┃ ┃ ┣ 📂31
 ┃ ┃ ┃ ┃ ┣ 📜01b380da0438fb07e09b1ebdddc5245357dc99
 ┃ ┃ ┃ ┃ ┣ 📜10e626a6bd1d30286b42735321f8781d134ab1
 ┃ ┃ ┃ ┃ ┣ 📜33f203929a45ba423a3f9826c7ccfdbf01ed16
 ┃ ┃ ┃ ┃ ┣ 📜46fd6f6540688fa7cc1ac0d660527b7cee570c
 ┃ ┃ ┃ ┃ ┣ 📜5d4fdc9a405ea55e1cad0aa9a5073c5c298cd8
 ┃ ┃ ┃ ┃ ┣ 📜851eb721916cebddabaa2bd0e65b551643ce37
 ┃ ┃ ┃ ┃ ┣ 📜87e004b0f92ddb340fe566326e485b161ee43a
 ┃ ┃ ┃ ┃ ┣ 📜93ffa215881514bdbdb3549188271c62191b8d
 ┃ ┃ ┃ ┃ ┣ 📜9bc2ab71eccc7694c7cc0e36fe4ad1044710bb
 ┃ ┃ ┃ ┃ ┣ 📜f6085329cdeea6f9655a2e9c35358c45659485
 ┃ ┃ ┃ ┃ ┗ 📜f60d70016825db370c1d83101990f7e9cbd4e4
 ┃ ┃ ┃ ┣ 📂32
 ┃ ┃ ┃ ┃ ┣ 📜230655df9890474d809906d679e2846cefca4e
 ┃ ┃ ┃ ┃ ┣ 📜3f1424f0c3911729d6127ad7bbe6d028fc667a
 ┃ ┃ ┃ ┃ ┣ 📜4a166bc903abc6aaeba9f08eff22ea14afa46a
 ┃ ┃ ┃ ┃ ┣ 📜a0f68ccd710579889a6ad5c0c3cb7946e3cd86
 ┃ ┃ ┃ ┃ ┣ 📜c9475a52994cb484c0f77c3f846ff153adf7f9
 ┃ ┃ ┃ ┃ ┗ 📜e46a3cd15b9aa54cccc46fc53990f382062325
 ┃ ┃ ┃ ┣ 📂33
 ┃ ┃ ┃ ┃ ┣ 📜25040a2ecf907694e2487efe537cb08925689b
 ┃ ┃ ┃ ┃ ┣ 📜557744396b80e0e5419b3080131e54634e1ebc
 ┃ ┃ ┃ ┃ ┣ 📜59173e3bed33b67f42b6f902d0f2ca4951cb3d
 ┃ ┃ ┃ ┃ ┣ 📜5a7417ed9f136bc16a7dbf3111045babb11223
 ┃ ┃ ┃ ┃ ┣ 📜87292dba51f5f0bfd165b48270382363afe95b
 ┃ ┃ ┃ ┃ ┣ 📜a387d49d3607cc330bc30b44358b6b3169672a
 ┃ ┃ ┃ ┃ ┣ 📜cca707e588eaa4b09390d85b1dcdd1741930be
 ┃ ┃ ┃ ┃ ┣ 📜ccecc9d00c70359b8dda38107536901c1701bd
 ┃ ┃ ┃ ┃ ┣ 📜e1ae207c1260c4682803d30ad70e0058805eda
 ┃ ┃ ┃ ┃ ┗ 📜e2c2358d954dd5daa66469d65cb69b93ff4754
 ┃ ┃ ┃ ┣ 📂34
 ┃ ┃ ┃ ┃ ┣ 📜30481f8e758bf33bfa29f05cdb688242168baf
 ┃ ┃ ┃ ┃ ┣ 📜4b094ac161f3fbd6a6297fdecefbf0762835ec
 ┃ ┃ ┃ ┃ ┣ 📜879dcce7bbb8e09860dc2b512c658885d1106a
 ┃ ┃ ┃ ┃ ┣ 📜bf2b9457dbeabce537157f59e04e1fd379b6d2
 ┃ ┃ ┃ ┃ ┣ 📜d27632c0c6d3f4142927a945bfb413b59b0539
 ┃ ┃ ┃ ┃ ┗ 📜f1e84edf2aad9df97936539fd3882290ca376f
 ┃ ┃ ┃ ┣ 📂35
 ┃ ┃ ┃ ┃ ┣ 📜04bb1aa5d7aa2c3d75aedd4a03916afaa7e4d9
 ┃ ┃ ┃ ┃ ┣ 📜166ff3c4779b05aa75b49d10498cd2134c1b5c
 ┃ ┃ ┃ ┃ ┣ 📜204698838e4dc9c67b6985740b6f571c7ce208
 ┃ ┃ ┃ ┃ ┣ 📜31cbb42dfa32c8f9a9ea88f2a7cff52ec45353
 ┃ ┃ ┃ ┃ ┣ 📜460b2e92f6f1e61f3b2ffa92b914a74b6092de
 ┃ ┃ ┃ ┃ ┣ 📜4f5648b8f860800fee8919b0be4db000624711
 ┃ ┃ ┃ ┃ ┣ 📜81dc1a39a322db9beb2f66180a52398f9070e5
 ┃ ┃ ┃ ┃ ┣ 📜8cfc8834abf47c2467b90ad79771edd7eda517
 ┃ ┃ ┃ ┃ ┣ 📜90c0fdd3c70d448429fa45a0345748c3459240
 ┃ ┃ ┃ ┃ ┣ 📜9ddb2c9f5a08d4d20084861cd984d448b15770
 ┃ ┃ ┃ ┃ ┣ 📜b9824baefb8118ada8e6d03f62278f00d1028e
 ┃ ┃ ┃ ┃ ┗ 📜f7f478edcead6aafecf5c9992340ab748b79f8
 ┃ ┃ ┃ ┣ 📂36
 ┃ ┃ ┃ ┃ ┣ 📜2fb7e21a9f7f60df4ce5e045db904e8b7450b9
 ┃ ┃ ┃ ┃ ┣ 📜3dc4393b73ed1f01c4b81eb3263da291d836bb
 ┃ ┃ ┃ ┃ ┣ 📜4621e450a83e36a632cfecd651cbd33d053e57
 ┃ ┃ ┃ ┃ ┣ 📜68c06e7dc01f793aa65204a22989f02a71afb9
 ┃ ┃ ┃ ┃ ┣ 📜721f8b1ec33612a7dacb8f998b430b6ed01bbb
 ┃ ┃ ┃ ┃ ┣ 📜757bf89e5321517f412e780945701c22ae5b37
 ┃ ┃ ┃ ┃ ┣ 📜82ff1f074acd27f98790593f16604d0d6e2772
 ┃ ┃ ┃ ┃ ┣ 📜cb69966b13558f8bcebd2c1c0c2ad0ae6e45ae
 ┃ ┃ ┃ ┃ ┣ 📜cf4db44fb435ab6d149a8eb3dfbb3a41b65551
 ┃ ┃ ┃ ┃ ┗ 📜f4b48e62ed40844f544f85f83d833c58d9608e
 ┃ ┃ ┃ ┣ 📂37
 ┃ ┃ ┃ ┃ ┣ 📜0a28cb8706c58fc82e34a7bafd54b8464060c5
 ┃ ┃ ┃ ┃ ┣ 📜0f79a9ffed613dd2c9dbf1bc7fc8ee29956f86
 ┃ ┃ ┃ ┃ ┣ 📜12a44a7d15dcde1559e142c92c2632db88b260
 ┃ ┃ ┃ ┃ ┣ 📜1913a52bdabd45c3f116dbbe7758f20298b7b1
 ┃ ┃ ┃ ┃ ┣ 📜4119ad9e45b5c5825dd9d815cf2bbdf367d328
 ┃ ┃ ┃ ┃ ┣ 📜6e6a294506b91872243aa9a3518c63a77a219f
 ┃ ┃ ┃ ┃ ┣ 📜cdc5a4b904b02384f765b269781c92a5e51935
 ┃ ┃ ┃ ┃ ┗ 📜ef08687ec97bf630e5fa37e4c5fe70bcf5683c
 ┃ ┃ ┃ ┣ 📂38
 ┃ ┃ ┃ ┃ ┣ 📜2465f2223cae3116ab85a8f30117016fcae710
 ┃ ┃ ┃ ┃ ┣ 📜3f3d50585a67f04d33ddd5ac0d1349272a713a
 ┃ ┃ ┃ ┃ ┣ 📜5032ba9f7ded1cdf23f5b01d42ac59f9d7797b
 ┃ ┃ ┃ ┃ ┣ 📜57cb9af95189cb2915850818e269df4585febd
 ┃ ┃ ┃ ┃ ┣ 📜8a79128bd9d9cc377e062807f09a638d28780c
 ┃ ┃ ┃ ┃ ┣ 📜9ab5353e5a8aa98b5599b9553712c4e385f069
 ┃ ┃ ┃ ┃ ┣ 📜9bdf768a2e704b0bb0214e12581b88fe99a4f9
 ┃ ┃ ┃ ┃ ┗ 📜da42c69a7737961d1a396119a5566fa35d350b
 ┃ ┃ ┃ ┣ 📂39
 ┃ ┃ ┃ ┃ ┣ 📜2f3a826521cd8be8e2f0a56508742885a33685
 ┃ ┃ ┃ ┃ ┣ 📜5c518bc47b94752d00b8dc7aeb7d241633e7cf
 ┃ ┃ ┃ ┃ ┣ 📜8d3cad5ebd8795c19edde35397dcd7a692a9a1
 ┃ ┃ ┃ ┃ ┣ 📜bffc2f9a6a8a6c173cb09c2d31488d6ddec63c
 ┃ ┃ ┃ ┃ ┗ 📜ecafcd2f0d5be8843b4be9ff5d7ac0d608c350
 ┃ ┃ ┃ ┣ 📂3a
 ┃ ┃ ┃ ┃ ┣ 📜269bc01d9f495a67bb5408567a54e52ab5c517
 ┃ ┃ ┃ ┃ ┣ 📜30d6c68c5c0758143f246e71610b1ebafd7463
 ┃ ┃ ┃ ┃ ┣ 📜8a5eab23066ddc0c9ebe67e984d01600c39bd9
 ┃ ┃ ┃ ┃ ┗ 📜b6c9094a7f9ec20379013e40099cac0736b79e
 ┃ ┃ ┃ ┣ 📂3b
 ┃ ┃ ┃ ┃ ┣ 📜2d1bf53a1d49cfec3ebb66b58e279ece61edda
 ┃ ┃ ┃ ┃ ┣ 📜4fc36e6f039e6db164e36eeb9c37d7f14da13b
 ┃ ┃ ┃ ┃ ┣ 📜988914df4cf6ca95b931283999bf692b0c5895
 ┃ ┃ ┃ ┃ ┣ 📜9c7768ea15988f1e4b5a8db610f9a333ded6ec
 ┃ ┃ ┃ ┃ ┗ 📜ba1103e602a5743e62dae09ee31a4765b7bf51
 ┃ ┃ ┃ ┣ 📂3c
 ┃ ┃ ┃ ┃ ┣ 📜26551c06fb6c3c6f6628bf74d3964396ca8bb4
 ┃ ┃ ┃ ┃ ┣ 📜3016171bd9a2246c04e14b2f6cc8a6e96ce911
 ┃ ┃ ┃ ┃ ┣ 📜65eb3fb093611c989a0486af190a0c85bb9a4e
 ┃ ┃ ┃ ┃ ┣ 📜80309aea635e9a9eb9db766b5f6bb88c82b78b
 ┃ ┃ ┃ ┃ ┣ 📜927cf5951a474dc88d70e60d0faa85445d148e
 ┃ ┃ ┃ ┃ ┣ 📜f5e09286183fa233fe39d26dad9f902fc1c69e
 ┃ ┃ ┃ ┃ ┗ 📜fbc7625f817ad97cb79c47c16db1472f9fd9b2
 ┃ ┃ ┃ ┣ 📂3d
 ┃ ┃ ┃ ┃ ┣ 📜af90f06b21baef5cf26731a72a2d8c3100d4af
 ┃ ┃ ┃ ┃ ┣ 📜b317e4c0e5d13109f9dc886e2ce93866e3b2db
 ┃ ┃ ┃ ┃ ┣ 📜e521f70f228adefec53f8efda8d566e1c46036
 ┃ ┃ ┃ ┃ ┗ 📜ea2bccdc7b68ef222445e2a356686a8448d80e
 ┃ ┃ ┃ ┣ 📂3e
 ┃ ┃ ┃ ┃ ┣ 📜4e1ee5fa20f6e8eb71fab472e58e824a593304
 ┃ ┃ ┃ ┃ ┣ 📜5a0a685acae274a27169af001d207ad0785951
 ┃ ┃ ┃ ┃ ┣ 📜5e9de09a0b8cca42cf7d5a48d0c8547b951748
 ┃ ┃ ┃ ┃ ┣ 📜78bd49f64ed749c0a65c549e03732df5ca4838
 ┃ ┃ ┃ ┃ ┣ 📜ae8ab48b114414f0e3101875d76faf1b97612b
 ┃ ┃ ┃ ┃ ┣ 📜cdf46ed1093aaefc8932b0cb72e946599c8a64
 ┃ ┃ ┃ ┃ ┣ 📜e2f6639facd64cefaacf238845818ec0719e45
 ┃ ┃ ┃ ┃ ┗ 📜ea3f91ac383dbbc23bf3ed841463ca4de641a3
 ┃ ┃ ┃ ┣ 📂3f
 ┃ ┃ ┃ ┃ ┣ 📜0347705089471a079643eb0ac38bb6c94c81fe
 ┃ ┃ ┃ ┃ ┗ 📜b9f9aa7c0e893972bf9cefc86c6c4a32941182
 ┃ ┃ ┃ ┣ 📂40
 ┃ ┃ ┃ ┃ ┣ 📜249bb16006946a0f7cad975bd5ddafb9d556a1
 ┃ ┃ ┃ ┃ ┣ 📜333cd6d4063354aaedd2780a8f7a7cc7f91184
 ┃ ┃ ┃ ┃ ┣ 📜69207ee7670425dbd8ca783a8f863f844b578b
 ┃ ┃ ┃ ┃ ┣ 📜c55f65c25644d4f09d3c734b219a2aa736b134
 ┃ ┃ ┃ ┃ ┗ 📜f170e27298695fc2b506c1beb024a6e8fbfea0
 ┃ ┃ ┃ ┣ 📂41
 ┃ ┃ ┃ ┃ ┣ 📜3b8a49256662f92d965f6f1128769b643edf95
 ┃ ┃ ┃ ┃ ┣ 📜451141f742e7395994b7d769a219dec9bf18e9
 ┃ ┃ ┃ ┃ ┣ 📜72541f3fe5b539778a05ee99ec9e26616a7ca6
 ┃ ┃ ┃ ┃ ┣ 📜87376b49c67378852c5d3f5ff98996d11bea20
 ┃ ┃ ┃ ┃ ┗ 📜bcba4d521efa621feb83cc1aa97c49a2facacd
 ┃ ┃ ┃ ┣ 📂42
 ┃ ┃ ┃ ┃ ┣ 📜66120799ade4946837179a40a62e8cb72f2aa3
 ┃ ┃ ┃ ┃ ┣ 📜bb6033f7ad6ce7f700107c44fc27e789aa5b73
 ┃ ┃ ┃ ┃ ┣ 📜c59005a0159f31b2d83bc6ee1c94338515b3fe
 ┃ ┃ ┃ ┃ ┣ 📜dabed83b1338c1ddc64f2d009012710d98204d
 ┃ ┃ ┃ ┃ ┗ 📜e0ba5e7159f985eee18341928a60b76b90c28a
 ┃ ┃ ┃ ┣ 📂43
 ┃ ┃ ┃ ┃ ┣ 📜107e3a11e11dce32f59ae969167055e073f880
 ┃ ┃ ┃ ┃ ┣ 📜1b5fd578576df73968ebee30f72b1ad94f430e
 ┃ ┃ ┃ ┃ ┣ 📜81526cc7304c9a409956998888741bc0bb4102
 ┃ ┃ ┃ ┃ ┣ 📜85b7b044ee7c3cf9e5178aa9b7e96969c1e407
 ┃ ┃ ┃ ┃ ┗ 📜b735db97627232255faa0ac1da099dfb1db45b
 ┃ ┃ ┃ ┣ 📂44
 ┃ ┃ ┃ ┃ ┣ 📜167d1b8b293e7cc430b915a89cfffbca60fb71
 ┃ ┃ ┃ ┃ ┣ 📜2e838aaf879c02aa9098035a5602d82f6f99b7
 ┃ ┃ ┃ ┃ ┣ 📜ace98bec490e9e499324bb7dbff37bfcdb9c1f
 ┃ ┃ ┃ ┃ ┗ 📜df1d698b21c35ffefbed2520b2561093294ed1
 ┃ ┃ ┃ ┣ 📂45
 ┃ ┃ ┃ ┃ ┣ 📜0b4ab7349a54aba1307b34f9cdb9beb5929bcd
 ┃ ┃ ┃ ┃ ┣ 📜31edc194c5da854c08e56d540eb347305442ad
 ┃ ┃ ┃ ┃ ┣ 📜3d35b90c609169cf0a9fbb4ef8d584248b649c
 ┃ ┃ ┃ ┃ ┣ 📜5c22816b65fad4daf4701e660c649894f120e6
 ┃ ┃ ┃ ┃ ┣ 📜84cbcd6ad53d355c4448fc0823febd90fb5340
 ┃ ┃ ┃ ┃ ┣ 📜85bad665a2e839c6ac2ff27df494022a0da96d
 ┃ ┃ ┃ ┃ ┣ 📜88235a84b6d8b423069880f6210ea8f812a55e
 ┃ ┃ ┃ ┃ ┗ 📜ed613f472eace46e1ca3201f2641fb51c333c5
 ┃ ┃ ┃ ┣ 📂46
 ┃ ┃ ┃ ┃ ┣ 📜153e833bdb50282a1b811922edd5abd0b21330
 ┃ ┃ ┃ ┃ ┣ 📜1a5478a11c165e41a1ce1ca6ea7813710c54b9
 ┃ ┃ ┃ ┃ ┣ 📜5c39ccf90e6ef4c4c673da45209828155f7908
 ┃ ┃ ┃ ┃ ┣ 📜751e627fbddeb7cbef624bfed0e15ac910358b
 ┃ ┃ ┃ ┃ ┣ 📜8e311074e5c673edb90b1721560aa94ef997f2
 ┃ ┃ ┃ ┃ ┣ 📜9e939f0da9bdff14dd3355b275f64f46c80760
 ┃ ┃ ┃ ┃ ┣ 📜af1552d880ceedbe0b2f584f7cb657fdebc400
 ┃ ┃ ┃ ┃ ┣ 📜d96a6b2330f8f95f8e9a1063af4caf435eaf96
 ┃ ┃ ┃ ┃ ┗ 📜e6d8efecfb8329fb5bd8ffa2ef12f37000931b
 ┃ ┃ ┃ ┣ 📂47
 ┃ ┃ ┃ ┃ ┣ 📜255a6677377cc8295492b683bac6280ae32ac2
 ┃ ┃ ┃ ┃ ┣ 📜2f5d2b6b57e10e139f21e5d042d11ded739ffd
 ┃ ┃ ┃ ┃ ┣ 📜60ff03d19c1fc18f53160c4b34e2468fbec0d8
 ┃ ┃ ┃ ┃ ┣ 📜75ff0e648704e1430077a9c23ebf5e6b82209d
 ┃ ┃ ┃ ┃ ┣ 📜c6d25ee254b2516f70077c88a56b56e48d58fe
 ┃ ┃ ┃ ┃ ┗ 📜d6c4c0611905c1319039d47415988d58bfee37
 ┃ ┃ ┃ ┣ 📂48
 ┃ ┃ ┃ ┃ ┣ 📜0cf32585c8d163ecadb53d90c573918bfd058c
 ┃ ┃ ┃ ┃ ┣ 📜432005b7fa55c31241feef4265f9f6ac9076ac
 ┃ ┃ ┃ ┃ ┣ 📜63d3b378872264f101b367af16e43c083eb601
 ┃ ┃ ┃ ┃ ┣ 📜768db913052437696ca3b1e805419a5f6b4bbf
 ┃ ┃ ┃ ┃ ┣ 📜a0caa44524732256c768b411bcdcd14115df50
 ┃ ┃ ┃ ┃ ┣ 📜af310a1a6ccee88fa4da3531233cd402a561eb
 ┃ ┃ ┃ ┃ ┣ 📜d25d3ef7d06bc5acee170b84e56453fcb5f517
 ┃ ┃ ┃ ┃ ┗ 📜dd4cfcfabeded53bd9b0c5040302c2f8a8478d
 ┃ ┃ ┃ ┣ 📂49
 ┃ ┃ ┃ ┃ ┗ 📜376842c44678ccfda63972aeb027f6f7c18ed3
 ┃ ┃ ┃ ┣ 📂4a
 ┃ ┃ ┃ ┃ ┣ 📜1a1b4887ff9004352eccf02ed83a6d8213bca2
 ┃ ┃ ┃ ┃ ┣ 📜28fdee9011c0cff0c50466e273a60e4f291db5
 ┃ ┃ ┃ ┃ ┣ 📜83fefb3442b7778993ab844e3cb2876f8c17fe
 ┃ ┃ ┃ ┃ ┣ 📜d111b9db86d89725b9eefc468f2641f3d6f4ec
 ┃ ┃ ┃ ┃ ┗ 📜d23287b15022725d3470e0e7ef7e6ce88554d5
 ┃ ┃ ┃ ┣ 📂4b
 ┃ ┃ ┃ ┃ ┣ 📜0f0360ba21f25b85ea5ec4f7c78e1abd7344e7
 ┃ ┃ ┃ ┃ ┣ 📜30a9c24ec477f7cff4b817e0479bf7424957d8
 ┃ ┃ ┃ ┃ ┣ 📜80c329b07e8ada67495119a189961b4a0af3af
 ┃ ┃ ┃ ┃ ┣ 📜b7f61581814c584890accea7f42099c9689518
 ┃ ┃ ┃ ┃ ┗ 📜bd2bdaf4a72037958c9ac77e17010aa2c16407
 ┃ ┃ ┃ ┣ 📂4c
 ┃ ┃ ┃ ┃ ┣ 📜053b9bafb4465919d3503d03ca2ceb8bc5f636
 ┃ ┃ ┃ ┃ ┣ 📜4ea12423c168c63c56edac45a062c0791bc1ea
 ┃ ┃ ┃ ┃ ┣ 📜cfd733e1b10ea8ebcf1ea8a027f86e6bb55e90
 ┃ ┃ ┃ ┃ ┣ 📜f0f007f6d1def3cf9d8a1f406a18be1183217b
 ┃ ┃ ┃ ┃ ┗ 📜f66dfe1299d3537f5294710c33f7b771b76dc4
 ┃ ┃ ┃ ┣ 📂4d
 ┃ ┃ ┃ ┃ ┣ 📜2370a0bf3c9244bca1b2b482a6b9f25572eea6
 ┃ ┃ ┃ ┃ ┣ 📜63792100df9f609123a09845b2c8f3edcda6dc
 ┃ ┃ ┃ ┃ ┣ 📜dd0749fe4367e738144df342392a5f440e9ee0
 ┃ ┃ ┃ ┃ ┗ 📜dfeafd813cca29585643c7d4eff832ff5c53dc
 ┃ ┃ ┃ ┣ 📂4e
 ┃ ┃ ┃ ┃ ┣ 📜239a5b2d151eb87824da48f1a03c5a4c36281b
 ┃ ┃ ┃ ┃ ┣ 📜2cadb6f06bc8314a523e2b9b8bbe3125336049
 ┃ ┃ ┃ ┃ ┣ 📜50967fd0832de0808cfcc2e4617554dc5acbda
 ┃ ┃ ┃ ┃ ┣ 📜be05fccba878c9e65feb71f16a7ba9c7fd0140
 ┃ ┃ ┃ ┃ ┗ 📜da3f68ade92e2dc88227a7c7caadc77129c303
 ┃ ┃ ┃ ┣ 📂4f
 ┃ ┃ ┃ ┃ ┗ 📜582595336e8f9e2fc99b14a943b586c0a46c0c
 ┃ ┃ ┃ ┣ 📂50
 ┃ ┃ ┃ ┃ ┣ 📜368293a99b12b238c9b2be91491b3f440dda40
 ┃ ┃ ┃ ┃ ┣ 📜a938b911814363f6ac33f930730a70b2a4904e
 ┃ ┃ ┃ ┃ ┗ 📜be468e67b496608f08ecc9b791efd164485203
 ┃ ┃ ┃ ┣ 📂51
 ┃ ┃ ┃ ┃ ┣ 📜18f76aafc05c19e574d7721245cae42c338e9f
 ┃ ┃ ┃ ┃ ┣ 📜1b52a881196c9559a94d9cb0f84ff6d7891d1c
 ┃ ┃ ┃ ┃ ┣ 📜2fd52670457866ef8d58e65676b07851a713fb
 ┃ ┃ ┃ ┃ ┣ 📜3338f0f7a478ea4cec185b5fd519038b2cefb5
 ┃ ┃ ┃ ┃ ┣ 📜3898644d269facf02002502d5e34819c67939e
 ┃ ┃ ┃ ┃ ┣ 📜bc79bcf15c236dab6059cd1924766c21e46f74
 ┃ ┃ ┃ ┃ ┣ 📜ca7b5b5e8a0c8605c6778cd794672dab0abfe8
 ┃ ┃ ┃ ┃ ┗ 📜fa73a5498357179993cd2a421fb68e3d83a9a4
 ┃ ┃ ┃ ┣ 📂52
 ┃ ┃ ┃ ┃ ┣ 📜5d6b50d2a656d410bcc1bc80f68de00a8c8ffb
 ┃ ┃ ┃ ┃ ┣ 📜70708c19708923ed074fa26cf99084705b5b4b
 ┃ ┃ ┃ ┃ ┗ 📜7eee349f6b28e33d32d8e6ba62a54ee4c98a23
 ┃ ┃ ┃ ┣ 📂53
 ┃ ┃ ┃ ┃ ┣ 📜33a43973c756ceede8866a5f75e582020aa142
 ┃ ┃ ┃ ┃ ┣ 📜38fc1ab8dbaf22b3fdc493a842ab193ea2ff22
 ┃ ┃ ┃ ┃ ┣ 📜5ff8a4c4ad01f446cd96d89cdc16ad0a99d4d7
 ┃ ┃ ┃ ┃ ┣ 📜6593eb105e62837ce30c16048b96174e55682b
 ┃ ┃ ┃ ┃ ┣ 📜7a24b64ebb31743ba3361a6a1c88380392104e
 ┃ ┃ ┃ ┃ ┣ 📜8b158bcfedf648d6de470c5c01755e36b1fee4
 ┃ ┃ ┃ ┃ ┣ 📜a4e7fb839027f13d2ef57a44f4742a47bd6ba4
 ┃ ┃ ┃ ┃ ┣ 📜aee6e02a53672be736dc8f2e5c5275e6fe2936
 ┃ ┃ ┃ ┃ ┗ 📜d48bf3b6cb90a7393f256a7a426d517e73d80e
 ┃ ┃ ┃ ┣ 📂54
 ┃ ┃ ┃ ┃ ┣ 📜2396af74f59436418f815283af7aff49617a43
 ┃ ┃ ┃ ┃ ┣ 📜2f5a448d604a506bfeec88962cbc7d1e61effc
 ┃ ┃ ┃ ┃ ┣ 📜5e4ae37fb26bf139437735bbfb806e06fb0a19
 ┃ ┃ ┃ ┃ ┣ 📜7152f6a933b1c1f409283d7bdfe1ba556d4069
 ┃ ┃ ┃ ┃ ┗ 📜b5e8c0340847338f7e2d7817dbbfaf0721d8f6
 ┃ ┃ ┃ ┣ 📂55
 ┃ ┃ ┃ ┃ ┣ 📜21e635d9cc2c9f9d296abfc5e1fef36c404869
 ┃ ┃ ┃ ┃ ┣ 📜37b2b7b7c80441d6e6ccbb93ed21461c03907e
 ┃ ┃ ┃ ┃ ┣ 📜4c5ecc3f70e533151b590956a2b3d0ea6a4dd9
 ┃ ┃ ┃ ┃ ┣ 📜7bd2155cf954e1fbbc08ead5239f4c2fb14099
 ┃ ┃ ┃ ┃ ┗ 📜f846103ddd3ed06eac0e7b64023a12f6375736
 ┃ ┃ ┃ ┣ 📂56
 ┃ ┃ ┃ ┃ ┣ 📜05d7792db51881b0f5dab95c7992ec238ac334
 ┃ ┃ ┃ ┃ ┣ 📜0c7bba6023f0530933b2f00b39cad3bc66882c
 ┃ ┃ ┃ ┃ ┣ 📜3ce9495de1470f236db7526b4ba8c6ff81cb8f
 ┃ ┃ ┃ ┃ ┣ 📜52031df7081bf4d6517e02ca073552c26f94b3
 ┃ ┃ ┃ ┃ ┣ 📜86f31fe0928add916982a2896c7c9e98ea5f3d
 ┃ ┃ ┃ ┃ ┣ 📜98224fdf473de2cc54143b6c0aed22af6a009e
 ┃ ┃ ┃ ┃ ┗ 📜c489becce4937aa7006b42ae0a1c8df16b390f
 ┃ ┃ ┃ ┣ 📂57
 ┃ ┃ ┃ ┃ ┣ 📜15ab00cedf2f7517c38d4d8adf630eca379687
 ┃ ┃ ┃ ┃ ┣ 📜639ffed584c1cbaca911fd5f6b8082f8cfbd8c
 ┃ ┃ ┃ ┃ ┗ 📜a0ac2e8409547a25afaab29da654fccebdc88e
 ┃ ┃ ┃ ┣ 📂58
 ┃ ┃ ┃ ┃ ┣ 📜60055d1eae86714e827f2888237682c7edbf20
 ┃ ┃ ┃ ┃ ┗ 📜af9f43e039b1c1f22736e3983216dc5cf17bbd
 ┃ ┃ ┃ ┣ 📂59
 ┃ ┃ ┃ ┃ ┣ 📜0297689b0c158f2ea042a66599e85edc2620be
 ┃ ┃ ┃ ┃ ┣ 📜836030778b5cfcedf6abcc548f83532628711d
 ┃ ┃ ┃ ┃ ┗ 📜bd1644f4bbe79aa096f5e0098200e0bebb5cff
 ┃ ┃ ┃ ┣ 📂5a
 ┃ ┃ ┃ ┃ ┣ 📜05712644f77d4399b41e89b787d9d5df65fa5a
 ┃ ┃ ┃ ┃ ┣ 📜27c3d6e2a95d95d7c4699a77277cfd1bd593e0
 ┃ ┃ ┃ ┃ ┣ 📜2c4aa7ce78c4da5b96601fcf3a4f7d02e28385
 ┃ ┃ ┃ ┃ ┣ 📜32c989f5354d08a3021b4b2dd887d9e19e58d6
 ┃ ┃ ┃ ┃ ┣ 📜4f10c160eae1fff9bb125aab23ef95d28b8a9a
 ┃ ┃ ┃ ┃ ┣ 📜50e7b5c8bd8a5c853ee7edb8170508f2ff4f86
 ┃ ┃ ┃ ┃ ┣ 📜6ad30f17bc416012b9bd345bb7b9722ca79b3b
 ┃ ┃ ┃ ┃ ┗ 📜886ec9648acc6db53b4bad0ed73ecb812afff8
 ┃ ┃ ┃ ┣ 📂5b
 ┃ ┃ ┃ ┃ ┣ 📜11fd75563291dac2aedb58d542156b7c4d61d1
 ┃ ┃ ┃ ┃ ┣ 📜19ce510468e5d639cad3925479408a6a989536
 ┃ ┃ ┃ ┃ ┣ 📜69736fca8ba3cdb83eae7416849f6f3ebcc889
 ┃ ┃ ┃ ┃ ┣ 📜6cddf73f203645859282d222c245590351609f
 ┃ ┃ ┃ ┃ ┣ 📜8a3aac45830072bc9c264ee3161f118aa4ee58
 ┃ ┃ ┃ ┃ ┣ 📜90eb5bb098769c671637b328e3df9bae5a105d
 ┃ ┃ ┃ ┃ ┗ 📜b38c3d1f0d663810cc8c841b8f33f79ae91724
 ┃ ┃ ┃ ┣ 📂5c
 ┃ ┃ ┃ ┃ ┣ 📜23e2e24fc5d9e8224d7357dbb583c83884582b
 ┃ ┃ ┃ ┃ ┣ 📜533d7f5daa6a9d7c10dc64a15563ddbfc8f868
 ┃ ┃ ┃ ┃ ┣ 📜7d6993520365b58d550f12a6524ac760b0010c
 ┃ ┃ ┃ ┃ ┣ 📜7e71af36b61a04dab0bc63eab4be75c129346d
 ┃ ┃ ┃ ┃ ┗ 📜f897253aaeeb66b9d34558b6b5510d8fb322cc
 ┃ ┃ ┃ ┣ 📂5d
 ┃ ┃ ┃ ┃ ┣ 📜0218d4665113fc6cbde7126edf9662cc56108e
 ┃ ┃ ┃ ┃ ┣ 📜30f2ca2cf2cf50ecedecadd6918e1067002b88
 ┃ ┃ ┃ ┃ ┣ 📜39e7c0969dd9ac46ea0c7ca975d13b3620b25b
 ┃ ┃ ┃ ┃ ┣ 📜4b54c5006460c6ae4c37edadbfd3ed59a71b85
 ┃ ┃ ┃ ┃ ┗ 📜af516f2d43d1b5be585b24c0c74d291f37abf7
 ┃ ┃ ┃ ┣ 📂5e
 ┃ ┃ ┃ ┃ ┣ 📜385bf2639cb0b6501cd2bb21780a85ec66c9a1
 ┃ ┃ ┃ ┃ ┣ 📜43aea991127403803b7cab0e2931d6dcfc8b5a
 ┃ ┃ ┃ ┃ ┣ 📜7285084f6d1ce35fa8d4ca16555c10c0803c8e
 ┃ ┃ ┃ ┃ ┣ 📜749af86f4351bbba50052c34084f66a30c2f65
 ┃ ┃ ┃ ┃ ┣ 📜a3116763bbe3f4b8148e8984fc926346dc661e
 ┃ ┃ ┃ ┃ ┗ 📜f27202412ac1ed7c09c85386a56672ce40dae2
 ┃ ┃ ┃ ┣ 📂5f
 ┃ ┃ ┃ ┃ ┣ 📜22fe7d3dc972f8508cf6b3db1388e8f1808b53
 ┃ ┃ ┃ ┃ ┣ 📜47545a72a92e71520badc19d88834c12637d3f
 ┃ ┃ ┃ ┃ ┣ 📜8e5137ae4b6db2c9278f21ebca0d65a3d55083
 ┃ ┃ ┃ ┃ ┣ 📜b6379e71f275a1784e6638adc96420aaa5c5b2
 ┃ ┃ ┃ ┃ ┗ 📜ea227cbd58b0a9186fb85f9cf2519c56c67a5e
 ┃ ┃ ┃ ┣ 📂60
 ┃ ┃ ┃ ┃ ┣ 📜33a5f7dbe6753e6fc8a03f7aca2ef95c09aae6
 ┃ ┃ ┃ ┃ ┣ 📜43a4cf4e87504f2cdf0455bbba07f8b2245da2
 ┃ ┃ ┃ ┃ ┣ 📜6de8c1b60d955ff4e09548288e3771b3e8c177
 ┃ ┃ ┃ ┃ ┣ 📜9ece0a51a92e1f4a8bc1b318850c043b30bd86
 ┃ ┃ ┃ ┃ ┣ 📜a13aba8a01a81d1cc409d580db3d38f3bfce50
 ┃ ┃ ┃ ┃ ┣ 📜a876eef4fbfde5c45b2d71283fca5ea653c8b8
 ┃ ┃ ┃ ┃ ┣ 📜d596ed4bea2a3415f30eac69f4fc920d4e1581
 ┃ ┃ ┃ ┃ ┣ 📜e3631aa0836cf8817a3671c5427d39db4b9315
 ┃ ┃ ┃ ┃ ┗ 📜f5b714b30120ce333ce48e2b05f8aa7a6844a2
 ┃ ┃ ┃ ┣ 📂61
 ┃ ┃ ┃ ┃ ┣ 📜1ffb87a7076e54105cfd9a0847758c484e9d29
 ┃ ┃ ┃ ┃ ┣ 📜2026d8b9b1730c37cfd18c65b021a786358eb2
 ┃ ┃ ┃ ┃ ┣ 📜422ee5452b71cc27668c6471b08c10e22af865
 ┃ ┃ ┃ ┃ ┣ 📜44453933647f5ad9127654359f0ef049e99bab
 ┃ ┃ ┃ ┃ ┣ 📜5b155ab5e6fc5a24ad1bcceba31ea481319703
 ┃ ┃ ┃ ┃ ┣ 📜6decbce315dfd273605fa67fb74e99ed475b1a
 ┃ ┃ ┃ ┃ ┗ 📜82b2c8ee30138b5ce5b1d5766581ce9b986d99
 ┃ ┃ ┃ ┣ 📂62
 ┃ ┃ ┃ ┃ ┣ 📜76963ab654bddd3638f1d8ac0ef8418924fbfd
 ┃ ┃ ┃ ┃ ┣ 📜89f641664e442be91a3da01c3dcfb11a3ef1c4
 ┃ ┃ ┃ ┃ ┣ 📜a716ade5060bd628269b2c2b744e3aa70656b5
 ┃ ┃ ┃ ┃ ┣ 📜db82e4ffcb1715a3fcc17e15c85dff1e6a23b7
 ┃ ┃ ┃ ┃ ┣ 📜e1d398e3529d998c6ac989973a534477014f31
 ┃ ┃ ┃ ┃ ┗ 📜faf7e08a2d6747e10256ba145d5f4146c96ff0
 ┃ ┃ ┃ ┣ 📂63
 ┃ ┃ ┃ ┃ ┣ 📜51b19f8aa3c59a7dedc5c2c55517c85b0f0d87
 ┃ ┃ ┃ ┃ ┣ 📜5dd6e1ae4c6afef680b192b3367c057f1660b1
 ┃ ┃ ┃ ┃ ┣ 📜70b830f79fcba3e4a6a14c1f62e4be8030809e
 ┃ ┃ ┃ ┃ ┣ 📜9200c6f5db26c83b624f19375bf8ef86ec5042
 ┃ ┃ ┃ ┃ ┣ 📜a83c79f74a110dfc33d2b77520de43923bbb20
 ┃ ┃ ┃ ┃ ┗ 📜c21999cb9e7467997c00573b66b36fe7a2a58e
 ┃ ┃ ┃ ┣ 📂64
 ┃ ┃ ┃ ┃ ┣ 📜21dac9a85271cba3f65b5e7116c7fd907b9e72
 ┃ ┃ ┃ ┃ ┣ 📜342c26d27c4f640e63814288f6e3a9c8c1e333
 ┃ ┃ ┃ ┃ ┣ 📜656003803cb9e8b0149fba5594ae9f4df76011
 ┃ ┃ ┃ ┃ ┣ 📜7e2edbb7c01755736d25621134ac1e0566f6bd
 ┃ ┃ ┃ ┃ ┣ 📜9f9ec07df1434d3f97e37fb7ff785e5f36d22d
 ┃ ┃ ┃ ┃ ┣ 📜d7797f0390e908877fa8c0ee83251eaa9eb2b5
 ┃ ┃ ┃ ┃ ┗ 📜dde162a7eb19f1b72f24030ce6670236c2429d
 ┃ ┃ ┃ ┣ 📂65
 ┃ ┃ ┃ ┃ ┣ 📜37ca4677ee97ddcb112f22d086b92721fd578c
 ┃ ┃ ┃ ┃ ┣ 📜527a8b654c83b15957a4284395622cc6dd0f76
 ┃ ┃ ┃ ┃ ┣ 📜8b86fe26e7f68b38e325955ee34e95e3dbd828
 ┃ ┃ ┃ ┃ ┣ 📜cd1084e957210b2bbeb24258ed706fa5dbd99a
 ┃ ┃ ┃ ┃ ┗ 📜dce9c4d31828e784c21195bfb9c5297ec7b292
 ┃ ┃ ┃ ┣ 📂66
 ┃ ┃ ┃ ┃ ┣ 📜0ef8159071a866e6188e18d099befd13f3de88
 ┃ ┃ ┃ ┃ ┣ 📜5e0382cd927e7587b05cf4eaf6aa2800472af3
 ┃ ┃ ┃ ┃ ┣ 📜97f355f1d93b755fd966abd17c08aae1cdeafa
 ┃ ┃ ┃ ┃ ┣ 📜af67efa09788cf805c6c9ac249f4eaf489532c
 ┃ ┃ ┃ ┃ ┣ 📜d36677d1d034bf9b506c44d784170fdfb09fd4
 ┃ ┃ ┃ ┃ ┣ 📜f233a63c988deb5a59dde3bba962082fbdfd57
 ┃ ┃ ┃ ┃ ┗ 📜f27895fde3514f7ead498ec0b6584c66c67436
 ┃ ┃ ┃ ┣ 📂67
 ┃ ┃ ┃ ┃ ┣ 📜0d3b6b0745fc0ded0936eb2afe4ed6775df63e
 ┃ ┃ ┃ ┃ ┣ 📜601ea3870c496ad37e492d23bc6a417ddd2004
 ┃ ┃ ┃ ┃ ┣ 📜699cbbcce7239b75a06eebc757b5d0df5340f9
 ┃ ┃ ┃ ┃ ┗ 📜926802ad669f3835433bc9c5af68019b2e6be2
 ┃ ┃ ┃ ┣ 📂68
 ┃ ┃ ┃ ┃ ┣ 📜6931594536a45b64a2017d5e4c169b32d92001
 ┃ ┃ ┃ ┃ ┣ 📜7a0f496dd59515167c17065ddc217a10958b5c
 ┃ ┃ ┃ ┃ ┣ 📜e166453bd03b9a7b1b8b33e0b510aa8ed88f36
 ┃ ┃ ┃ ┃ ┗ 📜e69ba9f66db96084c4c6ba4cee0d7540fd10c2
 ┃ ┃ ┃ ┣ 📂69
 ┃ ┃ ┃ ┃ ┣ 📜22577695e66ffdb3803e559490798898341abc
 ┃ ┃ ┃ ┃ ┣ 📜2a131194703496b66d7d4834b22ff5a1b2c833
 ┃ ┃ ┃ ┃ ┣ 📜6d551397116a97dc6d21ba397432f4ab40157f
 ┃ ┃ ┃ ┃ ┣ 📜9398ca4a9717d38b2ddae453edecfe32613fb0
 ┃ ┃ ┃ ┃ ┣ 📜a19ac14105e201447a9f7f210de4862bba00e2
 ┃ ┃ ┃ ┃ ┣ 📜b344bb82432061dff34d9983d57e56a8fc2f29
 ┃ ┃ ┃ ┃ ┗ 📜d019e0a5c15c2b36cb924407d212b386fc9c65
 ┃ ┃ ┃ ┣ 📂6a
 ┃ ┃ ┃ ┃ ┣ 📜178809993c05720f3081d1de90d933641608d4
 ┃ ┃ ┃ ┃ ┣ 📜2f60a577ad347266dd3b884c128f64089625c4
 ┃ ┃ ┃ ┃ ┣ 📜b0507065c4bed34b862faae69fc3b1027993f3
 ┃ ┃ ┃ ┃ ┣ 📜d631dc76b98f11bb6fc734173f9badf89e48bc
 ┃ ┃ ┃ ┃ ┗ 📜d89e398de08e8f55e79a9fd71d5b77c922ddb4
 ┃ ┃ ┃ ┣ 📂6b
 ┃ ┃ ┃ ┃ ┣ 📜15b13b7e6d4c9f8df3f15c9aab3a32131c5138
 ┃ ┃ ┃ ┃ ┣ 📜619b9d600255598052e81f24b0478570cc0acc
 ┃ ┃ ┃ ┃ ┣ 📜7a7fc604267ade3360d2252a5bf859677b309d
 ┃ ┃ ┃ ┃ ┣ 📜adef41f7d11252ac518e2973765c9414b20607
 ┃ ┃ ┃ ┃ ┣ 📜badfefee25c9ef1e12b64088569bc209a5ac31
 ┃ ┃ ┃ ┃ ┗ 📜ebc7738928724343163cf9aeb28e3050a76ede
 ┃ ┃ ┃ ┣ 📂6c
 ┃ ┃ ┃ ┃ ┣ 📜0fca022f3bc75b3250c65cfdc4d461db2e1b11
 ┃ ┃ ┃ ┃ ┣ 📜8611592496df79621fa0734f94a3da42b632b2
 ┃ ┃ ┃ ┃ ┣ 📜8663c17b4f3bace470eb8a1ffc56c17d5b8f68
 ┃ ┃ ┃ ┃ ┣ 📜b973a2fb51371c081dd39fe03287c4383d634c
 ┃ ┃ ┃ ┃ ┣ 📜cfb98450e028e0bf10790d6d8a45f677a106c4
 ┃ ┃ ┃ ┃ ┣ 📜e1af0e8784adc21fab043d8407b6f6d50d8a79
 ┃ ┃ ┃ ┃ ┗ 📜f909489f101fb9a0034c3d9d7c6509b531f70a
 ┃ ┃ ┃ ┣ 📂6d
 ┃ ┃ ┃ ┃ ┣ 📜0770cda7b35196653262d54701fb0599f9736c
 ┃ ┃ ┃ ┃ ┣ 📜38d5fee0dbae832f7a977636255ca5946b0413
 ┃ ┃ ┃ ┃ ┣ 📜53d01d0628b2bcc4ede2dcefba0b42c234f0d1
 ┃ ┃ ┃ ┃ ┣ 📜617f392d82eab48afe03d00532ce23ede96749
 ┃ ┃ ┃ ┃ ┣ 📜b5e85945ec00e50b5b32950c4392eac6b221f1
 ┃ ┃ ┃ ┃ ┗ 📜e29ce13d076c90f74b3a81c98b1c8782494072
 ┃ ┃ ┃ ┣ 📂6e
 ┃ ┃ ┃ ┃ ┣ 📜47c0201befbf811f5d41a3de36f6f8bb673640
 ┃ ┃ ┃ ┃ ┗ 📜82aa91abcc26323eaf0c3dfe0a5c4c12145a03
 ┃ ┃ ┃ ┣ 📂6f
 ┃ ┃ ┃ ┃ ┣ 📜a747c8a549d097a1bb191d25a3e3f3c7f5bce7
 ┃ ┃ ┃ ┃ ┣ 📜ae1c388ae26a95f8421b2b471db266d5cdca64
 ┃ ┃ ┃ ┃ ┣ 📜d4f411d7f08ccef5cc04a668d98e1c48966f03
 ┃ ┃ ┃ ┃ ┗ 📜e390ca64bc87864b5735f356b2801bbdba2795
 ┃ ┃ ┃ ┣ 📂70
 ┃ ┃ ┃ ┃ ┣ 📜4089a7fe757c137d99241b758c912d8391e19d
 ┃ ┃ ┃ ┃ ┗ 📜4f2d417badde43a1fe1ef84ef1e84d2d048610
 ┃ ┃ ┃ ┣ 📂71
 ┃ ┃ ┃ ┃ ┣ 📜a0d0608fd24abe4a24235bf9613893f9a307d3
 ┃ ┃ ┃ ┃ ┣ 📜a1c755afa64426e48948d6dd47d4317cf9da19
 ┃ ┃ ┃ ┃ ┣ 📜c5d618312100df558370858291c9c87aa351d3
 ┃ ┃ ┃ ┃ ┗ 📜d95d8eb0ff9cf9a718c54001fefc3d2dae9753
 ┃ ┃ ┃ ┣ 📂72
 ┃ ┃ ┃ ┃ ┣ 📜051eef3c5804b4b5a7478e18938e4d43c1622d
 ┃ ┃ ┃ ┃ ┣ 📜2612197a364e4f4e6cc3dc1c86d6621f1c5573
 ┃ ┃ ┃ ┃ ┣ 📜3a290d57d86b15a0da5924b60220a161b2967f
 ┃ ┃ ┃ ┃ ┣ 📜4de4b9dba66fa0cdb8c7af14a80039b0e64dea
 ┃ ┃ ┃ ┃ ┣ 📜52572d8b034401e24ed27240ffa65ee9490c2f
 ┃ ┃ ┃ ┃ ┗ 📜cda397d8c2aa242bd000f558a54fc2f3c06024
 ┃ ┃ ┃ ┣ 📂73
 ┃ ┃ ┃ ┃ ┣ 📜04a00924688544396c46c61d9bf0dcbffd82dc
 ┃ ┃ ┃ ┃ ┣ 📜53e223272b9f94a9887afe3e0937f17584575e
 ┃ ┃ ┃ ┃ ┣ 📜780c65e992761fc6f03711b7fcbaae11fa01af
 ┃ ┃ ┃ ┃ ┣ 📜8e112ed711d791df8158d8a3f9efb5c44c985a
 ┃ ┃ ┃ ┃ ┗ 📜937392d67467d40759ee57582e5d9c4931f5c0
 ┃ ┃ ┃ ┣ 📂74
 ┃ ┃ ┃ ┃ ┣ 📜1edead6192a670fb2b10e4fbf303ab2ab3be12
 ┃ ┃ ┃ ┃ ┣ 📜8247d71894f6a880b34e5b7840728284cecbf5
 ┃ ┃ ┃ ┃ ┣ 📜82acfd5addba94ca4cd29fe244d8f26611541f
 ┃ ┃ ┃ ┃ ┣ 📜9f9c52276287ac11aa6bfc7a4b7b8d8721dd38
 ┃ ┃ ┃ ┃ ┣ 📜b25ca873029f0fb5f6241b09471dd3ece500a6
 ┃ ┃ ┃ ┃ ┣ 📜c37fd9c013055ca45439289092963dbed54d08
 ┃ ┃ ┃ ┃ ┣ 📜e6eabce0b580dfd8ae721dde959c8c19b5c380
 ┃ ┃ ┃ ┃ ┗ 📜fb924299e0a9a48970fdce788b1a9a6664315e
 ┃ ┃ ┃ ┣ 📂75
 ┃ ┃ ┃ ┃ ┣ 📜1572c50ab524990fc2f9135e3b0a649d93c50d
 ┃ ┃ ┃ ┃ ┣ 📜3a6ee641748a385747b07aae54c861aa3acbc0
 ┃ ┃ ┃ ┃ ┣ 📜44f3d7c0fd6cc05c9020294d9b501799a3f9d3
 ┃ ┃ ┃ ┃ ┗ 📜dd0cdedbe262bacc85395e4155d35f20d7db99
 ┃ ┃ ┃ ┣ 📂76
 ┃ ┃ ┃ ┃ ┣ 📜21b9687709c04da4bb4bd4ba996ff60c1b302f
 ┃ ┃ ┃ ┃ ┣ 📜38df30e4e1455937ae7aa6aa48651abc8a9ced
 ┃ ┃ ┃ ┃ ┣ 📜45ad511e515fa0ce58a843b9aced53f303a313
 ┃ ┃ ┃ ┃ ┣ 📜5d07690da435d304e0b473c39636fad467d545
 ┃ ┃ ┃ ┃ ┣ 📜8d6343f8c4119ce87c195fcb8ebbd0a0af0598
 ┃ ┃ ┃ ┃ ┣ 📜ae2e77d0199413f36aa1e5357e8a6cedcecadd
 ┃ ┃ ┃ ┃ ┗ 📜c22bc31ceb051f88862ee5b8cb58ab265a2a12
 ┃ ┃ ┃ ┣ 📂77
 ┃ ┃ ┃ ┃ ┣ 📜370a3a7205cb8683c361884dc66587805ee988
 ┃ ┃ ┃ ┃ ┣ 📜74687b8cb0dd9f10455069a657f1c33025069e
 ┃ ┃ ┃ ┃ ┣ 📜9350eaaac4225ca7e551ea85caf969143bc18f
 ┃ ┃ ┃ ┃ ┗ 📜b9fe6a38df41ddaea2c62128e622f9c0411b12
 ┃ ┃ ┃ ┣ 📂78
 ┃ ┃ ┃ ┃ ┣ 📜1c51905977043181c81e3f43800185eded2811
 ┃ ┃ ┃ ┃ ┣ 📜6410efe6640fd0b157baba5a1c2be460cc6373
 ┃ ┃ ┃ ┃ ┣ 📜69ed60883053a77f578452a3a403e967d96666
 ┃ ┃ ┃ ┃ ┣ 📜761d61a673ea397a9d013407439ccfc1481ee0
 ┃ ┃ ┃ ┃ ┣ 📜ad7338705caa7509baf577f1964e9d78504e4f
 ┃ ┃ ┃ ┃ ┣ 📜aee72e1f5e4179d5f04e7792470b65de4cf8c7
 ┃ ┃ ┃ ┃ ┣ 📜bc4d8e3ec9ac34844c93262c6cfdaf91978949
 ┃ ┃ ┃ ┃ ┣ 📜c59c1d78d02a584814177d343006a886342416
 ┃ ┃ ┃ ┃ ┣ 📜c7dfa97c87055bd72f2ae0a87008dc8d64af7c
 ┃ ┃ ┃ ┃ ┣ 📜d824608f403f81edafe451a3144ba4af74bb1a
 ┃ ┃ ┃ ┃ ┣ 📜d93b0a5cb2ac6d5a85caa1569ee911188702ff
 ┃ ┃ ┃ ┃ ┗ 📜de6beb8660fe7ad47457190f85bd8e113bb264
 ┃ ┃ ┃ ┣ 📂79
 ┃ ┃ ┃ ┃ ┣ 📜011d2c989ee2962f5060f383b50a78d8e3a770
 ┃ ┃ ┃ ┃ ┣ 📜277c091183cf3184d40b5a407506fc34e8565d
 ┃ ┃ ┃ ┃ ┣ 📜50d1f44c380cf5886774adc8429af2a2f38881
 ┃ ┃ ┃ ┃ ┣ 📜62ca23ad8e30a07cf72694b0c53070aad483d4
 ┃ ┃ ┃ ┃ ┣ 📜8753ecba27e9afdf3f9ed261e2c1f28d8e9d8f
 ┃ ┃ ┃ ┃ ┣ 📜9347013a711b9beac1063d74dbdf2e81b24f45
 ┃ ┃ ┃ ┃ ┗ 📜b8b37736c0b458d51c03e9b434999a6e824fb0
 ┃ ┃ ┃ ┣ 📂7a
 ┃ ┃ ┃ ┃ ┣ 📜587760c9404e850d68974e79fc7ccbbf2c311b
 ┃ ┃ ┃ ┃ ┣ 📜755ffc25e4b4a708378e8f92cdf84c7ac2aeb9
 ┃ ┃ ┃ ┃ ┣ 📜7f9bc82b8eda7ce4bc9edc96898121c0939e90
 ┃ ┃ ┃ ┃ ┗ 📜d7915dc3734e02ddea62283eea878f1137f50c
 ┃ ┃ ┃ ┣ 📂7b
 ┃ ┃ ┃ ┃ ┣ 📜17d849ac0f6f8aab561e4c54065c3180941658
 ┃ ┃ ┃ ┃ ┣ 📜3519504f318463c63bbe496b8b6c632ee9ed04
 ┃ ┃ ┃ ┃ ┣ 📜620f35f9543fe3f5377b3197ea90ac8f9d8007
 ┃ ┃ ┃ ┃ ┣ 📜6988fdcafbb554b41da9623343edeaf85a9b72
 ┃ ┃ ┃ ┃ ┣ 📜6a481740e01a207f92ec6882ef47f7d98774f1
 ┃ ┃ ┃ ┃ ┣ 📜70b3a030919492c052279eeb4aaea2f3e86f47
 ┃ ┃ ┃ ┃ ┣ 📜b810333bdf2d5deed6674aba7f7f3d4008b010
 ┃ ┃ ┃ ┃ ┣ 📜c33725dc6c6a24d4e175f884965341c5c51da3
 ┃ ┃ ┃ ┃ ┗ 📜cfd316b00efaf35e0cfeb67d745797a9eaaf19
 ┃ ┃ ┃ ┣ 📂7c
 ┃ ┃ ┃ ┃ ┣ 📜42313d191ae9fe6710a67c6fdea6c284f09c52
 ┃ ┃ ┃ ┃ ┣ 📜b0f950f76208bd2c58bbcac3abe9388cededd9
 ┃ ┃ ┃ ┃ ┣ 📜bc2c3f0cbc4fd1d2b2734a0301b416f54f479e
 ┃ ┃ ┃ ┃ ┣ 📜c1cf44368d6bd47d452c565556c5a195fab0f0
 ┃ ┃ ┃ ┃ ┗ 📜ec6d07a35659ad898283469695d4d25e78925c
 ┃ ┃ ┃ ┣ 📂7d
 ┃ ┃ ┃ ┃ ┣ 📜059806186456f85ad3d93b6ea5b29d9ab907ad
 ┃ ┃ ┃ ┃ ┣ 📜3161488578902299cad9727dacb1a10110c68f
 ┃ ┃ ┃ ┃ ┣ 📜5a1c485c7d533556e88d7744cbb9349c5843a7
 ┃ ┃ ┃ ┃ ┣ 📜6e0f74bfad407332723a988d114805d3cd3514
 ┃ ┃ ┃ ┃ ┣ 📜72da3080f5c36a838c00568f589807880ff006
 ┃ ┃ ┃ ┃ ┣ 📜900b3b25a993e68e4106733e33170538761607
 ┃ ┃ ┃ ┃ ┣ 📜aa9f10d835747d0e8f82841cfb316af96904ea
 ┃ ┃ ┃ ┃ ┣ 📜c9a23bdb6f8af918ff9c579c4fd8b2d4bbddef
 ┃ ┃ ┃ ┃ ┗ 📜dc07ed6a9c9fff3df383ad1220ab8d0d37fb41
 ┃ ┃ ┃ ┣ 📂7e
 ┃ ┃ ┃ ┃ ┣ 📜36455d35dc0ad87ce5a0bb10b83559d37628a3
 ┃ ┃ ┃ ┃ ┣ 📜5e0bd3dcfcd3a308ecc944dcc906209717b4b7
 ┃ ┃ ┃ ┃ ┣ 📜748b914e9ba8c028d299e7c9281ce425aad0be
 ┃ ┃ ┃ ┃ ┗ 📜8c3639db02fdb1cbd2651fc89a2f8a280a9798
 ┃ ┃ ┃ ┣ 📂7f
 ┃ ┃ ┃ ┃ ┣ 📜03345a9cdde066b19744386308444dcb9d54ac
 ┃ ┃ ┃ ┃ ┣ 📜0cdd1e74c94496f2249c9cdd63d0a6e5265d15
 ┃ ┃ ┃ ┃ ┣ 📜2d3494fb73e6332a482de132a59a64842b4a4b
 ┃ ┃ ┃ ┃ ┣ 📜2e70a4fe2d2a94c70f059e0f35b1b7820db8a3
 ┃ ┃ ┃ ┃ ┣ 📜3ced3e02d0e23649787bc357866439b3484d61
 ┃ ┃ ┃ ┃ ┗ 📜eef4dd177391028fce0d803c8b06cdbe4035d6
 ┃ ┃ ┃ ┣ 📂80
 ┃ ┃ ┃ ┃ ┣ 📜3475b1b8da8adf6bca1c7ead4aaef924e5d63a
 ┃ ┃ ┃ ┃ ┣ 📜ce6a3b1ed5f89ac0776018300e7b21a3ef6923
 ┃ ┃ ┃ ┃ ┗ 📜fdf4752b06837fea292586abb5db9ce68f0a75
 ┃ ┃ ┃ ┣ 📂81
 ┃ ┃ ┃ ┃ ┣ 📜0aa34949b08f9fae9892d008272d72c22c56d0
 ┃ ┃ ┃ ┃ ┣ 📜2492204d5602f6e18af41a87122c3c58e70ab0
 ┃ ┃ ┃ ┃ ┣ 📜4382d20985bba8f9cb971ee0e568dd3b71afa8
 ┃ ┃ ┃ ┃ ┣ 📜5206228afac2a07e1c78166a8425bac9382997
 ┃ ┃ ┃ ┃ ┣ 📜5991c40e8186c494e01983e4638799ec663994
 ┃ ┃ ┃ ┃ ┣ 📜79bd534c14c49816dca81e1fb9b1e6363e4ae3
 ┃ ┃ ┃ ┃ ┣ 📜7cef3725d02da5b9f8d8c45e17866573fdd706
 ┃ ┃ ┃ ┃ ┣ 📜8e0e63e140e33bd69653a13719dccc11caceda
 ┃ ┃ ┃ ┃ ┣ 📜a5ecdced8eba198749bb65db089328979aff20
 ┃ ┃ ┃ ┃ ┣ 📜bcd5095477ed21991fa6d83cb9271af51ca1eb
 ┃ ┃ ┃ ┃ ┗ 📜d26b69d7787752a342ed1677e74f7e7d94d820
 ┃ ┃ ┃ ┣ 📂82
 ┃ ┃ ┃ ┃ ┣ 📜2846c8059885851e1d2391dd866c06ce77830a
 ┃ ┃ ┃ ┃ ┣ 📜30f4b796edaeda661ceb392e82f9aefa010ab6
 ┃ ┃ ┃ ┃ ┣ 📜6144c992f4ed0d60a40942b2f0928cec559611
 ┃ ┃ ┃ ┃ ┗ 📜b937086890196102359fb982423d066fc7b998
 ┃ ┃ ┃ ┣ 📂83
 ┃ ┃ ┃ ┃ ┣ 📜08780a55f63390c79acd4d56e4d8fdc243a98e
 ┃ ┃ ┃ ┃ ┣ 📜2811f9440c447cabbca502b2dab7590f284af6
 ┃ ┃ ┃ ┃ ┣ 📜37712ea57f00733b5709b27efb3de797e95575
 ┃ ┃ ┃ ┃ ┣ 📜421cd53285c2c06b68eb77f7049298b3a5e533
 ┃ ┃ ┃ ┃ ┣ 📜5339a6dda2bd3072b28776ebb6cab5d49b0b06
 ┃ ┃ ┃ ┃ ┣ 📜9c1c84ac38a760843e24158bccdc674d5fb76b
 ┃ ┃ ┃ ┃ ┣ 📜afb8386479d61f9e9ac6a769941999e83706b7
 ┃ ┃ ┃ ┃ ┣ 📜b5acf91ed9ac7bba9146caa50fe1474586059c
 ┃ ┃ ┃ ┃ ┗ 📜e812203bbd2c5fbe8e9807d1bf6a1e6a104803
 ┃ ┃ ┃ ┣ 📂84
 ┃ ┃ ┃ ┃ ┣ 📜2577087c0c8c1beae4882d39504a04a207b28a
 ┃ ┃ ┃ ┃ ┣ 📜406fec489cd63ac92eaa79435382aceeaa985c
 ┃ ┃ ┃ ┃ ┣ 📜4e62441c69d4bd709699ed77a5bed109e12938
 ┃ ┃ ┃ ┃ ┣ 📜815a909cb3463663fb46059994a4ab0196773b
 ┃ ┃ ┃ ┃ ┗ 📜fca25c4ef3d5a50ce56bf1543c4fec35247e90
 ┃ ┃ ┃ ┣ 📂85
 ┃ ┃ ┃ ┃ ┣ 📜30d7271b57ff6fb90437cd7c9ecdc81a32a80c
 ┃ ┃ ┃ ┃ ┣ 📜7b8edbb7091373da7b9275c985245aa4f5a4f0
 ┃ ┃ ┃ ┃ ┣ 📜7ebc3eab8caad9ccb8b214dafb8e413cd5de9b
 ┃ ┃ ┃ ┃ ┣ 📜a3dd77bd78d12ca2887cf346756f9814c987b2
 ┃ ┃ ┃ ┃ ┣ 📜a5b15ecbbdbffc7c95714015407717148db0d7
 ┃ ┃ ┃ ┃ ┗ 📜d862fa2f709186ed369d9143b4198c8a1c7bd1
 ┃ ┃ ┃ ┣ 📂86
 ┃ ┃ ┃ ┃ ┣ 📜112737f311f78f82f6bd7abe7354b0afd64ac8
 ┃ ┃ ┃ ┃ ┣ 📜1d27f9547abd178caf08b1d3834c2a92208a5a
 ┃ ┃ ┃ ┃ ┣ 📜3d444bb16a0b437c8a38ec5d9bad725f392f50
 ┃ ┃ ┃ ┃ ┣ 📜47f4550235cd3728ccbb4fc7d6648a5ac9d15a
 ┃ ┃ ┃ ┃ ┣ 📜671dfcfa5c99a9786266915b380ec1e54e9284
 ┃ ┃ ┃ ┃ ┣ 📜7b6f3893632a11b459b23e59a2d6f136ed40f6
 ┃ ┃ ┃ ┃ ┣ 📜a560f1c7b20421750f842ce57a0f17eaa2dace
 ┃ ┃ ┃ ┃ ┣ 📜da1d708f834a9be364e8508256ee5fad735704
 ┃ ┃ ┃ ┃ ┣ 📜f6570406f430a08742e2b660e9f3bc01129ab3
 ┃ ┃ ┃ ┃ ┗ 📜fcc8693956208ef8d7729002371c635396455c
 ┃ ┃ ┃ ┣ 📂87
 ┃ ┃ ┃ ┃ ┣ 📜19ea701c496a8987c2437de10832b9a35fb1da
 ┃ ┃ ┃ ┃ ┗ 📜3988cada716a9465e95f74b9d01b7bf6adb2f7
 ┃ ┃ ┃ ┣ 📂88
 ┃ ┃ ┃ ┃ ┣ 📜17b819a385d37ef1664b29ee933df4b1e7908f
 ┃ ┃ ┃ ┃ ┣ 📜427abe192a91543174e0396de7b8b1540d84ca
 ┃ ┃ ┃ ┃ ┗ 📜db81163f3c9893209591ba435fe32d30c96b5c
 ┃ ┃ ┃ ┣ 📂89
 ┃ ┃ ┃ ┃ ┣ 📜30b11dae63e9f5d31659499ed355522d61cd0e
 ┃ ┃ ┃ ┃ ┣ 📜891e3821fb90e086969f43fe7463b8d75f7060
 ┃ ┃ ┃ ┃ ┣ 📜921f2bcba729fb2683eb2144d7b6f04605af94
 ┃ ┃ ┃ ┃ ┣ 📜ef37b72890562b225c006b405faefb72a7b916
 ┃ ┃ ┃ ┃ ┣ 📜fd88af2034087cd4cc948ca2036a0686230606
 ┃ ┃ ┃ ┃ ┗ 📜fded5cb78f804e2c34ca57ce60d39ab1a63c4b
 ┃ ┃ ┃ ┣ 📂8a
 ┃ ┃ ┃ ┃ ┣ 📜6133dda183100363b034aaa966794671ab660d
 ┃ ┃ ┃ ┃ ┣ 📜923cbb4bafb98cf8103195c70ea8af1b2d17af
 ┃ ┃ ┃ ┃ ┣ 📜96c452c54f1fc609e5ebf65c0abbd5aa959acd
 ┃ ┃ ┃ ┃ ┗ 📜98dfe384228842e99033d5560850778a342e0f
 ┃ ┃ ┃ ┣ 📂8b
 ┃ ┃ ┃ ┃ ┣ 📜1c98964186073922e6151081c629cce24a7640
 ┃ ┃ ┃ ┃ ┣ 📜2e2b8eb6f05d61a6194474b4ddf2489487471f
 ┃ ┃ ┃ ┃ ┣ 📜75e8a6779a907b091fbabd14d3e23e732e51de
 ┃ ┃ ┃ ┃ ┗ 📜cc67855a4021a56adc038c55d6c79b1d6ac77b
 ┃ ┃ ┃ ┣ 📂8c
 ┃ ┃ ┃ ┃ ┣ 📜0347740a588b5fd6c804637c2b39f4ef8db79a
 ┃ ┃ ┃ ┃ ┣ 📜167dedf54873890f514809e2fa56bc5bf52aba
 ┃ ┃ ┃ ┃ ┣ 📜288b1d7059f5c1a48927ffc0c0ccf559d0b051
 ┃ ┃ ┃ ┃ ┣ 📜36e325b1a191d2521d4e63895438ef3d144f5e
 ┃ ┃ ┃ ┃ ┣ 📜bb1fcb55f0585154909571bce925f5d54c2fbe
 ┃ ┃ ┃ ┃ ┗ 📜c0e7fdb37aef53703d08e627d0f66aedc73114
 ┃ ┃ ┃ ┣ 📂8d
 ┃ ┃ ┃ ┃ ┣ 📜4b71731b33502c8fe06f1dd043b23e762eaa22
 ┃ ┃ ┃ ┃ ┣ 📜a95a22e6fd8db850f9a7f717398d1f5c8f3e8d
 ┃ ┃ ┃ ┃ ┣ 📜b9f1c2e8493797533f8b237f7db2af028e0464
 ┃ ┃ ┃ ┃ ┗ 📜db861fceec4d31041525aa56e23da58ac26ee7
 ┃ ┃ ┃ ┣ 📂8e
 ┃ ┃ ┃ ┃ ┣ 📜17f0f27cf389760793a4d38772656522d8c847
 ┃ ┃ ┃ ┃ ┣ 📜373a58a55ee6639771cdbd831952a17695328a
 ┃ ┃ ┃ ┃ ┣ 📜51dfa7d286d02bc569a3bca18aa42bfe694269
 ┃ ┃ ┃ ┃ ┣ 📜7ebd21d00b2b1390c2341b095cdfb06ad08221
 ┃ ┃ ┃ ┃ ┣ 📜97ad1a8dca481b59c8592ad2d8fbda096c0a03
 ┃ ┃ ┃ ┃ ┗ 📜d3b9b8d14fa9021ce3cd5d09f9fc4595e8d8c6
 ┃ ┃ ┃ ┣ 📂8f
 ┃ ┃ ┃ ┃ ┣ 📜4803c05638697d84ea28d40693324ec70f7990
 ┃ ┃ ┃ ┃ ┣ 📜4adf656c498ff924925c257f7f4bb5f30be4d9
 ┃ ┃ ┃ ┃ ┣ 📜566d12f8461a8b609f5779630014c5763f631f
 ┃ ┃ ┃ ┃ ┣ 📜bd43920c7283dd90ee9e62ca13e411699f900c
 ┃ ┃ ┃ ┃ ┗ 📜e1299b127ae86a1210e2aea9a5605c653962ba
 ┃ ┃ ┃ ┣ 📂90
 ┃ ┃ ┃ ┃ ┣ 📜497aa0a91bb0ceb77c08b8651dbbacdcee3022
 ┃ ┃ ┃ ┃ ┣ 📜8d6d7f03205c9c6b983118f0a1b1b585304241
 ┃ ┃ ┃ ┃ ┗ 📜9f84740906277e36d5cdd98763547abfe79d94
 ┃ ┃ ┃ ┣ 📂91
 ┃ ┃ ┃ ┃ ┣ 📜3cda028d3e544f200132bfae8bacf8455a2a12
 ┃ ┃ ┃ ┃ ┣ 📜4c6e49fb5f35a357a8b9111db70fba656198ee
 ┃ ┃ ┃ ┃ ┣ 📜8f4ac75f6b67251b10c282c265ef9f2c36a979
 ┃ ┃ ┃ ┃ ┣ 📜921a2dcf4f4b632271ab26d457468245aa8395
 ┃ ┃ ┃ ┃ ┣ 📜973731c2ce7bb8d0e6177b762f1fdcb75b412c
 ┃ ┃ ┃ ┃ ┣ 📜b6cefe516d9ad00a68fef4abc189860ba03671
 ┃ ┃ ┃ ┃ ┗ 📜cb6d1c2dede4151cd947c27d31f2faa7ab5a8b
 ┃ ┃ ┃ ┣ 📂92
 ┃ ┃ ┃ ┃ ┣ 📜0d8d00d524c1b70b222523f394d367d87ef6fb
 ┃ ┃ ┃ ┃ ┣ 📜6294badef3de59ed6588050a89ceaf1883de65
 ┃ ┃ ┃ ┃ ┣ 📜692375732a801236c4659273b8323390d569bd
 ┃ ┃ ┃ ┃ ┗ 📜c1d79db4652ddd210088b763a17599216f20e4
 ┃ ┃ ┃ ┣ 📂93
 ┃ ┃ ┃ ┃ ┣ 📜098e63701def4696f1303655721156af400c4a
 ┃ ┃ ┃ ┃ ┣ 📜8a42ee8824dabcd7d9031cfbbe3c282fbb7c33
 ┃ ┃ ┃ ┃ ┗ 📜a025c831e6d59f9b0150c8f43936611636cf1f
 ┃ ┃ ┃ ┣ 📂94
 ┃ ┃ ┃ ┃ ┣ 📜161c94ca632b1759bffe92d22bb80379ab5a8c
 ┃ ┃ ┃ ┃ ┣ 📜673eed62dfaa1645834f4930f0a9777f6880c0
 ┃ ┃ ┃ ┃ ┣ 📜6e47ccba9ffa5adae73e90bb3cbaec2b5757d5
 ┃ ┃ ┃ ┃ ┗ 📜f821c257165c1122c5900de33fea211059d0a6
 ┃ ┃ ┃ ┣ 📂95
 ┃ ┃ ┃ ┃ ┣ 📜239428469031ad497ba8a799988fc56d1743b8
 ┃ ┃ ┃ ┃ ┣ 📜41f8dd180259c96218561ca59de9fbbf1be766
 ┃ ┃ ┃ ┃ ┣ 📜459182404d182739efbf13c5b17a04a6853a5f
 ┃ ┃ ┃ ┃ ┣ 📜64a4230e332868f940de53fdbf44c4a416c6f8
 ┃ ┃ ┃ ┃ ┗ 📜a50bf35a50fe84af884395ded2bf0a8526ad13
 ┃ ┃ ┃ ┣ 📂96
 ┃ ┃ ┃ ┃ ┣ 📜233b34ccba706a9f89dca87a9282a3cd836e0a
 ┃ ┃ ┃ ┃ ┣ 📜7315dd3d16d50942fa7abd383dfb95ec685491
 ┃ ┃ ┃ ┃ ┣ 📜93b48e59f2d06d98bf35f8675788e808400772
 ┃ ┃ ┃ ┃ ┗ 📜ae08daf2228642a7b956cf3a931ddf9b92a662
 ┃ ┃ ┃ ┣ 📂97
 ┃ ┃ ┃ ┃ ┣ 📜0e31a95d1cec0dc976e0b1cf5b7cf806c52f0c
 ┃ ┃ ┃ ┃ ┣ 📜ba392f65da91ddbd2a7094add89202ce3425a9
 ┃ ┃ ┃ ┃ ┣ 📜ed949a8a8be23c2d07f6cbb6838803e5f43e4a
 ┃ ┃ ┃ ┃ ┗ 📜fc976772abfa2c511ea712bfdc55c97856e2fd
 ┃ ┃ ┃ ┣ 📂98
 ┃ ┃ ┃ ┃ ┣ 📜282d7bd2d7ca1df8c10b8b03f1b714800ea79d
 ┃ ┃ ┃ ┃ ┣ 📜9931d3a20f846813f8bfbad204bc3bd1df8207
 ┃ ┃ ┃ ┃ ┣ 📜aed1aeadbec834b0bf2dff3844c74971cf0543
 ┃ ┃ ┃ ┃ ┗ 📜fa153e29132f3df546e3ada3c24d991a451145
 ┃ ┃ ┃ ┣ 📂99
 ┃ ┃ ┃ ┃ ┣ 📜220edf0d07e122e9e91223d8b7215a9bad8398
 ┃ ┃ ┃ ┃ ┣ 📜3912dfcd79ca55f4f1cdc9911d59b05180368e
 ┃ ┃ ┃ ┃ ┣ 📜4e3ac521a446d4e0d1966e6f1d44cb43a9091b
 ┃ ┃ ┃ ┃ ┣ 📜a03b2d8704ac19b506eca565ea1c19fcb8297f
 ┃ ┃ ┃ ┃ ┣ 📜ad3a6019e0271c4e95f56611fbf020d7e7d156
 ┃ ┃ ┃ ┃ ┣ 📜bf091a7168aa7bc9afa9c57e3dc5df52e790c3
 ┃ ┃ ┃ ┃ ┗ 📜c031a8cfc13e61178f4190403381dc0e91f2eb
 ┃ ┃ ┃ ┣ 📂9a
 ┃ ┃ ┃ ┃ ┗ 📜5b08ccc3ebcd1660e9977b873fab4f2fd02d3b
 ┃ ┃ ┃ ┣ 📂9b
 ┃ ┃ ┃ ┃ ┣ 📜225e79679ab2e01e39036d94edf2c729e3c9c9
 ┃ ┃ ┃ ┃ ┣ 📜64be147ba0d95edb142e15de715a88cf315ecf
 ┃ ┃ ┃ ┃ ┣ 📜74c83c9ebb171a3a819f6435be507ac50a0ab5
 ┃ ┃ ┃ ┃ ┣ 📜863e401e7e05ad95ac559eec6cc0e38e0c65c2
 ┃ ┃ ┃ ┃ ┣ 📜8ee630b96580852ac52db53f8acf178af88d66
 ┃ ┃ ┃ ┃ ┣ 📜9566f4b968a5d5325c7ece1f10fee3b5d8e674
 ┃ ┃ ┃ ┃ ┣ 📜a0d9a54e2a22914bd354d5d6acc6d85e2b1eb3
 ┃ ┃ ┃ ┃ ┣ 📜b2bbe8197ccfd51c74857f9c1cfe6ce0a50c2a
 ┃ ┃ ┃ ┃ ┗ 📜d36803c210abd7f5772ed348cdb233699b834f
 ┃ ┃ ┃ ┣ 📂9c
 ┃ ┃ ┃ ┃ ┣ 📜18c1b14934315355fdf322f2f78fe86bdb87c0
 ┃ ┃ ┃ ┃ ┣ 📜1b2972768d9377d36750dab0025f453939651f
 ┃ ┃ ┃ ┃ ┣ 📜39904640e2576cd6cd8281f713ee9a3177b6d7
 ┃ ┃ ┃ ┃ ┣ 📜466662e07121eb6966c2780ecbdb8e95dabdfe
 ┃ ┃ ┃ ┃ ┣ 📜4d328ac07bb90a978f315ad04c909d73cabe81
 ┃ ┃ ┃ ┃ ┗ 📜d5d9760992525e68ff4e2b607b628659404ca9
 ┃ ┃ ┃ ┣ 📂9d
 ┃ ┃ ┃ ┃ ┗ 📜ced8536b49bdfac7663e80fd04bbb398621602
 ┃ ┃ ┃ ┣ 📂9e
 ┃ ┃ ┃ ┃ ┣ 📜396d7edba2b52fb463184670b3f9300c54e25e
 ┃ ┃ ┃ ┃ ┣ 📜40a5a7b5ba0edd37d8b72dcce28fbf2bd4c353
 ┃ ┃ ┃ ┃ ┣ 📜5923437c442f211dca988100dc61ae51754df5
 ┃ ┃ ┃ ┃ ┣ 📜5b36cfe181a1d273ae8ac8f8fb87f0781b64ae
 ┃ ┃ ┃ ┃ ┣ 📜99ecc2bb07fed7c2d12520daed649a8fe404dd
 ┃ ┃ ┃ ┃ ┣ 📜d4444654c4d9022201f8a6adac3604df137ea6
 ┃ ┃ ┃ ┃ ┗ 📜d9a937d9184ac8cbd4f1425cd4839f225b537a
 ┃ ┃ ┃ ┣ 📂9f
 ┃ ┃ ┃ ┃ ┣ 📜0790fd175492d0b56a0b1d14cddb2560e836ae
 ┃ ┃ ┃ ┃ ┣ 📜2a4be254e4af0441d0b4ad95e2c487f6236ba0
 ┃ ┃ ┃ ┃ ┣ 📜2efefb2dbefba212e599bd1262807b1ca1d6c5
 ┃ ┃ ┃ ┃ ┣ 📜464a526f4bc19f4933fa3f42183123a76377bf
 ┃ ┃ ┃ ┃ ┗ 📜d0fb02a42c4ea47054c40a9ab1312138780bc9
 ┃ ┃ ┃ ┣ 📂a0
 ┃ ┃ ┃ ┃ ┣ 📜092dad19b3be1c7f9a2bd0860e200f7418d7e4
 ┃ ┃ ┃ ┃ ┣ 📜357c66d0dcadeface1ce2f5a53614c3bc79390
 ┃ ┃ ┃ ┃ ┣ 📜51f286d307682503f696a9b495454dae564e6f
 ┃ ┃ ┃ ┃ ┣ 📜84e3d596d57681e20fa7afd9bda580165d2d43
 ┃ ┃ ┃ ┃ ┣ 📜a2a8a34a6221e4dceb24a759ed14e911f74c57
 ┃ ┃ ┃ ┃ ┣ 📜c778ac28336191db975488503b3f013b1c7926
 ┃ ┃ ┃ ┃ ┗ 📜fbb31738df326250648ae0e3002c24238934a0
 ┃ ┃ ┃ ┣ 📂a1
 ┃ ┃ ┃ ┃ ┣ 📜09a0f905bbd7dc40537009f4789ec344240fcb
 ┃ ┃ ┃ ┃ ┣ 📜151efae196098e18aa7888248c5cf150b4d68a
 ┃ ┃ ┃ ┃ ┣ 📜1957d112058b0b971ce9cd27a71bda739b809c
 ┃ ┃ ┃ ┃ ┣ 📜4fd8e9456616a6d196f32d007543331075b961
 ┃ ┃ ┃ ┃ ┣ 📜7821f124f6fc8e98756aab9870a1e3768a58b0
 ┃ ┃ ┃ ┃ ┣ 📜845341cf145e550ef4309eb3f04045bfc5fb92
 ┃ ┃ ┃ ┃ ┣ 📜9708ca607cdb02f2251dacaac6cd135d4bcdf3
 ┃ ┃ ┃ ┃ ┣ 📜b700a113004d0d2112cd57694a8d85de0f38eb
 ┃ ┃ ┃ ┃ ┗ 📜db1ef5fb7e50069c4a6ce31ba2fca25a0d506d
 ┃ ┃ ┃ ┣ 📂a2
 ┃ ┃ ┃ ┃ ┣ 📜223899b37f7154677948916266b6f8e3804e3b
 ┃ ┃ ┃ ┃ ┣ 📜5b5b29351b05f4ae2fc3c533fb1e89b54ab530
 ┃ ┃ ┃ ┃ ┣ 📜5b949896a4a75c67232643f98a1c09a3e0f030
 ┃ ┃ ┃ ┃ ┣ 📜82b4cd1c0e9230dc307a24d440bef713a638aa
 ┃ ┃ ┃ ┃ ┣ 📜98e35afa71491a893cbffe4d9f95032b18321c
 ┃ ┃ ┃ ┃ ┣ 📜9c3ed19c9f6e6a6bd175cd3598d1a321156aff
 ┃ ┃ ┃ ┃ ┗ 📜c346cfab3ebe94f74c806108c07e2e3a5f7e1e
 ┃ ┃ ┃ ┣ 📂a3
 ┃ ┃ ┃ ┃ ┣ 📜0fd11107efb6f2da90daa5c783ed78ad32a013
 ┃ ┃ ┃ ┃ ┣ 📜28dc59835d5d7b043a28997b2d32def95206d3
 ┃ ┃ ┃ ┃ ┣ 📜2e24f126c1f7b33d4ee6be7d7223f8c3da9e23
 ┃ ┃ ┃ ┃ ┣ 📜a79b1fca631ff8fc16ed20bf44253b935b9557
 ┃ ┃ ┃ ┃ ┣ 📜aef29331db0e5205a73dcd53d01d9216eba896
 ┃ ┃ ┃ ┃ ┗ 📜c33628f4c7eb0229f9b86fae663d789b3a6cf7
 ┃ ┃ ┃ ┣ 📂a4
 ┃ ┃ ┃ ┃ ┣ 📜22d38160dba49835df3635043cee3f1145cbbf
 ┃ ┃ ┃ ┃ ┣ 📜72314776b14a940f6e640c5a10ad0572141f3e
 ┃ ┃ ┃ ┃ ┣ 📜7fe9679ea135d8c13e6bb188466cfb1b2556ca
 ┃ ┃ ┃ ┃ ┣ 📜8046aea38920d6cac8543badd87443256dcdd1
 ┃ ┃ ┃ ┃ ┣ 📜a2c77cf6783fd5e61837fb257359fce4dff511
 ┃ ┃ ┃ ┃ ┣ 📜c31cc69438ffbb5675993d19f21a91f61cbe74
 ┃ ┃ ┃ ┃ ┣ 📜c42f7bdbd595cc9528c6cf818910268af7efaf
 ┃ ┃ ┃ ┃ ┗ 📜fc4940cd5ee2f84c32f1b65ed8d8f40791a5b6
 ┃ ┃ ┃ ┣ 📂a5
 ┃ ┃ ┃ ┃ ┣ 📜38f5e9dd52774da6f71fc88ff5d4a17987809a
 ┃ ┃ ┃ ┃ ┣ 📜48b3313f886f0bd0ce3cf7d31ea0c191ef365d
 ┃ ┃ ┃ ┃ ┣ 📜54964d0821df58e8d7a4683392eac4a27e740d
 ┃ ┃ ┃ ┃ ┣ 📜5555e2e9210791d7b7e706c161055229d1aa18
 ┃ ┃ ┃ ┃ ┗ 📜96c63f73fa6267ecab20a749fba8f4fd289240
 ┃ ┃ ┃ ┣ 📂a6
 ┃ ┃ ┃ ┃ ┣ 📜0824b33dece91efe5c0c1b173bfd118989aae2
 ┃ ┃ ┃ ┃ ┣ 📜0bf3d2f2fbf2cdd06055e5b2801a0fb96e5124
 ┃ ┃ ┃ ┃ ┣ 📜1ac0b83485e4539b45b42c325e339eb09e67c3
 ┃ ┃ ┃ ┃ ┣ 📜518b03a8e6752ee6b8a9c21956c8c6c5ec4e69
 ┃ ┃ ┃ ┃ ┣ 📜5914f9d0f2ebc446876824a356c28d71b373f9
 ┃ ┃ ┃ ┃ ┣ 📜683abf7f9725a0bf48f65daf61fb6dbc15d256
 ┃ ┃ ┃ ┃ ┣ 📜844ec9ec2268e9a76624f793b186ff8e3e4170
 ┃ ┃ ┃ ┃ ┣ 📜88e1e25adafee748b39c37622bc8821b634589
 ┃ ┃ ┃ ┃ ┣ 📜9e1be6b3131c2d21f91e7148a87868b6372ed3
 ┃ ┃ ┃ ┃ ┣ 📜b8f666e9b701c0ba1bdc0105892c8239f81975
 ┃ ┃ ┃ ┃ ┗ 📜cf3dd714775121a22f10d375f1f02c7d9f1a15
 ┃ ┃ ┃ ┣ 📂a7
 ┃ ┃ ┃ ┃ ┣ 📜2187e861e1f3e15f247ed634e58ac98bec7041
 ┃ ┃ ┃ ┃ ┣ 📜2e44da7904d110ecf54db443a276007c9c3992
 ┃ ┃ ┃ ┃ ┣ 📜38fb5422fb96ce1b629d1c110396a5be723e62
 ┃ ┃ ┃ ┃ ┣ 📜627451802d7c8cbc8e3c8f466a1d1b2da52a51
 ┃ ┃ ┃ ┃ ┣ 📜b8c4c41c1136cc2223bcf04cafdf958c89dc6f
 ┃ ┃ ┃ ┃ ┣ 📜db02d26ce55e779fe883fcb2adbad1b9fb84a3
 ┃ ┃ ┃ ┃ ┗ 📜ea36a09889a2502b5645cc5a42f77a34ef207d
 ┃ ┃ ┃ ┣ 📂a8
 ┃ ┃ ┃ ┃ ┣ 📜28df5d30548b161d477ac8b803f71adcbf6f50
 ┃ ┃ ┃ ┃ ┣ 📜5838fdce7eea2cef61e0f9e459ac187d76082e
 ┃ ┃ ┃ ┃ ┣ 📜c4514166e3ba1fbb3c12541171066f02478b97
 ┃ ┃ ┃ ┃ ┣ 📜c51585931fd6692a2aa6306daea3575c0fc411
 ┃ ┃ ┃ ┃ ┣ 📜eea3116e7d4e384cb9977bb844fbc935114b5f
 ┃ ┃ ┃ ┃ ┗ 📜f47ecd7884fa6b6ab42896db7515b7154eae7f
 ┃ ┃ ┃ ┣ 📂a9
 ┃ ┃ ┃ ┃ ┣ 📜1e1d3c4baed140741e1eb20305b2f13f6ee468
 ┃ ┃ ┃ ┃ ┣ 📜30805d446acf34b918ca4e777105f972867501
 ┃ ┃ ┃ ┃ ┣ 📜39ff5ed67623256fdb7a5b22f1079936f3cf6e
 ┃ ┃ ┃ ┃ ┣ 📜6a659ab2fa95dd7f3ed7804719b82453685ba0
 ┃ ┃ ┃ ┃ ┣ 📜ab9f3f76d7619495dd6e83e20de5b330e94af3
 ┃ ┃ ┃ ┃ ┣ 📜bb006b5ed3626b3ede5c4de3660f6bdea7af58
 ┃ ┃ ┃ ┃ ┗ 📜c3052ddb3057b809310cef5b602f409c735d5a
 ┃ ┃ ┃ ┣ 📂aa
 ┃ ┃ ┃ ┃ ┣ 📜110956d4ace69cdbefadfc2cc973e94291055f
 ┃ ┃ ┃ ┃ ┣ 📜19e31aefbfc9439d0502dcf3718ffef8f02e37
 ┃ ┃ ┃ ┃ ┣ 📜c77727cbfff786c1e6cd24e0a1c74f56e3f3e4
 ┃ ┃ ┃ ┃ ┗ 📜f982bcdcee30074e582002f9832c2e5621f639
 ┃ ┃ ┃ ┣ 📂ab
 ┃ ┃ ┃ ┃ ┣ 📜5bc04931272d516ac2ed5abdc0c30e1598f72f
 ┃ ┃ ┃ ┃ ┣ 📜7da91adcd7f3d0b5d7608fd78641b1da111ea8
 ┃ ┃ ┃ ┃ ┗ 📜be6e0f3d5fd6ea8d3c7ce8f222040e6a4765a5
 ┃ ┃ ┃ ┣ 📂ac
 ┃ ┃ ┃ ┃ ┗ 📜f9dfcd8502e3d79bd8f235a470df7e92aed4b1
 ┃ ┃ ┃ ┣ 📂ad
 ┃ ┃ ┃ ┃ ┣ 📜57c7653bd537a8d719729cbbbdfecd7e2b0868
 ┃ ┃ ┃ ┃ ┣ 📜7dfcad9a945d2a0ea08396aab5e8e9f6273f20
 ┃ ┃ ┃ ┃ ┣ 📜b363a1ea62334651d690b106041974d5a2f582
 ┃ ┃ ┃ ┃ ┗ 📜bea4065bda6f964ef44b72ad8203c66fe09354
 ┃ ┃ ┃ ┣ 📂ae
 ┃ ┃ ┃ ┃ ┣ 📜1d841d551e1b6ff450137a22e52736d1d735c4
 ┃ ┃ ┃ ┃ ┗ 📜35b85229976a4f5a80c38a9f41036bb9d88eaf
 ┃ ┃ ┃ ┣ 📂af
 ┃ ┃ ┃ ┃ ┣ 📜3eb9f9dcfe4b93331aa95348e44d5bcb1da824
 ┃ ┃ ┃ ┃ ┣ 📜4b37682e223e2db7b6a323cc8e249a075e45f5
 ┃ ┃ ┃ ┃ ┣ 📜56499999e2a44b74561bbcd0d6d5d3504ac6f1
 ┃ ┃ ┃ ┃ ┣ 📜6b8a54592f9f758d5de6d7c481bb587d60e528
 ┃ ┃ ┃ ┃ ┗ 📜d2a292d10d56f541465cce94b281cecf5ed978
 ┃ ┃ ┃ ┣ 📂b0
 ┃ ┃ ┃ ┃ ┣ 📜036cb39edb0909586013ae1922959326780cbd
 ┃ ┃ ┃ ┃ ┣ 📜2b700f1bfc86318cff8f4c4a7f0820974d464a
 ┃ ┃ ┃ ┃ ┣ 📜753efa2f115a7e002b8f59c4f70872d21e9c46
 ┃ ┃ ┃ ┃ ┣ 📜bc8c7e0b2bfab30eb5e25528e3ab4114dadf32
 ┃ ┃ ┃ ┃ ┣ 📜cb02522997a349432228a72630e7c7e1224f79
 ┃ ┃ ┃ ┃ ┗ 📜d2f2700a115efae1a84e7b33f672ad011db4d9
 ┃ ┃ ┃ ┣ 📂b1
 ┃ ┃ ┃ ┃ ┣ 📜1b45a37bb04e620d288b2434a9b66218870966
 ┃ ┃ ┃ ┃ ┣ 📜726a36483c8f0fce4d80b65bab46a873f10dab
 ┃ ┃ ┃ ┃ ┣ 📜99004f0cd900d08ded55df421c155d158dfd72
 ┃ ┃ ┃ ┃ ┗ 📜fa2d0750e66607a4b1b9262c2e6caab84477b3
 ┃ ┃ ┃ ┣ 📂b2
 ┃ ┃ ┃ ┃ ┣ 📜1c6d7e903aa00b1f7a24ee11b6a538803cb709
 ┃ ┃ ┃ ┃ ┣ 📜2897f29f77d961076df31e3d01f382e93e4e60
 ┃ ┃ ┃ ┃ ┣ 📜370b6cc93fd583b574437276f2766744a8994f
 ┃ ┃ ┃ ┃ ┣ 📜76988ef4398ddcbb455f3cc500e23fb7f3b809
 ┃ ┃ ┃ ┃ ┣ 📜80174db7e40974ed764c5724998e1049eda725
 ┃ ┃ ┃ ┃ ┣ 📜c896020fcfefbdac21fa7c72b951918ff5e997
 ┃ ┃ ┃ ┃ ┣ 📜d395531338527b6d320ddf7b458cf1326d693b
 ┃ ┃ ┃ ┃ ┣ 📜d524ca1443466e483836bca225d58f76b15800
 ┃ ┃ ┃ ┃ ┗ 📜ef409996b7cf9a80de400e855e9360e54179f8
 ┃ ┃ ┃ ┣ 📂b3
 ┃ ┃ ┃ ┃ ┣ 📜4250a71000f60f100898704f903a6b6b5225e2
 ┃ ┃ ┃ ┃ ┣ 📜4988fbfaaf0de814050bc37037743e19a5f1a1
 ┃ ┃ ┃ ┃ ┣ 📜9aa51edb891c93341206043d38f1b408c0f63e
 ┃ ┃ ┃ ┃ ┣ 📜cb8914e2682268b60ccfacf9e78d5366115b72
 ┃ ┃ ┃ ┃ ┗ 📜d92f3e3618d994c20d29b6591e412b4860bc9a
 ┃ ┃ ┃ ┣ 📂b4
 ┃ ┃ ┃ ┃ ┣ 📜2d9b30a5463e6f1c739724d5debdc487f94748
 ┃ ┃ ┃ ┃ ┣ 📜2fde9d37be45a64d90e95f089b897c6edda2d6
 ┃ ┃ ┃ ┃ ┣ 📜6716921a465241653d84ad09efde8a021befb2
 ┃ ┃ ┃ ┃ ┣ 📜6c183a2936ee3b6cd655f03e44cf43e8537c1a
 ┃ ┃ ┃ ┃ ┣ 📜7d3e40819af0b8c15b00ef549f2bad0f9d5787
 ┃ ┃ ┃ ┃ ┗ 📜9b846291d8fd77cfe575ea4d1253cf2e64a6d2
 ┃ ┃ ┃ ┣ 📂b5
 ┃ ┃ ┃ ┃ ┣ 📜40ef51885ed30c8ee8d45bcf7e4c18328f88e1
 ┃ ┃ ┃ ┃ ┣ 📜4bb793309da0cd73f4f024f112e6a29e4551b1
 ┃ ┃ ┃ ┃ ┗ 📜95ddb41b936877bbb5b0f20786566a2d997d2b
 ┃ ┃ ┃ ┣ 📂b6
 ┃ ┃ ┃ ┃ ┗ 📜d7394ffa2f60ac68b4fb3b13a18aa54b316fa8
 ┃ ┃ ┃ ┣ 📂b7
 ┃ ┃ ┃ ┃ ┣ 📜02c900535d36be6dd5d049f98f6179a5251d46
 ┃ ┃ ┃ ┃ ┣ 📜1c84178dd24b001556cab8104713dd53592d5d
 ┃ ┃ ┃ ┃ ┣ 📜2834318885a1330ed2841c9d371378ec736280
 ┃ ┃ ┃ ┃ ┣ 📜5525bedcd85c37bf37df77ecb65dcadfa617d1
 ┃ ┃ ┃ ┃ ┣ 📜61de3174446e28213113a01e1aa395dd801141
 ┃ ┃ ┃ ┃ ┗ 📜bc5a2aaf68ce7602702309d9d63d9bb013c8c1
 ┃ ┃ ┃ ┣ 📂b8
 ┃ ┃ ┃ ┃ ┣ 📜1160992de564d2eb7319ac56e0ca025fe5c3da
 ┃ ┃ ┃ ┃ ┣ 📜433702d36686cd5bf3771d9d621243836b2f31
 ┃ ┃ ┃ ┃ ┗ 📜a1d2d3b00781f20e746027b1bf287bae943ec7
 ┃ ┃ ┃ ┣ 📂b9
 ┃ ┃ ┃ ┃ ┣ 📜00ba36e1c3872cc5c54bd8dd913bfd585b2060
 ┃ ┃ ┃ ┃ ┣ 📜07963d66c45f7322f69842e76f9956ea513b7a
 ┃ ┃ ┃ ┃ ┣ 📜468ac610409828bd462b6421f94e6223679c29
 ┃ ┃ ┃ ┃ ┣ 📜6cc3e884a6a6f4295856eeca5d128b23bfeb80
 ┃ ┃ ┃ ┃ ┣ 📜b22a2d00cded21a90d36f693b1bd37940a8205
 ┃ ┃ ┃ ┃ ┣ 📜ec5ac715dbd0ca61a40c686804d33fa9c489d7
 ┃ ┃ ┃ ┃ ┗ 📜f58d875586a1526b943f87edfaa693625e29c2
 ┃ ┃ ┃ ┣ 📂ba
 ┃ ┃ ┃ ┃ ┣ 📜33621ef2cc0b564de01b595770fa6c34f94f3e
 ┃ ┃ ┃ ┃ ┣ 📜777b5c60e173b34bb56ff74991baa4fd96e989
 ┃ ┃ ┃ ┃ ┣ 📜77f97a27f67c3fdb88cbdec33ecb8b658e7e61
 ┃ ┃ ┃ ┃ ┣ 📜8d737f0005c4dbdfd49ecaac2a9ecf1a96b017
 ┃ ┃ ┃ ┃ ┣ 📜b67124f81c3d6682370ecb4d11c03bfca85671
 ┃ ┃ ┃ ┃ ┗ 📜db8ec0a52315447573c5df7e6e7325c48e6f59
 ┃ ┃ ┃ ┣ 📂bb
 ┃ ┃ ┃ ┃ ┣ 📜00f5747d97421bd5833742b50e6cff0b2678f8
 ┃ ┃ ┃ ┃ ┣ 📜329f991662ca5da6de5d4dcfd0d1088fa34c18
 ┃ ┃ ┃ ┃ ┣ 📜9ba05785f3d0412b101ae32263be61935aeed6
 ┃ ┃ ┃ ┃ ┣ 📜b7fbc57c98790e2cd01eb51a8e9a853e2e7dc1
 ┃ ┃ ┃ ┃ ┗ 📜d473da1870d2e4786377a0ea380a9f29e941a3
 ┃ ┃ ┃ ┣ 📂bc
 ┃ ┃ ┃ ┃ ┣ 📜08d52b4b55b5daf1e8d3a85e3d5405b3403ce4
 ┃ ┃ ┃ ┃ ┣ 📜cfad708e16f9a59317df8b6b7b435b0e4f2716
 ┃ ┃ ┃ ┃ ┣ 📜e5279a18a1092cb97635247c7aeeb12e0fc4cf
 ┃ ┃ ┃ ┃ ┣ 📜eb9201c8ee40bb6ffa0e4c65a26536a9bdd6df
 ┃ ┃ ┃ ┃ ┗ 📜edf2bd742be522bd3b1539795f65381628ae54
 ┃ ┃ ┃ ┣ 📂bd
 ┃ ┃ ┃ ┃ ┣ 📜112fa3e64a516606770d4b61945a748ba25709
 ┃ ┃ ┃ ┃ ┣ 📜11700076314425ab59e89ff355b93ed38d002a
 ┃ ┃ ┃ ┃ ┗ 📜4d2f4a21c4e26370e5035a3c1af65ee4fdaf38
 ┃ ┃ ┃ ┣ 📂be
 ┃ ┃ ┃ ┃ ┣ 📜0e82c06dec53a7362cb774d9e8c76b944b27b3
 ┃ ┃ ┃ ┃ ┣ 📜1153e6cce167ab235cca0c1217e3b0cacbc1ea
 ┃ ┃ ┃ ┃ ┣ 📜28a06201e06e8b246ec79642ee41641936bb00
 ┃ ┃ ┃ ┃ ┣ 📜618cf5b8a548e39641fba2f3c86c1ecde60b9c
 ┃ ┃ ┃ ┃ ┣ 📜858899e4b9b5dfc9c62a6f1b77c9a9dc642f66
 ┃ ┃ ┃ ┃ ┣ 📜b36b02034cd44aa2c3e3fb42c8720aa2da9703
 ┃ ┃ ┃ ┃ ┣ 📜dbdae2deaf98b4f68c5ec8b156456c6caf094b
 ┃ ┃ ┃ ┃ ┗ 📜e59351720afff0049cfe6f3e863319b843f5cf
 ┃ ┃ ┃ ┣ 📂bf
 ┃ ┃ ┃ ┃ ┣ 📜1246bc72e61cb4d2b8c641054d7ea4c1c685dd
 ┃ ┃ ┃ ┃ ┣ 📜2dd696d8b319ff967a5da369c558dee6c0ff83
 ┃ ┃ ┃ ┃ ┣ 📜413135fd72061e6c02ffeb3f7237a98dd64a6c
 ┃ ┃ ┃ ┃ ┣ 📜5f6854ad8db5e6ff67d28d1417bd194ed0b341
 ┃ ┃ ┃ ┃ ┗ 📜d860c2566d1afd0298063e58450358a18a65fc
 ┃ ┃ ┃ ┣ 📂c0
 ┃ ┃ ┃ ┃ ┣ 📜3a9cbd53376b92752b1ba3c1a8a1193711bc48
 ┃ ┃ ┃ ┃ ┣ 📜6fa4a9802aa7bae86699f03e0746cb7df5ab0b
 ┃ ┃ ┃ ┃ ┗ 📜8b89a7fbb9fe71cec4344892678ceac7cc3e53
 ┃ ┃ ┃ ┣ 📂c1
 ┃ ┃ ┃ ┃ ┣ 📜2b97e57731b97b20bdacdc4710923f1ff5877f
 ┃ ┃ ┃ ┃ ┣ 📜3e3c7e787d18f6025c80f52d1d483ac4d7ec8a
 ┃ ┃ ┃ ┃ ┣ 📜4bfb0386e0f5e07515ad9f279be1fdeb32508f
 ┃ ┃ ┃ ┃ ┣ 📜7836df86f6962689283fc24bd212df08831bdb
 ┃ ┃ ┃ ┃ ┣ 📜d3ea7dfea40ccac5fcb76b37141c3631af06db
 ┃ ┃ ┃ ┃ ┗ 📜e0a87bacf93c2b511faac06ea64e7efb8e1c6d
 ┃ ┃ ┃ ┣ 📂c2
 ┃ ┃ ┃ ┃ ┣ 📜0b9ae112edceb871475b05da7be68cab35a58c
 ┃ ┃ ┃ ┃ ┣ 📜26dbd21c2e1cf8f861b8a809f1b912c1ad1548
 ┃ ┃ ┃ ┃ ┣ 📜5c03bb81536ee0e95eac589a22cca0fc93267d
 ┃ ┃ ┃ ┃ ┗ 📜c14204792d655633f87c10fd632a0b8f4387a4
 ┃ ┃ ┃ ┣ 📂c3
 ┃ ┃ ┃ ┃ ┣ 📜65a465d1aa5f0e3e177ae648496e36d97db974
 ┃ ┃ ┃ ┃ ┣ 📜9111833de154b58a928a1579d42888676ff0eb
 ┃ ┃ ┃ ┃ ┗ 📜9587b408f18f6a2c0b3fb0190b434ea4729aef
 ┃ ┃ ┃ ┣ 📂c4
 ┃ ┃ ┃ ┃ ┣ 📜1998698150a77495456d4f5ddcf268dadbea31
 ┃ ┃ ┃ ┃ ┣ 📜8123a716c8d2df599e7297d9265f68570d444b
 ┃ ┃ ┃ ┃ ┗ 📜8dc8ba3c2d7fb51183bf6860db27a72b70ba19
 ┃ ┃ ┃ ┣ 📂c5
 ┃ ┃ ┃ ┃ ┣ 📜1e7955f7b5a3db8ed9a55c6a82dcacb18ecbaf
 ┃ ┃ ┃ ┃ ┣ 📜38b97fb5d341f8e201093fc5ece41e160c4860
 ┃ ┃ ┃ ┃ ┗ 📜a7805e1b840d50b9b3081fde054e89c7da3129
 ┃ ┃ ┃ ┣ 📂c6
 ┃ ┃ ┃ ┃ ┣ 📜393a18fbbc13cc487fd9c6b34d152fc2d864ec
 ┃ ┃ ┃ ┃ ┣ 📜41ca5e5b96c5fb325a43709dea06dd5b3cfade
 ┃ ┃ ┃ ┃ ┗ 📜a6de672709676254ebf05f4ddd5597972645cd
 ┃ ┃ ┃ ┣ 📂c7
 ┃ ┃ ┃ ┃ ┣ 📜72c13b5d01fa107c9a642ea7a74f385eafc10c
 ┃ ┃ ┃ ┃ ┣ 📜c08a2ecc862383a744f9621078178e3a9608c6
 ┃ ┃ ┃ ┃ ┗ 📜f97c193e20acb52f2e662f4f2ccc3af1aa14e9
 ┃ ┃ ┃ ┣ 📂c9
 ┃ ┃ ┃ ┃ ┣ 📜64e18eb5fc10920e0ce8b0edc3328f6a88534b
 ┃ ┃ ┃ ┃ ┣ 📜65c311fa8a32faa382c88bebd3e3c4091b80f3
 ┃ ┃ ┃ ┃ ┣ 📜8ecc2763d50b95f90e6e9af95681fafd6d4102
 ┃ ┃ ┃ ┃ ┗ 📜ebabde29ceeff9da9d2c7b4e2e90e418fedc60
 ┃ ┃ ┃ ┣ 📂ca
 ┃ ┃ ┃ ┃ ┣ 📜093f4c0dde71c7c17134c9229138671ac9799e
 ┃ ┃ ┃ ┃ ┣ 📜0d5665fe4168b09f1914faefdf5d352c0134fe
 ┃ ┃ ┃ ┃ ┣ 📜27e39b8e3668273f089a83846255892dcf3c16
 ┃ ┃ ┃ ┃ ┣ 📜6d0fbce90050eb29295160274f4cf8e8b26a59
 ┃ ┃ ┃ ┃ ┣ 📜a901f014885f7c413fc5983de54bf76b48cff1
 ┃ ┃ ┃ ┃ ┣ 📜e34490f8d6e99f0cbd0304d5634d61483c459f
 ┃ ┃ ┃ ┃ ┗ 📜e788835a9839d90971f669760b7c288aa3b314
 ┃ ┃ ┃ ┣ 📂cb
 ┃ ┃ ┃ ┃ ┣ 📜027b80b5d65a12d74a14331f6ec29ecd405d2c
 ┃ ┃ ┃ ┃ ┣ 📜2a773053b09c03b157070c7608d62246371127
 ┃ ┃ ┃ ┃ ┣ 📜6f79be8926fb2257c8ace54b9472792be0e212
 ┃ ┃ ┃ ┃ ┣ 📜ce3aa2ecc597b9db193556f92773101b402990
 ┃ ┃ ┃ ┃ ┗ 📜ef7668bb2f69a9ded416ce97b662a251e22e0d
 ┃ ┃ ┃ ┣ 📂cc
 ┃ ┃ ┃ ┃ ┣ 📜41cc4f485602076713ac2e7d5c4214d0360038
 ┃ ┃ ┃ ┃ ┣ 📜46e57aa0a6b857f8472fe4ab30653c101570b3
 ┃ ┃ ┃ ┃ ┣ 📜5ffd62eca4e6d9800c99fe211f0ccf5b5578b0
 ┃ ┃ ┃ ┃ ┗ 📜7fdc3405de96856005f0ade70c7b68c7ceafd4
 ┃ ┃ ┃ ┣ 📂cd
 ┃ ┃ ┃ ┃ ┣ 📜149f9f66d752dd7c9e3a93dd6344ae90552260
 ┃ ┃ ┃ ┃ ┣ 📜30a92704627176129e2a526a2b179cfb0632cd
 ┃ ┃ ┃ ┃ ┣ 📜4bb6513e6a872f75bc0a352f6e09d8d440b4bf
 ┃ ┃ ┃ ┃ ┣ 📜5ea9359c935289766d03b127c5a92dce8e6f4d
 ┃ ┃ ┃ ┃ ┣ 📜6c3019a81fdc7b60dbd4b3c2cdd11d0e55f8b3
 ┃ ┃ ┃ ┃ ┣ 📜84698f557f3ff09057b444c88510e18a73a69a
 ┃ ┃ ┃ ┃ ┣ 📜b5111934c530977ccae72ac0c405c4582cfb2f
 ┃ ┃ ┃ ┃ ┗ 📜c8034eb357a5057a3842961264422b40b2546d
 ┃ ┃ ┃ ┣ 📂ce
 ┃ ┃ ┃ ┃ ┣ 📜265b96bdb7d808c15c7ec5822c15c59eed1a0f
 ┃ ┃ ┃ ┃ ┣ 📜b114f353f53c6bffef21e370f6703b2e3d2fc2
 ┃ ┃ ┃ ┃ ┗ 📜c686e5645cca3f69c1793bc8d950ed33052abc
 ┃ ┃ ┃ ┣ 📂cf
 ┃ ┃ ┃ ┃ ┣ 📜0464866de77c332264c999c22c41d408534d50
 ┃ ┃ ┃ ┃ ┣ 📜274a4063df198ac66f38949eabfb614ee804a5
 ┃ ┃ ┃ ┃ ┣ 📜28b434aa6cd3c6352a0efcce993e32a57c7955
 ┃ ┃ ┃ ┃ ┣ 📜46f0ef27402b9fd43d8f5e1cc7341cc8b89619
 ┃ ┃ ┃ ┃ ┗ 📜7eff0980ea0ddff71d6bf2390077ea633a4773
 ┃ ┃ ┃ ┣ 📂d0
 ┃ ┃ ┃ ┃ ┣ 📜32fc42157b6be2b3877eb5e0b5aa6496656206
 ┃ ┃ ┃ ┃ ┣ 📜7416252b29e20ec764b8f738061502e1da019d
 ┃ ┃ ┃ ┃ ┣ 📜86612064a8b085999de439f7c834b297c57f58
 ┃ ┃ ┃ ┃ ┣ 📜964226c0a2fcf800729b1dbee3edd917f2851f
 ┃ ┃ ┃ ┃ ┣ 📜af7e8be6779bf4395a3ed8f142ef0cc732f1d7
 ┃ ┃ ┃ ┃ ┣ 📜cfd95af93617dcffb582c6c83841624fcbacab
 ┃ ┃ ┃ ┃ ┗ 📜de49cbf6fc0868c0e80b30495e065f1daf68d7
 ┃ ┃ ┃ ┣ 📂d1
 ┃ ┃ ┃ ┃ ┣ 📜2eef0bc26aa7e715d4c019dd27299f31fa9a19
 ┃ ┃ ┃ ┃ ┣ 📜6becab55a2757136acd3707d0ba93f70cf1d58
 ┃ ┃ ┃ ┃ ┣ 📜6d97d2138dc4aa52c1430857c486711d743022
 ┃ ┃ ┃ ┃ ┣ 📜73990ff4b2af2a55f538ec8da143a8b85f62b9
 ┃ ┃ ┃ ┃ ┗ 📜ab9f3f78141551023d47ff66a6f348b4b9898c
 ┃ ┃ ┃ ┣ 📂d2
 ┃ ┃ ┃ ┃ ┣ 📜36a48ecb6d7ffb0d4a4fb62d7be35a79019cac
 ┃ ┃ ┃ ┃ ┣ 📜6fa2d60d2e98c2b7e352ea65e81e6659892f5f
 ┃ ┃ ┃ ┃ ┣ 📜c912af877257f8864dc9b0bcb40dc708ff4fc9
 ┃ ┃ ┃ ┃ ┗ 📜db2b86a505b7dcad767a218ce9081f8b041257
 ┃ ┃ ┃ ┣ 📂d3
 ┃ ┃ ┃ ┃ ┣ 📜1dbf4872d7a1c908ce3d54a689141bcdc3faed
 ┃ ┃ ┃ ┃ ┣ 📜6eef7eeb0117327127a225147a0bc5515ca56e
 ┃ ┃ ┃ ┃ ┣ 📜711d7c5b8e302ccd0e3a6e32250391028e00da
 ┃ ┃ ┃ ┃ ┣ 📜89c3042e1d2f4d3e9d30d9661551006d0d6a95
 ┃ ┃ ┃ ┃ ┣ 📜962f969930b0a5cb13fa079f273167082abf68
 ┃ ┃ ┃ ┃ ┣ 📜a124f94ec8df6aa7af027976adb3edbb54e92c
 ┃ ┃ ┃ ┃ ┣ 📜a15ff28105f3929ad44d062789d908fd27e7ce
 ┃ ┃ ┃ ┃ ┗ 📜f70a2022d741c1904dec602e293fbaf04c593a
 ┃ ┃ ┃ ┣ 📂d4
 ┃ ┃ ┃ ┃ ┣ 📜37f6d8f4202e6ab29c6b41d97e3c56bd73cfda
 ┃ ┃ ┃ ┃ ┣ 📜8141187786931ec2cf8645e384be7878c7dc53
 ┃ ┃ ┃ ┃ ┣ 📜acff5f9390619043047c8bbd4d97192c22f09b
 ┃ ┃ ┃ ┃ ┗ 📜c87d1eeecd8b0a048af0bb069a559c64f20b06
 ┃ ┃ ┃ ┣ 📂d5
 ┃ ┃ ┃ ┃ ┣ 📜0aa6e9b745b0dba78abd8fa5b1c9a57946a46a
 ┃ ┃ ┃ ┃ ┣ 📜253f2c3bb4953b0325be66440c696de3ea841e
 ┃ ┃ ┃ ┃ ┣ 📜734b4d7b94bf0e49bbca1daf959b5a0f5b63f8
 ┃ ┃ ┃ ┃ ┣ 📜8f55bf59000ce2d680d341c6bdea3160925fc9
 ┃ ┃ ┃ ┃ ┣ 📜bdee8b4a239d6accd9336075d0ed35dec661b8
 ┃ ┃ ┃ ┃ ┗ 📜e60790057e33188bba9cc854600b837c442470
 ┃ ┃ ┃ ┣ 📂d6
 ┃ ┃ ┃ ┃ ┣ 📜94ad1b2968a15806c3ac4a5a1cc4cfdabfaaa6
 ┃ ┃ ┃ ┃ ┣ 📜abfb284f0f75372f9cddc2f25e1d0e06014d6f
 ┃ ┃ ┃ ┃ ┣ 📜b7ef32c8478a48c3994dcadc86837f4371184d
 ┃ ┃ ┃ ┃ ┗ 📜ebbad874f22fac5787578c5a33a0914497eb99
 ┃ ┃ ┃ ┣ 📂d7
 ┃ ┃ ┃ ┃ ┣ 📜1bc2544f90e5c437d79b0060684091b405e9ec
 ┃ ┃ ┃ ┃ ┣ 📜57eac7404ae08d6f4c38254b2d6cc93b9a0004
 ┃ ┃ ┃ ┃ ┗ 📜f3378020549b54f7154a77d7b93f09c1bdde61
 ┃ ┃ ┃ ┣ 📂d8
 ┃ ┃ ┃ ┃ ┣ 📜230ea8dd83a95eaa257d5c5cb3775366e98bff
 ┃ ┃ ┃ ┃ ┣ 📜323f8942fcb5d7c4a86f357cf19d34d5926249
 ┃ ┃ ┃ ┃ ┣ 📜356cace3af642b41c5eeaf08d0fabc7eaa3d5f
 ┃ ┃ ┃ ┃ ┣ 📜691ec6ad34ddc5086456099f7bfa78f3aca1a9
 ┃ ┃ ┃ ┃ ┣ 📜97a916db810b823039feeb86949219d7aa8b93
 ┃ ┃ ┃ ┃ ┣ 📜e6c0068b9630fa4e60e67f845555d91a3bcae0
 ┃ ┃ ┃ ┃ ┗ 📜ec80a021c762c2643a84a94fc51305a8118fc4
 ┃ ┃ ┃ ┣ 📂d9
 ┃ ┃ ┃ ┃ ┣ 📜06223639fc29acd6108a50842af557d62ed6a8
 ┃ ┃ ┃ ┃ ┣ 📜0e0dc4f0e8fb149c0d28ab4a16a7b3140b62b3
 ┃ ┃ ┃ ┃ ┣ 📜5a2681d5c15cbe87bf0e5e8002db7a2d9b81c5
 ┃ ┃ ┃ ┃ ┗ 📜cc8be574e0dda97467e5e9e09b0bc884abd761
 ┃ ┃ ┃ ┣ 📂da
 ┃ ┃ ┃ ┃ ┣ 📜5b344087a7347c29a84a16888168d15ec51d3a
 ┃ ┃ ┃ ┃ ┣ 📜64578d8e8bb6e20ba5ae12906276308e62d2a2
 ┃ ┃ ┃ ┃ ┗ 📜e07615c1278597e1dd6f00cc30665800be43f4
 ┃ ┃ ┃ ┣ 📂db
 ┃ ┃ ┃ ┃ ┣ 📜002658e6cfa2e9a3c16b2c35809954dc3cd9c7
 ┃ ┃ ┃ ┃ ┣ 📜3d0b65788e72d35206947c9ee660ff67939ed4
 ┃ ┃ ┃ ┃ ┣ 📜4a7595ef93605dfcce17dbf72fb22af0d22350
 ┃ ┃ ┃ ┃ ┣ 📜513a24e001174a4fd7fece243ad43e100dfcb0
 ┃ ┃ ┃ ┃ ┗ 📜c2b5296c1d6c501c30d4755ea667e8eb4e72b9
 ┃ ┃ ┃ ┣ 📂dc
 ┃ ┃ ┃ ┃ ┣ 📜c372a2c13f0aaa8cce1b8964fa70ca30612726
 ┃ ┃ ┃ ┃ ┣ 📜c65b986a013561fb24cb82966d1afc26c17a56
 ┃ ┃ ┃ ┃ ┗ 📜fdb25da0898fbf104372acd126d8b920b57767
 ┃ ┃ ┃ ┣ 📂dd
 ┃ ┃ ┃ ┃ ┣ 📜0464ca983d0511b0e5fe529d24e3b53c132e67
 ┃ ┃ ┃ ┃ ┣ 📜0500aea9acf0ee2bfaf27b4dda43ae012c92f7
 ┃ ┃ ┃ ┃ ┣ 📜0eebc31eb6c737a5864aca125daa69c723a444
 ┃ ┃ ┃ ┃ ┣ 📜2cb4f34c1a14f7b419cd4f2adb16f547eda47c
 ┃ ┃ ┃ ┃ ┣ 📜35170b6d95e8c67021e9dc976d51e41e80c030
 ┃ ┃ ┃ ┃ ┣ 📜63fbdda9d4c66a0e19ba5c430d0fd61c530ada
 ┃ ┃ ┃ ┃ ┣ 📜733e7f0d5f9700ad6cc5299a192bc5c2b525b6
 ┃ ┃ ┃ ┃ ┣ 📜974b495032a8162c8b77449df0df3ea0c9ae9b
 ┃ ┃ ┃ ┃ ┣ 📜d04bc5997775f80081ec032074ddd9c98c70e3
 ┃ ┃ ┃ ┃ ┣ 📜d085557d5742354e350f91944ba8ebc7a50e7a
 ┃ ┃ ┃ ┃ ┣ 📜da784977b41d2bd03f076b05b71f6ae3f72690
 ┃ ┃ ┃ ┃ ┗ 📜e1cd4fc81a5968a4fe80ccbe9aa297f556175d
 ┃ ┃ ┃ ┣ 📂de
 ┃ ┃ ┃ ┃ ┣ 📜06f4ec3bfac3e3fcf5a72a173c73e23f6582e1
 ┃ ┃ ┃ ┃ ┣ 📜600422cc40a23d9242c7c4e4e0bc5ff8e71158
 ┃ ┃ ┃ ┃ ┣ 📜b65e86d4e098fdf5e8fe399a26480adb89a99b
 ┃ ┃ ┃ ┃ ┗ 📜d028b30d2109f136992127d9394ff7d3519cc2
 ┃ ┃ ┃ ┣ 📂df
 ┃ ┃ ┃ ┃ ┣ 📜0565390ffdf23b16ebba32449a43c07c605e17
 ┃ ┃ ┃ ┃ ┣ 📜070bc59634213b5f9d49b4d6169d61dd6ac40d
 ┃ ┃ ┃ ┃ ┣ 📜0ef41b3d6201a17bd244960f69882f3a0e2450
 ┃ ┃ ┃ ┃ ┣ 📜16d743bcc3bdf1acbf33b8d4bc31fd5ec709fc
 ┃ ┃ ┃ ┃ ┣ 📜7df53f56549b6a1651717a632715fc4721c3b0
 ┃ ┃ ┃ ┃ ┣ 📜8b90b8c16a20e1e2c218c0a47e814e48b7481a
 ┃ ┃ ┃ ┃ ┣ 📜909c2b4c901898deffed3957a8cec964608226
 ┃ ┃ ┃ ┃ ┗ 📜9c4beb017b1b39d16ee4a1abac326fafe58a56
 ┃ ┃ ┃ ┣ 📂e0
 ┃ ┃ ┃ ┃ ┣ 📜2a64c0552ac0e706a556042bcc24217b0fbd56
 ┃ ┃ ┃ ┃ ┣ 📜4fc3f7cc152495c335f36834d508aca1c65169
 ┃ ┃ ┃ ┃ ┣ 📜62b7f3e65d73f3439d9b36baddafc2631369e7
 ┃ ┃ ┃ ┃ ┣ 📜6675937bd7903eeeb68a3ea1a9c6e3d649bfd8
 ┃ ┃ ┃ ┃ ┣ 📜689abb24cf9e55c852f6d15c82f2ec4b1acbc1
 ┃ ┃ ┃ ┃ ┣ 📜7ee3d9f1d60212eebe84c21bd5ef224051dcb0
 ┃ ┃ ┃ ┃ ┣ 📜84eac04925be0e5bca9407c121df86c2d2266f
 ┃ ┃ ┃ ┃ ┣ 📜a6be77c1a579c19ac9b7ebe45d57e12d78d004
 ┃ ┃ ┃ ┃ ┗ 📜dd0162fa725ca56cd7376316221557d4443426
 ┃ ┃ ┃ ┣ 📂e1
 ┃ ┃ ┃ ┃ ┗ 📜df779f840c9772033df27891948cf767f09bd4
 ┃ ┃ ┃ ┣ 📂e2
 ┃ ┃ ┃ ┃ ┣ 📜257122290883624f62d22193729df29993911f
 ┃ ┃ ┃ ┃ ┣ 📜2e5136511616ae330758baedea71c3248173c4
 ┃ ┃ ┃ ┃ ┗ 📜8ccf960769684204f4cc6252eaf34a5078c17a
 ┃ ┃ ┃ ┣ 📂e3
 ┃ ┃ ┃ ┃ ┣ 📜03c0706cad06112fedc4db726f3aefe4657c9a
 ┃ ┃ ┃ ┃ ┣ 📜2edc7ce213f827834ef7d6a5560da03215d2c2
 ┃ ┃ ┃ ┃ ┣ 📜314219f6413db9d32b68185422532aa1bcd974
 ┃ ┃ ┃ ┃ ┣ 📜4c90518ef57bc47d8d62f7f0e7ad2bd4ed0ba0
 ┃ ┃ ┃ ┃ ┣ 📜68af8a45e4c33ca2cc178677641357e350ae87
 ┃ ┃ ┃ ┃ ┣ 📜e60e56d0ec8e52a93b766acbd9da7ecef8930c
 ┃ ┃ ┃ ┃ ┗ 📜e75735998f319323e9a3c2af621a6e1949e483
 ┃ ┃ ┃ ┣ 📂e4
 ┃ ┃ ┃ ┃ ┣ 📜228417dfead31733de1c819355ecd1a67d506a
 ┃ ┃ ┃ ┃ ┣ 📜97bedfd4496355729aca7dd78993388a2297e2
 ┃ ┃ ┃ ┃ ┗ 📜9d83488a8cd58e2d921963d5d2e31973847c7b
 ┃ ┃ ┃ ┣ 📂e5
 ┃ ┃ ┃ ┃ ┣ 📜00042ae8842c14332d2a08a947d6dbf099dadc
 ┃ ┃ ┃ ┃ ┣ 📜17e9ec76b009dc3960ebf77e10a3e37ed1e49e
 ┃ ┃ ┃ ┃ ┣ 📜506df2907a7c8f63f3841a918611b93d67e84e
 ┃ ┃ ┃ ┃ ┣ 📜5415f2b8185aa85a48cd0dbd2a5d2894906754
 ┃ ┃ ┃ ┃ ┗ 📜fc900705a22da818e87a20e584aebc9111b787
 ┃ ┃ ┃ ┣ 📂e6
 ┃ ┃ ┃ ┃ ┣ 📜0056c03d9a294273e529b2b2a0b258ee41ce2b
 ┃ ┃ ┃ ┃ ┣ 📜0c45026e6a9dfa0a30981086dcb741477e6809
 ┃ ┃ ┃ ┃ ┣ 📜2d5e1b2ddfee118be8e401ce3dc36c706d614f
 ┃ ┃ ┃ ┃ ┣ 📜4a138f3c427ec39c360de4e09003d74ae4e2b0
 ┃ ┃ ┃ ┃ ┣ 📜56205a32e028f23aeb392bb2ebb1cb54b1264a
 ┃ ┃ ┃ ┃ ┣ 📜84d010c0edce3131db234ec2a154f3332d010f
 ┃ ┃ ┃ ┃ ┣ 📜9562420ce35cab53a80bbba806bba37d364130
 ┃ ┃ ┃ ┃ ┣ 📜9de29bb2d1d6434b8b29ae775ad8c2e48c5391
 ┃ ┃ ┃ ┃ ┣ 📜d76541af7592eced7b0a4347481dc34d5bf5e8
 ┃ ┃ ┃ ┃ ┗ 📜e49982403d23bb072533c463896a7e94d5bd18
 ┃ ┃ ┃ ┣ 📂e7
 ┃ ┃ ┃ ┃ ┣ 📜34764314383251bc4bca297bab4474bf4dc43f
 ┃ ┃ ┃ ┃ ┣ 📜9dab7fea8f6601eaf07072ee6b12fbe20bdbe1
 ┃ ┃ ┃ ┃ ┗ 📜c4cef33d9454ccd75134ef2395179e41a6257f
 ┃ ┃ ┃ ┣ 📂e8
 ┃ ┃ ┃ ┃ ┣ 📜094db7c420a0cd1d53d1f86aa18ca9333cc8b5
 ┃ ┃ ┃ ┃ ┣ 📜3b03f6539ef66532df694109dc1efb3d53e992
 ┃ ┃ ┃ ┃ ┣ 📜47f7773533fe61d3e7bf34db0e49f0c9f9a656
 ┃ ┃ ┃ ┃ ┣ 📜cf2f90a13dbedd0d2ba1b2022e69339e728d1f
 ┃ ┃ ┃ ┃ ┗ 📜eb4b81fb983a15cec5b34076b8a8e5e7599773
 ┃ ┃ ┃ ┣ 📂e9
 ┃ ┃ ┃ ┃ ┣ 📜113d99bbe1dfb747738aaf39900a9d39d00c39
 ┃ ┃ ┃ ┃ ┣ 📜235cd3db1d94411fc7eaeada9441dfaa0cf38c
 ┃ ┃ ┃ ┃ ┣ 📜2522a94d82a571b84ac1de470bcb70b176023c
 ┃ ┃ ┃ ┃ ┣ 📜550e677958d83694dac24f760d77b0dbb63e44
 ┃ ┃ ┃ ┃ ┣ 📜b4d04e51d59a42c8cd9b9ca51b548ed00738c6
 ┃ ┃ ┃ ┃ ┣ 📜b98b7cce0e09a58451d5879ed70a9ce8e47eda
 ┃ ┃ ┃ ┃ ┣ 📜c625d29a3cc6a267fb1418df5ee7eff3e2e49e
 ┃ ┃ ┃ ┃ ┣ 📜ca919df075371585a81451cba2aa9b70b06fcd
 ┃ ┃ ┃ ┃ ┣ 📜ccc8216f9c268e65b2c3efbe7a991bbdf87a42
 ┃ ┃ ┃ ┃ ┣ 📜e1f437bc67b12b4fee889d5a19627d8d8ff6e0
 ┃ ┃ ┃ ┃ ┗ 📜e52dd53d196e03512d1fe37416bf536f9b6b4b
 ┃ ┃ ┃ ┣ 📂ea
 ┃ ┃ ┃ ┃ ┣ 📜3b2c6815635ef4760c490b176ef483740346d1
 ┃ ┃ ┃ ┃ ┣ 📜9585e625d24f3d9a521de084851b63f43696d3
 ┃ ┃ ┃ ┃ ┗ 📜fdb00df46a3d301c05a96f2736150359bb0548
 ┃ ┃ ┃ ┣ 📂eb
 ┃ ┃ ┃ ┃ ┣ 📜0536286f3081c6c0646817037faf5446e3547d
 ┃ ┃ ┃ ┃ ┣ 📜18ca27aa3f05ee4ccd92038f1ad56fa966801e
 ┃ ┃ ┃ ┃ ┣ 📜865f0721006d908c034ae55f76f1d1a57fb337
 ┃ ┃ ┃ ┃ ┣ 📜b1be4ace082a8c5fd9e218ec9c39ed0f01ecff
 ┃ ┃ ┃ ┃ ┗ 📜d0bd81cae9fb6b7697183b42dbc5ef07957192
 ┃ ┃ ┃ ┣ 📂ec
 ┃ ┃ ┃ ┃ ┣ 📜1e5e5053c4fc4871ff9099ec8a5a609b62aaad
 ┃ ┃ ┃ ┃ ┗ 📜59607c107c20e50e44b9ac52509cf81c8fb42c
 ┃ ┃ ┃ ┣ 📂ed
 ┃ ┃ ┃ ┃ ┣ 📜46fee50be342491204c905efaf5419ce1a1a7d
 ┃ ┃ ┃ ┃ ┣ 📜566af7140e4f9a6fe8b9098f621bf44900d809
 ┃ ┃ ┃ ┃ ┣ 📜7bc9c13a85bf81c49483acabd1f6b2b9a6db83
 ┃ ┃ ┃ ┃ ┗ 📜87827ed0016ecba197c7083f59b6d40adcc53a
 ┃ ┃ ┃ ┣ 📂ee
 ┃ ┃ ┃ ┃ ┣ 📜28b4702a9d71d004e1a17974994755311c641a
 ┃ ┃ ┃ ┃ ┣ 📜4287c9201d3035e6b7fc38935836e53fd25597
 ┃ ┃ ┃ ┃ ┣ 📜5b5958ed929c04c17a0df6ea61a56cbf0da5a5
 ┃ ┃ ┃ ┃ ┣ 📜6044d0dd32907ff94f8ed4331499c0b3ee6bf6
 ┃ ┃ ┃ ┃ ┣ 📜74c40aba8cdcd4e5ef3a000906aa78b34a0e58
 ┃ ┃ ┃ ┃ ┣ 📜8ca5bcd8f77d219f29529a9163587235c545d5
 ┃ ┃ ┃ ┃ ┣ 📜a24db46cf0b8ccf998cb0ec3e40158b22636ab
 ┃ ┃ ┃ ┃ ┣ 📜b1a83ebfe9adbc0569d35f786e02647b4a733d
 ┃ ┃ ┃ ┃ ┗ 📜c085789a7505b80ade30306a99a4e2610ea203
 ┃ ┃ ┃ ┣ 📂ef
 ┃ ┃ ┃ ┃ ┣ 📜4d2a13b4c52257ef89e7d16a6ceb4623361d58
 ┃ ┃ ┃ ┃ ┣ 📜6443ccdd58a01790476e3ce084a239f9c1188e
 ┃ ┃ ┃ ┃ ┣ 📜6d7dafb45fd5e970c73c547414b476f44ecb84
 ┃ ┃ ┃ ┃ ┣ 📜8c496696f94d00a8d47ce3f64197b7ceeafc6b
 ┃ ┃ ┃ ┃ ┣ 📜8c743e1e51f12c720dd1537d0d6ed7f4e24718
 ┃ ┃ ┃ ┃ ┗ 📜ed2d3d55090a629e9b612e2c30f5f596ae8b59
 ┃ ┃ ┃ ┣ 📂f0
 ┃ ┃ ┃ ┃ ┣ 📜19cd55610fe70336dde51598fc1dcbbc0f6a0e
 ┃ ┃ ┃ ┃ ┣ 📜35fe2a2755dca96e80d02d05e7dc089847279f
 ┃ ┃ ┃ ┃ ┣ 📜52c1ba42adfb3f692c1cc38a1979e10fa35486
 ┃ ┃ ┃ ┃ ┣ 📜80d1a23437b8d2bfc4a0109dd998661fcff87f
 ┃ ┃ ┃ ┃ ┣ 📜ccbd14ecf0e02f6dafbf7d6e690438e844fa8e
 ┃ ┃ ┃ ┃ ┗ 📜fd5ccb28467803ec6bf342f4fcb96c5845fa03
 ┃ ┃ ┃ ┣ 📂f1
 ┃ ┃ ┃ ┃ ┣ 📜1c6f9420cadc7563889c7d320ed1c0be05cbed
 ┃ ┃ ┃ ┃ ┣ 📜6a20b9bfc5092708c7f00900a6655c4c1079d3
 ┃ ┃ ┃ ┃ ┣ 📜9fca72127cee21f22a2670ff4512833ed3328e
 ┃ ┃ ┃ ┃ ┗ 📜d9945641bc9217564694d2adaf35089f6dfeed
 ┃ ┃ ┃ ┣ 📂f2
 ┃ ┃ ┃ ┃ ┣ 📜20d253c5d72a2f48a6912def64bcc1dcb2bc8c
 ┃ ┃ ┃ ┃ ┣ 📜5a5b42c1b9ebfb31bf61cf7e3e0727e6e2d545
 ┃ ┃ ┃ ┃ ┣ 📜9bb81d4776e517e1ec314b68dd31bc92297d8f
 ┃ ┃ ┃ ┃ ┗ 📜f1c34fc561afd20d005e3c5225cddba6c041ad
 ┃ ┃ ┃ ┣ 📂f3
 ┃ ┃ ┃ ┃ ┣ 📜46fe24f940dccdd1a0c3f1a190f778f738e85d
 ┃ ┃ ┃ ┃ ┣ 📜4f3de68edc0154a44d7df029497597bba796ed
 ┃ ┃ ┃ ┃ ┣ 📜b88366c6ffcf0bc9d69faafd3b59153d2b8d17
 ┃ ┃ ┃ ┃ ┣ 📜b8a6868af70eb27084035c40c63a8aab71cc6f
 ┃ ┃ ┃ ┃ ┗ 📜eb59511da6cac0f35147f853c81fd8af0a87fb
 ┃ ┃ ┃ ┣ 📂f4
 ┃ ┃ ┃ ┃ ┣ 📜5340d55ca45b9be1c14d037f0cdd5fcaf72d5f
 ┃ ┃ ┃ ┃ ┣ 📜696ff34317f7470a208e9edf09372ca4d0f54e
 ┃ ┃ ┃ ┃ ┗ 📜e8617cc04d04bfc6fb6b13815442bcd852115b
 ┃ ┃ ┃ ┣ 📂f5
 ┃ ┃ ┃ ┃ ┣ 📜7cf31681cdebe8adc5e79e37f0175271e5e92f
 ┃ ┃ ┃ ┃ ┣ 📜8bf6861f110714fdce3d96ee3e3b464a48e8c5
 ┃ ┃ ┃ ┃ ┗ 📜fb4a45bb1abf10a13da1a324ccaf8676987f8c
 ┃ ┃ ┃ ┣ 📂f6
 ┃ ┃ ┃ ┃ ┣ 📜7060996f960a802e8c9583079cf283dea65fe2
 ┃ ┃ ┃ ┃ ┣ 📜b8d754c675e7ecb78029fb1b8449d0d9eab5fd
 ┃ ┃ ┃ ┃ ┣ 📜bc73d858261ad5076c172e47b7ee0c86528cc5
 ┃ ┃ ┃ ┃ ┣ 📜d11fe61d1fbab3a85b4e75e66b416027441f50
 ┃ ┃ ┃ ┃ ┗ 📜d5cbdc1ef83c398946d689f08e3922178eb1b6
 ┃ ┃ ┃ ┣ 📂f7
 ┃ ┃ ┃ ┃ ┣ 📜13810b84ed05a058946770ad9871485fe8a6f0
 ┃ ┃ ┃ ┃ ┣ 📜2a574a9ca204fb5f3f2d3a4f130aadfba1e97d
 ┃ ┃ ┃ ┃ ┣ 📜364aea461d64b078dffc26050fb2558800da2c
 ┃ ┃ ┃ ┃ ┣ 📜4537e933c4f9e64b07f6e5c50d4147a696b12f
 ┃ ┃ ┃ ┃ ┣ 📜646b0a149c40317d73506520d5710afe078544
 ┃ ┃ ┃ ┃ ┣ 📜7994aa78e1a9f5bf2dbb79b8e9d9a23e4caa83
 ┃ ┃ ┃ ┃ ┣ 📜dd8028e4df7375857c2d0e97f68e9f6ac6d519
 ┃ ┃ ┃ ┃ ┣ 📜f643f8aadeb82b6452b2252e9869e1c4080426
 ┃ ┃ ┃ ┃ ┗ 📜fafe743ac2193eea3221e13776c319892fd03a
 ┃ ┃ ┃ ┣ 📂f8
 ┃ ┃ ┃ ┃ ┣ 📜8f1e5f5e953950d6b7ec35e32e2ec28611677c
 ┃ ┃ ┃ ┃ ┣ 📜e694873c3e6927ad8abdce21b512e08ddce9fa
 ┃ ┃ ┃ ┃ ┗ 📜fbeba44249055b3e8b146ca6d006c400f4ba3c
 ┃ ┃ ┃ ┣ 📂f9
 ┃ ┃ ┃ ┃ ┣ 📜5f400a1f8f75cfe6e5f07b10593758b14b3170
 ┃ ┃ ┃ ┃ ┣ 📜b1775e96500bf39c83c84c79cd61746c8cf634
 ┃ ┃ ┃ ┃ ┣ 📜c894bb1d0334e575ef3eacca9a1bf9919d3502
 ┃ ┃ ┃ ┃ ┣ 📜d4f7e112d8e1c0532aad592f308eec685554e3
 ┃ ┃ ┃ ┃ ┗ 📜e3f6ba1119c4b9098ca6383ea57b26eccbeef6
 ┃ ┃ ┃ ┣ 📂fa
 ┃ ┃ ┃ ┃ ┣ 📜3f45a0d374ec0406cd19cad14e1b2b54ff0baa
 ┃ ┃ ┃ ┃ ┣ 📜403dc36b1aea715fa4817ac5ecaac91168eea1
 ┃ ┃ ┃ ┃ ┣ 📜4ca59a4aa2d7a7685e88adc9b9f2685d5f95d6
 ┃ ┃ ┃ ┃ ┣ 📜579600b150dfe96277f923c509bc473517b32a
 ┃ ┃ ┃ ┃ ┗ 📜e4ac547e82f7e0bb09cdef7cb157abbf9aeee9
 ┃ ┃ ┃ ┣ 📂fb
 ┃ ┃ ┃ ┃ ┗ 📜657fbeaf4f32e8abfd922bc25eacc5446bb7a9
 ┃ ┃ ┃ ┣ 📂fc
 ┃ ┃ ┃ ┃ ┣ 📜3acceaec16e27357aec3ecf63f9a8576abd8ce
 ┃ ┃ ┃ ┃ ┣ 📜5412d9ffc4fdd42e7b668f012b4badc3e18889
 ┃ ┃ ┃ ┃ ┣ 📜5b743f517859d2ceecdd5fa3257f94cb2809b4
 ┃ ┃ ┃ ┃ ┣ 📜62ceb88255c01cf964b16d49ec3f232d4a34f2
 ┃ ┃ ┃ ┃ ┣ 📜752eb01f1bb3c4c469a72c8f5a4de1ed2ad2ed
 ┃ ┃ ┃ ┃ ┣ 📜7dae4fd69165ce96504c35282b476b1a4034c8
 ┃ ┃ ┃ ┃ ┣ 📜be55a6e43469d3d2907b7767ed43c7779e9dda
 ┃ ┃ ┃ ┃ ┣ 📜d54c7aeb2c58103d3afaa3cb035b85933eb525
 ┃ ┃ ┃ ┃ ┗ 📜ed2a0a31c96ba329356475cb6f796ac50476de
 ┃ ┃ ┃ ┣ 📂fd
 ┃ ┃ ┃ ┃ ┣ 📜18559d499beeaabab8d7c633b5fb7a9754763d
 ┃ ┃ ┃ ┃ ┣ 📜20d2ba4f687506a8e36614ea823d25081118c3
 ┃ ┃ ┃ ┃ ┣ 📜6ec1b5f61b18db80a5454ad9874356fe5e2001
 ┃ ┃ ┃ ┃ ┣ 📜9fe4ea539c3fa3762cb46864b30c012e377b0c
 ┃ ┃ ┃ ┃ ┣ 📜c3b45851bfc9fa9ea529be42666535f9650a7a
 ┃ ┃ ┃ ┃ ┗ 📜ceca233b65eac37bb40099cea316bd2f610bc4
 ┃ ┃ ┃ ┣ 📂fe
 ┃ ┃ ┃ ┃ ┣ 📜6eb41b46ed3c487a104e6ac68fd5e74dfdde42
 ┃ ┃ ┃ ┃ ┣ 📜74791edbd7ad714d1a51b3d629dcfb92ea345d
 ┃ ┃ ┃ ┃ ┣ 📜7d9198773b123bacd0bbc8cce57420fad2dfa1
 ┃ ┃ ┃ ┃ ┗ 📜cc992b4c1e1085e2bbdc729a40fd847f2c03a8
 ┃ ┃ ┃ ┣ 📂ff
 ┃ ┃ ┃ ┃ ┣ 📜14c9522110ba93dc56f6f6edff95d650dfba5a
 ┃ ┃ ┃ ┃ ┣ 📜25fee04fdc3ed7410a61f3540ed237031d8eb0
 ┃ ┃ ┃ ┃ ┣ 📜728d9bb57ef10a20e2f1bfc6674fce763efbab
 ┃ ┃ ┃ ┃ ┣ 📜b27d869c730be6924ac71449414f759ef5b99d
 ┃ ┃ ┃ ┃ ┣ 📜be587b99d305db3a0b0400d197507a0e4b4d27
 ┃ ┃ ┃ ┃ ┣ 📜c62bb285df049f1957dfa4088592b07728c7a0
 ┃ ┃ ┃ ┃ ┣ 📜e5049d3f929f032669a7e29d86d431449cbbb4
 ┃ ┃ ┃ ┃ ┗ 📜e6d8718d131ff6035ba3ac9e5628e4a3bbff4e
 ┃ ┃ ┃ ┣ 📂info
 ┃ ┃ ┃ ┗ 📂pack
 ┃ ┃ ┃ ┃ ┣ 📜pack-585e4449ada2e4be4c56d5decf2fe99a1193568a.idx
 ┃ ┃ ┃ ┃ ┣ 📜pack-585e4449ada2e4be4c56d5decf2fe99a1193568a.pack
 ┃ ┃ ┃ ┃ ┣ 📜pack-be59c8bb8df2177e506bc8dd6683d67a2f856096.idx
 ┃ ┃ ┃ ┃ ┗ 📜pack-be59c8bb8df2177e506bc8dd6683d67a2f856096.pack
 ┃ ┃ ┣ 📂refs
 ┃ ┃ ┃ ┣ 📂heads
 ┃ ┃ ┃ ┃ ┗ 📜master
 ┃ ┃ ┃ ┣ 📂remotes
 ┃ ┃ ┃ ┃ ┗ 📂origin
 ┃ ┃ ┃ ┃ ┃ ┗ 📜master
 ┃ ┃ ┃ ┗ 📂tags
 ┃ ┃ ┣ 📜COMMIT_EDITMSG
 ┃ ┃ ┣ 📜config
 ┃ ┃ ┣ 📜description
 ┃ ┃ ┣ 📜FETCH_HEAD
 ┃ ┃ ┣ 📜HEAD
 ┃ ┃ ┣ 📜index
 ┃ ┃ ┗ 📜ORIG_HEAD
 ┃ ┣ 📂app
 ┃ ┃ ┣ 📂Console
 ┃ ┃ ┃ ┗ 📜Kernel.php
 ┃ ┃ ┣ 📂Exceptions
 ┃ ┃ ┃ ┗ 📜Handler.php
 ┃ ┃ ┣ 📂Exports
 ┃ ┃ ┃ ┣ 📜CadetsExport.php
 ┃ ┃ ┃ ┣ 📜ExtensionsExport.php
 ┃ ┃ ┃ ┣ 📜FamiliesExport.php
 ┃ ┃ ┃ ┣ 📜IdentitiesExport.php
 ┃ ┃ ┃ ┣ 📜LkksExport.php
 ┃ ┃ ┃ ┣ 📜LksesExport.php
 ┃ ┃ ┃ ┣ 📜ProfesionalsExport.php
 ┃ ┃ ┃ ┣ 📜PsmsExport.php
 ┃ ┃ ┃ ┣ 📜TksksExport.php
 ┃ ┃ ┃ ┣ 📜WksbmsExport.php
 ┃ ┃ ┃ ┣ 📜WorldsExport.php
 ┃ ┃ ┃ ┣ 📜WpksesExport.php
 ┃ ┃ ┃ ┗ 📜YouthsExport.php
 ┃ ┃ ┣ 📂Http
 ┃ ┃ ┃ ┣ 📂Controllers
 ┃ ┃ ┃ ┃ ┣ 📂Auth
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConfirmPasswordController.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ForgotPasswordController.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜LoginController.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RegisterController.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ResetPasswordController.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜VerificationController.php
 ┃ ┃ ┃ ┃ ┣ 📜CadetController.php
 ┃ ┃ ┃ ┃ ┣ 📜Controller.php
 ┃ ┃ ┃ ┃ ┣ 📜CoordinatorController.php
 ┃ ┃ ┃ ┃ ┣ 📜ExtensionController.php
 ┃ ┃ ┃ ┃ ┣ 📜FamilyController.php
 ┃ ┃ ┃ ┃ ┣ 📜HomeController.php
 ┃ ┃ ┃ ┃ ┣ 📜IdentityController.php
 ┃ ┃ ┃ ┃ ┣ 📜LkkkController.php
 ┃ ┃ ┃ ┃ ┣ 📜LksController.php
 ┃ ┃ ┃ ┃ ┣ 📜OperatorController.php
 ┃ ┃ ┃ ┃ ┣ 📜PlaceController.php
 ┃ ┃ ┃ ┃ ┣ 📜ProfesionalController.php
 ┃ ┃ ┃ ┃ ┣ 📜ProfileController.php
 ┃ ┃ ┃ ┃ ┣ 📜PsmController.php
 ┃ ┃ ┃ ┃ ┣ 📜TkskController.php
 ┃ ┃ ┃ ┃ ┣ 📜WksbmController.php
 ┃ ┃ ┃ ┃ ┣ 📜WorldController.php
 ┃ ┃ ┃ ┃ ┣ 📜WpksController.php
 ┃ ┃ ┃ ┃ ┗ 📜YouthController.php
 ┃ ┃ ┃ ┣ 📂Middleware
 ┃ ┃ ┃ ┃ ┣ 📜Authenticate.php
 ┃ ┃ ┃ ┃ ┣ 📜CheckForMaintenanceMode.php
 ┃ ┃ ┃ ┃ ┣ 📜EncryptCookies.php
 ┃ ┃ ┃ ┃ ┣ 📜RedirectIfAuthenticated.php
 ┃ ┃ ┃ ┃ ┣ 📜TrimStrings.php
 ┃ ┃ ┃ ┃ ┣ 📜TrustProxies.php
 ┃ ┃ ┃ ┃ ┗ 📜VerifyCsrfToken.php
 ┃ ┃ ┃ ┣ 📂Requests
 ┃ ┃ ┃ ┃ ┗ 📜UpdateProfileRequest.php
 ┃ ┃ ┃ ┗ 📜Kernel.php
 ┃ ┃ ┣ 📂Providers
 ┃ ┃ ┃ ┣ 📜AppServiceProvider.php
 ┃ ┃ ┃ ┣ 📜AuthServiceProvider.php
 ┃ ┃ ┃ ┣ 📜BroadcastServiceProvider.php
 ┃ ┃ ┃ ┣ 📜EventServiceProvider.php
 ┃ ┃ ┃ ┗ 📜RouteServiceProvider.php
 ┃ ┃ ┣ 📜Cadet.php
 ┃ ┃ ┣ 📜Coordinator.php
 ┃ ┃ ┣ 📜Extension.php
 ┃ ┃ ┣ 📜Family.php
 ┃ ┃ ┣ 📜Identity.php
 ┃ ┃ ┣ 📜Lkkk.php
 ┃ ┃ ┣ 📜Lks.php
 ┃ ┃ ┣ 📜Operator.php
 ┃ ┃ ┣ 📜Place.php
 ┃ ┃ ┣ 📜Profesional.php
 ┃ ┃ ┣ 📜Profile.php
 ┃ ┃ ┣ 📜Psm.php
 ┃ ┃ ┣ 📜Tksk.php
 ┃ ┃ ┣ 📜User.php
 ┃ ┃ ┣ 📜Wksbm.php
 ┃ ┃ ┣ 📜World.php
 ┃ ┃ ┣ 📜Wpks.php
 ┃ ┃ ┗ 📜Youth.php
 ┃ ┣ 📂bootstrap
 ┃ ┃ ┣ 📂cache
 ┃ ┃ ┃ ┣ 📜.gitignore
 ┃ ┃ ┃ ┣ 📜packages.php
 ┃ ┃ ┃ ┗ 📜services.php
 ┃ ┃ ┗ 📜app.php
 ┃ ┣ 📂config
 ┃ ┃ ┣ 📜app.php
 ┃ ┃ ┣ 📜auth.php
 ┃ ┃ ┣ 📜broadcasting.php
 ┃ ┃ ┣ 📜cache.php
 ┃ ┃ ┣ 📜database.php
 ┃ ┃ ┣ 📜dompdf.php
 ┃ ┃ ┣ 📜excel.php
 ┃ ┃ ┣ 📜filesystems.php
 ┃ ┃ ┣ 📜hashing.php
 ┃ ┃ ┣ 📜logging.php
 ┃ ┃ ┣ 📜mail.php
 ┃ ┃ ┣ 📜queue.php
 ┃ ┃ ┣ 📜services.php
 ┃ ┃ ┣ 📜session.php
 ┃ ┃ ┣ 📜sweetalert.php
 ┃ ┃ ┗ 📜view.php
 ┃ ┣ 📂database
 ┃ ┃ ┣ 📂factories
 ┃ ┃ ┃ ┗ 📜UserFactory.php
 ┃ ┃ ┣ 📂migrations
 ┃ ┃ ┃ ┣ 📜2014_10_12_000000_create_users_table.php
 ┃ ┃ ┃ ┣ 📜2014_10_12_100000_create_password_resets_table.php
 ┃ ┃ ┃ ┣ 📜2019_08_19_000000_create_failed_jobs_table.php
 ┃ ┃ ┃ ┣ 📜2022_03_31_082135_create_profiles_table.php
 ┃ ┃ ┃ ┣ 📜2022_03_31_083502_create_coordinators_table.php
 ┃ ┃ ┃ ┣ 📜2022_03_31_084205_create_operators_table.php
 ┃ ┃ ┃ ┣ 📜2022_03_31_084659_create_places_table.php
 ┃ ┃ ┃ ┣ 📜2022_03_31_085524_create_identities_table.php
 ┃ ┃ ┃ ┣ 📜2022_03_31_090914_create_wpkses_table.php
 ┃ ┃ ┃ ┣ 📜2022_03_31_091347_create_extensions_table.php
 ┃ ┃ ┃ ┣ 📜2022_03_31_091833_create_profesionals_table.php
 ┃ ┃ ┃ ┣ 📜2022_03_31_092344_create_psms_table.php
 ┃ ┃ ┃ ┣ 📜2022_03_31_093225_create_cadets_table.php
 ┃ ┃ ┃ ┣ 📜2022_03_31_093715_create_youths_table.php
 ┃ ┃ ┃ ┣ 📜2022_03_31_094453_create_families_table.php
 ┃ ┃ ┃ ┣ 📜2022_03_31_095442_create_worlds_table.php
 ┃ ┃ ┃ ┣ 📜2022_03_31_100135_create_wksbms_table.php
 ┃ ┃ ┃ ┣ 📜2022_03_31_100550_create_lkses_table.php
 ┃ ┃ ┃ ┣ 📜2022_03_31_100908_create_lkkks_table.php
 ┃ ┃ ┃ ┗ 📜2022_04_01_192650_create_tksks_table.php
 ┃ ┃ ┣ 📂seeds
 ┃ ┃ ┃ ┗ 📜DatabaseSeeder.php
 ┃ ┃ ┗ 📜.gitignore
 ┃ ┣ 📂public
 ┃ ┃ ┣ 📂bukti
 ┃ ┃ ┃ ┣ 📜ERD PMKS - Database ER diagram (crow's foot) (1).png-1648874788.png
 ┃ ┃ ┃ ┣ 📜ERD PMKS - Database ER diagram (crow's foot) (1).png-1648874789.png
 ┃ ┃ ┃ ┣ 📜ERD PSKS (revision).png-1648824737.png
 ┃ ┃ ┃ ┣ 📜ERD PSKS (revision).png-1648824929.png
 ┃ ┃ ┃ ┣ 📜ERD PSKS (revision).png-1648874808.png
 ┃ ┃ ┃ ┣ 📜fix sidik.png-1648861488.png
 ┃ ┃ ┃ ┣ 📜fix sidik.png-1648874169.png
 ┃ ┃ ┃ ┣ 📜fix sidik.png-1648874618.png
 ┃ ┃ ┃ ┣ 📜fix sidik.png-1648875639.png
 ┃ ┃ ┃ ┣ 📜fix sidik.png-1648970576.png
 ┃ ┃ ┃ ┣ 📜home.png-1648990360.png
 ┃ ┃ ┃ ┗ 📜Mari (1).png-1648989461.png
 ┃ ┃ ┣ 📂css
 ┃ ┃ ┃ ┗ 📜app.css
 ┃ ┃ ┣ 📂ERD
 ┃ ┃ ┃ ┗ 📜ERD PSKS (revision III).png
 ┃ ┃ ┣ 📂img
 ┃ ┃ ┃ ┣ 📜1.png
 ┃ ┃ ┃ ┣ 📜2.png
 ┃ ┃ ┃ ┣ 📜3.png
 ┃ ┃ ┃ ┣ 📜a.JPG
 ┃ ┃ ┃ ┣ 📜b.JPG
 ┃ ┃ ┃ ┣ 📜c.JPG
 ┃ ┃ ┃ ┣ 📜d.JPG
 ┃ ┃ ┃ ┣ 📜e.JPG
 ┃ ┃ ┃ ┣ 📜f.JPG
 ┃ ┃ ┃ ┣ 📜fitri.png
 ┃ ┃ ┃ ┣ 📜g.JPG
 ┃ ┃ ┃ ┣ 📜h.JPG
 ┃ ┃ ┃ ┣ 📜home.png
 ┃ ┃ ┃ ┣ 📜i.JPG
 ┃ ┃ ┃ ┣ 📜j.JPG
 ┃ ┃ ┃ ┣ 📜k.JPG
 ┃ ┃ ┃ ┣ 📜l.JPG
 ┃ ┃ ┃ ┣ 📜logo.png
 ┃ ┃ ┃ ┣ 📜mari.png
 ┃ ┃ ┃ ┣ 📜mari1.png
 ┃ ┃ ┃ ┣ 📜mari2.png
 ┃ ┃ ┃ ┣ 📜mari3.png
 ┃ ┃ ┃ ┣ 📜mari4.png
 ┃ ┃ ┃ ┗ 📜psks.png
 ┃ ┃ ┣ 📂js
 ┃ ┃ ┃ ┗ 📜app.js
 ┃ ┃ ┣ 📂layout
 ┃ ┃ ┃ ┣ 📂.github
 ┃ ┃ ┃ ┃ ┗ 📂workflows
 ┃ ┃ ┃ ┃ ┃ ┣ 📜handle-new-issue-comment.yml
 ┃ ┃ ┃ ┃ ┃ ┣ 📜handle-new-issue.yml
 ┃ ┃ ┃ ┃ ┃ ┗ 📜issue-staler.yml
 ┃ ┃ ┃ ┣ 📂assets
 ┃ ┃ ┃ ┃ ┣ 📂css
 ┃ ┃ ┃ ┃ ┃ ┗ 📜demo.css
 ┃ ┃ ┃ ┃ ┣ 📂img
 ┃ ┃ ┃ ┃ ┃ ┣ 📂avatars
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜1.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜5.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜6.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜7.png
 ┃ ┃ ┃ ┃ ┃ ┣ 📂backgrounds
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜18.jpg
 ┃ ┃ ┃ ┃ ┃ ┣ 📂elements
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜1.jpg
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜11.jpg
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜12.jpg
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜13.jpg
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜17.jpg
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜18.jpg
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜19.jpg
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜2.jpg
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜20.jpg
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜3.jpg
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜4.jpg
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜5.jpg
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜7.jpg
 ┃ ┃ ┃ ┃ ┃ ┣ 📂favicon
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜favicon.ico
 ┃ ┃ ┃ ┃ ┃ ┣ 📂icons
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂brands
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜asana.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜behance.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dribbble.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜facebook.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜github.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜google.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜instagram.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mailchimp.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜slack.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜twitter.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂unicons
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜cc-primary.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜cc-success.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜cc-warning.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜chart-success.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜chart.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜paypal.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜wallet-info.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜wallet.png
 ┃ ┃ ┃ ┃ ┃ ┣ 📂illustrations
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜girl-doing-yoga-light.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜man-with-laptop-light.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜page-misc-error-light.png
 ┃ ┃ ┃ ┃ ┃ ┗ 📂layouts
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜layout-container-light.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜layout-fluid-light.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜layout-without-menu-light.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜layout-without-navbar-light.png
 ┃ ┃ ┃ ┃ ┣ 📂js
 ┃ ┃ ┃ ┃ ┃ ┣ 📜config.js
 ┃ ┃ ┃ ┃ ┃ ┣ 📜dashboards-analytics.js
 ┃ ┃ ┃ ┃ ┃ ┣ 📜extended-ui-perfect-scrollbar.js
 ┃ ┃ ┃ ┃ ┃ ┣ 📜form-basic-inputs.js
 ┃ ┃ ┃ ┃ ┃ ┣ 📜main.js
 ┃ ┃ ┃ ┃ ┃ ┣ 📜pages-account-settings-account.js
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ui-modals.js
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ui-popover.js
 ┃ ┃ ┃ ┃ ┃ ┗ 📜ui-toasts.js
 ┃ ┃ ┃ ┃ ┗ 📂vendor
 ┃ ┃ ┃ ┃ ┃ ┣ 📂css
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂pages
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜page-account-settings.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜page-auth.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜page-icons.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜page-misc.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜core.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜theme-default.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📂fonts
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂boxicons
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜boxicons.eot
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜boxicons.svg
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜boxicons.ttf
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜boxicons.woff
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜boxicons.woff2
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜boxicons.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📂js
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bootstrap.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜helpers.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜menu.js
 ┃ ┃ ┃ ┃ ┃ ┗ 📂libs
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂apex-charts
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜apex-charts.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜apexcharts.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂highlight
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜highlight-github.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜highlight.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜highlight.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂jquery
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜jquery.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂masonry
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜masonry.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂perfect-scrollbar
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜perfect-scrollbar.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜perfect-scrollbar.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂popper
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜popper.js
 ┃ ┃ ┃ ┣ 📂fonts
 ┃ ┃ ┃ ┃ ┗ 📜boxicons.scss
 ┃ ┃ ┃ ┣ 📂html
 ┃ ┃ ┃ ┃ ┣ 📜auth-forgot-password-basic.html
 ┃ ┃ ┃ ┃ ┣ 📜auth-login-basic.html
 ┃ ┃ ┃ ┃ ┣ 📜auth-register-basic.html
 ┃ ┃ ┃ ┃ ┣ 📜cards-basic.html
 ┃ ┃ ┃ ┃ ┣ 📜extended-ui-perfect-scrollbar.html
 ┃ ┃ ┃ ┃ ┣ 📜extended-ui-text-divider.html
 ┃ ┃ ┃ ┃ ┣ 📜form-layouts-horizontal.html
 ┃ ┃ ┃ ┃ ┣ 📜form-layouts-vertical.html
 ┃ ┃ ┃ ┃ ┣ 📜forms-basic-inputs.html
 ┃ ┃ ┃ ┃ ┣ 📜forms-input-groups.html
 ┃ ┃ ┃ ┃ ┣ 📜icons-boxicons.html
 ┃ ┃ ┃ ┃ ┣ 📜index.html
 ┃ ┃ ┃ ┃ ┣ 📜layouts-blank.html
 ┃ ┃ ┃ ┃ ┣ 📜layouts-container.html
 ┃ ┃ ┃ ┃ ┣ 📜layouts-fluid.html
 ┃ ┃ ┃ ┃ ┣ 📜layouts-without-menu.html
 ┃ ┃ ┃ ┃ ┣ 📜layouts-without-navbar.html
 ┃ ┃ ┃ ┃ ┣ 📜pages-account-settings-account.html
 ┃ ┃ ┃ ┃ ┣ 📜pages-account-settings-connections.html
 ┃ ┃ ┃ ┃ ┣ 📜pages-account-settings-notifications.html
 ┃ ┃ ┃ ┃ ┣ 📜pages-misc-error.html
 ┃ ┃ ┃ ┃ ┣ 📜pages-misc-under-maintenance.html
 ┃ ┃ ┃ ┃ ┣ 📜tables-basic.html
 ┃ ┃ ┃ ┃ ┣ 📜ui-accordion.html
 ┃ ┃ ┃ ┃ ┣ 📜ui-alerts.html
 ┃ ┃ ┃ ┃ ┣ 📜ui-badges.html
 ┃ ┃ ┃ ┃ ┣ 📜ui-buttons.html
 ┃ ┃ ┃ ┃ ┣ 📜ui-carousel.html
 ┃ ┃ ┃ ┃ ┣ 📜ui-collapse.html
 ┃ ┃ ┃ ┃ ┣ 📜ui-dropdowns.html
 ┃ ┃ ┃ ┃ ┣ 📜ui-footer.html
 ┃ ┃ ┃ ┃ ┣ 📜ui-list-groups.html
 ┃ ┃ ┃ ┃ ┣ 📜ui-modals.html
 ┃ ┃ ┃ ┃ ┣ 📜ui-navbar.html
 ┃ ┃ ┃ ┃ ┣ 📜ui-offcanvas.html
 ┃ ┃ ┃ ┃ ┣ 📜ui-pagination-breadcrumbs.html
 ┃ ┃ ┃ ┃ ┣ 📜ui-progress.html
 ┃ ┃ ┃ ┃ ┣ 📜ui-spinners.html
 ┃ ┃ ┃ ┃ ┣ 📜ui-tabs-pills.html
 ┃ ┃ ┃ ┃ ┣ 📜ui-toasts.html
 ┃ ┃ ┃ ┃ ┣ 📜ui-tooltips-popovers.html
 ┃ ┃ ┃ ┃ ┗ 📜ui-typography.html
 ┃ ┃ ┃ ┣ 📂js
 ┃ ┃ ┃ ┃ ┣ 📜bootstrap.js
 ┃ ┃ ┃ ┃ ┣ 📜helpers.js
 ┃ ┃ ┃ ┃ ┗ 📜menu.js
 ┃ ┃ ┃ ┣ 📂libs
 ┃ ┃ ┃ ┃ ┣ 📂apex-charts
 ┃ ┃ ┃ ┃ ┃ ┣ 📜apex-charts.scss
 ┃ ┃ ┃ ┃ ┃ ┗ 📜apexcharts.js
 ┃ ┃ ┃ ┃ ┣ 📂highlight
 ┃ ┃ ┃ ┃ ┃ ┣ 📜highlight-github.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜highlight.js
 ┃ ┃ ┃ ┃ ┃ ┗ 📜highlight.scss
 ┃ ┃ ┃ ┃ ┣ 📂jquery
 ┃ ┃ ┃ ┃ ┃ ┗ 📜jquery.js
 ┃ ┃ ┃ ┃ ┣ 📂masonry
 ┃ ┃ ┃ ┃ ┃ ┗ 📜masonry.js
 ┃ ┃ ┃ ┃ ┣ 📂perfect-scrollbar
 ┃ ┃ ┃ ┃ ┃ ┣ 📜perfect-scrollbar.js
 ┃ ┃ ┃ ┃ ┃ ┗ 📜perfect-scrollbar.scss
 ┃ ┃ ┃ ┃ ┗ 📂popper
 ┃ ┃ ┃ ┃ ┃ ┗ 📜popper.js
 ┃ ┃ ┃ ┣ 📂scss
 ┃ ┃ ┃ ┃ ┣ 📂pages
 ┃ ┃ ┃ ┃ ┃ ┣ 📜page-account-settings.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜page-auth.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜page-icons.scss
 ┃ ┃ ┃ ┃ ┃ ┗ 📜page-misc.scss
 ┃ ┃ ┃ ┃ ┣ 📂_bootstrap-extended
 ┃ ┃ ┃ ┃ ┃ ┣ 📂forms
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_floating-labels.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_form-control.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_form-range.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_form-select.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_input-group.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_labels.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜_validation.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📂mixins
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_alert.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_badge.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_buttons.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_caret.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_dropdown.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_forms.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_list-group.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_misc.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_navs.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_pagination.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_progress.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_table-variants.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜_toasts.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_accordion.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_alert.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_badge.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_breadcrumb.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_button-group.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_buttons.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_card.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_carousel.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_close.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_dropdown.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_forms.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_functions.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_include.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_list-group.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_mixins.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_modal.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_nav.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_navbar.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_offcanvas.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_pagination.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_popover.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_progress.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_reboot.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_root.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_spinners.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_tables.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_toasts.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_tooltip.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_type.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_utilities-ltr.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_utilities.scss
 ┃ ┃ ┃ ┃ ┃ ┗ 📜_variables.scss
 ┃ ┃ ┃ ┃ ┣ 📂_components
 ┃ ┃ ┃ ┃ ┃ ┣ 📂mixins
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_app-brand.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_avatar.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_footer.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_menu.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_misc.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_navbar.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜_text-divider.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_app-brand.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_avatar.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_base.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_common.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_footer.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_include.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_layout.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_menu.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_mixins.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_text-divider.scss
 ┃ ┃ ┃ ┃ ┃ ┗ 📜_variables.scss
 ┃ ┃ ┃ ┃ ┣ 📂_custom-variables
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_bootstrap-extended.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_components.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_libs.scss
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_pages.scss
 ┃ ┃ ┃ ┃ ┃ ┗ 📜_support.scss
 ┃ ┃ ┃ ┃ ┣ 📂_theme
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_common.scss
 ┃ ┃ ┃ ┃ ┃ ┗ 📜_theme.scss
 ┃ ┃ ┃ ┃ ┣ 📜core.scss
 ┃ ┃ ┃ ┃ ┣ 📜theme-default.scss
 ┃ ┃ ┃ ┃ ┣ 📜_bootstrap-extended.scss
 ┃ ┃ ┃ ┃ ┣ 📜_bootstrap.scss
 ┃ ┃ ┃ ┃ ┣ 📜_colors.scss
 ┃ ┃ ┃ ┃ ┗ 📜_components.scss
 ┃ ┃ ┃ ┣ 📂tasks
 ┃ ┃ ┃ ┃ ┣ 📜build.js
 ┃ ┃ ┃ ┃ ┗ 📜prod.js
 ┃ ┃ ┃ ┣ 📜.browserslistrc
 ┃ ┃ ┃ ┣ 📜.eslintignore
 ┃ ┃ ┃ ┣ 📜.eslintrc.json
 ┃ ┃ ┃ ┣ 📜.gitignore
 ┃ ┃ ┃ ┣ 📜.prettierignore
 ┃ ┃ ┃ ┣ 📜.prettierrc.json
 ┃ ┃ ┃ ┣ 📜build-config.js
 ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┣ 📜documentation.html
 ┃ ┃ ┃ ┣ 📜gulpfile.js
 ┃ ┃ ┃ ┣ 📜index.html
 ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┣ 📜package-lock.json
 ┃ ┃ ┃ ┣ 📜package.json
 ┃ ┃ ┃ ┣ 📜README.md
 ┃ ┃ ┃ ┣ 📜webpack.config.js
 ┃ ┃ ┃ ┗ 📜yarn.lock
 ┃ ┃ ┣ 📂page
 ┃ ┃ ┃ ┣ 📂css
 ┃ ┃ ┃ ┃ ┣ 📜bootstrap.min.css
 ┃ ┃ ┃ ┃ ┗ 📜style.css
 ┃ ┃ ┃ ┣ 📂img
 ┃ ┃ ┃ ┃ ┣ 📜about.png
 ┃ ┃ ┃ ┃ ┣ 📜bg-bottom.png
 ┃ ┃ ┃ ┃ ┣ 📜bg-circle.png
 ┃ ┃ ┃ ┃ ┣ 📜bg-top.png
 ┃ ┃ ┃ ┃ ┣ 📜bg-triangle.png
 ┃ ┃ ┃ ┃ ┣ 📜screenshot-1.png
 ┃ ┃ ┃ ┃ ┣ 📜screenshot-2.png
 ┃ ┃ ┃ ┃ ┣ 📜screenshot-3.png
 ┃ ┃ ┃ ┃ ┣ 📜screenshot-4.png
 ┃ ┃ ┃ ┃ ┣ 📜screenshot-5.png
 ┃ ┃ ┃ ┃ ┣ 📜screenshot-frame.png
 ┃ ┃ ┃ ┃ ┣ 📜testimonial-1.jpg
 ┃ ┃ ┃ ┃ ┣ 📜testimonial-2.jpg
 ┃ ┃ ┃ ┃ ┣ 📜testimonial-3.jpg
 ┃ ┃ ┃ ┃ ┗ 📜testimonial-4.jpg
 ┃ ┃ ┃ ┣ 📂js
 ┃ ┃ ┃ ┃ ┗ 📜main.js
 ┃ ┃ ┃ ┣ 📂lib
 ┃ ┃ ┃ ┃ ┣ 📂animate
 ┃ ┃ ┃ ┃ ┃ ┣ 📜animate.css
 ┃ ┃ ┃ ┃ ┃ ┗ 📜animate.min.css
 ┃ ┃ ┃ ┃ ┣ 📂counterup
 ┃ ┃ ┃ ┃ ┃ ┗ 📜counterup.min.js
 ┃ ┃ ┃ ┃ ┣ 📂easing
 ┃ ┃ ┃ ┃ ┃ ┣ 📜easing.js
 ┃ ┃ ┃ ┃ ┃ ┗ 📜easing.min.js
 ┃ ┃ ┃ ┃ ┣ 📂owlcarousel
 ┃ ┃ ┃ ┃ ┃ ┣ 📂assets
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ajax-loader.gif
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜owl.carousel.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜owl.carousel.min.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜owl.theme.default.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜owl.theme.default.min.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜owl.theme.green.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜owl.theme.green.min.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜owl.video.play.png
 ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┃ ┣ 📜owl.carousel.js
 ┃ ┃ ┃ ┃ ┃ ┗ 📜owl.carousel.min.js
 ┃ ┃ ┃ ┃ ┣ 📂waypoints
 ┃ ┃ ┃ ┃ ┃ ┣ 📜links.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜waypoints.min.js
 ┃ ┃ ┃ ┃ ┗ 📂wow
 ┃ ┃ ┃ ┃ ┃ ┣ 📜wow.js
 ┃ ┃ ┃ ┃ ┃ ┗ 📜wow.min.js
 ┃ ┃ ┃ ┣ 📂scss
 ┃ ┃ ┃ ┃ ┣ 📂bootstrap
 ┃ ┃ ┃ ┃ ┃ ┗ 📂scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂forms
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_floating-labels.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_form-check.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_form-control.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_form-range.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_form-select.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_form-text.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_input-group.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_labels.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜_validation.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂helpers
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_clearfix.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_colored-links.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_position.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_ratio.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_stretched-link.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_text-truncation.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜_visually-hidden.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂mixins
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_alert.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_border-radius.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_box-shadow.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_breakpoints.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_buttons.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_caret.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_clearfix.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_color-scheme.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_container.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_deprecate.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_forms.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_gradients.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_grid.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_image.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_list-group.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_lists.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_pagination.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_reset-text.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_resize.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_table-variants.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_text-truncate.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_transition.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_utilities.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜_visually-hidden.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂utilities
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜_api.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂vendor
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜_rfs.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bootstrap-grid.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bootstrap-reboot.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bootstrap-utilities.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bootstrap.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_accordion.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_alert.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_badge.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_breadcrumb.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_button-group.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_buttons.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_card.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_carousel.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_close.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_containers.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_dropdown.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_forms.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_functions.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_grid.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_helpers.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_images.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_list-group.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_mixins.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_modal.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_nav.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_navbar.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_offcanvas.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_pagination.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_popover.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_progress.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_reboot.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_root.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_spinners.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_tables.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_toasts.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_tooltip.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_transitions.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_type.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜_utilities.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜_variables.scss
 ┃ ┃ ┃ ┃ ┗ 📜bootstrap.scss
 ┃ ┃ ┃ ┣ 📜LICENSE.txt
 ┃ ┃ ┃ ┣ 📜mobile-app-html-template.jpg
 ┃ ┃ ┃ ┗ 📜READ-ME.txt
 ┃ ┃ ┣ 📂ttd
 ┃ ┃ ┃ ┣ 📜ERD PSKS (revision).png-1648808291.png
 ┃ ┃ ┃ ┣ 📜ERD PSKS (revision).png-1648809336.png
 ┃ ┃ ┃ ┣ 📜ERD PSKS (revision).png-1648823793.png
 ┃ ┃ ┃ ┣ 📜ERD PSKS (revision).png-1648823937.png
 ┃ ┃ ┃ ┣ 📜ERD PSKS (revision).png-1648872044.png
 ┃ ┃ ┃ ┣ 📜fix sidik.png-1648816582.png
 ┃ ┃ ┃ ┣ 📜fix sidik.png-1648816847.png
 ┃ ┃ ┃ ┣ 📜fix sidik.png-1648816952.png
 ┃ ┃ ┃ ┣ 📜fix sidik.png-1648819049.png
 ┃ ┃ ┃ ┣ 📜fix sidik.png-1648873391.png
 ┃ ┃ ┃ ┣ 📜fix sidik.png-1648873660.png
 ┃ ┃ ┃ ┣ 📜fix sidik.png-1648874130.png
 ┃ ┃ ┃ ┣ 📜fix sidik.png-1648874524.png
 ┃ ┃ ┃ ┣ 📜fix sidik.png-1648875601.png
 ┃ ┃ ┃ ┣ 📜fix sidik.png-1648970397.png
 ┃ ┃ ┃ ┣ 📜fix sidik.png-1648970607.png
 ┃ ┃ ┃ ┣ 📜fix sidik.png-1648970626.png
 ┃ ┃ ┃ ┗ 📜ttd.jpg-1648812347.jpg
 ┃ ┃ ┣ 📂vendor
 ┃ ┃ ┃ ┗ 📂sweetalert
 ┃ ┃ ┃ ┃ ┗ 📜sweetalert.all.js
 ┃ ┃ ┣ 📜.htaccess
 ┃ ┃ ┣ 📜favicon.ico
 ┃ ┃ ┣ 📜index.php
 ┃ ┃ ┣ 📜mix-manifest.json
 ┃ ┃ ┣ 📜robots.txt
 ┃ ┃ ┗ 📜web.config
 ┃ ┣ 📂resources
 ┃ ┃ ┣ 📂js
 ┃ ┃ ┃ ┣ 📂components
 ┃ ┃ ┃ ┃ ┗ 📜ExampleComponent.vue
 ┃ ┃ ┃ ┣ 📜app.js
 ┃ ┃ ┃ ┗ 📜bootstrap.js
 ┃ ┃ ┣ 📂lang
 ┃ ┃ ┃ ┗ 📂en
 ┃ ┃ ┃ ┃ ┣ 📜auth.php
 ┃ ┃ ┃ ┃ ┣ 📜pagination.php
 ┃ ┃ ┃ ┃ ┣ 📜passwords.php
 ┃ ┃ ┃ ┃ ┗ 📜validation.php
 ┃ ┃ ┣ 📂sass
 ┃ ┃ ┃ ┣ 📜app.scss
 ┃ ┃ ┃ ┗ 📜_variables.scss
 ┃ ┃ ┗ 📂views
 ┃ ┃ ┃ ┣ 📂archieve
 ┃ ┃ ┃ ┃ ┣ 📜archieve.blade.php
 ┃ ┃ ┃ ┃ ┗ 📜welcome_backup.blade.php
 ┃ ┃ ┃ ┣ 📂auth
 ┃ ┃ ┃ ┃ ┣ 📂passwords
 ┃ ┃ ┃ ┃ ┃ ┣ 📜email.blade.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜reset.blade.php
 ┃ ┃ ┃ ┃ ┣ 📜login.blade.php
 ┃ ┃ ┃ ┃ ┣ 📜login_backup.blade.php
 ┃ ┃ ┃ ┃ ┣ 📜logon.blade.php
 ┃ ┃ ┃ ┃ ┣ 📜profile.blade.php
 ┃ ┃ ┃ ┃ ┣ 📜register.blade.php
 ┃ ┃ ┃ ┃ ┣ 📜register_backup.blade.php
 ┃ ┃ ┃ ┃ ┣ 📜rejister.blade.php
 ┃ ┃ ┃ ┃ ┗ 📜verify.blade.php
 ┃ ┃ ┃ ┣ 📂data
 ┃ ┃ ┃ ┃ ┣ 📂cadet
 ┃ ┃ ┃ ┃ ┃ ┣ 📜create.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜edit.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜index.blade.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜show.blade.php
 ┃ ┃ ┃ ┃ ┣ 📂coordinator
 ┃ ┃ ┃ ┃ ┃ ┣ 📜create.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜edit.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜index.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜print.blade.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜show.blade.php
 ┃ ┃ ┃ ┃ ┣ 📂extension
 ┃ ┃ ┃ ┃ ┃ ┣ 📜create.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜edit.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜index.blade.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜show.blade.php
 ┃ ┃ ┃ ┃ ┣ 📂family
 ┃ ┃ ┃ ┃ ┃ ┣ 📜create.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜edit.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜index.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜print.blade.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜show.blade.php
 ┃ ┃ ┃ ┃ ┣ 📂identity
 ┃ ┃ ┃ ┃ ┃ ┣ 📜create.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜edit.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜index.blade.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜show.blade.php
 ┃ ┃ ┃ ┃ ┣ 📂lkkk
 ┃ ┃ ┃ ┃ ┃ ┣ 📜create.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜edit.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜index.blade.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜show.blade.php
 ┃ ┃ ┃ ┃ ┣ 📂lks
 ┃ ┃ ┃ ┃ ┃ ┣ 📜create.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜edit.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜index.blade.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜show.blade.php
 ┃ ┃ ┃ ┃ ┣ 📂operator
 ┃ ┃ ┃ ┃ ┃ ┣ 📜create.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜edit.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜index.blade.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜show.blade.php
 ┃ ┃ ┃ ┃ ┣ 📂place
 ┃ ┃ ┃ ┃ ┃ ┣ 📜create.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜index.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜print.blade.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜show.blade.php
 ┃ ┃ ┃ ┃ ┣ 📂profesional
 ┃ ┃ ┃ ┃ ┃ ┣ 📜create.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜edit.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜index.blade.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜show.blade.php
 ┃ ┃ ┃ ┃ ┣ 📂profile
 ┃ ┃ ┃ ┃ ┃ ┣ 📜create.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜edit.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜index.blade.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜show.blade.php
 ┃ ┃ ┃ ┃ ┣ 📂psm
 ┃ ┃ ┃ ┃ ┃ ┣ 📜create.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜edit.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜index.blade.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜show.blade.php
 ┃ ┃ ┃ ┃ ┣ 📂tksk
 ┃ ┃ ┃ ┃ ┃ ┣ 📜create.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜edit.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜index.blade.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜show.blade.php
 ┃ ┃ ┃ ┃ ┣ 📂wksbm
 ┃ ┃ ┃ ┃ ┃ ┣ 📜create.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜edit.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜index.blade.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜show.blade.php
 ┃ ┃ ┃ ┃ ┣ 📂world
 ┃ ┃ ┃ ┃ ┃ ┣ 📜create.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜edit.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜index.blade.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜show.blade.php
 ┃ ┃ ┃ ┃ ┣ 📂wpks
 ┃ ┃ ┃ ┃ ┃ ┣ 📜create.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜edit.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜index.blade.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜show.blade.php
 ┃ ┃ ┃ ┃ ┗ 📂youth
 ┃ ┃ ┃ ┃ ┃ ┣ 📜create.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜edit.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜index.blade.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜show.blade.php
 ┃ ┃ ┃ ┣ 📂form
 ┃ ┃ ┃ ┃ ┣ 📜cadets.blade.php
 ┃ ┃ ┃ ┃ ┣ 📜extensions.blade.php
 ┃ ┃ ┃ ┃ ┣ 📜families.blade.php
 ┃ ┃ ┃ ┃ ┣ 📜ikkks.blade.php
 ┃ ┃ ┃ ┃ ┣ 📜ikss.blade.php
 ┃ ┃ ┃ ┃ ┣ 📜profesional.blade.php
 ┃ ┃ ┃ ┃ ┣ 📜psms.blade.php
 ┃ ┃ ┃ ┃ ┣ 📜wksbms.blade.php
 ┃ ┃ ┃ ┃ ┣ 📜worlds.blade.php
 ┃ ┃ ┃ ┃ ┣ 📜wpkss.blade.php
 ┃ ┃ ┃ ┃ ┗ 📜youths.blade.php
 ┃ ┃ ┃ ┣ 📂landing
 ┃ ┃ ┃ ┃ ┗ 📜landing.blade.php
 ┃ ┃ ┃ ┣ 📂layout
 ┃ ┃ ┃ ┃ ┣ 📜app.blade.php
 ┃ ┃ ┃ ┃ ┣ 📜master.blade.php
 ┃ ┃ ┃ ┃ ┗ 📜nocard.blade.php
 ┃ ┃ ┃ ┣ 📂partial
 ┃ ┃ ┃ ┃ ┣ 📜nav.blade.php
 ┃ ┃ ┃ ┃ ┗ 📜sidebar.blade.php
 ┃ ┃ ┃ ┣ 📂vendor
 ┃ ┃ ┃ ┃ ┗ 📂sweetalert
 ┃ ┃ ┃ ┃ ┃ ┗ 📜alert.blade.php
 ┃ ┃ ┃ ┣ 📜aboutus.blade.php
 ┃ ┃ ┃ ┣ 📜first.blade.php
 ┃ ┃ ┃ ┣ 📜home.blade.php
 ┃ ┃ ┃ ┣ 📜home_backup.blade.php
 ┃ ┃ ┃ ┗ 📜welcome.blade.php
 ┃ ┣ 📂routes
 ┃ ┃ ┣ 📜api.php
 ┃ ┃ ┣ 📜channels.php
 ┃ ┃ ┣ 📜console.php
 ┃ ┃ ┗ 📜web.php
 ┃ ┣ 📂storage
 ┃ ┃ ┣ 📂app
 ┃ ┃ ┃ ┣ 📂public
 ┃ ┃ ┃ ┃ ┗ 📜.gitignore
 ┃ ┃ ┃ ┗ 📜.gitignore
 ┃ ┃ ┣ 📂framework
 ┃ ┃ ┃ ┣ 📂cache
 ┃ ┃ ┃ ┃ ┣ 📂data
 ┃ ┃ ┃ ┃ ┃ ┣ 📂02
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂cb
 ┃ ┃ ┃ ┃ ┃ ┣ 📂25
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂ce
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜25ced896a8f849eefbaa041fc3eaa103ee6369a7
 ┃ ┃ ┃ ┃ ┃ ┣ 📂68
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂72
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜6872743011ba4348bba53c4c50a8a8f93635ba19
 ┃ ┃ ┃ ┃ ┃ ┣ 📂b6
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂77
 ┃ ┃ ┃ ┃ ┃ ┗ 📜.gitignore
 ┃ ┃ ┃ ┃ ┗ 📜.gitignore
 ┃ ┃ ┃ ┣ 📂sessions
 ┃ ┃ ┃ ┃ ┣ 📜.gitignore
 ┃ ┃ ┃ ┃ ┗ 📜fVBi071lgswROuRhfuKk30AfkIu9ljP28imLdPeX
 ┃ ┃ ┃ ┣ 📂testing
 ┃ ┃ ┃ ┃ ┗ 📜.gitignore
 ┃ ┃ ┃ ┣ 📂views
 ┃ ┃ ┃ ┃ ┣ 📜.gitignore
 ┃ ┃ ┃ ┃ ┣ 📜01b087527a314fc3d833d8ea10d183cefbc36b8a.php
 ┃ ┃ ┃ ┃ ┣ 📜035681d8e76258b63fcc36440c178d1d33465a06.php
 ┃ ┃ ┃ ┃ ┣ 📜06f7932b6fdeba21142603de57a4d867144bb170.php
 ┃ ┃ ┃ ┃ ┣ 📜09c1390f7e117f10af0dac69ac6405a1c74c1c83.php
 ┃ ┃ ┃ ┃ ┣ 📜0e9369eeca6b9f9541928b65b224e7a1d3f04577.php
 ┃ ┃ ┃ ┃ ┣ 📜1a84a042b24e2104344e00be72a0b503cea9ee97.php
 ┃ ┃ ┃ ┃ ┣ 📜1b95d30ea49aa83f357966c7384373ff8f5a6a34.php
 ┃ ┃ ┃ ┃ ┣ 📜1cb1796cade2748cecda5ce2e63fe9adfde7127a.php
 ┃ ┃ ┃ ┃ ┣ 📜1cc9f673c55942b1b70fb5ece77d2a38adeefc8c.php
 ┃ ┃ ┃ ┃ ┣ 📜1fab26c8cad24c0bbd3985853ccbb2a13584c03c.php
 ┃ ┃ ┃ ┃ ┣ 📜22939b813854945aebe59423510a2a95cf0acca2.php
 ┃ ┃ ┃ ┃ ┣ 📜25e1d8968456967e47f0d2b9048b774766fc058e.php
 ┃ ┃ ┃ ┃ ┣ 📜267dde25f7bebfeafcd5822bb2cd605021c407ed.php
 ┃ ┃ ┃ ┃ ┣ 📜276cb3e6e14a96923a910d18453e76191fc989e4.php
 ┃ ┃ ┃ ┃ ┣ 📜27cdef43a22b30855988c3b4f0a35d6c9926cb8e.php
 ┃ ┃ ┃ ┃ ┣ 📜28f6925b522db7416c1c063800dad7f46ebed9e0.php
 ┃ ┃ ┃ ┃ ┣ 📜29d1c7c2c928f7f433ab6de81557cb308c0a545b.php
 ┃ ┃ ┃ ┃ ┣ 📜2adb2fdec829b107bac8e739d7c1c2233bf2cd1e.php
 ┃ ┃ ┃ ┃ ┣ 📜30c612ebfd064271b58bef353422ec47700709f5.php
 ┃ ┃ ┃ ┃ ┣ 📜39ef17160601bc58c20696e9edfbe3de7faad076.php
 ┃ ┃ ┃ ┃ ┣ 📜3a8daec79318b4ee81c83e3f6b12bbc92522dfb6.php
 ┃ ┃ ┃ ┃ ┣ 📜3b0f774333334936ad0a5f8b6ee7c6302ccb598d.php
 ┃ ┃ ┃ ┃ ┣ 📜401b7d4e2ca4c38a95a63b6f34f41b8abe839e38.php
 ┃ ┃ ┃ ┃ ┣ 📜4bbc94df05386b5a9a6751b678ee3410476c527c.php
 ┃ ┃ ┃ ┃ ┣ 📜540580bbd683dae0252056b8e9da872fd1ad6e3f.php
 ┃ ┃ ┃ ┃ ┣ 📜5c8051996eda17f78b0436499ade082653dcc209.php
 ┃ ┃ ┃ ┃ ┣ 📜5dc119cf4504ad092f52c791bbb6047b88406704.php
 ┃ ┃ ┃ ┃ ┣ 📜649e33611b7aecfdfa3751ac86d84618163d6397.php
 ┃ ┃ ┃ ┃ ┣ 📜6af0314855745171f34cee24d969385b04cfe263.php
 ┃ ┃ ┃ ┃ ┣ 📜6d02ec8b698f6ce0b020a000f71ed32a08177fcc.php
 ┃ ┃ ┃ ┃ ┣ 📜71e1ca01e317af10ca3079fd25b8aa73d11a97ee.php
 ┃ ┃ ┃ ┃ ┣ 📜774356d7a229dd1cb59d787d6689b4420f01a9ae.php
 ┃ ┃ ┃ ┃ ┣ 📜7afafabea9ba517e69b198c3d3ef9a0ea9c90d49.php
 ┃ ┃ ┃ ┃ ┣ 📜838b54b1168060e5961d8b432a1b546427fb7d48.php
 ┃ ┃ ┃ ┃ ┣ 📜891e506560001e78645427ff1724983e0b2a5b4e.php
 ┃ ┃ ┃ ┃ ┣ 📜89b478bbf551f53942adbce48da3aeb5b74ba402.php
 ┃ ┃ ┃ ┃ ┣ 📜91510305a7e40a270b7a98db38e92e63ee3bb97f.php
 ┃ ┃ ┃ ┃ ┣ 📜95b5eadbcafd1d7838d3450f9e5a4dcd944f6336.php
 ┃ ┃ ┃ ┃ ┣ 📜a16f64a2a94def7023fcee6049e07d30fe03d858.php
 ┃ ┃ ┃ ┃ ┣ 📜b011c5a4f580bc98f97bd5fb837451748a141f9e.php
 ┃ ┃ ┃ ┃ ┣ 📜b1ec3d3cf3aaa83e92414422a4e775a90fb9579d.php
 ┃ ┃ ┃ ┃ ┣ 📜b38552facf7e4458ad746761d0395236325fdc53.php
 ┃ ┃ ┃ ┃ ┣ 📜b4aaa5ff080b6c326acde352cb42f4e33ec745b7.php
 ┃ ┃ ┃ ┃ ┣ 📜b568060b352ada99f348f910f516bcfc711ce7dc.php
 ┃ ┃ ┃ ┃ ┣ 📜b5bafdee72abd20b220ef37f813fc60736924529.php
 ┃ ┃ ┃ ┃ ┣ 📜b835e8b401117d495f0d07dfa2eadeb4b8aba411.php
 ┃ ┃ ┃ ┃ ┣ 📜ba12450df55a021a3274ce384ddbe2a822b95381.php
 ┃ ┃ ┃ ┃ ┣ 📜bc871f0c032b6dfef4b1d137aaa615037940cb83.php
 ┃ ┃ ┃ ┃ ┣ 📜c31c718d736e03f7bdbaf8be465184e2e5c262a7.php
 ┃ ┃ ┃ ┃ ┣ 📜c8e71388e7bacc6d94f751a1ec6516c983ad6ec7.php
 ┃ ┃ ┃ ┃ ┣ 📜c93e6b6a94054fbb490a25e98a3e92bc1e987798.php
 ┃ ┃ ┃ ┃ ┣ 📜c9bd1459f40e28b6de449cebda45ae3536995e2f.php
 ┃ ┃ ┃ ┃ ┣ 📜cb989f636a7a310126c5985f7cc0dd5450ca6245.php
 ┃ ┃ ┃ ┃ ┣ 📜cf0f6860e62b51934c4a315b06b9f91a453737b7.php
 ┃ ┃ ┃ ┃ ┣ 📜d02f0d6744d11f90c9e983d4cdd73cd681417153.php
 ┃ ┃ ┃ ┃ ┣ 📜d3a239dc84d50b6d3eb22f07aad4d29b929e2d53.php
 ┃ ┃ ┃ ┃ ┣ 📜d568362b2e349ec93156441930c87badcd035f47.php
 ┃ ┃ ┃ ┃ ┣ 📜ddf86a95e34b277ee4097be505f38d10c4ed361e.php
 ┃ ┃ ┃ ┃ ┣ 📜df793664bdb90af41904468ce320aca20e950b1f.php
 ┃ ┃ ┃ ┃ ┣ 📜e15578e6be36e83e768feb628a3b58ca8d69e0f0.php
 ┃ ┃ ┃ ┃ ┣ 📜e262e39987e4de11388a238c02b6126200188308.php
 ┃ ┃ ┃ ┃ ┣ 📜e8abaea2eb255f2d1e7014b44784f28b7e924888.php
 ┃ ┃ ┃ ┃ ┣ 📜e93bf8e864734ac2a6deb952250a381de1d3c25e.php
 ┃ ┃ ┃ ┃ ┣ 📜e97f977542fcacfddb7c47d468d5738528a02667.php
 ┃ ┃ ┃ ┃ ┣ 📜eb6b211f5882a2e23965ba649427ceb48bb5b081.php
 ┃ ┃ ┃ ┃ ┣ 📜ed99811823b99e3431333a85c8501b0512548750.php
 ┃ ┃ ┃ ┃ ┣ 📜f7e5741bb137c83f472dc473afc3109b980645f7.php
 ┃ ┃ ┃ ┃ ┣ 📜f811558684396fdb7637e82c54646d38fd4e079e.php
 ┃ ┃ ┃ ┃ ┣ 📜f8821defccef31d4d3018c48406b48820b6fb264.php
 ┃ ┃ ┃ ┃ ┣ 📜f89fbc5c8e89aefc0b82bb33cff98f2dfa9e3ad1.php
 ┃ ┃ ┃ ┃ ┗ 📜fbfa294403ea6ea030716d49ef846929f5ff95a6.php
 ┃ ┃ ┃ ┗ 📜.gitignore
 ┃ ┃ ┗ 📂logs
 ┃ ┃ ┃ ┣ 📜.gitignore
 ┃ ┃ ┃ ┗ 📜laravel.log
 ┃ ┣ 📂tests
 ┃ ┃ ┣ 📂Feature
 ┃ ┃ ┃ ┗ 📜ExampleTest.php
 ┃ ┃ ┣ 📂Unit
 ┃ ┃ ┃ ┗ 📜ExampleTest.php
 ┃ ┃ ┣ 📜CreatesApplication.php
 ┃ ┃ ┗ 📜TestCase.php
 ┃ ┣ 📂vendor
 ┃ ┃ ┣ 📂barryvdh
 ┃ ┃ ┃ ┗ 📂laravel-dompdf
 ┃ ┃ ┃ ┃ ┣ 📂.github
 ┃ ┃ ┃ ┃ ┃ ┣ 📂ISSUE_TEMPLATE
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜bug_report.md
 ┃ ┃ ┃ ┃ ┃ ┣ 📂workflows
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜run-tests.yml
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FUNDING.yml
 ┃ ┃ ┃ ┃ ┃ ┗ 📜stale.yml
 ┃ ┃ ┃ ┃ ┣ 📂config
 ┃ ┃ ┃ ┃ ┃ ┗ 📜dompdf.php
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Facade
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Pdf.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Facade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PDF.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜ServiceProvider.php
 ┃ ┃ ┃ ┃ ┣ 📂tests
 ┃ ┃ ┃ ┃ ┃ ┣ 📂views
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜test.blade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PdfTest.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TestCase.php
 ┃ ┃ ┃ ┃ ┣ 📜.gitignore
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜grumphp.yml
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜phpstan.neon
 ┃ ┃ ┃ ┃ ┣ 📜phpunit.xml.dist
 ┃ ┃ ┃ ┃ ┗ 📜readme.md
 ┃ ┃ ┣ 📂bin
 ┃ ┃ ┃ ┣ 📜carbon
 ┃ ┃ ┃ ┣ 📜carbon.bat
 ┃ ┃ ┃ ┣ 📜commonmark
 ┃ ┃ ┃ ┣ 📜commonmark.bat
 ┃ ┃ ┃ ┣ 📜php-parse
 ┃ ┃ ┃ ┣ 📜php-parse.bat
 ┃ ┃ ┃ ┣ 📜phpunit
 ┃ ┃ ┃ ┣ 📜phpunit.bat
 ┃ ┃ ┃ ┣ 📜psysh
 ┃ ┃ ┃ ┣ 📜psysh.bat
 ┃ ┃ ┃ ┣ 📜var-dump-server
 ┃ ┃ ┃ ┗ 📜var-dump-server.bat
 ┃ ┃ ┣ 📂composer
 ┃ ┃ ┃ ┣ 📜autoload_classmap.php
 ┃ ┃ ┃ ┣ 📜autoload_files.php
 ┃ ┃ ┃ ┣ 📜autoload_namespaces.php
 ┃ ┃ ┃ ┣ 📜autoload_psr4.php
 ┃ ┃ ┃ ┣ 📜autoload_real.php
 ┃ ┃ ┃ ┣ 📜autoload_static.php
 ┃ ┃ ┃ ┣ 📜ClassLoader.php
 ┃ ┃ ┃ ┣ 📜installed.json
 ┃ ┃ ┃ ┣ 📜installed.php
 ┃ ┃ ┃ ┣ 📜InstalledVersions.php
 ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┗ 📜platform_check.php
 ┃ ┃ ┣ 📂doctrine
 ┃ ┃ ┃ ┣ 📂inflector
 ┃ ┃ ┃ ┃ ┣ 📂docs
 ┃ ┃ ┃ ┃ ┃ ┗ 📂en
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜index.rst
 ┃ ┃ ┃ ┃ ┣ 📂lib
 ┃ ┃ ┃ ┃ ┃ ┗ 📂Doctrine
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂Inflector
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Rules
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂English
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Inflectible.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InflectorFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Rules.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Uninflected.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂French
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Inflectible.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InflectorFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Rules.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Uninflected.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂NorwegianBokmal
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Inflectible.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InflectorFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Rules.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Uninflected.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Portuguese
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Inflectible.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InflectorFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Rules.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Uninflected.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Spanish
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Inflectible.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InflectorFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Rules.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Uninflected.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Turkish
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Inflectible.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InflectorFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Rules.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Uninflected.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Pattern.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Patterns.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Ruleset.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Substitution.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Substitutions.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Transformation.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Transformations.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Word.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CachedWordInflector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GenericLanguageInflectorFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Inflector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InflectorFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Language.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LanguageInflectorFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoopWordInflector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RulesetInflector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜WordInflector.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜phpstan.neon.dist
 ┃ ┃ ┃ ┃ ┣ 📜psalm.xml
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂instantiator
 ┃ ┃ ┃ ┃ ┣ 📂docs
 ┃ ┃ ┃ ┃ ┃ ┗ 📂en
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜index.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜sidebar.rst
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┗ 📂Doctrine
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂Instantiator
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExceptionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidArgumentException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UnexpectedValueException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Instantiator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜InstantiatorInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜.doctrine-project.json
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜CONTRIBUTING.md
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜psalm.xml
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┗ 📂lexer
 ┃ ┃ ┃ ┃ ┣ 📂lib
 ┃ ┃ ┃ ┃ ┃ ┗ 📂Doctrine
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂Common
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂Lexer
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜AbstractLexer.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜psalm.xml
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂dompdf
 ┃ ┃ ┃ ┗ 📂dompdf
 ┃ ┃ ┃ ┃ ┣ 📂lib
 ┃ ┃ ┃ ┃ ┃ ┣ 📂fonts
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Courier-Bold.afm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Courier-BoldOblique.afm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Courier-Oblique.afm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Courier.afm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSans-Bold.ttf
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSans-Bold.ufm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSans-BoldOblique.ttf
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSans-BoldOblique.ufm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSans-Oblique.ttf
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSans-Oblique.ufm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSans.ttf
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSans.ufm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSansMono-Bold.ttf
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSansMono-Bold.ufm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSansMono-BoldOblique.ttf
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSansMono-BoldOblique.ufm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSansMono-Oblique.ttf
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSansMono-Oblique.ufm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSansMono.ttf
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSansMono.ufm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSerif-Bold.ttf
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSerif-Bold.ufm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSerif-BoldItalic.ttf
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSerif-BoldItalic.ufm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSerif-Italic.ttf
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSerif-Italic.ufm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSerif.ttf
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DejaVuSerif.ufm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dompdf_font_family_cache.dist.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Helvetica-Bold.afm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Helvetica-BoldOblique.afm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Helvetica-Oblique.afm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Helvetica.afm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mustRead.html
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Symbol.afm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Times-Bold.afm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Times-BoldItalic.afm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Times-Italic.afm
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Times-Roman.afm
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ZapfDingbats.afm
 ┃ ┃ ┃ ┃ ┃ ┣ 📂html5lib
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Data.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InputStream.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜named-character-references.ser
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Parser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Tokenizer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TreeBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂res
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜broken_image.png
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜broken_image.svg
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜html.css
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Cpdf.php
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Adapter
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CPDF.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GD.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PDFLib.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Css
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AttributeTranslator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Style.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Stylesheet.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ImageException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Frame
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Factory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FrameList.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FrameListIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FrameTree.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FrameTreeIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜FrameTreeList.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂FrameDecorator
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractFrameDecorator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Block.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Image.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Inline.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ListBullet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ListBulletImage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullFrameDecorator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Page.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Table.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableCell.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableRow.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableRowGroup.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂FrameReflower
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractFrameReflower.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Block.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Image.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Inline.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ListBullet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullFrameReflower.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Page.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Table.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableCell.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableRow.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableRowGroup.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Image
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Cache.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Positioner
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Absolute.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractPositioner.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Block.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Fixed.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Inline.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ListBullet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullPositioner.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableCell.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TableRow.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Renderer
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Block.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Image.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Inline.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ListBullet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableCell.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableRowGroup.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Autoloader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Canvas.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CanvasFactory.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Cellmap.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Dompdf.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FontMetrics.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Frame.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Helpers.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜JavascriptEmbedder.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜LineBox.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Options.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PhpEvaluator.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Renderer.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE.LGPL
 ┃ ┃ ┃ ┃ ┣ 📜README.md
 ┃ ┃ ┃ ┃ ┗ 📜VERSION
 ┃ ┃ ┣ 📂dragonmantank
 ┃ ┃ ┃ ┗ 📂cron-expression
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┗ 📂Cron
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractField.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CronExpression.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DayOfMonthField.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DayOfWeekField.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FieldFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FieldInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HoursField.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MinutesField.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜MonthField.php
 ┃ ┃ ┃ ┃ ┣ 📂tests
 ┃ ┃ ┃ ┃ ┃ ┗ 📂Cron
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractFieldTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CronExpressionTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DayOfMonthFieldTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DayOfWeekFieldTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FieldFactoryTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HoursFieldTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MinutesFieldTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜MonthFieldTest.php
 ┃ ┃ ┃ ┃ ┣ 📜.editorconfig
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂egulias
 ┃ ┃ ┃ ┗ 📂email-validator
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AtextAfterCFWS.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CharNotAllowed.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CommaInDomain.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConsecutiveAt.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConsecutiveDot.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CRLFAtTheEnd.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CRLFX2.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CRNoLF.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DomainAcceptsNoMail.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DomainHyphened.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DotAtEnd.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DotAtStart.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExpectingAT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExpectingATEXT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExpectingCTEXT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExpectingDomainLiteralClose.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExpectingDTEXT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExpectingQPair.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidEmail.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LocalOrReservedDomain.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoDNSRecord.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoDomainPart.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoLocalPart.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnclosedComment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnclosedQuotedString.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UnopenedComment.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Parser
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DomainPart.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LocalPart.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Parser.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Validation
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Error
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RFCWarnings.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SpoofEmail.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜EmptyValidationList.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DNSCheckValidation.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EmailValidation.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MultipleErrors.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MultipleValidationWithAnd.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoRFCWarningsValidation.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RFCValidation.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SpoofCheckValidation.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Warning
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AddressLiteral.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CFWSNearAt.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CFWSWithFWS.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Comment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DeprecatedComment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DomainLiteral.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DomainTooLong.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EmailTooLong.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IPV6BadChar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IPV6ColonEnd.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IPV6ColonStart.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IPV6Deprecated.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IPV6DoubleColon.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IPV6GroupCount.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IPV6MaxGroups.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LabelTooLong.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LocalTooLong.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoDNSMXRecord.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ObsoleteDTEXT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜QuotedPart.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜QuotedString.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TLD.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Warning.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜EmailLexer.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜EmailParser.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜EmailValidator.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┗ 📜LICENSE
 ┃ ┃ ┣ 📂facade
 ┃ ┃ ┃ ┣ 📂flare-client-php
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Concerns
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HasContext.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UsesTime.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Context
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConsoleContext.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ContextContextDetector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ContextDetectorInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ContextInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RequestContext.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Contracts
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ProvidesFlareContext.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Enums
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GroupingTypes.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜MessageLevels.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Glows
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Glow.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Recorder.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Http
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Exceptions
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BadResponse.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BadResponseCode.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidData.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MissingParameter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜NotFound.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Client.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Response.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Middleware
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AddGlows.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AnonymizeIp.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜CensorRequestBodyFields.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Solutions
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ReportSolution.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Stacktrace
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Codesnippet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜File.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Frame.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Stacktrace.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Time
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SystemTime.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Time.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Truncation
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractTruncationStrategy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReportTrimmer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TrimContextItemsStrategy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TrimStringsStrategy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TruncationStrategy.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Api.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Flare.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Frame.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜helpers.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Report.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜View.php
 ┃ ┃ ┃ ┃ ┣ 📜.php-cs-fixer.cache
 ┃ ┃ ┃ ┃ ┣ 📜.php-cs-fixer.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂ignition
 ┃ ┃ ┃ ┃ ┣ 📂.github
 ┃ ┃ ┃ ┃ ┃ ┗ 📂workflows
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜run-tests.yml
 ┃ ┃ ┃ ┃ ┣ 📂config
 ┃ ┃ ┃ ┃ ┃ ┣ 📜flare.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜ignition.php
 ┃ ┃ ┃ ┃ ┣ 📂resources
 ┃ ┃ ┃ ┃ ┃ ┣ 📂compiled
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ignition.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜index.html
 ┃ ┃ ┃ ┃ ┃ ┣ 📂views
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜errorPage.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜.gitignore
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Actions
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ShareReportAction.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Commands
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜runnable-solution.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜solution.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SolutionMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TestCommand.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Context
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LaravelConsoleContext.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LaravelContextDetector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜LaravelRequestContext.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂DumpRecorder
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Dump.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DumpHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DumpRecorder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HtmlDumper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜MultiDumpHandler.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂ErrorPage
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ErrorPageHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ErrorPageViewModel.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IgnitionWhoopsHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Renderer.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Exceptions
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidConfig.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnableToShareErrorException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ViewException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ViewExceptionWithSolution.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Facades
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Flare.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Http
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Controllers
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExecuteSolutionController.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HealthCheckController.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ScriptController.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ShareReportController.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜StyleController.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Middleware
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IgnitionConfigValueEnabled.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜IgnitionEnabled.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂Requests
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExecuteSolutionRequest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ShareReportRequest.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Logger
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜FlareHandler.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂LogRecorder
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogMessage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜LogRecorder.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Middleware
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AddDumps.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AddEnvironmentInformation.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AddGitInformation.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AddLogs.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AddQueries.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AddSolutions.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CustomizeGrouping.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SetNotifierName.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂QueryRecorder
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Query.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜QueryRecorder.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂SolutionProviders
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BadMethodCallSolutionProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DefaultDbNameSolutionProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IncorrectValetDbCredentialsSolutionProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidRouteActionSolutionProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MergeConflictSolutionProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MissingAppKeySolutionProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MissingColumnSolutionProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MissingImportSolutionProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MissingPackageSolutionProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RouteNotDefinedSolutionProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RunningLaravelDuskInProductionProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SolutionProviderRepository.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableNotFoundSolutionProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UndefinedVariableSolutionProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnknownValidationSolutionProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ViewNotFoundSolutionProvider.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Solutions
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GenerateAppKeySolution.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MakeViewVariableOptionalSolution.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MissingPackageSolution.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RunMigrationsSolution.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SolutionTransformer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SuggestCorrectVariableNameSolution.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SuggestImportSolution.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SuggestUsingCorrectDbNameSolution.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UseDefaultValetDbCredentialsSolution.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Support
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Packagist
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Package.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Packagist.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ComposerClassMap.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FakeComposer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜StringComparator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Tabs
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Tab.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Views
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Compilers
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜BladeSourceMapCompiler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Concerns
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜CollectsViewExceptions.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂Engines
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompilerEngine.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhpEngine.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜helpers.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Ignition.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜IgnitionConfig.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜IgnitionServiceProvider.php
 ┃ ┃ ┃ ┃ ┣ 📜.babelrc
 ┃ ┃ ┃ ┃ ┣ 📜.prettierignore
 ┃ ┃ ┃ ┃ ┣ 📜.prettierrc
 ┃ ┃ ┃ ┃ ┣ 📜.styleci.yml
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┣ 📜package.json
 ┃ ┃ ┃ ┃ ┣ 📜postcss.config.js
 ┃ ┃ ┃ ┃ ┣ 📜README.md
 ┃ ┃ ┃ ┃ ┣ 📜tailwind.config.js
 ┃ ┃ ┃ ┃ ┣ 📜tsconfig.json
 ┃ ┃ ┃ ┃ ┗ 📜webpack.config.js
 ┃ ┃ ┃ ┗ 📂ignition-contracts
 ┃ ┃ ┃ ┃ ┣ 📂.github
 ┃ ┃ ┃ ┃ ┃ ┗ 📂workflows
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜php-cs-fixer.yml
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜psalm.yml
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜run-tests.yml
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📜BaseSolution.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜HasSolutionsForThrowable.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ProvidesSolution.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RunnableSolution.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Solution.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜SolutionProviderRepository.php
 ┃ ┃ ┃ ┃ ┣ 📜.php_cs
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┗ 📜psalm.xml
 ┃ ┃ ┣ 📂fakerphp
 ┃ ┃ ┃ ┗ 📂faker
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Faker
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Calculator
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Ean.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Iban.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Inn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Isbn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Luhn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TCNo.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Core
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Barcode.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Blood.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Coordinates.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜File.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Number.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Uuid.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Version.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Extension
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AddressExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BarcodeExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BloodExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ColorExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompanyExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Container.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ContainerBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ContainerException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CountryExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Extension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExtensionNotFound.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GeneratorAwareExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GeneratorAwareExtensionTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Helper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NotInContainerException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NumberExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PersonExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumberExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UuidExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜VersionExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Guesser
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Name.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂ORM
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂CakePHP
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ColumnTypeGuesser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EntityPopulator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Populator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Doctrine
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜backward-compatibility.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ColumnTypeGuesser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EntityPopulator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Populator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Mandango
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ColumnTypeGuesser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EntityPopulator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Populator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Propel
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ColumnTypeGuesser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EntityPopulator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Populator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Propel2
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ColumnTypeGuesser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EntityPopulator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Populator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂Spot
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ColumnTypeGuesser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EntityPopulator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Populator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Provider
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂ar_EG
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂ar_JO
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂ar_SA
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂at_AT
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂bg_BG
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂bn_BD
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Utils.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂cs_CZ
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DateTime.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂da_DK
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂de_AT
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂de_CH
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂de_DE
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂el_CY
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂el_GR
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂en_AU
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂en_CA
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂en_GB
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂en_HK
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂en_IN
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂en_NG
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂en_NZ
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂en_PH
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂en_SG
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂en_UG
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂en_US
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂en_ZA
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂es_AR
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂es_ES
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂es_PE
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂es_VE
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂et_EE
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂fa_IR
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂fi_FI
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂fr_BE
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂fr_CA
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂fr_CH
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂fr_FR
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂he_IL
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂hr_HR
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂hu_HU
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂hy_AM
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂id_ID
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂is_IS
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂it_CH
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂it_IT
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂ja_JP
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂ka_GE
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DateTime.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂kk_KZ
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂ko_KR
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂lt_LT
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂lv_LV
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂me_ME
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂mn_MN
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂ms_MY
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Miscellaneous.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂nb_NO
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂ne_NP
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂nl_BE
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂nl_NL
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂pl_PL
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LicensePlate.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂pt_BR
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜check_digit.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂pt_PT
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂ro_MD
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂ro_RO
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂ru_RU
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂sk_SK
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂sl_SI
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂sr_Cyrl_RS
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂sr_Latn_RS
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂sr_RS
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂sv_SE
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Municipality.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂th_TH
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂tr_TR
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DateTime.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂uk_UA
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂vi_VN
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂zh_CN
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DateTime.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂zh_TW
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DateTime.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Barcode.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Base.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Biased.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Company.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DateTime.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜File.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HtmlLorem.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Image.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Internet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Lorem.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Medical.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Miscellaneous.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Payment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Person.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhoneNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UserAgent.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Uuid.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ChanceGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DefaultGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Documentor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Factory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Generator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UniqueGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ValidGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜autoload.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜psalm.baseline.xml
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂fideloper
 ┃ ┃ ┃ ┗ 📂proxy
 ┃ ┃ ┃ ┃ ┣ 📂config
 ┃ ┃ ┃ ┃ ┃ ┗ 📜trustedproxy.php
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TrustedProxyServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TrustProxies.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┗ 📜LICENSE.md
 ┃ ┃ ┣ 📂filp
 ┃ ┃ ┃ ┗ 📂whoops
 ┃ ┃ ┃ ┃ ┣ 📂.github
 ┃ ┃ ┃ ┃ ┃ ┣ 📂workflows
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜tests.yml
 ┃ ┃ ┃ ┃ ┃ ┗ 📜FUNDING.yml
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┗ 📂Whoops
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ErrorException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Formatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Frame.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FrameCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Inspector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Handler
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CallbackHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Handler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HandlerInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜JsonResponseHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PlainTextHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PrettyPageHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜XmlResponseHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Resources
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂css
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜prism.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜whoops.base.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜clipboard.min.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜prism.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜whoops.base.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜zepto.min.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂views
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜env_details.html.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜frames_container.html.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜frames_description.html.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜frame_code.html.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜frame_list.html.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜header.html.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜header_outer.html.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜layout.html.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜panel_details.html.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜panel_details_outer.html.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜panel_left.html.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜panel_left_outer.html.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Util
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HtmlDumperOutput.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Misc.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SystemFacade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TemplateHelper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Run.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RunInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜.mailmap
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┗ 📜SECURITY.md
 ┃ ┃ ┣ 📂hamcrest
 ┃ ┃ ┃ ┗ 📂hamcrest-php
 ┃ ┃ ┃ ┃ ┣ 📂.github
 ┃ ┃ ┃ ┃ ┃ ┗ 📂workflows
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜tests.yml
 ┃ ┃ ┃ ┃ ┣ 📂generator
 ┃ ┃ ┃ ┃ ┃ ┣ 📂parts
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜file_header.txt
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜functions_footer.txt
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜functions_header.txt
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜functions_imports.txt
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜matchers_footer.txt
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜matchers_header.txt
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜matchers_imports.txt
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FactoryCall.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FactoryClass.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FactoryFile.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FactoryGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FactoryMethod.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FactoryParameter.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜GlobalFunctionFile.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜run.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜StaticMethodFile.php
 ┃ ┃ ┃ ┃ ┣ 📂hamcrest
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Hamcrest
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Arrays
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsArray.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsArrayContaining.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsArrayContainingInAnyOrder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsArrayContainingInOrder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsArrayContainingKey.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsArrayContainingKeyValuePair.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsArrayWithSize.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MatchingOnce.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SeriesMatchingOnce.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Collection
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsEmptyTraversable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜IsTraversableWithSize.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Core
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AllOf.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AnyOf.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CombinableMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DescribedAs.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Every.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HasToString.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Is.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsAnything.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsCollectionContaining.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsEqual.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsIdentical.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsInstanceOf.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsNot.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsNull.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsSame.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsTypeOf.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Set.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ShortcutCombination.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Internal
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SelfDescribingValue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Number
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsCloseTo.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜OrderingComparison.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Text
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsEmptyString.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsEqualIgnoringCase.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsEqualIgnoringWhiteSpace.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MatchesPattern.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StringContains.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StringContainsIgnoringCase.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StringContainsInOrder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StringEndsWith.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StringStartsWith.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SubstringMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Type
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsArray.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsBoolean.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsCallable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsDouble.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsInteger.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsNumeric.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsObject.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsResource.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsScalar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜IsString.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Xml
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜HasXPath.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AssertionError.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BaseDescription.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BaseMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Description.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DiagnosingMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FeatureMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Matcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MatcherAssert.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Matchers.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullDescription.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SelfDescribing.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StringDescription.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TypeSafeDiagnosingMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TypeSafeMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Util.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Hamcrest.php
 ┃ ┃ ┃ ┃ ┣ 📂tests
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Hamcrest
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Array
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsArrayContainingInAnyOrderTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsArrayContainingInOrderTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsArrayContainingKeyTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsArrayContainingKeyValuePairTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsArrayContainingTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsArrayTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜IsArrayWithSizeTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Collection
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsEmptyTraversableTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜IsTraversableWithSizeTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Core
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AllOfTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AnyOfTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CombinableMatcherTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DescribedAsTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EveryTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HasToStringTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsAnythingTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsCollectionContainingTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsEqualTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsIdenticalTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsInstanceOfTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsNotTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsNullTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsSameTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsTypeOfTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SampleBaseClass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SampleSubClass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SetTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Number
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsCloseToTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜OrderingComparisonTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Text
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsEmptyStringTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsEqualIgnoringCaseTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsEqualIgnoringWhiteSpaceTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MatchesPatternTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StringContainsIgnoringCaseTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StringContainsInOrderTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StringContainsTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StringEndsWithTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜StringStartsWithTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Type
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsArrayTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsBooleanTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsCallableTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsDoubleTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsIntegerTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsNumericTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsObjectTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsResourceTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsScalarTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜IsStringTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Xml
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜HasXPathTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractMatcherTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BaseMatcherTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FeatureMatcherTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvokedMatcherTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MatcherAssertTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StringDescriptionTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UtilTest.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜bootstrap.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜phpunit.xml.dist
 ┃ ┃ ┃ ┃ ┣ 📜.coveralls.yml
 ┃ ┃ ┃ ┃ ┣ 📜.gitignore
 ┃ ┃ ┃ ┃ ┣ 📜.gush.yml
 ┃ ┃ ┃ ┃ ┣ 📜.travis.yml
 ┃ ┃ ┃ ┃ ┣ 📜CHANGES.txt
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE.txt
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂laravel
 ┃ ┃ ┃ ┣ 📂framework
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┗ 📂Illuminate
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Auth
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Access
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AuthorizationException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Gate.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HandlesAuthorization.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Response.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Console
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂make
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂views
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂layouts
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜app.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ClearResetsCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Events
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Attempting.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Authenticated.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CurrentDeviceLogout.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Failed.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Lockout.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Login.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Logout.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜OtherDeviceLogout.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PasswordReset.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Registered.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Validated.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Verified.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Listeners
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SendEmailVerificationNotification.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Middleware
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Authenticate.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AuthenticateWithBasicAuth.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Authorize.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EnsureEmailIsVerified.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RequirePassword.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Notifications
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResetPassword.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜VerifyEmail.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Passwords
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CanResetPassword.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DatabaseTokenRepository.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PasswordBroker.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PasswordBrokerManager.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PasswordResetServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TokenRepositoryInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Authenticatable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AuthenticationException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AuthManager.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AuthServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CreatesUserProviders.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DatabaseUserProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EloquentUserProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GenericUser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GuardHelpers.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MustVerifyEmail.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Recaller.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RequestGuard.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SessionGuard.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TokenGuard.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Broadcasting
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Broadcasters
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Broadcaster.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogBroadcaster.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullBroadcaster.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PusherBroadcaster.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RedisBroadcaster.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UsePusherChannelConventions.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BroadcastController.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BroadcastEvent.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BroadcastException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BroadcastManager.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BroadcastServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Channel.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EncryptedPrivateChannel.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InteractsWithSockets.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PendingBroadcast.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PresenceChannel.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PrivateChannel.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Bus
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BusServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Dispatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Queueable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Cache
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Console
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜cache.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CacheTableCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClearCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ForgetCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Events
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CacheEvent.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CacheHit.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CacheMissed.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜KeyForgotten.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜KeyWritten.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ApcStore.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ApcWrapper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayLock.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayStore.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CacheManager.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CacheServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DatabaseStore.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DynamoDbLock.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DynamoDbStore.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileStore.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Lock.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LuaScripts.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MemcachedConnector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MemcachedLock.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MemcachedStore.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullStore.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RateLimiter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RedisLock.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RedisStore.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RedisTaggedCache.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Repository.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RetrievesMultipleKeys.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TaggableStore.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TaggedCache.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TagSet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Config
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Repository.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Console
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Concerns
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CallsCommands.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HasParameters.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜InteractsWithIO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Events
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArtisanStarting.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CommandFinished.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CommandStarting.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ScheduledTaskFinished.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ScheduledTaskSkipped.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ScheduledTaskStarting.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Scheduling
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CacheEventMutex.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CacheSchedulingMutex.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CallbackEvent.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CommandBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Event.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EventMutex.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ManagesFrequencies.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Schedule.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ScheduleFinishCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ScheduleRunCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SchedulingMutex.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Application.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Command.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConfirmableTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DetectsApplicationNamespace.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GeneratorCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜OutputStyle.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Parser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Container
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BoundMethod.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Container.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ContextualBindingBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EntryNotFoundException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RewindableGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Util.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Contracts
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Auth
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Access
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Authorizable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Gate.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Authenticatable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CanResetPassword.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Factory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Guard.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MustVerifyEmail.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PasswordBroker.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PasswordBrokerFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StatefulGuard.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SupportsBasicAuth.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UserProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Broadcasting
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Broadcaster.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Factory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ShouldBroadcast.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ShouldBroadcastNow.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Bus
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Dispatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜QueueingDispatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Cache
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Factory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Lock.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LockProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LockTimeoutException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Repository.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Store.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Config
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Repository.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Console
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Application.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Kernel.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Container
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BindingResolutionException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Container.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ContextualBindingBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Cookie
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Factory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜QueueingFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Database
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Events
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜MigrationEvent.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ModelIdentifier.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Debug
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ExceptionHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Encryption
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DecryptException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Encrypter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜EncryptException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Events
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Dispatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Filesystem
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Cloud.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Factory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileExistsException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileNotFoundException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Filesystem.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Foundation
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Application.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Hashing
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Hasher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Http
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Kernel.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Mail
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Mailable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Mailer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜MailQueue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Notifications
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Dispatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Factory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Pagination
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LengthAwarePaginator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Paginator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Pipeline
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Hub.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Pipeline.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Queue
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EntityNotFoundException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EntityResolver.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Factory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Job.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Monitor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Queue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜QueueableCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜QueueableEntity.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ShouldQueue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Redis
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Connection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Connector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Factory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜LimiterTimeoutException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Routing
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BindingRegistrar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Registrar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResponseFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UrlGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UrlRoutable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Session
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Session.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Support
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Arrayable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DeferrableProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Htmlable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Jsonable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MessageBag.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MessageProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Renderable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Responsable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Translation
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HasLocalePreference.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Loader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Translator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Validation
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Factory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ImplicitRule.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Rule.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ValidatesWhenResolved.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Validator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂View
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Engine.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Factory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜View.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Cookie
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Middleware
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AddQueuedCookiesToResponse.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜EncryptCookies.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CookieJar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CookieServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CookieValuePrefix.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Database
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Capsule
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Manager.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Concerns
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BuildsQueries.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ManagesTransactions.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Connectors
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConnectionFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Connector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConnectorInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MySqlConnector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PostgresConnector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SQLiteConnector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SqlServerConnector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Console
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Factories
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜factory.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜FactoryMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Migrations
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BaseCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FreshCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InstallCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MigrateCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MigrateMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RefreshCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResetCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RollbackCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StatusCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TableGuesser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Seeds
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜seeder.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SeedCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SeederMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜WipeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Eloquent
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Concerns
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GuardsAttributes.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HasAttributes.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HasEvents.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HasGlobalScopes.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HasRelationships.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HasTimestamps.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HidesAttributes.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜QueriesRelationships.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Relations
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Concerns
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AsPivot.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InteractsWithPivotTable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SupportsDefaultModels.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BelongsTo.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BelongsToMany.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HasMany.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HasManyThrough.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HasOne.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HasOneOrMany.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HasOneThrough.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MorphMany.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MorphOne.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MorphOneOrMany.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MorphPivot.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MorphTo.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MorphToMany.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Pivot.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Relation.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Builder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Collection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Factory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FactoryBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HigherOrderBuilderProxy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜JsonEncodingException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MassAssignmentException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Model.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ModelNotFoundException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜QueueEntityResolver.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RelationNotFoundException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Scope.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SoftDeletes.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SoftDeletingScope.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Events
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConnectionEvent.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MigrationEnded.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MigrationEvent.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MigrationsEnded.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MigrationsStarted.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MigrationStarted.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoPendingMigrations.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜QueryExecuted.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StatementPrepared.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TransactionBeginning.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TransactionCommitted.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TransactionRolledBack.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Migrations
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜blank.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜create.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜update.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DatabaseMigrationRepository.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Migration.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MigrationCreator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MigrationRepositoryInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Migrator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Query
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Grammars
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Grammar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MySqlGrammar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PostgresGrammar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SQLiteGrammar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SqlServerGrammar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Processors
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MySqlProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PostgresProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Processor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SQLiteProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SqlServerProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Builder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Expression.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜JoinClause.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Schema
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Grammars
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ChangeColumn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Grammar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MySqlGrammar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PostgresGrammar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RenameColumn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SQLiteGrammar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SqlServerGrammar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Blueprint.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Builder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ColumnDefinition.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ForeignKeyDefinition.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MySqlBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PostgresBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SQLiteBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SqlServerBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConfigurationUrlParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Connection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConnectionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConnectionResolver.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConnectionResolverInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DatabaseManager.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DatabaseServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DetectsConcurrencyErrors.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DetectsLostConnections.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Grammar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MigrationServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MySqlConnection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PostgresConnection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜QueryException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜README.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Seeder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SQLiteConnection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SqlServerConnection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Encryption
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Encrypter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EncryptionServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Events
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CallQueuedListener.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Dispatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EventServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜NullDispatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Filesystem
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Cache.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Filesystem.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FilesystemAdapter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FilesystemManager.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FilesystemServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Foundation
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Auth
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Access
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Authorizable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜AuthorizesRequests.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AuthenticatesUsers.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConfirmsPasswords.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RedirectsUsers.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RegistersUsers.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResetsPasswords.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SendsPasswordResetEmails.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ThrottlesLogins.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜User.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜VerifiesEmails.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Bootstrap
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BootProviders.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HandleExceptions.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LoadConfiguration.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LoadEnvironmentVariables.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RegisterFacades.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RegisterProviders.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SetRequestForConsole.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Bus
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Dispatchable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DispatchesJobs.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PendingChain.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PendingDispatch.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Console
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Presets
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂bootstrap-stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜app.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜_variables.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂none-stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜app.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bootstrap.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜webpack.mix.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂react-stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜app.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Example.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜webpack.mix.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂vue-stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜app.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExampleComponent.vue
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜webpack.mix.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Bootstrap.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜None.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Preset.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜React.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Vue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜channel.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜console.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜event.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜exception-render-report.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜exception-render.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜exception-report.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜exception.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜job-queued.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜job.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜listener-duck.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜listener-queued-duck.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜listener-queued.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜listener.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mail.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜markdown-mail.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜markdown-notification.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜markdown.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜model.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜notification.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜observer.plain.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜observer.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pivot.model.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜policy.plain.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜policy.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜provider.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜request.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜resource-collection.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜resource.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜routes.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜rule.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜test.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜unit-test.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ChannelMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClearCompiledCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClosureCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConfigCacheCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConfigClearCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConsoleMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DownCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EnvironmentCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EventCacheCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EventClearCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EventGenerateCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EventListCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EventMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExceptionMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜JobMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Kernel.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜KeyGenerateCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ListenerMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MailMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ModelMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NotificationMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ObserverMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜OptimizeClearCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜OptimizeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PackageDiscoverCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PolicyMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PresetCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ProviderMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜QueuedCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RequestMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResourceMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RouteCacheCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RouteClearCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RouteListCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RuleMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ServeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StorageLinkCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UpCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜VendorPublishCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ViewCacheCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ViewClearCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Events
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DiscoverEvents.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Dispatchable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜LocaleUpdated.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Exceptions
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂views
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜401.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜403.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜404.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜419.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜429.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜500.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜503.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜illustrated-layout.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜layout.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜minimal.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Handler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜WhoopsHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Http
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Events
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RequestHandled.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Exceptions
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜MaintenanceModeException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Middleware
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CheckForMaintenanceMode.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConvertEmptyStringsToNull.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TransformsRequest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TrimStrings.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ValidatePostSize.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜VerifyCsrfToken.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FormRequest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Kernel.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Providers
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArtisanServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ComposerServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConsoleSupportServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FormRequestServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜FoundationServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜facade.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Support
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂Providers
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AuthServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EventServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RouteServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Testing
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Concerns
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InteractsWithAuthentication.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InteractsWithConsole.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InteractsWithContainer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InteractsWithDatabase.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InteractsWithExceptionHandling.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InteractsWithRedis.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InteractsWithSession.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MakesHttpRequests.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜MocksApplicationServices.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Constraints
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArraySubset.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HasInDatabase.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SeeInOrder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SoftDeletedInDatabase.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Assert.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DatabaseMigrations.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DatabaseTransactions.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PendingCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RefreshDatabase.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RefreshDatabaseState.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestCase.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestResponse.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜WithFaker.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜WithoutEvents.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜WithoutMiddleware.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Validation
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ValidatesRequests.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AliasLoader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Application.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ComposerScripts.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EnvironmentDetector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜helpers.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Inspiring.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Mix.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PackageManifest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ProviderRepository.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Hashing
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractHasher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Argon2IdHasher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArgonHasher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BcryptHasher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HashManager.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HashServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Http
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Concerns
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InteractsWithContentTypes.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InteractsWithFlashData.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜InteractsWithInput.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Exceptions
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HttpResponseException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PostTooLargeException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ThrottleRequestsException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Middleware
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CheckResponseForModifications.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FrameGuard.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SetCacheHeaders.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TrustHosts.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Resources
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AnonymousResourceCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜JsonResource.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PaginatedResourceResponse.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Resource.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResourceCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ResourceResponse.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CollectsResources.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConditionallyLoadsAttributes.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DelegatesToResource.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MergeValue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MissingValue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PotentiallyMissing.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Testing
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜File.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜MimeType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜File.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileHelpers.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜JsonResponse.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RedirectResponse.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Request.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Response.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResponseTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UploadedFile.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Log
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Events
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜MessageLogged.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Logger.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogManager.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ParsesLogConfiguration.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Mail
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Events
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MessageSending.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜MessageSent.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂resources
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂views
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂html
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂promotion
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜button.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂themes
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜default.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜button.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜footer.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜header.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜layout.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜message.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜panel.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜promotion.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜subcopy.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜table.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂text
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂promotion
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜button.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜button.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜footer.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜header.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜layout.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜message.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜panel.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜promotion.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜subcopy.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜table.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Transport
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayTransport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogTransport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MailgunTransport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SesTransport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Transport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Mailable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Mailer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MailServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Markdown.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Message.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PendingMail.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SendQueuedMailable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TransportManager.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Notifications
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Channels
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BroadcastChannel.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DatabaseChannel.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜MailChannel.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Console
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜notifications.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜NotificationTableCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Events
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BroadcastNotificationCreated.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NotificationFailed.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NotificationSending.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜NotificationSent.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Messages
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BroadcastMessage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DatabaseMessage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MailMessage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SimpleMessage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂resources
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂views
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜email.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Action.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AnonymousNotifiable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ChannelManager.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DatabaseNotification.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DatabaseNotificationCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HasDatabaseNotifications.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Notifiable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Notification.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NotificationSender.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NotificationServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RoutesNotifications.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SendQueuedNotifications.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Pagination
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂resources
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂views
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bootstrap-4.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜default.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜semantic-ui.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜simple-bootstrap-4.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜simple-default.blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractPaginator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LengthAwarePaginator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PaginationServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Paginator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UrlWindow.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Pipeline
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Hub.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Pipeline.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PipelineServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Queue
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Capsule
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Manager.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Connectors
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BeanstalkdConnector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConnectorInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DatabaseConnector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullConnector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RedisConnector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SqsConnector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SyncConnector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Console
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜failed_jobs.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜jobs.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FailedTableCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FlushFailedCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ForgetFailedCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ListenCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ListFailedCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RestartCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RetryCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜WorkCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Events
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜JobExceptionOccurred.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜JobFailed.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜JobProcessed.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜JobProcessing.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Looping.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜WorkerStopping.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Failed
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DatabaseFailedJobProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DynamoDbFailedJobProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FailedJobProviderInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜NullFailedJobProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Jobs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BeanstalkdJob.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DatabaseJob.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DatabaseJobRecord.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Job.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜JobName.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RedisJob.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SqsJob.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SyncJob.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BeanstalkdQueue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CallQueuedClosure.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CallQueuedHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DatabaseQueue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InteractsWithQueue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidPayloadException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Listener.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ListenerOptions.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LuaScripts.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ManuallyFailedException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MaxAttemptsExceededException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullQueue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Queue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜QueueManager.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜QueueServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜README.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RedisQueue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SerializableClosure.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SerializesAndRestoresModelIdentifiers.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SerializesModels.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SqsQueue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SyncQueue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Worker.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜WorkerOptions.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Redis
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Connections
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Connection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhpRedisClusterConnection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhpRedisConnection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PredisClusterConnection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PredisConnection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Connectors
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhpRedisConnector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PredisConnector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Events
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜CommandExecuted.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Limiters
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConcurrencyLimiter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConcurrencyLimiterBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DurationLimiter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜DurationLimiterBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RedisManager.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RedisServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Routing
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Console
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜controller.api.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜controller.invokable.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜controller.model.api.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜controller.model.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜controller.nested.api.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜controller.nested.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜controller.plain.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜controller.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜middleware.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ControllerMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜MiddlewareMakeCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Contracts
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ControllerDispatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Events
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RouteMatched.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Exceptions
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidSignatureException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UrlGenerationException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Matching
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HostValidator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MethodValidator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SchemeValidator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UriValidator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ValidatorInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Middleware
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SubstituteBindings.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ThrottleRequests.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ThrottleRequestsWithRedis.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ValidateSignature.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Controller.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ControllerDispatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ControllerMiddlewareOptions.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ImplicitRouteBinding.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MiddlewareNameResolver.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PendingResourceRegistration.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Pipeline.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RedirectController.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Redirector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResourceRegistrar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResponseFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Route.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RouteAction.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RouteBinding.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RouteCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RouteCompiler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RouteDependencyResolverTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RouteFileRegistrar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RouteGroup.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RouteParameterBinder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Router.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RouteRegistrar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RouteSignatureParameters.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RouteUrlGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RoutingServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SortedMiddleware.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UrlGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ViewController.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Session
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Console
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜database.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SessionTableCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Middleware
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AuthenticateSession.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜StartSession.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CacheBasedSessionHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CookieSessionHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DatabaseSessionHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EncryptedStore.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExistenceAwareInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileSessionHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullSessionHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SessionManager.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SessionServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Store.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TokenMismatchException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Support
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Facades
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜App.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Artisan.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Auth.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Blade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Broadcast.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Bus.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Cache.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Config.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Cookie.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Crypt.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Date.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DB.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Event.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Facade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜File.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Gate.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Hash.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Lang.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Log.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Mail.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Notification.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Password.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Queue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Redirect.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Redis.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Request.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Response.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Route.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Schema.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Session.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Storage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜URL.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Validator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜View.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Testing
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂Fakes
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BusFake.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EventFake.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MailFake.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NotificationFake.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PendingMailFake.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜QueueFake.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Traits
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CapsuleManagerTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EnumeratesValues.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ForwardsCalls.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Localizable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Macroable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Tappable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AggregateServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Arr.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Carbon.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Collection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Composer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConfigurationUrlParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DateFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Enumerable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Env.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Fluent.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜helpers.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HigherOrderCollectionProxy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HigherOrderTapProxy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HtmlString.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InteractsWithTime.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LazyCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Manager.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MessageBag.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NamespacedItemResolver.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Optional.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Pluralizer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ProcessUtils.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Reflector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Str.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ViewErrorBag.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Translation
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayLoader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileLoader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MessageSelector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TranslationServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Translator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Validation
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Concerns
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FilterEmailValidation.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FormatsMessages.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReplacesAttributes.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ValidatesAttributes.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Rules
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DatabaseRule.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Dimensions.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exists.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜In.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NotIn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RequiredIf.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Unique.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClosureValidationRule.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DatabasePresenceVerifier.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Factory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PresenceVerifierInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Rule.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnauthorizedException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ValidatesWhenResolvedTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ValidationData.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ValidationException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ValidationRuleParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ValidationServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Validator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂View
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Compilers
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Concerns
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompilesAuthorizations.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompilesComments.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompilesComponents.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompilesConditionals.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompilesEchos.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompilesErrors.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompilesHelpers.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompilesIncludes.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompilesInjections.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompilesJson.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompilesLayouts.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompilesLoops.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompilesRawPhp.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompilesStacks.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜CompilesTranslations.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BladeCompiler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Compiler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜CompilerInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Concerns
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ManagesComponents.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ManagesEvents.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ManagesLayouts.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ManagesLoops.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ManagesStacks.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ManagesTranslations.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Engines
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompilerEngine.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Engine.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EngineResolver.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileEngine.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhpEngine.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Middleware
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ShareErrorsFromSession.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Factory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileViewFinder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜View.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ViewException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ViewFinderInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ViewName.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ViewServiceProvider.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂tinker
 ┃ ┃ ┃ ┃ ┣ 📂config
 ┃ ┃ ┃ ┃ ┃ ┗ 📜tinker.php
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Console
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TinkerCommand.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassAliasAutoloader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TinkerCaster.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TinkerServiceProvider.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┗ 📂ui
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Auth
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂bootstrap-stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂auth
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂passwords
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜email.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜reset.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜login.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜register.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜verify.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂layouts
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜app.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜home.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂controllers
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜HomeController.stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜routes.stub
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Presets
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂bootstrap-stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜app.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bootstrap.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜_variables.scss
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂react-stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜app.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Example.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜webpack.mix.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂vue-stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜app.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExampleComponent.vue
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜webpack.mix.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Bootstrap.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Preset.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜React.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Vue.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AuthCommand.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜UiCommand.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜UiServiceProvider.php
 ┃ ┃ ┃ ┃ ┣ 📜.editorconfig
 ┃ ┃ ┃ ┃ ┣ 📜.gitignore
 ┃ ┃ ┃ ┃ ┣ 📜.styleci.yml
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┣ 📜phpunit.xml.dist
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂league
 ┃ ┃ ┃ ┣ 📂commonmark
 ┃ ┃ ┃ ┃ ┣ 📂bin
 ┃ ┃ ┃ ┃ ┃ ┗ 📜commonmark
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Block
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Element
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractBlock.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractStringContainerBlock.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BlockQuote.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Document.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FencedCode.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Heading.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HtmlBlock.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IndentedCode.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InlineContainerInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ListBlock.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ListData.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ListItem.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Paragraph.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StringContainerInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ThematicBreak.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Parser
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ATXHeadingParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BlockParserInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BlockQuoteParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FencedCodeParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HtmlBlockParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IndentedCodeParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LazyParagraphParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ListParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SetExtHeadingParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ThematicBreakParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂Renderer
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BlockQuoteRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BlockRendererInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DocumentRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FencedCodeRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HeadingRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HtmlBlockRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IndentedCodeRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ListBlockRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ListItemRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ParagraphRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ThematicBreakRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Delimiter
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Processor
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DelimiterProcessorCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DelimiterProcessorCollectionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DelimiterProcessorInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EmphasisDelimiterProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜StaggeredDelimiterProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Delimiter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DelimiterInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜DelimiterStack.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Event
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractEvent.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DocumentParsedEvent.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜DocumentPreParsedEvent.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidOptionException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UnexpectedEncodingException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Extension
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Attributes
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Event
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜AttributesListener.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Node
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Attributes.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜AttributesInline.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Parser
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AttributesBlockParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜AttributesInlineParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Util
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜AttributesHelper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜AttributesExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Autolink
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AutolinkExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EmailAutolinkProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InlineMentionParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UrlAutolinkProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂DisallowedRawHtml
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DisallowedRawHtmlBlockRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DisallowedRawHtmlExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜DisallowedRawHtmlInlineRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂ExternalLink
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExternalLinkExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ExternalLinkProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Footnote
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Event
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AnonymousFootnotesListener.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GatherFootnotesListener.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜NumberFootnotesListener.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Node
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Footnote.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FootnoteBackref.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FootnoteContainer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜FootnoteRef.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Parser
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AnonymousFootnoteRefParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FootnoteParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜FootnoteRefParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Renderer
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FootnoteBackrefRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FootnoteContainerRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FootnoteRefRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜FootnoteRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜FootnoteExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂HeadingPermalink
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Slug
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DefaultSlugGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SlugGeneratorInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HeadingPermalink.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HeadingPermalinkExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HeadingPermalinkProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜HeadingPermalinkRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂InlinesOnly
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ChildRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜InlinesOnlyExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Mention
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Generator
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CallbackGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MentionGeneratorInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜StringTemplateLinkGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Mention.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MentionExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜MentionParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂SmartPunct
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PunctuationParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Quote.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜QuoteParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜QuoteProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜QuoteRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SmartPunctExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Strikethrough
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Strikethrough.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StrikethroughDelimiterProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StrikethroughExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜StrikethroughRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Table
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Table.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableCell.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableCellRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableRow.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableRowRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableSection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TableSectionRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂TableOfContents
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Node
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableOfContents.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TableOfContentsPlaceholder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Normalizer
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AsIsNormalizerStrategy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FlatNormalizerStrategy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NormalizerStrategyInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RelativeNormalizerStrategy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableOfContents.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableOfContentsBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableOfContentsExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableOfContentsGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableOfContentsGeneratorInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TableOfContentsPlaceholderParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TableOfContentsPlaceholderRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂TaskList
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TaskListExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TaskListItemMarker.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TaskListItemMarkerParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TaskListItemMarkerRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CommonMarkCoreExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExtensionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜GithubFlavoredMarkdownExtension.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Inline
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Element
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractInline.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractStringContainer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractWebResource.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Code.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Emphasis.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HtmlInline.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Image.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Link.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Newline.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Strong.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Parser
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AutolinkParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BacktickParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BangParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CloseBracketParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EntityParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EscapableParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HtmlInlineParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InlineParserInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NewlineParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜OpenBracketParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Renderer
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CodeRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EmphasisRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HtmlInlineRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ImageRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InlineRendererInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LinkRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NewlineRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StrongRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TextRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜AdjacentTextMerger.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Input
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MarkdownInput.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜MarkdownInputInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Node
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Node.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NodeWalker.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜NodeWalkerEvent.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Normalizer
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SlugNormalizer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TextNormalizer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TextNormalizerInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Reference
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Reference.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReferenceInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReferenceMap.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReferenceMapInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ReferenceParser.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Util
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Configuration.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConfigurationAwareInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConfigurationInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Html5Entities.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Html5EntityDecoder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LinkParserHelper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PrioritizedList.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RegexHelper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UrlEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Xml.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CommonMarkConverter.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConfigurableEnvironmentInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Context.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ContextInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Converter.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConverterInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Cursor.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DocParser.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DocParserInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ElementRendererInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Environment.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜EnvironmentAwareInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜EnvironmentInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜GithubFlavoredMarkdownConverter.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜HtmlElement.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜HtmlRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InlineParserContext.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InlineParserEngine.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MarkdownConverter.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MarkdownConverterInterface.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜UnmatchedBlockCloser.php
 ┃ ┃ ┃ ┃ ┣ 📜.phpstorm.meta.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG-0.x.md
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂flysystem
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Adapter
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Polyfill
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NotSupportingVisibilityTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StreamedCopyTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StreamedReadingTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StreamedTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜StreamedWritingTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractAdapter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractFtpAdapter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CanOverwriteFiles.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Ftp.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Ftpd.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Local.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullAdapter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SynologyFtp.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Plugin
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractPlugin.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EmptyDir.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ForcedCopy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ForcedRename.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GetWithMetadata.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ListFiles.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ListPaths.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ListWith.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PluggableTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PluginNotFoundException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Util
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ContentListingFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MimeType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜StreamHasher.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AdapterInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Config.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConfigAwareTrait.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConnectionErrorException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConnectionRuntimeException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CorruptedPathDetected.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Directory.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜File.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FileExistsException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FileNotFoundException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Filesystem.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FilesystemException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FilesystemInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FilesystemNotFoundException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Handler.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidRootException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MountManager.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜NotSupportedException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PluginInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ReadInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RootViolationException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SafeStorage.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜UnreadableFileException.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Util.php
 ┃ ┃ ┃ ┃ ┣ 📜CODE_OF_CONDUCT.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜deprecations.md
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜SECURITY.md
 ┃ ┃ ┃ ┗ 📂mime-type-detection
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📜EmptyExtensionToMimeTypeMap.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ExtensionMimeTypeDetector.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ExtensionToMimeTypeMap.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FinfoMimeTypeDetector.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜GeneratedExtensionToMimeTypeMap.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MimeTypeDetector.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜OverridingExtensionToMimeTypeMap.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┗ 📜LICENSE
 ┃ ┃ ┣ 📂mockery
 ┃ ┃ ┃ ┗ 📂mockery
 ┃ ┃ ┃ ┃ ┣ 📂docs
 ┃ ┃ ┃ ┃ ┃ ┣ 📂cookbook
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜big_parent_class.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜class_constants.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜default_expectations.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜detecting_mock_objects.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜index.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜map.rst.inc
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mockery_on.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mocking_class_within_class.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mocking_hard_dependencies.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜not_calling_the_constructor.rst
 ┃ ┃ ┃ ┃ ┃ ┣ 📂getting_started
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜index.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜installation.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜map.rst.inc
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜quick_reference.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜simple_example.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜upgrading.rst
 ┃ ┃ ┃ ┃ ┃ ┣ 📂mockery
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜configuration.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜exceptions.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gotchas.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜index.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜map.rst.inc
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜reserved_method_names.rst
 ┃ ┃ ┃ ┃ ┃ ┣ 📂reference
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜alternative_should_receive_syntax.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜argument_validation.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜creating_test_doubles.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜demeter_chains.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜expectations.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜final_methods_classes.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜index.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜instance_mocking.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜magic_methods.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜map.rst.inc
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜partial_mocks.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pass_by_reference_behaviours.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜phpunit_integration.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜protected_methods.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜public_properties.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜public_static_properties.rst
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜spies.rst
 ┃ ┃ ┃ ┃ ┃ ┣ 📜conf.py
 ┃ ┃ ┃ ┃ ┃ ┣ 📜index.rst
 ┃ ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┃ ┣ 📂library
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Mockery
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Adapter
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂Phpunit
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MockeryPHPUnitIntegration.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MockeryPHPUnitIntegrationAssertPostConditions.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MockeryTestCase.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MockeryTestCaseSetUp.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestListener.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TestListenerTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂CountValidator
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AtLeast.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AtMost.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CountValidatorAbstract.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exact.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BadMethodCallException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidArgumentException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidCountException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidOrderException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoMatchingExpectationException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RuntimeException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Generator
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂StringManipulation
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂Pass
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AvoidMethodClashPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CallTypeHintPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassNamePass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConstantsPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InstanceMockPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InterfacePass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MagicMethodTypeHintsPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MethodDefinitionPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Pass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RemoveBuiltinMethodsThatAreFinalPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RemoveDestructorPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RemoveUnserializeForInternalSerializableClassesPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TraitPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CachingGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DefinedTargetClass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Generator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Method.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MockConfiguration.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MockConfigurationBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MockDefinition.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MockNameBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Parameter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StringManipulationGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TargetClassInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UndefinedTargetClass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Loader
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EvalLoader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Loader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RequireLoader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Matcher
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AndAnyOtherArgs.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Any.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AnyArgs.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AnyOf.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArgumentListMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Closure.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Contains.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Ducktype.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HasKey.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HasValue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MatcherAbstract.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MultiArgumentClosure.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MustBe.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoArgs.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Not.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NotAnyOf.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Pattern.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Subset.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Type.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClosureWrapper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompositeExpectation.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Configuration.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Container.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Expectation.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExpectationDirector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExpectationInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExpectsHigherOrderMessage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HigherOrderMessage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Instantiator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LegacyMockInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MethodCall.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Mock.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MockInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜QuickDefinitionsConfiguration.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReceivedMethodCalls.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Reflector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Undefined.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜VerificationDirector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜VerificationExpectation.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜helpers.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Mockery.php
 ┃ ┃ ┃ ┃ ┣ 📜.phpstorm.meta.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜CONTRIBUTING.md
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂monolog
 ┃ ┃ ┃ ┗ 📂monolog
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┗ 📂Monolog
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Attribute
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜AsMonologProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Formatter
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ChromePHPFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ElasticaFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ElasticsearchFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FlowdockFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FluentdFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FormatterInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GelfMessageFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HtmlFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜JsonFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LineFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogglyFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogmaticFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogstashFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MongoDBFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NormalizerFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ScalarFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜WildfireFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Handler
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Curl
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Util.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂FingersCrossed
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ActivationStrategyInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ChannelLevelActivationStrategy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ErrorLevelActivationStrategy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Slack
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SlackRecord.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂SyslogUdp
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UdpSocket.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractProcessingHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractSyslogHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AmqpHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BrowserConsoleHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BufferHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ChromePHPHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CouchDBHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CubeHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DeduplicationHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DoctrineCouchDBHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DynamoDbHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ElasticaHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ElasticsearchHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ErrorLogHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FallbackGroupHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FilterHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FingersCrossedHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FirePHPHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FleepHookHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FlowdockHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FormattableHandlerInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FormattableHandlerTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GelfHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GroupHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Handler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HandlerInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HandlerWrapper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IFTTTHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InsightOpsHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogEntriesHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogglyHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogmaticHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MailHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MandrillHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MissingExtensionException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MongoDBHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NativeMailerHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NewRelicHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoopHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜OverflowHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PHPConsoleHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ProcessableHandlerInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ProcessableHandlerTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ProcessHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PsrHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PushoverHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RedisHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RedisPubSubHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RollbarHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RotatingFileHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SamplingHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SendGridHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SlackHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SlackWebhookHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SocketHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SqsHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StreamHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SwiftMailerHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SyslogHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SyslogUdpHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TelegramBotHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜WebRequestRecognizerTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜WhatFailureGroupHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ZendMonitorHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Processor
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GitProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HostnameProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IntrospectionProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MemoryPeakUsageProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MemoryProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MemoryUsageProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MercurialProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ProcessIdProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ProcessorInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PsrLogMessageProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TagProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UidProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜WebProcessor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Test
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TestCase.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DateTimeImmutable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ErrorHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Logger.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogRecord.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Registry.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResettableInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SignalHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Utils.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜README.md
 ┃ ┃ ┃ ┃ ┗ 📜UPGRADE.md
 ┃ ┃ ┣ 📂myclabs
 ┃ ┃ ┃ ┗ 📂deep-copy
 ┃ ┃ ┃ ┃ ┣ 📂.github
 ┃ ┃ ┃ ┃ ┃ ┣ 📂workflows
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ci.yaml
 ┃ ┃ ┃ ┃ ┃ ┗ 📜FUNDING.yml
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┗ 📂DeepCopy
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CloneException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PropertyException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Filter
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Doctrine
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DoctrineCollectionFilter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DoctrineEmptyCollectionFilter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜DoctrineProxyFilter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Filter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜KeepFilter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReplaceFilter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SetNullFilter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Matcher
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Doctrine
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜DoctrineProxyMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Matcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PropertyMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PropertyNameMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PropertyTypeMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Reflection
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ReflectionHelper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂TypeFilter
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Date
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜DateIntervalFilter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Spl
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayObjectFilter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SplDoublyLinkedList.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SplDoublyLinkedListFilter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReplaceFilter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ShallowCopyFilter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TypeFilter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂TypeMatcher
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TypeMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DeepCopy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜deep_copy.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂nesbot
 ┃ ┃ ┃ ┗ 📂carbon
 ┃ ┃ ┃ ┃ ┣ 📂bin
 ┃ ┃ ┃ ┃ ┃ ┣ 📜carbon
 ┃ ┃ ┃ ┃ ┃ ┗ 📜carbon.bat
 ┃ ┃ ┃ ┃ ┣ 📂lazy
 ┃ ┃ ┃ ┃ ┃ ┗ 📂Carbon
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂PHPStan
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MacroStrongType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜MacroWeakType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TranslatorStrongType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TranslatorWeakType.php
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┗ 📂Carbon
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Cli
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Invoker.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Doctrine
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CarbonDoctrineType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CarbonImmutableType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CarbonType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CarbonTypeConverter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DateTimeDefaultPrecision.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DateTimeImmutableType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜DateTimeType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Exceptions
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BadComparisonUnitException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BadFluentConstructorException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BadFluentSetterException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BadMethodCallException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ImmutableException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidArgumentException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidCastException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidDateException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidFormatException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidIntervalException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidPeriodDateException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidPeriodParameterException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidTimeZoneException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidTypeException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NotACarbonClassException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NotAPeriodException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NotLocaleAwareException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜OutOfRangeException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ParseErrorException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RuntimeException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnitException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnitNotConfiguredException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnknownGetterException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnknownMethodException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnknownSetterException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnknownUnitException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UnreachableException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Lang
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜aa.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜aa_DJ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜aa_ER.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜aa_ER@saaho.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜aa_ET.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜af.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜af_NA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜af_ZA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜agq.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜agr.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜agr_PE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ak.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ak_GH.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜am.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜am_ET.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜an.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜anp.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜anp_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜an_ES.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_AE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_BH.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_DJ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_DZ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_EG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_EH.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_ER.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_IL.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_IQ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_JO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_KM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_KW.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_LB.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_LY.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_MA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_MR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_OM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_PS.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_QA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_SA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_SD.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_Shakl.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_SO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_SS.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_SY.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_TD.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_TN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ar_YE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜as.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜asa.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ast.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ast_ES.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜as_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ayc.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ayc_PE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜az.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜az_AZ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜az_Cyrl.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜az_IR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜az_Latn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bas.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜be.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bem.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bem_ZM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ber_DZ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ber_MA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bez.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜be_BY.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜be_BY@latin.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bg.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bg_BG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bhb.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bhb_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bho.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bho_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bi.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bi_VU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bm.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bn_BD.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bn_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bo.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bo_CN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bo_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜br.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜brx.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜brx_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜br_FR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bs.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bs_BA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bs_Cyrl.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bs_Latn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜byn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜byn_ER.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ca.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ca_AD.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ca_ES.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ca_ES_Valencia.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ca_FR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ca_IT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ccp.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ccp_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ce.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ce_RU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜cgg.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜chr.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜chr_US.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜cmn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜cmn_TW.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜crh.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜crh_UA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜cs.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜csb.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜csb_PL.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜cs_CZ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜cu.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜cv.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜cv_RU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜cy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜cy_GB.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜da.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dav.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜da_DK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜da_GL.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜de.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜de_AT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜de_BE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜de_CH.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜de_DE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜de_IT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜de_LI.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜de_LU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dje.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜doi.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜doi_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dsb.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dsb_DE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dua.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dv.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dv_MV.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dyo.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dz.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dz_BT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ebu.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ee.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ee_TG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜el.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜el_CY.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜el_GR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_001.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_150.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_AG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_AI.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_AS.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_AT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_AU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_BB.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_BE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_BI.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_BM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_BS.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_BW.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_BZ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_CA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_CC.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_CH.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_CK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_CM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_CX.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_CY.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_DE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_DG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_DK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_DM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_ER.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_FI.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_FJ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_FK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_FM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_GB.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_GD.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_GG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_GH.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_GI.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_GM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_GU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_GY.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_HK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_IE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_IL.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_IM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_IO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_ISO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_JE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_JM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_KE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_KI.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_KN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_KY.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_LC.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_LR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_LS.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_MG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_MH.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_MO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_MP.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_MS.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_MT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_MU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_MW.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_MY.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_NA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_NF.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_NG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_NL.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_NR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_NU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_NZ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_PG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_PH.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_PK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_PN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_PR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_PW.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_RW.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_SB.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_SC.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_SD.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_SE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_SG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_SH.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_SI.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_SL.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_SS.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_SX.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_SZ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_TC.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_TK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_TO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_TT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_TV.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_TZ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_UG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_UM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_US.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_US_Posix.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_VC.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_VG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_VI.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_VU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_WS.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_ZA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_ZM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜en_ZW.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜eo.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_419.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_AR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_BO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_BR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_BZ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_CL.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_CO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_CR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_CU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_DO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_EA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_EC.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_ES.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_GQ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_GT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_HN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_IC.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_MX.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_NI.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_PA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_PE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_PH.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_PR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_PY.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_SV.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_US.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_UY.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜es_VE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜et.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜et_EE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜eu.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜eu_ES.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ewo.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fa.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fa_AF.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fa_IR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ff.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ff_CM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ff_GN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ff_MR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ff_SN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fi.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fil.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fil_PH.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fi_FI.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fo.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fo_DK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fo_FO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_BE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_BF.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_BI.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_BJ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_BL.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_CA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_CD.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_CF.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_CG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_CH.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_CI.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_CM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_DJ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_DZ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_FR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_GA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_GF.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_GN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_GP.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_GQ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_HT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_KM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_LU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_MA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_MC.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_MF.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_MG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_ML.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_MQ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_MR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_MU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_NC.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_NE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_PF.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_PM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_RE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_RW.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_SC.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_SN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_SY.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_TD.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_TG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_TN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_VU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_WF.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fr_YT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fur.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fur_IT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fy_DE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fy_NL.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ga.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ga_IE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gd.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gd_GB.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gez.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gez_ER.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gez_ET.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gl.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gl_ES.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gom.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gom_Latn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gsw.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gsw_CH.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gsw_FR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gsw_LI.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gu.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜guz.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gu_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gv.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gv_GB.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ha.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hak.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hak_TW.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜haw.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ha_GH.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ha_NE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ha_NG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜he.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜he_IL.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hi.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hif.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hif_FJ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hi_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hne.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hne_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hr.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hr_BA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hr_HR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hsb.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hsb_DE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ht.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ht_HT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hu.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hu_HU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hy_AM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜i18n.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ia.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ia_FR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜id.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜id_ID.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ig.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ig_NG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ii.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ik.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ik_CA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜in.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜is.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜is_IS.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜it.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜it_CH.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜it_IT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜it_SM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜it_VA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜iu.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜iu_CA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜iw.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ja.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ja_JP.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜jgo.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜jmc.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜jv.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ka.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜kab.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜kab_DZ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜kam.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ka_GE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜kde.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜kea.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜khq.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ki.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜kk.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜kkj.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜kk_KZ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜kl.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜kln.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜kl_GL.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜km.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜km_KH.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜kn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜kn_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ko.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜kok.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜kok_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ko_KP.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ko_KR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ks.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ksb.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ksf.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ksh.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ks_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ks_IN@devanagari.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ku.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ku_TR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜kw.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜kw_GB.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ky.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ky_KG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lag.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lb.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lb_LU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lg.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lg_UG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜li.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lij.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lij_IT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜li_NL.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lkt.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ln.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ln_AO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ln_CD.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ln_CF.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ln_CG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lo.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lo_LA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lrc.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lrc_IQ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lt.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lt_LT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lu.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜luo.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜luy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lv.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lv_LV.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lzh.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lzh_TW.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mag.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mag_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mai.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mai_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mas.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mas_TZ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mfe.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mfe_MU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mg.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mgh.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mgo.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mg_MG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mhr.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mhr_RU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mi.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜miq.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜miq_NI.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mi_NZ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mjw.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mjw_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mk.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mk_MK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ml.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ml_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mni.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mni_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mn_MN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mo.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mr.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mr_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ms.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ms_BN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ms_MY.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ms_SG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mt.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mt_MT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mua.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜my.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜my_MM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mzn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nan.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nan_TW.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nan_TW@latin.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜naq.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nb.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nb_NO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nb_SJ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nd.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nds.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nds_DE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nds_NL.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ne.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ne_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ne_NP.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nhn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nhn_MX.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜niu.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜niu_NU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nl.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nl_AW.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nl_BE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nl_BQ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nl_CW.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nl_NL.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nl_SR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nl_SX.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nmg.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nnh.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nn_NO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜no.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nr.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nr_ZA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nso.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nso_ZA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nus.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nyn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜oc.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜oc_FR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜om.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜om_ET.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜om_KE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜or.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜or_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜os.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜os_RU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pa.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pap.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pap_AW.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pap_CW.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pa_Arab.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pa_Guru.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pa_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pa_PK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pl.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pl_PL.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜prg.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ps.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ps_AF.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pt.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pt_AO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pt_BR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pt_CH.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pt_CV.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pt_GQ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pt_GW.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pt_LU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pt_MO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pt_MZ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pt_PT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pt_ST.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pt_TL.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜qu.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜quz.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜quz_PE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜qu_BO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜qu_EC.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜raj.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜raj_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜rm.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜rn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ro.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜rof.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ro_MD.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ro_RO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ru.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ru_BY.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ru_KG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ru_KZ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ru_MD.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ru_RU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ru_UA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜rw.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜rwk.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜rw_RW.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sa.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sah.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sah_RU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜saq.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sat.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sat_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sa_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sbp.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sc.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sc_IT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sd.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sd_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sd_IN@devanagari.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜se.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜seh.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ses.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜se_FI.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜se_NO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜se_SE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sg.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sgs.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sgs_LT.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sh.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜shi.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜shi_Latn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜shi_Tfng.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜shn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜shn_MM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜shs.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜shs_CA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜si.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sid.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sid_ET.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜si_LK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sk.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sk_SK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sl.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sl_SI.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sm.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜smn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sm_WS.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜so.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜so_DJ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜so_ET.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜so_KE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜so_SO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sq.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sq_AL.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sq_MK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sq_XK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sr.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sr_Cyrl.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sr_Cyrl_BA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sr_Cyrl_ME.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sr_Cyrl_XK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sr_Latn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sr_Latn_BA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sr_Latn_ME.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sr_Latn_XK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sr_ME.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sr_RS.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sr_RS@latin.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ss.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ss_ZA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜st.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜st_ZA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sv.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sv_AX.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sv_FI.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sv_SE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sw.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sw_CD.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sw_KE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sw_TZ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sw_UG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜szl.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜szl_PL.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ta.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ta_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ta_LK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ta_MY.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ta_SG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tcy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tcy_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜te.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜teo.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜teo_KE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜te_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tg.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tg_TJ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜th.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜the.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜the_NP.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜th_TH.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ti.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tig.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tig_ER.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ti_ER.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ti_ET.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tk.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tk_TM.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tl.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tlh.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tl_PH.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tn_ZA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜to.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜to_TO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tpi.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tpi_PG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tr.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tr_CY.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tr_TR.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ts.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ts_ZA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tt.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tt_RU.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tt_RU@iqtelif.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜twq.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tzl.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tzm.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tzm_Latn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ug.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ug_CN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜uk.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜uk_UA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜unm.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜unm_US.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ur.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ur_IN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ur_PK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜uz.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜uz_Arab.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜uz_Cyrl.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜uz_Latn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜uz_UZ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜uz_UZ@cyrillic.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜vai.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜vai_Latn.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜vai_Vaii.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ve.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ve_ZA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜vi.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜vi_VN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜vo.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜vun.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜wa.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜wae.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜wae_CH.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜wal.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜wal_ET.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜wa_BE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜wo.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜wo_SN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜xh.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜xh_ZA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜xog.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜yav.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜yi.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜yi_US.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜yo.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜yo_BJ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜yo_NG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜yue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜yue_Hans.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜yue_Hant.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜yue_HK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜yuw.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜yuw_PG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜zgh.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜zh.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜zh_CN.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜zh_Hans.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜zh_Hans_HK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜zh_Hans_MO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜zh_Hans_SG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜zh_Hant.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜zh_Hant_HK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜zh_Hant_MO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜zh_Hant_TW.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜zh_HK.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜zh_MO.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜zh_SG.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜zh_TW.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜zh_YUE.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜zu.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜zu_ZA.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Laravel
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂List
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜languages.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜regions.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂PHPStan
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractMacro.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Macro.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MacroExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜MacroScanner.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Traits
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Boundaries.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Cast.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Comparison.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Converter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Creator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Date.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DeprecatedProperties.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Difference.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IntervalRounding.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IntervalStep.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Localization.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Macro.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Mixin.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Modifiers.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Mutability.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ObjectInitialisation.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Options.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Rounding.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Serialization.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Test.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Timestamp.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Units.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Week.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractTranslator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Carbon.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CarbonConverterInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CarbonImmutable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CarbonInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CarbonInterval.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CarbonPeriod.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CarbonTimeZone.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Factory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FactoryImmutable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Language.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Translator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TranslatorImmutable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TranslatorStrongTypeInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜extension.neon
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜readme.md
 ┃ ┃ ┣ 📂nikic
 ┃ ┃ ┃ ┗ 📂php-parser
 ┃ ┃ ┃ ┃ ┣ 📂bin
 ┃ ┃ ┃ ┃ ┃ ┗ 📜php-parse
 ┃ ┃ ┃ ┃ ┣ 📂grammar
 ┃ ┃ ┃ ┃ ┃ ┣ 📜parser.template
 ┃ ┃ ┃ ┃ ┃ ┣ 📜php5.y
 ┃ ┃ ┃ ┃ ┃ ┣ 📜php7.y
 ┃ ┃ ┃ ┃ ┃ ┣ 📜phpyLang.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜README.md
 ┃ ┃ ┃ ┃ ┃ ┣ 📜rebuildParsers.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜tokens.template
 ┃ ┃ ┃ ┃ ┃ ┗ 📜tokens.y
 ┃ ┃ ┃ ┃ ┣ 📂lib
 ┃ ┃ ┃ ┃ ┃ ┗ 📂PhpParser
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Builder
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassConst.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Class_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Declaration.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EnumCase.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Enum_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FunctionLike.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Function_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Interface_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Method.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Namespace_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Param.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Property.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TraitUse.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TraitUseAdaptation.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Trait_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Use_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Comment
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Doc.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂ErrorHandler
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Collecting.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Throwing.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Internal
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DiffElem.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Differ.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PrintableNewAnonClassNode.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TokenStream.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Lexer
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂TokenEmulator
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AttributeEmulator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CoaleseEqualTokenEmulator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EnumTokenEmulator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExplicitOctalEmulator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FlexibleDocStringEmulator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FnTokenEmulator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜KeywordEmulator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MatchTokenEmulator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullsafeTokenEmulator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NumericLiteralSeparatorEmulator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReadonlyTokenEmulator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReverseEmulator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TokenEmulator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Emulative.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Node
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Expr
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂AssignOp
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BitwiseAnd.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BitwiseOr.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BitwiseXor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Coalesce.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Concat.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Div.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Minus.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Mod.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Mul.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Plus.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Pow.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ShiftLeft.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ShiftRight.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂BinaryOp
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BitwiseAnd.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BitwiseOr.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BitwiseXor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BooleanAnd.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BooleanOr.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Coalesce.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Concat.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Div.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Equal.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Greater.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GreaterOrEqual.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Identical.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogicalAnd.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogicalOr.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogicalXor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Minus.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Mod.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Mul.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NotEqual.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NotIdentical.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Plus.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Pow.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ShiftLeft.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ShiftRight.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Smaller.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SmallerOrEqual.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Spaceship.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Cast
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Array_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Bool_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Double.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Int_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Object_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜String_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Unset_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayDimFetch.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayItem.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Array_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrowFunction.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Assign.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AssignOp.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AssignRef.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BinaryOp.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BitwiseNot.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BooleanNot.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CallLike.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Cast.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassConstFetch.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Clone_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Closure.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClosureUse.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConstFetch.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Empty_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Error.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ErrorSuppress.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Eval_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exit_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FuncCall.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Include_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Instanceof_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Isset_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜List_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Match_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MethodCall.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜New_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullsafeMethodCall.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullsafePropertyFetch.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PostDec.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PostInc.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PreDec.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PreInc.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Print_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PropertyFetch.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ShellExec.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StaticCall.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StaticPropertyFetch.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Ternary.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Throw_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnaryMinus.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnaryPlus.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Variable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜YieldFrom.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Yield_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Name
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FullyQualified.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Relative.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Scalar
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂MagicConst
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Class_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Dir.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜File.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Function_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Line.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Method.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Namespace_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Trait_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Encapsed.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EncapsedStringPart.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LNumber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MagicConst.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜String_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Stmt
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂TraitUseAdaptation
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Alias.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Precedence.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Break_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Case_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Catch_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassConst.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassLike.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassMethod.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Class_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Const_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Continue_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DeclareDeclare.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Declare_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Do_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Echo_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ElseIf_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Else_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EnumCase.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Enum_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Expression.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Finally_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Foreach_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜For_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Function_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Global_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Goto_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GroupUse.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HaltCompiler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜If_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InlineHTML.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Interface_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Label.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Namespace_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Nop.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Property.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PropertyProperty.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Return_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StaticVar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Static_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Switch_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Throw_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TraitUse.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TraitUseAdaptation.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Trait_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TryCatch.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Unset_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UseUse.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Use_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜While_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Arg.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Attribute.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AttributeGroup.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ComplexType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Const_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Expr.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FunctionLike.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Identifier.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IntersectionType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MatchArm.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Name.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullableType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Param.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Scalar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Stmt.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnionType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜VariadicPlaceholder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜VarLikeIdentifier.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂NodeVisitor
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CloningVisitor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FindingVisitor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FirstFindingVisitor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NameResolver.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NodeConnectingVisitor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ParentConnectingVisitor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Parser
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Multiple.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Php5.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Php7.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Tokens.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂PrettyPrinter
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Standard.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Builder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BuilderFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BuilderHelpers.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Comment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConstExprEvaluationException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConstExprEvaluator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Error.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ErrorHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜JsonDecoder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Lexer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NameContext.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Node.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NodeAbstract.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NodeDumper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NodeFinder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NodeTraverser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NodeTraverserInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NodeVisitor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NodeVisitorAbstract.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Parser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ParserAbstract.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ParserFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PrettyPrinterAbstract.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂nunomaduro
 ┃ ┃ ┃ ┗ 📂collision
 ┃ ┃ ┃ ┃ ┣ 📂.github
 ┃ ┃ ┃ ┃ ┃ ┗ 📂workflows
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜tests.yml
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Adapters
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Laravel
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CollisionServiceProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExceptionHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Inspector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂Phpunit
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Listener.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Contracts
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Adapters
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂Phpunit
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Listener.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArgumentFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Handler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Highlighter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Provider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Writer.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ArgumentFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Handler.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Highlighter.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Provider.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Writer.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┣ 📜phpunit.xml.dist
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂opis
 ┃ ┃ ┃ ┗ 📂closure
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Analyzer.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ClosureContext.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ClosureScope.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ClosureStream.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ISecurityProvider.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ReflectionClosure.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SecurityException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SecurityProvider.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SelfReference.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜SerializableClosure.php
 ┃ ┃ ┃ ┃ ┣ 📜autoload.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜functions.php
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜NOTICE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂paragonie
 ┃ ┃ ┃ ┗ 📂random_compat
 ┃ ┃ ┃ ┃ ┣ 📂dist
 ┃ ┃ ┃ ┃ ┃ ┣ 📜random_compat.phar.pubkey
 ┃ ┃ ┃ ┃ ┃ ┗ 📜random_compat.phar.pubkey.asc
 ┃ ┃ ┃ ┃ ┣ 📂lib
 ┃ ┃ ┃ ┃ ┃ ┗ 📜random.php
 ┃ ┃ ┃ ┃ ┣ 📂other
 ┃ ┃ ┃ ┃ ┃ ┗ 📜build_phar.php
 ┃ ┃ ┃ ┃ ┣ 📜build-phar.sh
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜psalm-autoload.php
 ┃ ┃ ┃ ┃ ┗ 📜psalm.xml
 ┃ ┃ ┣ 📂phar-io
 ┃ ┃ ┃ ┣ 📂manifest
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂exceptions
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ElementCollectionException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidApplicationNameException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidEmailException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidUrlException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ManifestDocumentException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ManifestDocumentLoadingException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ManifestDocumentMapperException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ManifestElementException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ManifestLoaderException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂values
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Application.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ApplicationName.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Author.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AuthorCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AuthorCollectionIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BundledComponent.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BundledComponentCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BundledComponentCollectionIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CopyrightInformation.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Email.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Extension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Library.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜License.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Manifest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhpExtensionRequirement.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhpVersionRequirement.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Requirement.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RequirementCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RequirementCollectionIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Type.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Url.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂xml
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AuthorElement.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AuthorElementCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BundlesElement.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ComponentElement.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ComponentElementCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ContainsElement.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CopyrightElement.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ElementCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExtElement.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExtElementCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExtensionElement.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LicenseElement.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ManifestDocument.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ManifestElement.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhpElement.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RequiresElement.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ManifestDocumentMapper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ManifestLoader.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜ManifestSerializer.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜composer.lock
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┗ 📂version
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂constraints
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractVersionConstraint.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AndVersionConstraintGroup.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AnyVersionConstraint.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExactVersionConstraint.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GreaterThanOrEqualToVersionConstraint.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜OrVersionConstraintGroup.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SpecificMajorAndMinorVersionConstraint.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SpecificMajorVersionConstraint.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜VersionConstraint.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂exceptions
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidPreReleaseSuffixException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidVersionException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoBuildMetaDataException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoPreReleaseSuffixException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UnsupportedVersionConstraintException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜BuildMetaData.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PreReleaseSuffix.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Version.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜VersionConstraintParser.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜VersionConstraintValue.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜VersionNumber.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂phenx
 ┃ ┃ ┃ ┣ 📂php-font-lib
 ┃ ┃ ┃ ┃ ┣ 📂.github
 ┃ ┃ ┃ ┃ ┃ ┗ 📂workflows
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜phpunit.yml
 ┃ ┃ ┃ ┃ ┣ 📂maps
 ┃ ┃ ┃ ┃ ┃ ┣ 📜adobe-standard-encoding.map
 ┃ ┃ ┃ ┃ ┃ ┣ 📜cp1250.map
 ┃ ┃ ┃ ┃ ┃ ┣ 📜cp1251.map
 ┃ ┃ ┃ ┃ ┃ ┣ 📜cp1252.map
 ┃ ┃ ┃ ┃ ┃ ┣ 📜cp1253.map
 ┃ ┃ ┃ ┃ ┃ ┣ 📜cp1254.map
 ┃ ┃ ┃ ┃ ┃ ┣ 📜cp1255.map
 ┃ ┃ ┃ ┃ ┃ ┣ 📜cp1257.map
 ┃ ┃ ┃ ┃ ┃ ┣ 📜cp1258.map
 ┃ ┃ ┃ ┃ ┃ ┣ 📜cp874.map
 ┃ ┃ ┃ ┃ ┃ ┣ 📜iso-8859-1.map
 ┃ ┃ ┃ ┃ ┃ ┣ 📜iso-8859-11.map
 ┃ ┃ ┃ ┃ ┃ ┣ 📜iso-8859-15.map
 ┃ ┃ ┃ ┃ ┃ ┣ 📜iso-8859-16.map
 ┃ ┃ ┃ ┃ ┃ ┣ 📜iso-8859-2.map
 ┃ ┃ ┃ ┃ ┃ ┣ 📜iso-8859-4.map
 ┃ ┃ ┃ ┃ ┃ ┣ 📜iso-8859-5.map
 ┃ ┃ ┃ ┃ ┃ ┣ 📜iso-8859-7.map
 ┃ ┃ ┃ ┃ ┃ ┣ 📜iso-8859-9.map
 ┃ ┃ ┃ ┃ ┃ ┣ 📜koi8-r.map
 ┃ ┃ ┃ ┃ ┃ ┗ 📜koi8-u.map
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┗ 📂FontLib
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂EOT
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜File.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Header.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜FontNotFoundException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Glyph
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Outline.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜OutlineComponent.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜OutlineComposite.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜OutlineSimple.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂OpenType
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜File.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TableDirectoryEntry.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Table
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Type
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜cmap.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜glyf.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜head.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hhea.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hmtx.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜kern.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜loca.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜maxp.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜name.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nameRecord.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜os2.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜post.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DirectoryEntry.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Table.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂TrueType
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Collection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜File.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Header.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TableDirectoryEntry.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂WOFF
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜File.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Header.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TableDirectoryEntry.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AdobeFontMetrics.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Autoloader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BinaryStream.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EncodingMap.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Font.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Header.php
 ┃ ┃ ┃ ┃ ┣ 📜.htaccess
 ┃ ┃ ┃ ┃ ┣ 📜bower.json
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜index.php
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┗ 📂php-svg-lib
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Svg
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Gradient
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Stop.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Surface
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CPdf.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SurfaceCpdf.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SurfaceInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SurfacePDFLib.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Tag
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractTag.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Anchor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Circle.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClipPath.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Ellipse.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Group.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Image.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Line.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LinearGradient.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Path.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Polygon.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Polyline.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RadialGradient.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Rect.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Shape.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Stop.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StyleTag.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UseTag.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DefaultStyle.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Document.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Style.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜autoload.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂php-parallel-lint
 ┃ ┃ ┃ ┣ 📂php-console-color
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConsoleColor.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜InvalidStyleException.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜example.php
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┗ 📂php-console-highlighter
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Highlighter.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂phpdocumentor
 ┃ ┃ ┃ ┣ 📂reflection-common
 ┃ ┃ ┃ ┃ ┣ 📂.github
 ┃ ┃ ┃ ┃ ┃ ┣ 📂workflows
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜push.yml
 ┃ ┃ ┃ ┃ ┃ ┗ 📜dependabot.yml
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Element.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜File.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Fqsen.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Location.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Project.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜ProjectFactory.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂reflection-docblock
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂DocBlock
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Tags
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Factory
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜StaticMethod.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Formatter
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AlignFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PassthroughFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Reference
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Fqsen.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Reference.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Url.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Author.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BaseTag.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Covers.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Deprecated.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Example.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Formatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Generic.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidTag.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Link.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Method.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Param.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Property.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PropertyRead.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PropertyWrite.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Return_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜See.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Since.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Source.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TagWithType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Throws.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Uses.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Var_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Version.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Description.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DescriptionFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExampleFinder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Serializer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StandardTagFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Tag.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TagFactory.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PcreException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DocBlock.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DocBlockFactory.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DocBlockFactoryInterface.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Utils.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┗ 📂type-resolver
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂PseudoTypes
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CallableString.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜False_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HtmlEscapedString.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IntegerRange.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜List_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LiteralString.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LowercaseString.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NegativeInteger.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NonEmptyLowercaseString.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NonEmptyString.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NumericString.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Numeric_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PositiveInteger.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TraitString.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜True_.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Types
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractList.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AggregatedType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayKey.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Array_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Boolean.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Callable_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassString.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Collection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Compound.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Context.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ContextFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Expression.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Float_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Integer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InterfaceString.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Intersection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Iterable_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Mixed_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Never_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Nullable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Null_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Object_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Parent_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Resource_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Scalar.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Self_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Static_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜String_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜This.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Void_.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FqsenResolver.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PseudoType.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Type.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TypeResolver.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂phpoption
 ┃ ┃ ┃ ┗ 📂phpoption
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┗ 📂PhpOption
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LazyOption.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜None.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Option.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Some.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜Makefile
 ┃ ┃ ┣ 📂phpspec
 ┃ ┃ ┃ ┗ 📂prophecy
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┗ 📂Prophecy
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Argument
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Token
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AnyValuesToken.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AnyValueToken.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ApproximateValueToken.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayCountToken.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayEntryToken.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayEveryEntryToken.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CallbackToken.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExactValueToken.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IdenticalValueToken.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InArrayToken.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogicalAndToken.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogicalNotToken.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NotInArrayToken.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ObjectStateToken.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StringContainsToken.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TokenInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TypeToken.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ArgumentsWildcard.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Call
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Call.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜CallCenter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Comparator
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClosureComparator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Factory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ProphecyComparator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Doubler
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂ClassPatch
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassPatchInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DisableConstructorPatch.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HhvmExceptionPatch.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜KeywordPatch.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MagicCallPatch.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ProphecySubjectPatch.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReflectionClassNewInstancePatch.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SplFileInfoPatch.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ThrowablePatch.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TraversablePatch.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Generator
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Node
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArgumentNode.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArgumentTypeNode.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassNode.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MethodNode.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReturnTypeNode.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TypeNodeAbstract.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassCodeGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassCreator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassMirror.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReflectionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TypeHintReference.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CachedDoubler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DoubleInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Doubler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LazyDouble.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜NameGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Call
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UnexpectedCallException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Doubler
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassCreatorException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassMirrorException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassNotFoundException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DoubleException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DoublerException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InterfaceNotFoundException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MethodNotExtendableException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MethodNotFoundException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ReturnByReferenceException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Prediction
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AggregateException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FailedPredictionException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoCallsException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PredictionException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnexpectedCallsCountException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UnexpectedCallsException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Prophecy
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MethodProphecyException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ObjectProphecyException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ProphecyException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜InvalidArgumentException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂PhpDocumentor
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassAndInterfaceTagRetriever.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassTagRetriever.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LegacyClassTagRetriever.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜MethodTagRetrieverInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Prediction
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CallbackPrediction.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CallPrediction.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CallTimesPrediction.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoCallsPrediction.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PredictionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Promise
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CallbackPromise.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PromiseInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReturnArgumentPromise.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReturnPromise.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ThrowPromise.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Prophecy
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MethodProphecy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ObjectProphecy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ProphecyInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ProphecySubjectInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Revealer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RevealerInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Util
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExportUtil.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜StringUtil.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Argument.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Prophet.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGES.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂phpunit
 ┃ ┃ ┃ ┣ 📂php-code-coverage
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Driver
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Driver.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PcovDriver.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhpdbgDriver.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Selector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Xdebug2Driver.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Xdebug3Driver.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BranchAndPathCoverageNotSupportedException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DeadCodeDetectionNotSupportedException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DirectoryCouldNotBeCreatedException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidArgumentException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoCodeCoverageDriverAvailableException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoCodeCoverageDriverWithPathCoverageSupportAvailableException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ParserException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PathExistsButIsNotDirectoryException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PcovNotAvailableException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhpdbgNotAvailableException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReflectionException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReportAlreadyFinalizedException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StaticAnalysisCacheNotConfiguredException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestIdMissingException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnintentionallyCoveredCodeException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜WriteOperationFailedException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜WrongXdebugVersionException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Xdebug2NotEnabledException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Xdebug3NotEnabledException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜XdebugNotAvailableException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜XmlException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Node
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractNode.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Builder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CrapIndex.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Directory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜File.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Iterator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Report
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Html
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Renderer
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Template
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂css
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bootstrap.min.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜custom.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nv.d3.min.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜octicons.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜style.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂icons
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜file-code.svg
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜file-directory.svg
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bootstrap.min.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜d3.min.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜file.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜jquery.min.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nv.d3.min.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜popper.min.js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜branches.html.dist
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜coverage_bar.html.dist
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜coverage_bar_branch.html.dist
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dashboard.html.dist
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dashboard_branch.html.dist
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜directory.html.dist
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜directory_branch.html.dist
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜directory_item.html.dist
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜directory_item_branch.html.dist
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜file.html.dist
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜file_branch.html.dist
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜file_item.html.dist
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜file_item_branch.html.dist
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜line.html.dist
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lines.html.dist
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜method_item.html.dist
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜method_item_branch.html.dist
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜paths.html.dist
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Dashboard.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Directory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜File.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Facade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Renderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Xml
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BuildInformation.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Coverage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Directory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Facade.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜File.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Method.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Node.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Project.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Report.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Source.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Tests.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Totals.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Unit.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Clover.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Cobertura.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Crap4j.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PHP.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂StaticAnalysis
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CacheWarmer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CachingFileAnalyser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CodeUnitFindingVisitor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExecutableLinesFindingVisitor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileAnalyser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IgnoredLinesFindingVisitor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ParsingFileAnalyser.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Util
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Filesystem.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Percentage.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CodeCoverage.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Filter.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ProcessedCodeCoverageData.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RawCodeCoverageData.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Version.php
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂php-file-iterator
 ┃ ┃ ┃ ┃ ┣ 📂.psalm
 ┃ ┃ ┃ ┃ ┃ ┣ 📜baseline.xml
 ┃ ┃ ┃ ┃ ┃ ┗ 📜config.xml
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Facade.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Factory.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Iterator.php
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂php-invoker
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂exceptions
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ProcessControlExtensionNotLoadedException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TimeoutException.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Invoker.php
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂php-text-template
 ┃ ┃ ┃ ┃ ┣ 📂.psalm
 ┃ ┃ ┃ ┃ ┃ ┣ 📜baseline.xml
 ┃ ┃ ┃ ┃ ┃ ┗ 📜config.xml
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂exceptions
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidArgumentException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RuntimeException.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Template.php
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂php-timer
 ┃ ┃ ┃ ┃ ┣ 📂.psalm
 ┃ ┃ ┃ ┃ ┃ ┣ 📜baseline.xml
 ┃ ┃ ┃ ┃ ┃ ┗ 📜config.xml
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂exceptions
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoActiveTimerException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TimeSinceStartOfRequestNotAvailableException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Duration.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ResourceUsageFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Timer.php
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┗ 📂phpunit
 ┃ ┃ ┃ ┃ ┣ 📂schema
 ┃ ┃ ┃ ┃ ┃ ┣ 📜8.5.xsd
 ┃ ┃ ┃ ┃ ┃ ┗ 📜9.2.xsd
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Framework
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Assert
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Functions.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Constraint
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Boolean
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsFalse.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜IsTrue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Cardinality
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Count.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GreaterThan.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsEmpty.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LessThan.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SameSize.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Equality
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsEqual.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsEqualCanonicalizing.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsEqualIgnoringCase.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜IsEqualWithDelta.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExceptionCode.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExceptionMessage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ExceptionMessageRegularExpression.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Filesystem
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DirectoryExists.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileExists.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsReadable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜IsWritable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Math
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsFinite.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsInfinite.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜IsNan.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Object
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassHasAttribute.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassHasStaticAttribute.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ObjectEquals.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ObjectHasAttribute.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Operator
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BinaryOperator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogicalAnd.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogicalNot.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogicalOr.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogicalXor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Operator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UnaryOperator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂String
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsJson.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RegularExpression.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StringContains.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StringEndsWith.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StringMatchesFormatDescription.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜StringStartsWith.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Traversable
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayHasKey.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TraversableContains.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TraversableContainsEqual.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TraversableContainsIdentical.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TraversableContainsOnly.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Type
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsInstanceOf.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsNull.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜IsType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Callback.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Constraint.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsAnything.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IsIdentical.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜JsonMatches.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜JsonMatchesErrorMessageProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Error
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Deprecated.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Error.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Notice.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Warning.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ActualValueIsNotAnObjectException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AssertionFailedError.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CodeCoverageException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ComparisonMethodDoesNotAcceptParameterTypeException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ComparisonMethodDoesNotDeclareBoolReturnTypeException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ComparisonMethodDoesNotDeclareExactlyOneParameterException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ComparisonMethodDoesNotDeclareParameterTypeException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ComparisonMethodDoesNotExistException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CoveredCodeNotExecutedException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Error.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExpectationFailedException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IncompleteTestError.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidArgumentException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidCoversTargetException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidDataProviderException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MissingCoversAnnotationException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoChildTestSuiteException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜OutputError.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PHPTAssertionFailedError.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RiskyTestError.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SkippedTestError.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SkippedTestSuiteError.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SyntheticError.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SyntheticSkippedError.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnintentionallyCoveredCodeError.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Warning.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂MockObject
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Api
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Api.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Method.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MockedCloneMethod.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UnmockedCloneMethod.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Builder
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Identity.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvocationMocker.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvocationStubber.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MethodNameMatch.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ParametersMatch.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Stub.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BadMethodCallException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CannotUseAddMethodsException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CannotUseOnlyMethodsException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassAlreadyExistsException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassIsFinalException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConfigurableMethodsAlreadyInitializedException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DuplicateMethodException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IncompatibleReturnValueException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidMethodNameException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MatchBuilderNotFoundException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MatcherAlreadyRegisteredException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MethodCannotBeConfiguredException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MethodNameAlreadyConfiguredException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MethodNameNotConfiguredException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MethodParametersAlreadyConfiguredException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜OriginalConstructorInvocationRequiredException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReflectionException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReturnValueNotConfiguredException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RuntimeException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SoapExtensionNotAvailableException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnknownClassException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnknownTraitException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UnknownTypeException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Generator
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜deprecation.tpl
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜intersection.tpl
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mocked_class.tpl
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mocked_method.tpl
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mocked_method_never_or_void.tpl
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mocked_static_method.tpl
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜proxied_method.tpl
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜proxied_method_never_or_void.tpl
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜trait_class.tpl
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜wsdl_class.tpl
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜wsdl_method.tpl
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Rule
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AnyInvokedCount.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AnyParameters.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConsecutiveParameters.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvocationOrder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvokedAtIndex.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvokedAtLeastCount.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvokedAtLeastOnce.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvokedAtMostCount.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvokedCount.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MethodName.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Parameters.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ParametersRule.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Stub
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConsecutiveCalls.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReturnArgument.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReturnCallback.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReturnReference.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReturnSelf.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReturnStub.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReturnValueMap.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Stub.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConfigurableMethod.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Generator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Invocation.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvocationHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Matcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MethodNameConstraint.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MockBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MockClass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MockMethod.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MockMethodSet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MockObject.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MockTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MockType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Stub.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Verifiable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Assert.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DataProviderTestSuite.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ErrorTestCase.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExceptionWrapper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExecutionOrderDependency.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IncompleteTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IncompleteTestCase.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidParameterGroupException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Reorderable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SelfDescribing.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SkippedTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SkippedTestCase.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Test.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestCase.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestFailure.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestListener.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestListenerDefaultImplementation.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestResult.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestSuite.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestSuiteIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜WarningTestCase.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Runner
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Extension
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExtensionHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PharLoader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Filter
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExcludeGroupFilterIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Factory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GroupFilterIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IncludeGroupFilterIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜NameFilterIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Hook
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AfterIncompleteTestHook.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AfterLastTestHook.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AfterRiskyTestHook.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AfterSkippedTestHook.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AfterSuccessfulTestHook.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AfterTestErrorHook.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AfterTestFailureHook.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AfterTestHook.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AfterTestWarningHook.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BeforeFirstTestHook.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BeforeTestHook.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Hook.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestHook.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TestListenerAdapter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BaseTestRunner.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DefaultTestResultCache.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullTestResultCache.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhptTestCase.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResultCacheExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StandardTestSuiteLoader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestResultCache.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestSuiteLoader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestSuiteSorter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Version.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂TextUI
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂CliArguments
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Builder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Configuration.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Mapper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReflectionException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RuntimeException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestDirectoryNotFoundException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TestFileNotFoundException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂XmlConfiguration
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂CodeCoverage
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Filter
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Directory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DirectoryCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜DirectoryCollectionIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Report
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Clover.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Cobertura.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Crap4j.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Html.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Php.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Xml.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CodeCoverage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜FilterMapper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Filesystem
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Directory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DirectoryCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DirectoryCollectionIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜File.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜FileCollectionIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Group
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Group.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GroupCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GroupCollectionIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Groups.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Logging
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂TestDox
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Html.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Xml.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Junit.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Logging.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TeamCity.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Text.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Migration
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Migrations
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConvertLogTypes.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CoverageCloverToReport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CoverageCrap4jToReport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CoverageHtmlToReport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CoveragePhpToReport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CoverageTextToReport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CoverageXmlToReport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IntroduceCoverageElement.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogToReportMigration.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Migration.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MoveAttributesFromFilterWhitelistToCoverage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MoveAttributesFromRootToCoverage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MoveWhitelistDirectoriesToCoverage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MoveWhitelistExcludesToCoverage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RemoveCacheTokensAttribute.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RemoveEmptyFilter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RemoveLogTypes.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UpdateSchemaLocationTo93.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MigrationBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MigrationBuilderException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MigrationException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Migrator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂PHP
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Constant.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConstantCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConstantCollectionIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IniSetting.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IniSettingCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IniSettingCollectionIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Php.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhpHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Variable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜VariableCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜VariableCollectionIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂PHPUnit
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Extension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExtensionCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExtensionCollectionIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PHPUnit.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂TestSuite
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestDirectory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestDirectoryCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestDirectoryCollectionIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestFile.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestFileCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestFileCollectionIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestSuite.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestSuiteCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TestSuiteCollectionIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Configuration.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Generator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Loader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Command.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DefaultResultPrinter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Help.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResultPrinter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestRunner.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TestSuiteMapper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Util
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Annotation
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DocBlock.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Registry.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Log
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜JUnit.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TeamCity.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂PHP
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Template
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhptTestCase.tpl
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestCaseClass.tpl
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TestCaseMethod.tpl
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractPhpProcess.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DefaultPhpProcess.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜WindowsPhpProcess.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂TestDox
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CliTestDoxPrinter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HtmlResultPrinter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NamePrettifier.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResultPrinter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TestDoxPrinter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TextResultPrinter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜XmlResultPrinter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Xml
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FailedSchemaDetectionResult.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Loader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SchemaDetectionResult.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SchemaDetector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SchemaFinder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SnapshotNodeList.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SuccessfulSchemaDetectionResult.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ValidationResult.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Validator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Blacklist.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ErrorHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExcludeList.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileLoader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Filesystem.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Filter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GlobalState.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidDataSetException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Json.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Printer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RegularExpression.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Test.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TextTestListRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Type.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜VersionComparisonOperator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜XdebugFilterScriptGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Xml.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜XmlTestListRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Exception.php
 ┃ ┃ ┃ ┃ ┣ 📜.phpstorm.meta.php
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog-8.5.md
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog-9.5.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜phpunit
 ┃ ┃ ┃ ┃ ┣ 📜phpunit.xsd
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂psr
 ┃ ┃ ┃ ┣ 📂container
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ContainerExceptionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ContainerInterface.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜NotFoundExceptionInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜.gitignore
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂log
 ┃ ┃ ┃ ┃ ┣ 📂Psr
 ┃ ┃ ┃ ┃ ┃ ┗ 📂Log
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Test
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DummyTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LoggerInterfaceTest.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TestLogger.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractLogger.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidArgumentException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LoggerAwareInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LoggerAwareTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LoggerInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LoggerTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LogLevel.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜NullLogger.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┗ 📂simple-cache
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CacheException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CacheInterface.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜InvalidArgumentException.php
 ┃ ┃ ┃ ┃ ┣ 📜.editorconfig
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂psy
 ┃ ┃ ┃ ┗ 📂psysh
 ┃ ┃ ┃ ┃ ┣ 📂bin
 ┃ ┃ ┃ ┃ ┃ ┗ 📜psysh
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂CodeCleaner
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractClassPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AssignThisVariablePass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CalledClassPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CallTimePassByReferencePass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CodeCleanerPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EmptyArrayDimFetchPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExitPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FinalClassPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FunctionContextPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FunctionReturnInWriteContextPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ImplicitReturnPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InstanceOfPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IssetPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LabelContextPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LeavePsyshAlonePass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ListPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LoopContextPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MagicConstantsPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NamespaceAwarePass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NamespacePass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoReturnValue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PassableByReferencePass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RequirePass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReturnTypePass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StrictTypesPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UseStatementPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ValidClassNamePass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ValidConstructorPass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ValidFunctionNamePass.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Command
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂ListCommand
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassConstantEnumerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassEnumerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConstantEnumerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Enumerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FunctionEnumerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GlobalVariableEnumerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MethodEnumerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PropertyEnumerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜VariableEnumerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂TimeitCommand
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TimeitVisitor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BufferCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClearCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Command.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DocCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DumpCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EditCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExitCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HelpCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HistoryCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ListCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ParseCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PsyVersionCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReflectingCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ShowCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SudoCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ThrowUpCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TimeitCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TraceCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜WhereamiCommand.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜WtfCommand.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BreakException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DeprecatedException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ErrorException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FatalErrorException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ParseErrorException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RuntimeException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ThrowUpException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TypeErrorException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UnexpectedTargetException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂ExecutionLoop
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractListener.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Listener.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ProcessForker.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RunkitReloader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Formatter
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CodeFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DocblockFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Formatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReflectorFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SignatureFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TraceFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Input
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CodeArgument.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FilterOptions.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ShellInput.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SilentInput.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Output
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜OutputPager.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PassthruPager.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ProcOutputPager.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ShellOutput.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Readline
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GNUReadline.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HoaConsole.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Libedit.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Readline.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Transient.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Reflection
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReflectionClassConstant.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReflectionConstant.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReflectionConstant_.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReflectionLanguageConstruct.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReflectionLanguageConstructParameter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ReflectionNamespace.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Sudo
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SudoVisitor.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂TabCompletion
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Matcher
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractContextAwareMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractDefaultParametersMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassAttributesMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassMethodDefaultParametersMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassMethodsMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassNamesMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CommandsMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConstantsMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FunctionDefaultParametersMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FunctionsMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜KeywordsMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MongoClientMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MongoDatabaseMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ObjectAttributesMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ObjectMethodDefaultParametersMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ObjectMethodsMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜VariablesMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜AutoCompleter.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Util
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Docblock.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Json.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Mirror.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Str.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂VarDumper
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Cloner.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Dumper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Presenter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PresenterAware.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂VersionUpdater
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Checker.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GitHubChecker.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IntervalChecker.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜NoopChecker.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CodeCleaner.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConfigPaths.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Configuration.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConsoleColorFactory.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Context.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ContextAware.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜EnvInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ExecutionClosure.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ExecutionLoopClosure.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜functions.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ParserFactory.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Shell.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Sudo.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜SuperglobalsEnv.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂ramsey
 ┃ ┃ ┃ ┗ 📂uuid
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Builder
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DefaultUuidBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DegradedUuidBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UuidBuilderInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Codec
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CodecInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GuidStringCodec.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜OrderedTimeCodec.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StringCodec.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TimestampFirstCombCodec.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TimestampLastCombCodec.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Converter
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Number
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BigNumberConverter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜DegradedNumberConverter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Time
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BigNumberTimeConverter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DegradedTimeConverter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhpTimeConverter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NumberConverterInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TimeConverterInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidUuidStringException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnsatisfiedDependencyException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UnsupportedOperationException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Generator
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CombGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DefaultTimeGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MtRandGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜OpenSslGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PeclUuidRandomGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PeclUuidTimeGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RandomBytesGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RandomGeneratorFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RandomGeneratorInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RandomLibAdapter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SodiumRandomGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TimeGeneratorFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TimeGeneratorInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Provider
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Node
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FallbackNodeProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RandomNodeProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SystemNodeProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Time
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FixedTimeProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SystemTimeProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NodeProviderInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TimeProviderInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜BinaryUtils.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DegradedUuid.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FeatureSet.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜functions.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Uuid.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜UuidFactory.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜UuidFactoryInterface.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜UuidInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂sabberworm
 ┃ ┃ ┃ ┗ 📂php-css-parser
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Comment
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Comment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Commentable.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂CSSList
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AtRuleBlockList.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CSSBlockList.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CSSList.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Document.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜KeyFrame.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Parsing
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜OutputException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ParserState.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SourceException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnexpectedEOFException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UnexpectedTokenException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Property
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AtRule.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Charset.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CSSNamespace.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Import.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜KeyframeSelector.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Selector.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Rule
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Rule.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂RuleSet
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AtRuleSet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DeclarationBlock.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RuleSet.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Value
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CalcFunction.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CalcRuleValueList.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Color.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CSSFunction.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CSSString.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LineName.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PrimitiveValue.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RuleValueList.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Size.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜URL.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Value.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ValueList.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜OutputFormat.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜OutputFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Parser.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Renderable.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Settings.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂scrivo
 ┃ ┃ ┃ ┗ 📂highlight.php
 ┃ ┃ ┃ ┃ ┣ 📂Highlight
 ┃ ┃ ┃ ┃ ┃ ┣ 📂languages
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜1c.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜abnf.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜accesslog.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜actionscript.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ada.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜angelscript.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜apache.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜applescript.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜arcade.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜arduino.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜armasm.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜asciidoc.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜aspectj.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜autohotkey.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜autoit.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜avrasm.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜awk.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜axapta.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bash.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜basic.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜bnf.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜brainfuck.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜cal.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜capnproto.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ceylon.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜clean.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜clojure-repl.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜clojure.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜cmake.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜coffeescript.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜coq.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜cos.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜cpp.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜crmsh.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜crystal.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜cs.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜csp.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜css.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜d.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dart.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜delphi.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜diff.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜django.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dns.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dockerfile.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dos.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dsconfig.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dts.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜dust.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ebnf.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜elixir.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜elm.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜erb.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜erlang-repl.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜erlang.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜excel.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fix.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜flix.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fortran.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜fsharp.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gams.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gauss.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gcode.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gherkin.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜glsl.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gml.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜go.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜golo.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜gradle.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜groovy.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜haml.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜handlebars.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜haskell.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜haxe.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hsp.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜htmlbars.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜http.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜hy.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜inform7.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ini.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜irpf90.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜isbl.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜java.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜javascript.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜jboss-cli.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜json.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜julia-repl.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜julia.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜kotlin.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lasso.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ldif.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜leaf.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜less.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lisp.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜livecodeserver.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜livescript.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜llvm.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lsl.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lua.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜makefile.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜markdown.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mathematica.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜matlab.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜maxima.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mel.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mercury.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mipsasm.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mizar.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mojolicious.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜monkey.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜moonscript.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜n1ql.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nginx.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nimrod.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nix.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜nsis.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜objectivec.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ocaml.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜openscad.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜oxygene.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜parser3.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜perl.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pf.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pgsql.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜php.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜plaintext.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜pony.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜powershell.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜processing.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜profile.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜prolog.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜properties.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜protobuf.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜puppet.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜purebasic.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜python.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜q.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜qml.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜r.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜reasonml.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜rib.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜roboconf.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜routeros.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜rsl.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ruby.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ruleslanguage.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜rust.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sas.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜scala.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜scheme.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜scilab.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜scss.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜shell.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜smali.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜smalltalk.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sml.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sqf.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜sql.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜stan.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜stata.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜step21.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜stylus.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜subunit.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜swift.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜taggerscript.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tap.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tcl.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tex.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜thrift.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜tp.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜twig.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜typescript.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜vala.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜vbnet.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜vbscript-html.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜vbscript.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜verilog.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜vhdl.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜vim.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜x86asm.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜xl.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜xml.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜xquery.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜yaml.json
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜zephir.json
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Autoloader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Highlighter.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜HighlightResult.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜JsonRef.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Language.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Mode.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ModeDeprecations.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RegEx.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RegExMatch.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RegExUtils.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Terminators.php
 ┃ ┃ ┃ ┃ ┣ 📂HighlightUtilities
 ┃ ┃ ┃ ┃ ┃ ┣ 📜functions.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜_internals.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜_themeColors.php
 ┃ ┃ ┃ ┃ ┣ 📂styles
 ┃ ┃ ┃ ┃ ┃ ┣ 📜a11y-dark.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜a11y-light.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜agate.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜an-old-hope.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜androidstudio.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜arduino-light.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜arta.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ascetic.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atelier-cave-dark.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atelier-cave-light.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atelier-dune-dark.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atelier-dune-light.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atelier-estuary-dark.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atelier-estuary-light.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atelier-forest-dark.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atelier-forest-light.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atelier-heath-dark.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atelier-heath-light.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atelier-lakeside-dark.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atelier-lakeside-light.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atelier-plateau-dark.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atelier-plateau-light.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atelier-savanna-dark.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atelier-savanna-light.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atelier-seaside-dark.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atelier-seaside-light.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atelier-sulphurpool-dark.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atelier-sulphurpool-light.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atom-one-dark-reasonable.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atom-one-dark.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜atom-one-light.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜brown-paper.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜brown-papersq.png
 ┃ ┃ ┃ ┃ ┃ ┣ 📜codepen-embed.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜color-brewer.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜darcula.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜dark.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜darkula.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜default.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜docco.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜dracula.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜far.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜foundation.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜github-gist.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜github.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜gml.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜googlecode.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜gradient-dark.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜grayscale.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜gruvbox-dark.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜gruvbox-light.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜hopscotch.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜hybrid.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜idea.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ir-black.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜isbl-editor-dark.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜isbl-editor-light.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜kimbie.dark.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜kimbie.light.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜lightfair.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜magula.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜mono-blue.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜monokai-sublime.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜monokai.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜night-owl.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜nord.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜obsidian.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ocean.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜paraiso-dark.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜paraiso-light.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜pojoaque.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜pojoaque.jpg
 ┃ ┃ ┃ ┃ ┃ ┣ 📜purebasic.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜qtcreator_dark.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜qtcreator_light.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜railscasts.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜rainbow.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜routeros.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜school-book.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜school-book.png
 ┃ ┃ ┃ ┃ ┃ ┣ 📜shades-of-purple.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜solarized-dark.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜solarized-light.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜sunburst.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜tomorrow-night-blue.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜tomorrow-night-bright.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜tomorrow-night-eighties.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜tomorrow-night.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜tomorrow.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜vs.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜vs2015.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜xcode.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📜xt256.css
 ┃ ┃ ┃ ┃ ┃ ┗ 📜zenburn.css
 ┃ ┃ ┃ ┃ ┣ 📜.php-cs-fixer.dist.php
 ┃ ┃ ┃ ┃ ┣ 📜AUTHORS.txt
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜CONTRIBUTING.md
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE.md
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂sebastian
 ┃ ┃ ┃ ┣ 📂cli-parser
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂exceptions
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AmbiguousOptionException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜OptionDoesNotAllowArgumentException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RequiredOptionArgumentMissingException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UnknownOptionException.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Parser.php
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜infection.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂code-unit
 ┃ ┃ ┃ ┃ ┣ 📂.psalm
 ┃ ┃ ┃ ┃ ┃ ┣ 📜baseline.xml
 ┃ ┃ ┃ ┃ ┃ ┗ 📜config.xml
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂exceptions
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidCodeUnitException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoTraitException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ReflectionException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassMethodUnit.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassUnit.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CodeUnit.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CodeUnitCollection.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CodeUnitCollectionIterator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FunctionUnit.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InterfaceMethodUnit.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InterfaceUnit.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Mapper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TraitMethodUnit.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TraitUnit.php
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂code-unit-reverse-lookup
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Wizard.php
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂comparator
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂exceptions
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RuntimeException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayComparator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Comparator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ComparisonFailure.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DateTimeComparator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DOMNodeComparator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DoubleComparator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ExceptionComparator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Factory.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MockObjectComparator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜NumericComparator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ObjectComparator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ResourceComparator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ScalarComparator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SplObjectStorageComparator.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TypeComparator.php
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂complexity
 ┃ ┃ ┃ ┃ ┣ 📂.psalm
 ┃ ┃ ┃ ┃ ┃ ┣ 📜baseline.xml
 ┃ ┃ ┃ ┃ ┃ ┗ 📜config.xml
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Complexity
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Complexity.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ComplexityCollection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ComplexityCollectionIterator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RuntimeException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Visitor
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ComplexityCalculatingVisitor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜CyclomaticComplexityCalculatingVisitor.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Calculator.php
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂diff
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConfigurationException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜InvalidArgumentException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Output
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractChunkOutputBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DiffOnlyOutputBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DiffOutputBuilderInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StrictUnifiedDiffOutputBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UnifiedDiffOutputBuilder.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Chunk.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Diff.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Differ.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Line.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜LongestCommonSubsequenceCalculator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MemoryEfficientLongestCommonSubsequenceCalculator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Parser.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TimeEfficientLongestCommonSubsequenceCalculator.php
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂environment
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Console.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜OperatingSystem.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Runtime.php
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂exporter
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Exporter.php
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂global-state
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂exceptions
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RuntimeException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CodeExporter.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ExcludeList.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Restorer.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Snapshot.php
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂lines-of-code
 ┃ ┃ ┃ ┃ ┣ 📂.psalm
 ┃ ┃ ┃ ┃ ┃ ┣ 📜baseline.xml
 ┃ ┃ ┃ ┃ ┃ ┗ 📜config.xml
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IllogicalValuesException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NegativeValueException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RuntimeException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Counter.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜LineCountingVisitor.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜LinesOfCode.php
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂object-enumerator
 ┃ ┃ ┃ ┃ ┣ 📂.psalm
 ┃ ┃ ┃ ┃ ┃ ┣ 📜baseline.xml
 ┃ ┃ ┃ ┃ ┃ ┗ 📜config.xml
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Enumerator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜InvalidArgumentException.php
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜phpunit.xml
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂object-reflector
 ┃ ┃ ┃ ┃ ┣ 📂.psalm
 ┃ ┃ ┃ ┃ ┃ ┣ 📜baseline.xml
 ┃ ┃ ┃ ┃ ┃ ┗ 📜config.xml
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidArgumentException.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜ObjectReflector.php
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂recursion-context
 ┃ ┃ ┃ ┃ ┣ 📂.psalm
 ┃ ┃ ┃ ┃ ┃ ┣ 📜baseline.xml
 ┃ ┃ ┃ ┃ ┃ ┗ 📜config.xml
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Context.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜InvalidArgumentException.php
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂resource-operations
 ┃ ┃ ┃ ┃ ┣ 📂build
 ┃ ┃ ┃ ┃ ┃ ┗ 📜generate.php
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┗ 📜ResourceOperations.php
 ┃ ┃ ┃ ┃ ┣ 📜.gitattributes
 ┃ ┃ ┃ ┃ ┣ 📜.gitignore
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂type
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RuntimeException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂type
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CallableType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FalseType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GenericObjectType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IntersectionType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IterableType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MixedType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NeverType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ObjectType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SimpleType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StaticType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Type.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnionType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnknownType.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜VoidType.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ReflectionMapper.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TypeName.php
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┗ 📂version
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Version.php
 ┃ ┃ ┃ ┃ ┣ 📜.gitattributes
 ┃ ┃ ┃ ┃ ┣ 📜.gitignore
 ┃ ┃ ┃ ┃ ┣ 📜ChangeLog.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂swiftmailer
 ┃ ┃ ┃ ┗ 📂swiftmailer
 ┃ ┃ ┃ ┃ ┣ 📂.github
 ┃ ┃ ┃ ┃ ┃ ┣ 📂workflows
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜tests.yml
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ISSUE_TEMPLATE.md
 ┃ ┃ ┃ ┃ ┃ ┗ 📜PULL_REQUEST_TEMPLATE.md
 ┃ ┃ ┃ ┃ ┣ 📂doc
 ┃ ┃ ┃ ┃ ┃ ┣ 📜headers.rst
 ┃ ┃ ┃ ┃ ┃ ┣ 📜index.rst
 ┃ ┃ ┃ ┃ ┃ ┣ 📜introduction.rst
 ┃ ┃ ┃ ┃ ┃ ┣ 📜japanese.rst
 ┃ ┃ ┃ ┃ ┃ ┣ 📜messages.rst
 ┃ ┃ ┃ ┃ ┃ ┣ 📜plugins.rst
 ┃ ┃ ┃ ┃ ┃ ┗ 📜sending.rst
 ┃ ┃ ┃ ┃ ┣ 📂lib
 ┃ ┃ ┃ ┃ ┃ ┣ 📂classes
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Swift
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂AddressEncoder
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IdnAddressEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Utf8AddressEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂ByteStream
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractFilterableInputStream.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayByteStream.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileByteStream.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TemporaryFileByteStream.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂CharacterReader
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GenericFixedWidthReader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UsAsciiReader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Utf8Reader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂CharacterReaderFactory
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SimpleCharacterReaderFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂CharacterStream
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayCharacterStream.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜NgCharacterStream.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Encoder
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Base64Encoder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜QpEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Rfc2231Encoder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Events
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CommandEvent.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CommandListener.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Event.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EventDispatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EventListener.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EventObject.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResponseEvent.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResponseListener.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SendEvent.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SendListener.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SimpleEventDispatcher.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TransportChangeEvent.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TransportChangeListener.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TransportExceptionEvent.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TransportExceptionListener.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂KeyCache
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayKeyCache.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DiskKeyCache.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜KeyCacheInputStream.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullKeyCache.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SimpleKeyCacheInputStream.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Mailer
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayRecipientIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RecipientIterator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Mime
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂ContentEncoder
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Base64ContentEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NativeQpContentEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullContentEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PlainContentEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜QpContentEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜QpContentEncoderProxy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RawContentEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂HeaderEncoder
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Base64HeaderEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜QpHeaderEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Headers
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractHeader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DateHeader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IdentificationHeader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MailboxHeader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜OpenDKIMHeader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ParameterizedHeader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PathHeader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UnstructuredHeader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Attachment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CharsetObserver.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ContentEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EmbeddedFile.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EncodingObserver.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Header.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HeaderEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IdGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MimePart.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SimpleHeaderFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SimpleHeaderSet.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SimpleMessage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SimpleMimeEntity.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Plugins
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Decorator
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Replacements.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Loggers
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayLogger.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜EchoLogger.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Pop
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Pop3Connection.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Pop3Exception.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Reporters
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HitReporter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜HtmlReporter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AntiFloodPlugin.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BandwidthMonitorPlugin.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DecoratorPlugin.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ImpersonatePlugin.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Logger.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LoggerPlugin.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MessageLogger.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PopBeforeSmtpPlugin.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RedirectingPlugin.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Reporter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReporterPlugin.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Sleeper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ThrottlerPlugin.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Timer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Signers
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜BodySigner.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DKIMSigner.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DomainKeySigner.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HeaderSigner.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜OpenDKIMSigner.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SMimeSigner.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂StreamFilters
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ByteArrayReplacementFilter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StringReplacementFilter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜StringReplacementFilterFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Transport
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Esmtp
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Auth
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CramMd5Authenticator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LoginAuthenticator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NTLMAuthenticator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PlainAuthenticator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜XOAuth2Authenticator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Authenticator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AuthHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EightBitMimeHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SmtpUtf8Handler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractSmtpTransport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EsmtpHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EsmtpTransport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FailoverTransport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IoBuffer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LoadBalancedTransport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullTransport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SendmailTransport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SmtpAgent.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SpoolTransport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜StreamBuffer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AddressEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AddressEncoderException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Attachment.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CharacterReader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CharacterReaderFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CharacterStream.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ConfigurableSpool.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DependencyContainer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DependencyException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EmbeddedFile.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Encoder.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FailoverTransport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileSpool.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileStream.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Filterable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IdGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Image.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InputByteStream.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IoException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜KeyCache.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜LoadBalancedTransport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Mailer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MemorySpool.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Message.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MimePart.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullTransport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜OutputByteStream.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Preferences.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ReplacementFilterFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RfcComplianceException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SendmailTransport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Signer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SmtpTransport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Spool.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SpoolTransport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StreamFilter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SwiftException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Transport.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TransportException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Swift.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂dependency_maps
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜cache_deps.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜message_deps.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mime_deps.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜transport_deps.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜mime_types.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜preferences.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜swiftmailer_generate_mimes_config.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜swift_required.php
 ┃ ┃ ┃ ┃ ┣ 📜.gitattributes
 ┃ ┃ ┃ ┃ ┣ 📜.gitignore
 ┃ ┃ ┃ ┃ ┣ 📜.php_cs.dist
 ┃ ┃ ┃ ┃ ┣ 📜CHANGES
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂symfony
 ┃ ┃ ┃ ┣ 📂console
 ┃ ┃ ┃ ┃ ┣ 📂Command
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Command.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜HelpCommand.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ListCommand.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜LockableTrait.php
 ┃ ┃ ┃ ┃ ┣ 📂CommandLoader
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CommandLoaderInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ContainerCommandLoader.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜FactoryCommandLoader.php
 ┃ ┃ ┃ ┃ ┣ 📂DependencyInjection
 ┃ ┃ ┃ ┃ ┃ ┗ 📜AddConsoleCommandPass.php
 ┃ ┃ ┃ ┃ ┣ 📂Descriptor
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ApplicationDescription.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Descriptor.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DescriptorInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜JsonDescriptor.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MarkdownDescriptor.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TextDescriptor.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜XmlDescriptor.php
 ┃ ┃ ┃ ┃ ┣ 📂Event
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConsoleCommandEvent.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConsoleErrorEvent.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConsoleEvent.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜ConsoleTerminateEvent.php
 ┃ ┃ ┃ ┃ ┣ 📂EventListener
 ┃ ┃ ┃ ┃ ┃ ┗ 📜ErrorListener.php
 ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CommandNotFoundException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ExceptionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidArgumentException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidOptionException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜LogicException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MissingInputException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜NamespaceNotFoundException.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜RuntimeException.php
 ┃ ┃ ┃ ┃ ┣ 📂Formatter
 ┃ ┃ ┃ ┃ ┃ ┣ 📜OutputFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜OutputFormatterInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜OutputFormatterStyle.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜OutputFormatterStyleInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜OutputFormatterStyleStack.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜WrappableOutputFormatterInterface.php
 ┃ ┃ ┃ ┃ ┣ 📂Helper
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DebugFormatterHelper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DescriptorHelper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Dumper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FormatterHelper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Helper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜HelperInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜HelperSet.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InputAwareHelper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ProcessHelper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ProgressBar.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ProgressIndicator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜QuestionHelper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SymfonyQuestionHelper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Table.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TableCell.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TableRows.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TableSeparator.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TableStyle.php
 ┃ ┃ ┃ ┃ ┣ 📂Input
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ArgvInput.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayInput.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Input.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InputArgument.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InputAwareInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InputDefinition.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InputInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InputOption.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜StreamableInputInterface.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜StringInput.php
 ┃ ┃ ┃ ┃ ┣ 📂Logger
 ┃ ┃ ┃ ┃ ┃ ┗ 📜ConsoleLogger.php
 ┃ ┃ ┃ ┃ ┣ 📂Output
 ┃ ┃ ┃ ┃ ┃ ┣ 📜BufferedOutput.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConsoleOutput.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConsoleOutputInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConsoleSectionOutput.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜NullOutput.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Output.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜OutputInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜StreamOutput.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TrimmedBufferOutput.php
 ┃ ┃ ┃ ┃ ┣ 📂Question
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ChoiceQuestion.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConfirmationQuestion.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Question.php
 ┃ ┃ ┃ ┃ ┣ 📂Resources
 ┃ ┃ ┃ ┃ ┃ ┗ 📂bin
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜hiddeninput.exe
 ┃ ┃ ┃ ┃ ┣ 📂Style
 ┃ ┃ ┃ ┃ ┃ ┣ 📜OutputStyle.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜StyleInterface.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜SymfonyStyle.php
 ┃ ┃ ┃ ┃ ┣ 📂Tester
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ApplicationTester.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CommandTester.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TesterTrait.php
 ┃ ┃ ┃ ┃ ┣ 📜Application.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜ConsoleEvents.php
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜README.md
 ┃ ┃ ┃ ┃ ┗ 📜Terminal.php
 ┃ ┃ ┃ ┣ 📂css-selector
 ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ExceptionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ExpressionErrorException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InternalErrorException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ParseException.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜SyntaxErrorException.php
 ┃ ┃ ┃ ┃ ┣ 📂Node
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractNode.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AttributeNode.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassNode.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CombinedSelectorNode.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ElementNode.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FunctionNode.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜HashNode.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜NegationNode.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜NodeInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PseudoNode.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SelectorNode.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Specificity.php
 ┃ ┃ ┃ ┃ ┣ 📂Parser
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Handler
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CommentHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HandlerInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HashHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IdentifierHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NumberHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜StringHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜WhitespaceHandler.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Shortcut
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ElementParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EmptyStringParser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜HashParser.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Tokenizer
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Tokenizer.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TokenizerEscaping.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜TokenizerPatterns.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Parser.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ParserInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Reader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Token.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TokenStream.php
 ┃ ┃ ┃ ┃ ┣ 📂XPath
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Extension
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AttributeMatchingExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CombinationExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExtensionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FunctionExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜HtmlExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NodeExtension.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PseudoClassExtension.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Translator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TranslatorInterface.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜XPathExpr.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜CssSelectorConverter.php
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂debug
 ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassNotFoundException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FatalErrorException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FatalThrowableError.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FlattenException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜OutOfMemoryException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SilencedErrorContext.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜UndefinedFunctionException.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜UndefinedMethodException.php
 ┃ ┃ ┃ ┃ ┣ 📂FatalErrorHandler
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassNotFoundFatalErrorHandler.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FatalErrorHandlerInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜UndefinedFunctionFatalErrorHandler.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜UndefinedMethodFatalErrorHandler.php
 ┃ ┃ ┃ ┃ ┣ 📜BufferingLogger.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜Debug.php
 ┃ ┃ ┃ ┃ ┣ 📜DebugClassLoader.php
 ┃ ┃ ┃ ┃ ┣ 📜ErrorHandler.php
 ┃ ┃ ┃ ┃ ┣ 📜ExceptionHandler.php
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂deprecation-contracts
 ┃ ┃ ┃ ┃ ┣ 📜.gitignore
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜function.php
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂error-handler
 ┃ ┃ ┃ ┃ ┣ 📂Error
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassNotFoundError.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FatalError.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜OutOfMemoryError.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜UndefinedFunctionError.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜UndefinedMethodError.php
 ┃ ┃ ┃ ┃ ┣ 📂ErrorEnhancer
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassNotFoundErrorEnhancer.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ErrorEnhancerInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜UndefinedFunctionErrorEnhancer.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜UndefinedMethodErrorEnhancer.php
 ┃ ┃ ┃ ┃ ┣ 📂ErrorRenderer
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CliErrorRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ErrorRendererInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜HtmlErrorRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜SerializerErrorRenderer.php
 ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FlattenException.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜SilencedErrorContext.php
 ┃ ┃ ┃ ┃ ┣ 📂Resources
 ┃ ┃ ┃ ┃ ┃ ┣ 📂assets
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂css
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜error.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜exception.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜exception_full.css
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂images
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜chevron-right.svg
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜favicon.png.base64
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜icon-book.svg
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜icon-minus-square-o.svg
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜icon-minus-square.svg
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜icon-plus-square-o.svg
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜icon-plus-square.svg
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜icon-support.svg
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜symfony-ghost.svg.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜symfony-logo.svg
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂js
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜exception.js
 ┃ ┃ ┃ ┃ ┃ ┗ 📂views
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜error.html.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜exception.html.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜exception_full.html.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜logs.html.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜trace.html.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜traces.html.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜traces_text.html.php
 ┃ ┃ ┃ ┃ ┣ 📜BufferingLogger.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜Debug.php
 ┃ ┃ ┃ ┃ ┣ 📜DebugClassLoader.php
 ┃ ┃ ┃ ┃ ┣ 📜ErrorHandler.php
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜README.md
 ┃ ┃ ┃ ┃ ┗ 📜ThrowableUtils.php
 ┃ ┃ ┃ ┣ 📂event-dispatcher
 ┃ ┃ ┃ ┃ ┣ 📂Debug
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TraceableEventDispatcher.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TraceableEventDispatcherInterface.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜WrappedListener.php
 ┃ ┃ ┃ ┃ ┣ 📂DependencyInjection
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AddEventAliasesPass.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜RegisterListenersPass.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜Event.php
 ┃ ┃ ┃ ┃ ┣ 📜EventDispatcher.php
 ┃ ┃ ┃ ┃ ┣ 📜EventDispatcherInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜EventSubscriberInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜GenericEvent.php
 ┃ ┃ ┃ ┃ ┣ 📜ImmutableEventDispatcher.php
 ┃ ┃ ┃ ┃ ┣ 📜LegacyEventDispatcherProxy.php
 ┃ ┃ ┃ ┃ ┣ 📜LegacyEventProxy.php
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂event-dispatcher-contracts
 ┃ ┃ ┃ ┃ ┣ 📜.gitignore
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜Event.php
 ┃ ┃ ┃ ┃ ┣ 📜EventDispatcherInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂finder
 ┃ ┃ ┃ ┃ ┣ 📂Comparator
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Comparator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DateComparator.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜NumberComparator.php
 ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AccessDeniedException.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜DirectoryNotFoundException.php
 ┃ ┃ ┃ ┃ ┣ 📂Iterator
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CustomFilterIterator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DateRangeFilterIterator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DepthRangeFilterIterator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ExcludeDirectoryFilterIterator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FilecontentFilterIterator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FilenameFilterIterator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FileTypeFilterIterator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜LazyIterator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MultiplePcreFilterIterator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PathFilterIterator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RecursiveDirectoryIterator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SizeRangeFilterIterator.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜SortableIterator.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜Finder.php
 ┃ ┃ ┃ ┃ ┣ 📜Gitignore.php
 ┃ ┃ ┃ ┃ ┣ 📜Glob.php
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜README.md
 ┃ ┃ ┃ ┃ ┗ 📜SplFileInfo.php
 ┃ ┃ ┃ ┣ 📂http-client-contracts
 ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ClientExceptionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DecodingExceptionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ExceptionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜HttpExceptionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RedirectionExceptionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ServerExceptionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TimeoutExceptionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TransportExceptionInterface.php
 ┃ ┃ ┃ ┃ ┣ 📂Test
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Fixtures
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂web
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜index.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜HttpClientTestCase.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TestHttpServer.php
 ┃ ┃ ┃ ┃ ┣ 📜.gitignore
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜ChunkInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜HttpClientInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜README.md
 ┃ ┃ ┃ ┃ ┣ 📜ResponseInterface.php
 ┃ ┃ ┃ ┃ ┗ 📜ResponseStreamInterface.php
 ┃ ┃ ┃ ┣ 📂http-foundation
 ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConflictingHeadersException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RequestExceptionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜SuspiciousOperationException.php
 ┃ ┃ ┃ ┃ ┣ 📂File
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AccessDeniedException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CannotWriteFileException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExtensionFileException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileNotFoundException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FormSizeFileException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜IniSizeFileException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoFileException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NoTmpDirFileException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PartialFileException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnexpectedTypeException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜UploadException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂MimeType
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExtensionGuesser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExtensionGuesserInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileBinaryMimeTypeGuesser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FileinfoMimeTypeGuesser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MimeTypeExtensionGuesser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MimeTypeGuesser.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜MimeTypeGuesserInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜File.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Stream.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜UploadedFile.php
 ┃ ┃ ┃ ┃ ┣ 📂Session
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Attribute
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AttributeBag.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AttributeBagInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜NamespacedAttributeBag.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Flash
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AutoExpireFlashBag.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FlashBag.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜FlashBagInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Storage
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Handler
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractSessionHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MemcachedSessionHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MigratingSessionHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MongoDbSessionHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NativeFileSessionHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NullSessionHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PdoSessionHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RedisSessionHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SessionHandlerFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜StrictSessionHandler.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Proxy
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractProxy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SessionHandlerProxy.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MetadataBag.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MockArraySessionStorage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MockFileSessionStorage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NativeSessionStorage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhpBridgeSessionStorage.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SessionStorageInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Session.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SessionBagInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SessionBagProxy.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SessionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜SessionUtils.php
 ┃ ┃ ┃ ┃ ┣ 📂Test
 ┃ ┃ ┃ ┃ ┃ ┗ 📂Constraint
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RequestAttributeValueSame.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResponseCookieValueSame.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResponseHasCookie.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResponseHasHeader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResponseHeaderSame.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResponseIsRedirected.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ResponseIsSuccessful.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ResponseStatusCodeSame.php
 ┃ ┃ ┃ ┃ ┣ 📜AcceptHeader.php
 ┃ ┃ ┃ ┃ ┣ 📜AcceptHeaderItem.php
 ┃ ┃ ┃ ┃ ┣ 📜ApacheRequest.php
 ┃ ┃ ┃ ┃ ┣ 📜BinaryFileResponse.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜Cookie.php
 ┃ ┃ ┃ ┃ ┣ 📜ExpressionRequestMatcher.php
 ┃ ┃ ┃ ┃ ┣ 📜FileBag.php
 ┃ ┃ ┃ ┃ ┣ 📜HeaderBag.php
 ┃ ┃ ┃ ┃ ┣ 📜HeaderUtils.php
 ┃ ┃ ┃ ┃ ┣ 📜IpUtils.php
 ┃ ┃ ┃ ┃ ┣ 📜JsonResponse.php
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜ParameterBag.php
 ┃ ┃ ┃ ┃ ┣ 📜README.md
 ┃ ┃ ┃ ┃ ┣ 📜RedirectResponse.php
 ┃ ┃ ┃ ┃ ┣ 📜Request.php
 ┃ ┃ ┃ ┃ ┣ 📜RequestMatcher.php
 ┃ ┃ ┃ ┃ ┣ 📜RequestMatcherInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜RequestStack.php
 ┃ ┃ ┃ ┃ ┣ 📜Response.php
 ┃ ┃ ┃ ┃ ┣ 📜ResponseHeaderBag.php
 ┃ ┃ ┃ ┃ ┣ 📜ServerBag.php
 ┃ ┃ ┃ ┃ ┣ 📜StreamedResponse.php
 ┃ ┃ ┃ ┃ ┗ 📜UrlHelper.php
 ┃ ┃ ┃ ┣ 📂http-kernel
 ┃ ┃ ┃ ┃ ┣ 📂Bundle
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Bundle.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜BundleInterface.php
 ┃ ┃ ┃ ┃ ┣ 📂CacheClearer
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CacheClearerInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ChainCacheClearer.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Psr6CacheClearer.php
 ┃ ┃ ┃ ┃ ┣ 📂CacheWarmer
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CacheWarmer.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CacheWarmerAggregate.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CacheWarmerInterface.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜WarmableInterface.php
 ┃ ┃ ┃ ┃ ┣ 📂Config
 ┃ ┃ ┃ ┃ ┃ ┗ 📜FileLocator.php
 ┃ ┃ ┃ ┃ ┣ 📂Controller
 ┃ ┃ ┃ ┃ ┃ ┣ 📂ArgumentResolver
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DefaultValueResolver.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜NotTaggedControllerValueResolver.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RequestAttributeValueResolver.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RequestValueResolver.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ServiceValueResolver.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜SessionValueResolver.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜TraceableValueResolver.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜VariadicValueResolver.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ArgumentResolver.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ArgumentResolverInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ArgumentValueResolverInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ContainerControllerResolver.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ControllerReference.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ControllerResolver.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ControllerResolverInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ErrorController.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TraceableArgumentResolver.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TraceableControllerResolver.php
 ┃ ┃ ┃ ┃ ┣ 📂ControllerMetadata
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ArgumentMetadata.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ArgumentMetadataFactory.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜ArgumentMetadataFactoryInterface.php
 ┃ ┃ ┃ ┃ ┣ 📂DataCollector
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AjaxDataCollector.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConfigDataCollector.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DataCollector.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DataCollectorInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DumpDataCollector.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜EventDataCollector.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ExceptionDataCollector.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜LateDataCollectorInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜LoggerDataCollector.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MemoryDataCollector.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RequestDataCollector.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RouterDataCollector.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TimeDataCollector.php
 ┃ ┃ ┃ ┃ ┣ 📂Debug
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FileLinkFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TraceableEventDispatcher.php
 ┃ ┃ ┃ ┃ ┣ 📂DependencyInjection
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AddAnnotatedClassesToCachePass.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConfigurableExtension.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ControllerArgumentValueResolverPass.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Extension.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FragmentRendererPass.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜LazyLoadingFragmentHandler.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜LoggerPass.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MergeExtensionConfigurationPass.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RegisterControllerArgumentLocatorsPass.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RegisterLocaleAwareServicesPass.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RemoveEmptyControllerArgumentLocatorsPass.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ResettableServicePass.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜ServicesResetter.php
 ┃ ┃ ┃ ┃ ┣ 📂Event
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ControllerArgumentsEvent.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ControllerEvent.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ExceptionEvent.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FilterControllerArgumentsEvent.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FilterControllerEvent.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FilterResponseEvent.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FinishRequestEvent.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜GetResponseEvent.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜GetResponseForControllerResultEvent.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜GetResponseForExceptionEvent.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜KernelEvent.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PostResponseEvent.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RequestEvent.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ResponseEvent.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TerminateEvent.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜ViewEvent.php
 ┃ ┃ ┃ ┃ ┣ 📂EventListener
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractSessionListener.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractTestSessionListener.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AddRequestFormatsListener.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DebugHandlersListener.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DisallowRobotsIndexingListener.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DumpListener.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ErrorListener.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ExceptionListener.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FragmentListener.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜LocaleAwareListener.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜LocaleListener.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ProfilerListener.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ResponseListener.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RouterListener.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SaveSessionListener.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SessionListener.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜StreamedResponseListener.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SurrogateListener.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TestSessionListener.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TranslatorListener.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜ValidateRequestListener.php
 ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AccessDeniedHttpException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜BadRequestHttpException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConflictHttpException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ControllerDoesNotReturnResponseException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜GoneHttpException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜HttpException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜HttpExceptionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜LengthRequiredHttpException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MethodNotAllowedHttpException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜NotAcceptableHttpException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜NotFoundHttpException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PreconditionFailedHttpException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PreconditionRequiredHttpException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ServiceUnavailableHttpException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TooManyRequestsHttpException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜UnauthorizedHttpException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜UnprocessableEntityHttpException.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜UnsupportedMediaTypeHttpException.php
 ┃ ┃ ┃ ┃ ┣ 📂Fragment
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractSurrogateFragmentRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜EsiFragmentRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FragmentHandler.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FragmentRendererInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜HIncludeFragmentRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InlineFragmentRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RoutableFragmentRenderer.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜SsiFragmentRenderer.php
 ┃ ┃ ┃ ┃ ┣ 📂HttpCache
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractSurrogate.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Esi.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜HttpCache.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ResponseCacheStrategy.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ResponseCacheStrategyInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Ssi.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Store.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜StoreInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SubRequestHandler.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜SurrogateInterface.php
 ┃ ┃ ┃ ┃ ┣ 📂Log
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DebugLoggerInterface.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Logger.php
 ┃ ┃ ┃ ┃ ┣ 📂Profiler
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FileProfilerStorage.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Profile.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Profiler.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜ProfilerStorageInterface.php
 ┃ ┃ ┃ ┃ ┣ 📂Resources
 ┃ ┃ ┃ ┃ ┃ ┗ 📜welcome.html.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜Client.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜HttpClientKernel.php
 ┃ ┃ ┃ ┃ ┣ 📜HttpKernel.php
 ┃ ┃ ┃ ┃ ┣ 📜HttpKernelBrowser.php
 ┃ ┃ ┃ ┃ ┣ 📜HttpKernelInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜Kernel.php
 ┃ ┃ ┃ ┃ ┣ 📜KernelEvents.php
 ┃ ┃ ┃ ┃ ┣ 📜KernelInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜README.md
 ┃ ┃ ┃ ┃ ┣ 📜RebootableInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜TerminableInterface.php
 ┃ ┃ ┃ ┃ ┗ 📜UriSigner.php
 ┃ ┃ ┃ ┣ 📂mime
 ┃ ┃ ┃ ┃ ┣ 📂Crypto
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DkimOptions.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DkimSigner.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SMime.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SMimeEncrypter.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜SMimeSigner.php
 ┃ ┃ ┃ ┃ ┣ 📂DependencyInjection
 ┃ ┃ ┃ ┃ ┃ ┗ 📜AddMimeTypeGuesserPass.php
 ┃ ┃ ┃ ┃ ┣ 📂Encoder
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AddressEncoderInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Base64ContentEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Base64Encoder.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Base64MimeHeaderEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ContentEncoderInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜EightBitContentEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜EncoderInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜IdnAddressEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MimeHeaderEncoderInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜QpContentEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜QpEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜QpMimeHeaderEncoder.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Rfc2231Encoder.php
 ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AddressEncoderException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ExceptionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidArgumentException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜LogicException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RfcComplianceException.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜RuntimeException.php
 ┃ ┃ ┃ ┃ ┣ 📂Header
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractHeader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DateHeader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜HeaderInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Headers.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜IdentificationHeader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MailboxHeader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MailboxListHeader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ParameterizedHeader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PathHeader.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜UnstructuredHeader.php
 ┃ ┃ ┃ ┃ ┣ 📂Part
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Multipart
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AlternativePart.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DigestPart.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FormDataPart.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MixedPart.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RelatedPart.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractMultipartPart.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractPart.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DataPart.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MessagePart.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SMimePart.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TextPart.php
 ┃ ┃ ┃ ┃ ┣ 📂Resources
 ┃ ┃ ┃ ┃ ┃ ┗ 📂bin
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜update_mime_types.php
 ┃ ┃ ┃ ┃ ┣ 📂Test
 ┃ ┃ ┃ ┃ ┃ ┗ 📂Constraint
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EmailAddressContains.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EmailAttachmentCount.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EmailHasHeader.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EmailHeaderSame.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EmailHtmlBodyContains.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜EmailTextBodyContains.php
 ┃ ┃ ┃ ┃ ┣ 📜Address.php
 ┃ ┃ ┃ ┃ ┣ 📜BodyRendererInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜CharacterStream.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜Email.php
 ┃ ┃ ┃ ┃ ┣ 📜FileBinaryMimeTypeGuesser.php
 ┃ ┃ ┃ ┃ ┣ 📜FileinfoMimeTypeGuesser.php
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜Message.php
 ┃ ┃ ┃ ┃ ┣ 📜MessageConverter.php
 ┃ ┃ ┃ ┃ ┣ 📜MimeTypeGuesserInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜MimeTypes.php
 ┃ ┃ ┃ ┃ ┣ 📜MimeTypesInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜RawMessage.php
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂polyfill-ctype
 ┃ ┃ ┃ ┃ ┣ 📜bootstrap.php
 ┃ ┃ ┃ ┃ ┣ 📜bootstrap80.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜Ctype.php
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂polyfill-iconv
 ┃ ┃ ┃ ┃ ┣ 📂Resources
 ┃ ┃ ┃ ┃ ┃ ┗ 📂charset
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.big5.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp037.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp1006.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp1026.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp424.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp437.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp500.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp737.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp775.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp850.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp852.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp855.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp856.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp857.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp860.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp861.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp862.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp863.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp864.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp865.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp866.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp869.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp874.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp875.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp932.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp936.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp949.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.cp950.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.iso-8859-1.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.iso-8859-10.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.iso-8859-11.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.iso-8859-13.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.iso-8859-14.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.iso-8859-15.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.iso-8859-16.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.iso-8859-2.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.iso-8859-3.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.iso-8859-4.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.iso-8859-5.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.iso-8859-6.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.iso-8859-7.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.iso-8859-8.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.iso-8859-9.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.koi8-r.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.koi8-u.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.us-ascii.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.windows-1250.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.windows-1251.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.windows-1252.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.windows-1253.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.windows-1254.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.windows-1255.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.windows-1256.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.windows-1257.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜from.windows-1258.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜translit.php
 ┃ ┃ ┃ ┃ ┣ 📜bootstrap.php
 ┃ ┃ ┃ ┃ ┣ 📜bootstrap80.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜Iconv.php
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂polyfill-intl-idn
 ┃ ┃ ┃ ┃ ┣ 📂Resources
 ┃ ┃ ┃ ┃ ┃ ┗ 📂unidata
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜deviation.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜disallowed.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DisallowedRanges.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜disallowed_STD3_mapped.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜disallowed_STD3_valid.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ignored.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜mapped.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Regex.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜virama.php
 ┃ ┃ ┃ ┃ ┣ 📜bootstrap.php
 ┃ ┃ ┃ ┃ ┣ 📜bootstrap80.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜Idn.php
 ┃ ┃ ┃ ┃ ┣ 📜Info.php
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂polyfill-intl-normalizer
 ┃ ┃ ┃ ┃ ┣ 📂Resources
 ┃ ┃ ┃ ┃ ┃ ┣ 📂stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Normalizer.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📂unidata
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜canonicalComposition.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜canonicalDecomposition.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜combiningClass.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜compatibilityDecomposition.php
 ┃ ┃ ┃ ┃ ┣ 📜bootstrap.php
 ┃ ┃ ┃ ┃ ┣ 📜bootstrap80.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜Normalizer.php
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂polyfill-mbstring
 ┃ ┃ ┃ ┃ ┣ 📂Resources
 ┃ ┃ ┃ ┃ ┃ ┗ 📂unidata
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜lowerCase.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜titleCaseRegexp.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜upperCase.php
 ┃ ┃ ┃ ┃ ┣ 📜bootstrap.php
 ┃ ┃ ┃ ┃ ┣ 📜bootstrap80.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜Mbstring.php
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂polyfill-php72
 ┃ ┃ ┃ ┃ ┣ 📜bootstrap.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜Php72.php
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂polyfill-php73
 ┃ ┃ ┃ ┃ ┣ 📂Resources
 ┃ ┃ ┃ ┃ ┃ ┗ 📂stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜JsonException.php
 ┃ ┃ ┃ ┃ ┣ 📜bootstrap.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜Php73.php
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂polyfill-php80
 ┃ ┃ ┃ ┃ ┣ 📂Resources
 ┃ ┃ ┃ ┃ ┃ ┗ 📂stubs
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Attribute.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhpToken.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Stringable.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜UnhandledMatchError.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ValueError.php
 ┃ ┃ ┃ ┃ ┣ 📜bootstrap.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜Php80.php
 ┃ ┃ ┃ ┃ ┣ 📜PhpToken.php
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂process
 ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ExceptionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidArgumentException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜LogicException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ProcessFailedException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ProcessSignaledException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ProcessTimedOutException.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜RuntimeException.php
 ┃ ┃ ┃ ┃ ┣ 📂Pipes
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractPipes.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PipesInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜UnixPipes.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜WindowsPipes.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜ExecutableFinder.php
 ┃ ┃ ┃ ┃ ┣ 📜InputStream.php
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜PhpExecutableFinder.php
 ┃ ┃ ┃ ┃ ┣ 📜PhpProcess.php
 ┃ ┃ ┃ ┃ ┣ 📜Process.php
 ┃ ┃ ┃ ┃ ┣ 📜ProcessUtils.php
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┃ ┣ 📂routing
 ┃ ┃ ┃ ┃ ┣ 📂Annotation
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Route.php
 ┃ ┃ ┃ ┃ ┣ 📂DependencyInjection
 ┃ ┃ ┃ ┃ ┃ ┗ 📜RoutingResolverPass.php
 ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ExceptionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidParameterException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MethodNotAllowedException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MissingMandatoryParametersException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜NoConfigurationException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ResourceNotFoundException.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜RouteNotFoundException.php
 ┃ ┃ ┃ ┃ ┣ 📂Generator
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Dumper
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompiledUrlGeneratorDumper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GeneratorDumper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜GeneratorDumperInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜PhpGeneratorDumper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CompiledUrlGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConfigurableRequirementsInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜UrlGenerator.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜UrlGeneratorInterface.php
 ┃ ┃ ┃ ┃ ┣ 📂Loader
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Configurator
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Traits
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AddTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RouteTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CollectionConfigurator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ImportConfigurator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RouteConfigurator.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜RoutingConfigurator.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂DependencyInjection
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ServiceRouterLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂schema
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📂routing
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜routing-1.0.xsd
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AnnotationClassLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AnnotationDirectoryLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AnnotationFileLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ClosureLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ContainerLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DirectoryLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜GlobFileLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ObjectLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ObjectRouteLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PhpFileLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜XmlFileLoader.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜YamlFileLoader.php
 ┃ ┃ ┃ ┃ ┣ 📂Matcher
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Dumper
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompiledUrlMatcherDumper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CompiledUrlMatcherTrait.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MatcherDumper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜MatcherDumperInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PhpMatcherDumper.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜StaticPrefixCollection.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CompiledUrlMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RedirectableUrlMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RedirectableUrlMatcherInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RequestMatcherInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TraceableUrlMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜UrlMatcher.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜UrlMatcherInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜CompiledRoute.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜README.md
 ┃ ┃ ┃ ┃ ┣ 📜RequestContext.php
 ┃ ┃ ┃ ┃ ┣ 📜RequestContextAwareInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜Route.php
 ┃ ┃ ┃ ┃ ┣ 📜RouteCollection.php
 ┃ ┃ ┃ ┃ ┣ 📜RouteCollectionBuilder.php
 ┃ ┃ ┃ ┃ ┣ 📜RouteCompiler.php
 ┃ ┃ ┃ ┃ ┣ 📜RouteCompilerInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜Router.php
 ┃ ┃ ┃ ┃ ┗ 📜RouterInterface.php
 ┃ ┃ ┃ ┣ 📂service-contracts
 ┃ ┃ ┃ ┃ ┣ 📂Attribute
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Required.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜SubscribedService.php
 ┃ ┃ ┃ ┃ ┣ 📂Test
 ┃ ┃ ┃ ┃ ┃ ┗ 📜ServiceLocatorTest.php
 ┃ ┃ ┃ ┃ ┣ 📜.gitignore
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜README.md
 ┃ ┃ ┃ ┃ ┣ 📜ResetInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜ServiceLocatorTrait.php
 ┃ ┃ ┃ ┃ ┣ 📜ServiceProviderInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜ServiceSubscriberInterface.php
 ┃ ┃ ┃ ┃ ┗ 📜ServiceSubscriberTrait.php
 ┃ ┃ ┃ ┣ 📂translation
 ┃ ┃ ┃ ┃ ┣ 📂Catalogue
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractOperation.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MergeOperation.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜OperationInterface.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TargetOperation.php
 ┃ ┃ ┃ ┃ ┣ 📂Command
 ┃ ┃ ┃ ┃ ┃ ┗ 📜XliffLintCommand.php
 ┃ ┃ ┃ ┃ ┣ 📂DataCollector
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TranslationDataCollector.php
 ┃ ┃ ┃ ┃ ┣ 📂DependencyInjection
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TranslationDumperPass.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TranslationExtractorPass.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TranslatorPass.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TranslatorPathsPass.php
 ┃ ┃ ┃ ┃ ┣ 📂Dumper
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CsvFileDumper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DumperInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FileDumper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜IcuResFileDumper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜IniFileDumper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜JsonFileDumper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MoFileDumper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PhpFileDumper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PoFileDumper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜QtFileDumper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜XliffFileDumper.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜YamlFileDumper.php
 ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ExceptionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidArgumentException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidResourceException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜LogicException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜NotFoundResourceException.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜RuntimeException.php
 ┃ ┃ ┃ ┃ ┣ 📂Extractor
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractFileExtractor.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ChainExtractor.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ExtractorInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PhpExtractor.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜PhpStringTokenParser.php
 ┃ ┃ ┃ ┃ ┣ 📂Formatter
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ChoiceMessageFormatterInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜IntlFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜IntlFormatterInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MessageFormatter.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜MessageFormatterInterface.php
 ┃ ┃ ┃ ┃ ┣ 📂Loader
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CsvFileLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FileLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜IcuDatFileLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜IcuResFileLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜IniFileLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜JsonFileLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜LoaderInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MoFileLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PhpFileLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PoFileLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜QtFileLoader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜XliffFileLoader.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜YamlFileLoader.php
 ┃ ┃ ┃ ┃ ┣ 📂Reader
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TranslationReader.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TranslationReaderInterface.php
 ┃ ┃ ┃ ┃ ┣ 📂Resources
 ┃ ┃ ┃ ┃ ┃ ┣ 📂bin
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜translation-status.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂data
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜parents.json
 ┃ ┃ ┃ ┃ ┃ ┗ 📂schemas
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜xliff-core-1.2-strict.xsd
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜xliff-core-2.0.xsd
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜xml.xsd
 ┃ ┃ ┃ ┃ ┣ 📂Util
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayConverter.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜XliffUtils.php
 ┃ ┃ ┃ ┃ ┣ 📂Writer
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TranslationWriter.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TranslationWriterInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜DataCollectorTranslator.php
 ┃ ┃ ┃ ┃ ┣ 📜IdentityTranslator.php
 ┃ ┃ ┃ ┃ ┣ 📜Interval.php
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜LoggingTranslator.php
 ┃ ┃ ┃ ┃ ┣ 📜MessageCatalogue.php
 ┃ ┃ ┃ ┃ ┣ 📜MessageCatalogueInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜MessageSelector.php
 ┃ ┃ ┃ ┃ ┣ 📜MetadataAwareInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜PluralizationRules.php
 ┃ ┃ ┃ ┃ ┣ 📜README.md
 ┃ ┃ ┃ ┃ ┣ 📜Translator.php
 ┃ ┃ ┃ ┃ ┣ 📜TranslatorBagInterface.php
 ┃ ┃ ┃ ┃ ┗ 📜TranslatorInterface.php
 ┃ ┃ ┃ ┣ 📂translation-contracts
 ┃ ┃ ┃ ┃ ┣ 📂Test
 ┃ ┃ ┃ ┃ ┃ ┗ 📜TranslatorTest.php
 ┃ ┃ ┃ ┃ ┣ 📜.gitignore
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜LocaleAwareInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜README.md
 ┃ ┃ ┃ ┃ ┣ 📜TranslatableInterface.php
 ┃ ┃ ┃ ┃ ┣ 📜TranslatorInterface.php
 ┃ ┃ ┃ ┃ ┗ 📜TranslatorTrait.php
 ┃ ┃ ┃ ┗ 📂var-dumper
 ┃ ┃ ┃ ┃ ┣ 📂Caster
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AmqpCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ArgsStub.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Caster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ClassStub.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ConstStub.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CutArrayStub.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CutStub.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DateCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DoctrineCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DOMCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DsCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DsPairStub.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜EnumStub.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ExceptionCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜FrameStub.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜GmpCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ImagineCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ImgStub.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜IntlCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜LinkStub.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MemcachedCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜MysqliCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PdoCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜PgSqlCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ProxyManagerCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜RedisCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ReflectionCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ResourceCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SplCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜StubCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜SymfonyCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TraceStub.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜UuidCaster.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜XmlReaderCaster.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜XmlResourceCaster.php
 ┃ ┃ ┃ ┃ ┣ 📂Cloner
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractCloner.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ClonerInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Cursor.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Data.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DumperInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Stub.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜VarCloner.php
 ┃ ┃ ┃ ┃ ┣ 📂Command
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Descriptor
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CliDescriptor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DumpDescriptorInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜HtmlDescriptor.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜ServerDumpCommand.php
 ┃ ┃ ┃ ┃ ┣ 📂Dumper
 ┃ ┃ ┃ ┃ ┃ ┣ 📂ContextProvider
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜CliContextProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ContextProviderInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜RequestContextProvider.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜SourceContextProvider.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractDumper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜CliDumper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜ContextualizedDumper.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜DataDumperInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜HtmlDumper.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜ServerDumper.php
 ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┗ 📜ThrowingCasterException.php
 ┃ ┃ ┃ ┃ ┣ 📂Resources
 ┃ ┃ ┃ ┃ ┃ ┣ 📂bin
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜var-dump-server
 ┃ ┃ ┃ ┃ ┃ ┣ 📂css
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜htmlDescriptor.css
 ┃ ┃ ┃ ┃ ┃ ┣ 📂functions
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜dump.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📂js
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜htmlDescriptor.js
 ┃ ┃ ┃ ┃ ┣ 📂Server
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Connection.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜DumpServer.php
 ┃ ┃ ┃ ┃ ┣ 📂Test
 ┃ ┃ ┃ ┃ ┃ ┗ 📜VarDumperTestTrait.php
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜README.md
 ┃ ┃ ┃ ┃ ┗ 📜VarDumper.php
 ┃ ┃ ┣ 📂theseer
 ┃ ┃ ┃ ┗ 📂tokenizer
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Exception.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜NamespaceUri.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜NamespaceUriException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Token.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TokenCollection.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜TokenCollectionException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Tokenizer.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜XMLSerializer.php
 ┃ ┃ ┃ ┃ ┣ 📜.php_cs.dist
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜composer.lock
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┣ 📂tijsverkoyen
 ┃ ┃ ┃ ┗ 📂css-to-inline-styles
 ┃ ┃ ┃ ┃ ┣ 📂.github
 ┃ ┃ ┃ ┃ ┃ ┗ 📂workflows
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ci.yml
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Css
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Property
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Processor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Property.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Rule
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Processor.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Rule.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Processor.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜CssToInlineStyles.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┗ 📜LICENSE.md
 ┃ ┃ ┣ 📂vlucas
 ┃ ┃ ┃ ┗ 📂phpdotenv
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Environment
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📂Adapter
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AdapterInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ApacheAdapter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ArrayAdapter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜EnvConstAdapter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜PutenvAdapter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ServerConstAdapter.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜AbstractVariables.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DotenvFactory.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜DotenvVariables.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜FactoryInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜VariablesInterface.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Exception
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜ExceptionInterface.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidFileException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidPathException.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ValidationException.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📂Regex
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Error.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Regex.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ 📜Result.php
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜Success.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Dotenv.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Lines.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Loader.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Parser.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Validator.php
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┗ 📜LICENSE
 ┃ ┃ ┣ 📂webmozart
 ┃ ┃ ┃ ┗ 📂assert
 ┃ ┃ ┃ ┃ ┣ 📂.github
 ┃ ┃ ┃ ┃ ┃ ┗ 📂workflows
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ 📜ci.yaml
 ┃ ┃ ┃ ┃ ┣ 📂src
 ┃ ┃ ┃ ┃ ┃ ┣ 📜Assert.php
 ┃ ┃ ┃ ┃ ┃ ┣ 📜InvalidArgumentException.php
 ┃ ┃ ┃ ┃ ┃ ┗ 📜Mixin.php
 ┃ ┃ ┃ ┃ ┣ 📜.editorconfig
 ┃ ┃ ┃ ┃ ┣ 📜.php_cs
 ┃ ┃ ┃ ┃ ┣ 📜CHANGELOG.md
 ┃ ┃ ┃ ┃ ┣ 📜composer.json
 ┃ ┃ ┃ ┃ ┣ 📜LICENSE
 ┃ ┃ ┃ ┃ ┣ 📜psalm.xml
 ┃ ┃ ┃ ┃ ┗ 📜README.md
 ┃ ┃ ┗ 📜autoload.php
 ┃ ┣ 📜.editorconfig
 ┃ ┣ 📜.env
 ┃ ┣ 📜.env.example
 ┃ ┣ 📜.gitattributes
 ┃ ┣ 📜.gitignore
 ┃ ┣ 📜.styleci.yml
 ┃ ┣ 📜artisan
 ┃ ┣ 📜composer.json
 ┃ ┣ 📜composer.lock
 ┃ ┣ 📜package-lock.json
 ┃ ┣ 📜package.json
 ┃ ┣ 📜phpunit.xml
 ┃ ┣ 📜README.md
 ┃ ┣ 📜server.php
 ┃ ┗ 📜webpack.mix.js

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[British Software Development](https://www.britishsoftware.co)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- [UserInsights](https://userinsights.com)
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)
- [User10](https://user10.com)
- [Soumettre.fr](https://soumettre.fr/)
- [CodeBrisk](https://codebrisk.com)
- [1Forge](https://1forge.com)
- [TECPRESSO](https://tecpresso.co.jp/)
- [Runtime Converter](http://runtimeconverter.com/)
- [WebL'Agence](https://weblagence.com/)
- [Invoice Ninja](https://www.invoiceninja.com)
- [iMi digital](https://www.imi-digital.de/)
- [Earthlink](https://www.earthlink.ro/)
- [Steadfast Collective](https://steadfastcollective.com/)
- [We Are The Robots Inc.](https://watr.mx/)
- [Understand.io](https://www.understand.io/)
- [Abdel Elrafa](https://abdelelrafa.com)
- [Hyper Host](https://hyper.host)
- [Appoly](https://www.appoly.co.uk)
- [OP.GG](https://op.gg)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
