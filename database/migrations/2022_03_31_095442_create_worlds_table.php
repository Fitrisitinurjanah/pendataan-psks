<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worlds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kecamatan');
            $table->string('kelurahan');
            $table->string('nama');
            $table->string('alamat');
            $table->integer('tahun');
            $table->integer('jenis');
            $table->integer('organisasi');
            $table->integer('perusahaan');
            $table->integer('masyarakat');
            $table->integer('jumlah');
            $table->string('jaringan');
            $table->unsignedBigInteger('identity_id');
            $table->foreign('identity_id')->references('id')->on('identities');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worlds');
    }
}
