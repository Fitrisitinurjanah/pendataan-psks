<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfesionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profesionals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kecamatan');
            $table->string('kelurahan');
            $table->string('nama');
            $table->string('alamat');
            $table->integer('jenis_kelamin');
            $table->integer('usia');
            $table->integer('pendidikan');
            $table->integer('pekerjaan');
            $table->string('kegiatan');
            $table->string('nomor');
            $table->date('tanggal');
            $table->string('jabatan');
            $table->unsignedBigInteger('identity_id');
            $table->foreign('identity_id')->references('id')->on('identities');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profesionals');
    }
}
