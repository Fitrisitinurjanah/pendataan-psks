<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWksbmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wksbms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kecamatan');
            $table->string('kelurahan');
            $table->string('nama');
            $table->string('alamat');
            $table->integer('tahun');
            $table->string('unsur');
            $table->string('kegiatan');
            $table->unsignedBigInteger('identity_id');
            $table->foreign('identity_id')->references('id')->on('identities');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wksbms');
    }
}
