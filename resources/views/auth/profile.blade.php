@extends('layout.master')
@section('judul')
    Profile
@endsection

@section('content')
    <h1 class="h3 mb-4 text-gray-800">
        <h5 class="card-title text-primary">Wellcome Fitri Siti Nurjanah! 🎉</h5>
    </h1>
{{-- gambar --}}
    <div class="row">
        <div class="col">
            <div class="card" style="width: 18rem;">
                <img src="{{asset('img/fitri.png')}}" class="card-img-top" alt="...">

              </div>
        </div>


        {{-- identitas --}}
        <form method="POST" action="{{ route('profile.update') }}">
            @method('patch')
            @csrf

            <div class="form-group row mb-5">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', $user->name) }}" autocomplete="name" autofocus>

                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row mb-5">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email', $user->email) }}" autocomplete="email">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        Update Profile
                    </button>
                </div>
            </div>
        </form>
    </div>


@endsection
