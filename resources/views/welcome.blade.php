<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>KTHG - Pendataan PSKS</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    <link href="{{ asset('img/logo.png') }}" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500&family=Jost:wght@500;600;700&display=swap"
        rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="{{ asset('page/lib/animate/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('page/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{ asset('page/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="{{ asset('page/css/style.css') }}" rel="stylesheet">
</head>

<body data-bs-spy="scroll" data-bs-target=".navbar" data-bs-offset="51">
    <div class="container-xxl bg-white p-0">
        <!-- Spinner Start -->
        <div id="spinner"
            class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-grow text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <!-- Spinner End -->


        <!-- Navbar & Hero Start -->
        <div class="container-xxl position-relative p-0" id="home">
            <nav class="navbar navbar-expand-lg navbar-light px-4 px-lg-5 py-3 py-lg-0">
                <a href="" class="navbar-brand p-0">
                    <h1 class="m-0">haur galur</h1>
                    <!-- <img src="img/logo.png" alt="Logo"> -->
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                    <span class="fa fa-bars"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="navbar-nav mx-auto py-0">
                        <a href="#home" class="nav-item nav-link active">Home</a>
                        <a href="#about" class="nav-item nav-link">About</a>
                        <a href="#contact" class="nav-item nav-link">Contact</a>
                    </div>
                    @if (Route::has('login'))
                    <div class="top-right links">
                        @auth
                            <a href="{{ url('/home') }}" class="btn btn-primary-gradient rounded-pill py-2 px-4 ms-3 d-none d-lg-block">Home</a>
                        @else
                            <a href="{{ route('login') }}" class="btn btn-primary-gradient rounded-pill py-2 px-4 ms-3 d-none d-lg-block">Login</a>
                        @endauth
                    </div>
                    @endif
                </div>
            </nav>

            <div class="container-xxl bg-primary hero-header">
                <div class="container px-lg-5">
                    <div class="row g-5">
                        <div class="col-lg-8 text-center text-lg-start">
                            <h1 class="text-white mb-4 animated slideInDown">Data sudah menjadi kebutuhan penting dalam segala aspek
                            </h1>
                            <p class="text-white pb-3 animated slideInDown">Karang Taruna Haur Galur pada tahun 2022 akan melaksanakan
                                pendataan PSKS dan PPKS demi mendukung penyelenggaraan kesejahteraan sosial bagi masyarakat Indonesia.
                                Data PSKS dan PPKS adalah sumber untuk menyelenggarakan kesejahteraan sosial dengan tepat sasaran.

                            </p>
                            <a href="https://instagram.com/karta_haurgalur"
                                class="btn btn-secondary-gradient py-sm-3 px-4 px-sm-5 rounded-pill animated slideInRight">Contact
                                Us</a>
                        </div>
                        <div class="col-lg-4 d-flex justify-content-center justify-content-lg-end wow fadeInUp"
                            data-wow-delay="0.3s">
                            <div class="owl-carousel screenshot-carousel">
                                <img class="img-fluid" src="{{ asset('img/mari.png') }}" alt="">
                                <img class="img-fluid" src="{{ asset('img/mari1.png') }}" alt="">
                                <img class="img-fluid" src="{{ asset('img/mari2.png') }}" alt="">
                                <img class="img-fluid" src="{{ asset('img/mari3.png') }}" alt="">
                                <img class="img-fluid" src="{{ asset('img/mari4.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Navbar & Hero End -->


        <!-- About Start -->
        <div class="container-xxl py-5" id="about">
            <div class="container py-5 px-lg-5">
                <div class="row g-5 align-items-center">
                    <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
                        <h5 class="text-primary-gradient fw-medium">Tentang PSKS</h5>
                        <h1 class="mb-4">PSKS</h1>
                        <p class="mb-4">Potensi dan Sumber Kesejahteraan Sosial yang selanjutnya disebut
                            PSKS adalah perseorangan, keluarga, kelompok, dan/atau masyarakat
                            yang dapat berperan serta untuk menjaga, menciptakan, mendukung,
                            dan memperkuat penyelenggaraan kesejahteraan sosial. Menurut open data jabar, jumlah
                            PSKS di Kota Bandung mencapai:</p>
                        <div class="row g-4 mb-4">
                            <div class="col-sm-6 wow fadeIn" data-wow-delay="0.5s">
                                <div class="d-flex">
                                    <i class="fa fa-cogs fa-2x text-primary-gradient flex-shrink-0 mt-1"></i>
                                    <div class="ms-3">
                                        <h2 class="mb-0" data-toggle="counter-up">360</h2>
                                        <p class="text-primary-gradient mb-0">orang</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <img class="img-fluid wow fadeInUp" data-wow-delay="0.5s" src="{{ asset('img/home.png') }}">
                    </div>
                </div>
            </div>
        </div>
        <!-- About End -->

        <!-- Contact Start -->
        <div class="container-xxl py-5" id="contact">
            <div class="container py-5 px-lg-5">
                <div class="text-center wow fadeInUp" data-wow-delay="0.1s">
                    <h5 class="text-primary-gradient fw-medium">Hubungi Kami</h5>
                    <h1 class="mb-5">Salam Someah!</h1>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-9">
                        <div class="wow fadeInUp" data-wow-delay="0.3s">
                            <p class="text-center mb-4"> ini menganal lebih jauh tentang pendataan PSKS di Kota Bandung
                                oleh Karang Taruna Haur Galur? Klik link berikut untuk menghubungi kami
                                <a href="https://instagram.com/karta_haurgalur">Klik disini!</a>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact End -->


        <!-- Footer Start -->
        <div class="container-fluid bg-primary text-light footer wow fadeIn" data-wow-delay="0.1s">
            <div class="container py-5 px-lg-5">
                <div class="row g-5">
                    <div class="col-md-6 col-lg-3">
                        <h4 class="text-white mb-4">Address</h4>
                        <p><i class="fa fa-map-marker-alt me-3"></i>Jl. Babakan Jeruk II No.35, Sukagalih, Kec. Sukajadi, Kota Bandung, Jawa Barat 40163</p>
                        <p><i class="fa fa-phone-alt me-3"></i>(022) 2019745</p>
                        <p><i class="fa fa-envelope me-3"></i>karta.haurgalur@gmail.com</p>
                        <div class="d-flex pt-2">
                            <a class="btn btn-outline-light btn-social" href="https://instagram.com/karta_haurgalur"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <h4 class="text-white mb-4">Quick Link</h4>
                        <a class="btn btn-link" href="#home">Home</a>
                        <a class="btn btn-link" href="#about">Tentang kami</a>
                        <a class="btn btn-link" href="#contact">Hubungi Kami</a>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <h4 class="text-white mb-4">Halo</h4>
                        <p>Salam kesetiakawanan sosial!</p>
                    </div>
                </div>
            </div>
            <div class="container px-lg-5">
                <div class="copyright">
                    <div class="row">
                        <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">
                            &copy; <a class="border-bottom" href="/">Haur Galur</a>, All Right Reserved.

                            <!--/*** This template is free as long as you keep the footer author’s credit link/attribution link/backlink. If you'd like to use the template without the footer author’s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
                            Designed By <a class="border-bottom" href="https://htmlcodex.com">HTML Codex</a>
                        </div>
                        <div class="col-md-6 text-center text-md-end">
                            <div class="footer-menu">
                                <a href="/">Home</a>
                                <a href="https://instagram.com/dikdikmusfar">Developer 1</a>
                                <a href="https://instagram.com/fitrisnurjannah">Developer 2</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer End -->


        <!-- Back to Top -->
        <a href="#home" class="btn btn-lg btn-lg-square back-to-top pt-2"><i class="bi bi-arrow-up text-white"></i></a>
    </div>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('page/lib/wow/wow.min.js') }}"></script>
    <script src="{{ asset('page/lib/easing/easing.min.js') }}"></script>
    <script src="{{ asset('page/lib/waypoints/waypoints.min.js') }}"></script>
    <script src="{{ asset('page/lib/counterup/counterup.min.js') }}"></script>
    <script src="{{ asset('page/lib/owlcarousel/owl.carousel.min.js') }}"></script>

    <!-- Template Javascript -->
    <script src="{{ asset('page/js/main.js') }}"></script>
</body>

</html>
