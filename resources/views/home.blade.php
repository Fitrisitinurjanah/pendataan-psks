@extends('layout.nocard')


@section('content')
    <h5 class="pb-1 mb-4">Horizontal</h5>
    <div class="row mb-5">
        <div class="card mb-3">
        <div class="row g-0">
            <div class="col-md-4">
            <img class="card-img card-img-left" src="{{ asset('img/psks.png') }}" alt="Card image" />
            </div>
            <div class="col-md-8">
            <div class="card-body">
                <h5 class="card-title">PSKS (POTENSI DAN SUMBER KESEJAHTERAAN SOSIAL)</h5>
                <p class="card-text">
                    Pengertian PSKS pada dasarnya mencakup: <br>
                    1. <strong>Kesejahteran Sosial</strong> adalah individu, kelompok, organisasi,
                    dan lembaga yang belum memiliki dan atau belum memperoleh
                    pelatihan dan atau pengembangan di berbagai aspek pembangunan
                    kesejahteraan sosial sehingga keberadaannya belum dapat
                    didayagunakan secara langsung untuk mendukung pembangunan
                    kesejahteraan sosial.<br>
                    2. <strong>Sumber Kesejahteraan Sosial</strong> adalah individu, kelompok, organisasi,
                    dan lembaga yang telah memiliki kemampuan dan atau telah
                    memperoleh pelatihan dan atau pengembangan di berbagai aspek
                    pembangunan kesejahteraan sosial sehingga keberadaannya dapat
                    didayagunakan secara langsung untuk mendukung pembangunan
                    kesejahteraan social.<br>
                    3. <strong>Potensi dan Sumber Kesejahteraan Sosial</strong> yang selanjutnya disebut
                    PSKS adalah perseorangan, keluarga, kelompok, dan/atau masyarakat
                    yang dapat berperan serta untuk menjaga, menciptakan, mendukung,
                    dan memperkuat penyelenggaraan kesejahteraan social
                </p>
                <p class="card-text"><small class="text-muted">Sumber : Kementerian Sosial</small></p>
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>
    <h5 class="pb-1 mb-4">Panduan Cara Membuat Data PSKS</h5>
    <div class="row mb-5">
        <div class="col-md-6 col-lg-4 mb-3">
            <div class="card h-100">
              <img class="card-img-top" src="{{ asset('img/1.png') }}" alt="Card image cap" />
              <div class="card-body">
                <h5 class="card-title">Tahap Pertama</h5>
                <p class="card-text">
                  Buat data <strong>Petugas Pendata</strong> dan <strong>Petugas Pengawas</strong> terlebih dahulu
                </p>
                <a href="{{ route('operator.index') }}" class="btn btn-outline-primary">Data Pendata</a>
                <a href="{{ route('coordinator.index') }}" class="btn btn-outline-primary">Data Pengawas</a>
              </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 mb-3">
            <div class="card h-100">
              <img class="card-img-top" src="{{ asset('img/2.png') }}" alt="Card image cap" />
              <div class="card-body">
                <h5 class="card-title">Tahap Kedua</h5>
                <p class="card-text">
                  Lihat kode atau buat data <strong>Pengenalan tempat</strong> terlebih dahulu
                </p>
                <a href="{{ route('place.index') }}" class="btn btn-outline-primary">Lihat data Tempat</a>
              </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 mb-3">
            <div class="card h-100">
              <img class="card-img-top" src="{{ asset('img/3.png') }}" alt="Card image cap" />
              <div class="card-body">
                <h5 class="card-title">Tahap Ketiga</h5>
                <p class="card-text">
                    Setelah kita mengetahui kode tempat selanjutnya, Lihat kode atau buat data <strong>Identitas Lembaga</strong>
                </p>
                <a href="{{ route('identity.index') }}" class="btn btn-outline-primary">Lihat data Identitas </a>
              </div>
            </div>
        </div>
    </div>
    <h5 class="pb-1 mb-4">Pilih Jenis PSKS</h5>
    <div class="row mb-5">
      <div class="col-md">
        <div class="card mb-3">
          <div class="row g-0">
            <div class="col-md-4">
              <img class="card-img card-img-left" src="{{ asset('img/b.JPG') }}" alt="Card image" />
            </div>
            <div class="col-md-8">
              <div class="card-body">
                <h5 class="card-title">Pekerja Sosial Profesional</h5>
                <p class="card-text">
                    seseorang pekerja yang
                    berprofesi pekerjaan sosial dan kepedulian dalam pekerjaan sosial
                </p>
                <a href="{{ route('profesional.create') }}" class="btn btn-outline-primary">Buat data</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md">
        <div class="card mb-3">
          <div class="row g-0">
            <div class="col-md-8">
              <div class="card-body">
                <h5 class="card-title">Pekerja Sosial Masyarakat (PSM)</h5>
                <p class="card-text">
                    warga masyarakat yang
                    mengabdi di bidang kesejahteraan sosial
                </p>
                <a href="{{ route('psm.create') }}" class="btn btn-outline-primary">Buat data</a>
              </div>
            </div>
            <div class="col-md-4">
              <img class="card-img card-img-right" src="{{ asset('img/c.JPG') }}" alt="Card image" />
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row mb-5">
      <div class="col-md">
        <div class="card mb-3">
          <div class="row g-0">
            <div class="col-md-4">
              <img class="card-img card-img-left" src="{{ asset('img/d.JPG') }}" alt="Card image" />
            </div>
            <div class="col-md-8">
              <div class="card-body">
                <h5 class="card-title">Wanita Pemimpin Kesejahteraan Sosial (WPKS)</h5>
                <p class="card-text">
                    wanita yang mampu
                    menggerakkan penyelenggaraan kesejahteraan sosial
                </p>
                <a href="{{ route('wpks.create') }}" class="btn btn-outline-primary">Buat data</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md">
        <div class="card mb-3">
          <div class="row g-0">
            <div class="col-md-8">
              <div class="card-body">
                <h5 class="card-title">Penyuluh Sosial</h5>
                <p class="card-text">
                  Terdapat penyuluh Sosial Fungsional dan Penyuluh Sosial Masyarakat
                </p>
                <a href="{{ route('extension.create') }}" class="btn btn-outline-primary">Buat data</a>
              </div>
            </div>
            <div class="col-md-4">
              <img class="card-img card-img-right" src="{{ asset('img/e.JPG') }}" alt="Card image" />
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row mb-5">
      <div class="col-md">
        <div class="card mb-3">
          <div class="row g-0">
            <div class="col-md-4">
              <img class="card-img card-img-left" src="{{ asset('img/f.JPG') }}" alt="Card image" />
            </div>
            <div class="col-md-8">
              <div class="card-body">
                <h5 class="card-title">Taruna Siaga Bencana</h5>
                <p class="card-text">
                    seorang relawan yang
                    berasal dari masyarakat yang aktif dalam
                    penanggulangan bencana
                </p>
                <a href="{{ route('cadet.create') }}" class="btn btn-outline-primary">Buat data</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md">
        <div class="card mb-3">
          <div class="row g-0">
            <div class="col-md-8">
              <div class="card-body">
                <h5 class="card-title">Karang Taruna</h5>
                <p class="card-text">
                    Organisasi sosial kemasyarakatan sebagai
                    wadah dan sarana pengembangan
                </p>
                <a href="{{ route('youth.create') }}" class="btn btn-outline-primary">Buat data</a>
              </div>
            </div>
            <div class="col-md-4">
              <img class="card-img card-img-right" src="{{ asset('img/g.JPG') }}" alt="Card image" />
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row mb-5">
      <div class="col-md">
        <div class="card mb-3">
          <div class="row g-0">
            <div class="col-md-4">
              <img class="card-img card-img-left" src="{{ asset('img/h.JPG') }}" alt="Card image" />
            </div>
            <div class="col-md-8">
              <div class="card-body">
                <h5 class="card-title">Wahan Kesejahteraan Sosial Berbasis Masyarakat</h5>
                <p class="card-text">
                    Sistim kerjasama pelayanan sosial
                </p>
                <a href="/wksbm" class="btn btn-outline-primary">Buat data</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md">
        <div class="card mb-3">
          <div class="row g-0">
            <div class="col-md-8">
              <div class="card-body">
                <h5 class="card-title">Lembaga Kesejahteraan Sosial</h5>
                <p class="card-text">
                    perkumpulan sosial yang melaksanakan
                    penyelenggaraan kesejahteraan sosial
                </p>
                <a href="{{ route('lks.create') }}" class="btn btn-outline-primary">Buat data</a>
              </div>
            </div>
            <div class="col-md-4">
              <img class="card-img card-img-right" src="{{ asset('img/i.JPG') }}" alt="Card image" />
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row mb-5">
      <div class="col-md">
        <div class="card mb-3">
          <div class="row g-0">
            <div class="col-md-4">
              <img class="card-img card-img-left" src="{{ asset('img/j.JPG') }}" alt="Card image" />
            </div>
            <div class="col-md-8">
              <div class="card-body">
                <h5 class="card-title">Lembaga Konsultasi Kesejahteraan Keluarga (LK3)</h5>
                <p class="card-text">
                    Lembaga/Organisasi yang memberikan pelayanan
                    konseling
                </p>
                <a href="{{ route('lkkk.create') }}" class="btn btn-outline-primary">Buat data</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md">
        <div class="card mb-3">
          <div class="row g-0">
            <div class="col-md-8">
              <div class="card-body">
                <h5 class="card-title">Keluarga Pioner</h5>
                <p class="card-text">
                    keluarga yang mampu mengatasi
                    masalahnya dengan cara-cara efektif
                </p>
                <a href="{{ route('family.create') }}" class="btn btn-outline-primary">Buat data</a>
              </div>
            </div>
            <div class="col-md-4">
              <img class="card-img card-img-right" src="{{ asset('img/k.JPG') }}" alt="Card image" />
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row mb-5">
      <div class="col-md">
        <div class="card mb-3">
          <div class="row g-0">
            <div class="col-md-4">
              <img class="card-img card-img-left" src="{{ asset('img/a.JPG') }}" alt="Card image" />
            </div>
            <div class="col-md-8">
              <div class="card-body">
                <h5 class="card-title">Tenaga Kesejahteraan Sosial Masyarakat (TKSK)</h5>
                <p class="card-text">
                    Tenaga inti pengendali kegiatan penyelenggaraan
                    kesejahteraan sosial
                </p>
                <a href="{{ route('tksk.create') }}" class="btn btn-outline-primary">Buat data</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md">
        <div class="card mb-3">
          <div class="row g-0">
            <div class="col-md-8">
              <div class="card-body">
                <h5 class="card-title">Dunia Usaha yang Melakukan Kesejahteraan Sosial</h5>
                <p class="card-text">
                    berpartisipasi dalam penyelenggaraan
                    kesejahteraan sosial
                </p>
                <a href="/world" class="btn btn-outline-primary">Buat data</a>
              </div>
            </div>
            <div class="col-md-4">
              <img class="card-img card-img-right" src="{{ asset('img/l.JPG') }}" alt="Card image" />
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
