
@extends('layout.master')

@section('judul')
    Membuat identitas Karang Taruna baru
@endsection
@section('content')
    <div class="card-body">
        <div class="row">
            <div class="demo-vertical-spacing demo-only-element col-md-5 ">
                <form action="/youth" method="POST" enctype="multipart/form-data">
                    @csrf
                        <div class="mb-3">
                            <label class="form-label" for="kecamatan">Kecamatan</label>
                            <input type="text" class="form-control" id="kecamatan" name="kecamatan" placeholder="Masukan kecamatan" />
                        </div>
                            @error('kecamatan')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="kelurahan">Kelurahan</label>
                            <input type="text" class="form-control" id="kelurahan" name="kelurahan" placeholder="Masukan kelurahan" />
                        </div>
                            @error('kelurahan')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="nama">Nama Lembaga</label>
                            <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan nama lembaga" />
                        </div>
                            @error('nama')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="alamat">Alamat (Jl./RT/RW)</label>
                            <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Masukan alamat lembaga" />
                        </div>
                            @error('alamat')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="tahun">Tahun Berdiri</label>
                            <input type="number" class="form-control" id="tahun" name="tahun" placeholder="Tahun Berdiri" />
                        </div>
                            @error('tahun')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="col-md-6">
                            <label class="text-dark fw-semibold">Klasifikasi</label>
                            <div class="form-check">
                                <label for="tumbuh" class="form-label">Tumbuh</label>
                                <input type="radio" class="form-check-input" id="tumbuh" name="klasifikasi" value="1">
                                @error('klasifikasi')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="berkembang" class="form-label">Berkembang</label>
                                <input type="radio" class="form-check-input" id="berkembang" name="klasifikasi" value="2">
                                @error('klasifikasi')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="maju" class="form-label">Maju</label>
                                <input type="radio" class="form-check-input" id="maju" name="klasifikasi" value="2">
                                @error('klasifikasi')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="percobaan" class="form-label">Percobaan</label>
                                <input type="radio" class="form-check-input" id="percobaan" name="klasifikasi" value="2">
                                @error('klasifikasi')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="laki">Jumlah Pengurus laki-laki</label>
                            <input type="number" class="form-control" id="laki" name="laki" placeholder="Jumlah Pengurus laki-laki" />
                        </div>
                            @error('laki')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="perempuan">Jumlah pengurus perempuan</label>
                            <input type="number" class="form-control" id="perempuan" name="perempuan" placeholder="Masukan nama responden" />
                        </div>
                            @error('perempuan')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="jumlah">Jumlah anggota</label>
                            <input type="number" class="form-control" id="jumlah" name="jumlah" placeholder="Jumlah seluruh anggota" />
                        </div>
                            @error('jumlah')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="col">
                            <label class="text-dark fw-semibold">Kegiatan Usaha Kesejahteraan Sosial</label>
                            <div class="form-check">
                                <label for="penyantunan" class="form-label">Penyantunan PMKS (anak,lansia, cacat, fakir miskin)</label>
                                <input type="radio" class="form-check-input" id="penyantunan" name="kegiatan" value="1">
                                @error('kegiatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="pendidikan" class="form-label">Pendidikan (Olah Raga & Kesenian)</label>
                                <input type="radio" class="form-check-input" id="pendidikan" name="kegiatan" value="2">
                                @error('kegiatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="pendidikan" class="form-label">Pendidikan (Olahraga & Kesenian)</label>
                                <input type="radio" class="form-check-input" id="pendidikan" name="kegiatan" value="3">
                                @error('kegiatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="kesehatan" class="form-label">Kesehatan</label>
                                <input type="radio" class="form-check-input" id="kesehatan" name="kegiatan" value="4">
                                @error('kegiatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="kematian" class="form-label">Kematian</label>
                                <input type="radio" class="form-check-input" id="kematian" name="kegiatan" value="5">
                                @error('kegiatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="pendataan" class="form-label">Pendataan</label>
                                <input type="radio" class="form-check-input" id="pendataan" name="kegiatan" value="6">
                                @error('kegiatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="lainnya" class="form-label">Lainnya</label>
                                <input type="radio" class="form-check-input" id="lainnya" name="kegiatan" value="7">
                                @error('kegiatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="identity_id">Kode Identitas Lembaga</label>
                            <select class="form-control" id="identity_id" name="identity_id">
                                @foreach ($identities as $identity)
                                   <option value="{{ $identity->id }}">{{ $identity->nama }}</option>
                                @endforeach
                             </select>                           </div>
                            @error('identity_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <button type="submit" class="btn btn-primary">Tambah</button>
                </form>
            </div>
        </div>
    </div>
@endsection
