@extends('layout.master')
@section('judul')
<div class="row">
    <div class="col">
        Data Lembaga Karang Taruna
        <div class="mt-3">
            <a href="{{ route('youth.create') }}">
                <button class="btn btn-primary" type="button">Tambah Data</button>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <label for="datepicker_start">Tanggal Pendataan</label>
        <input type="date" class="form-control mt-1" name="tgl_surat" placeholder="Contoh. 2021-11-14" id="datepicker_start">
        <div class="d-grid gap-2 d-md-block mt-2">
            <button class="btn btn-primary" type="button">PDF</button>
            <a href="{{ route('youth.export') }}"><button class="btn btn-primary" type="button"> Excel</button></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Kecamatan</th>
                    <th rowspan="2">Desa/Kelurahan</th>
                    <th rowspan="2">Nama Lembaga</th>
                    <th rowspan="2">Alamat</th>
                    <th rowspan="2">Tahun Berdiri</th>
                    <th rowspan="2">Status (kode)</th>
                    <th rowspan="2">Tipologi (kode)</th>
                    <th colspan="2">Jumlah Pengurus</th>
                    <th rowspan="2">Jumlah Anggota</th>
                    <th rowspan="2">Kegiatan Usaha Kesejahteraan Sosial (kode)</th>
                </tr>
                <tr>
                    <th>(L)</th>
                    <th>(P)</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($youths as $key => $youth)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $youth->kecamatan }}</td>
                        <td>{{ $youth->kelurahan }}</td>
                        <td><a href="/youth/{{$youth->id}}">{{ $youth->nama }}</a></td>
                        <td>{{ $youth->alamat }}</td>
                        <td>{{ $youth->tahun }}</td>
                        <td>{{ $youth->klasifikasi }}</td>
                        <td>{{ $youth->laki }}</td>
                        <td>{{ $youth->perempuan }}</td>
                        <td>{{ $youth->jumlah }}</td>
                        <td>{{ $youth->kegiatan }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="12" align="center"> No Data Recorded </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection
