
@extends('layout.master')

@section('judul')
    Perbaharui data tksk
@endsection
@section('content')
    <div class="card-body">
        <div class="row">
            <div class="demo-vertical-spacing demo-only-element col-md-5 ">
                <form action="/tksk/{{ $id->id }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                        <div class="mb-3">
                            <label class="form-label" for="kecamatan">Kecamatan</label>
                            <input type="text" class="form-control" id="kecamatan" name="kecamatan" value="{{ old('kecamatan', $id->kecamatan)}}" placeholder="Masukan kecamatan" />
                        </div>
                            @error('kecamatan')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="kelurahan">Kelurahan</label>
                            <input type="text" class="form-control" id="kelurahan" name="kelurahan" value="{{ old('kelurahan', $id->kelurahan)}}" placeholder="Masukan kelurahan" />
                        </div>
                            @error('kelurahan')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="nama">Nama</label>
                            <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', $id->nama)}}" placeholder="Masukan nama " />
                        </div>
                            @error('nama')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="alamat">Alamat (Jl./RT/RW)</label>
                            <input type="text" class="form-control" id="alamat" name="alamat" value="{{ old('alamat', $id->alamat)}}" placeholder="Masukan alamat lembaga" />
                        </div>
                            @error('alamat')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="col">
                                <label class="form-label">Jenis Kelamin</label>
                                <div class="form-check">
                                    <label for="laki" class="form-label">Laki-Laki</label>
                                    <input type="radio" class="form-check-input" id="laki" name="jenis_kelamin" value="1">
                                    @error('jenis_kelamin')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-check">
                                    <label for="perempuan" class="form-label">Perempuan</label>
                                    <input type="radio" class="form-check-input" id="perempuan" name="jenis_kelamin" value="2">
                                    @error('jenis_kelamin')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label class="form-label" for="usia">Usia</label>
                                    <input type="text" class="form-control" id="usia" name="usia" value="{{ old('usia', $id->usia)}}" placeholder="usia " />
                                </div>
                                    @error('usia')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror

                                    {{-- PENDIDIKAN --}}
                                    <div class="col">
                                        <label class="text-dark fw-semibold">Pendidikan</label>
                                        <div class="form-check">
                                            <label for="belum" class="form-label">Tidak Tamat SD</label>
                                            <input type="radio" class="form-check-input" id="belum" name="pendidikan" value="1">
                                            @error('pendidikan')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-check">
                                            <label for="sd" class="form-label">SD/MI/Sedrajat</label>
                                            <input type="radio" class="form-check-input" id="sd" name="pendidikan" value="2">
                                            @error('pendidikan')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-check">
                                            <label for="smp" class="form-label">SLTP/MTS/Sedrejat</label>
                                            <input type="radio" class="form-check-input" id="smp" name="pendidikan" value="3">
                                            @error('pendidikan')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-check">
                                            <label for="sma" class="form-label">SLTP/MA/Sedrajat</label>
                                            <input type="radio" class="form-check-input" id="sma" name="pendidikan" value="4">
                                            @error('pendidikan')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-check">
                                            <label for="diploma" class="form-label">DiplomaI/II</label>
                                            <input type="radio" class="form-check-input" id="diploma" name="pendidikan" value="5">
                                            @error('pendidikan')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-check">
                                            <label for="sarjanamuda" class="form-label">Diploma III/Sarjana Muda</label>
                                            <input type="radio" class="form-check-input" id="sarjanamuda" name="pendidikan" value="6">
                                            @error('pendidikan')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-check">
                                            <label for="sarjana" class="form-label">DIV/S1</label>
                                            <input type="radio" class="form-check-input" id="sarjana" name="pendidikan" value="7">
                                            @error('pendidikan')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-check">
                                            <label for="magister" class="form-label">S2/S3</label>
                                            <input type="radio" class="form-check-input" id="magister" name="pendidikan" value="8">
                                            @error('pendidikan')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>


                                        {{-- Pekerjaan --}}
                                        <div class="col">
                                            <label class="text-dark fw-semibold">Pekerjaan</label>
                                            <div class="form-check">
                                                <label for="pns" class="form-label">PNS Bukan Guru</label>
                                                <input type="radio" class="form-check-input" id="pns" name="pekerjaan" value="1">
                                                @error('pekerjaan')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-check">
                                                <label for="guru" class="form-label">PNS GURU</label>
                                                <input type="radio" class="form-check-input" id="guru" name="pekerjaan" value="2">
                                                @error('pekerjaan')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-check">
                                                <label for="wiraswasta" class="form-label">Wiraswasta</label>
                                                <input type="radio" class="form-check-input" id="wiraswasta" name="pekerjaan" value="3">
                                                @error('pekerjaan')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-check">
                                                <label for="petani" class="form-label">Petani</label>
                                                <input type="radio" class="form-check-input" id="petani" name="pekerjaan" value="4">
                                                @error('pekerjaan')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-check">
                                                <label for="pedagang" class="form-label">Pedagang</label>
                                                <input type="radio" class="form-check-input" id="pedagang" name="pekerjaan" value="5">
                                                @error('pekerjaan')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-check">
                                                <label for="nelayan" class="form-label">Nelayan</label>
                                                <input type="radio" class="form-check-input" id="nelayan" name="pekerjaan" value="6">
                                                @error('pekerjaan')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-check">
                                                <label for="buruh" class="form-label">Buruh</label>
                                                <input type="radio" class="form-check-input" id="buruh" name="pekerjaan" value="7">
                                                @error('pekerjaan')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-check">
                                                <label for="pensiunan" class="form-label">Purnawirawan/Pensiunan</label>
                                                <input type="radio" class="form-check-input" id="pensiunan" name="pekerjaan" value="8">
                                                @error('pekerjaan')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-check">
                                                <label for="jasa" class="form-label">Jasa</label>
                                                <input type="radio" class="form-check-input" id="jasa" name="pekerjaan" value="9">
                                                @error('pekerjaan')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-check">
                                                <label for="lainnya" class="form-label">Lainnya</label>
                                                <input type="radio" class="form-check-input" id="lainnya" name="pekerjaan" value="10">
                                                @error('pekerjaan')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>


                                            {{-- KEGIATAN --}}
                                            <div class="col">
                                                <label class="text-dark fw-semibold">kegiatan</label>
                                                <div class="form-check">
                                                    <label for="pendataan" class="form-label">Pendataan, Pendekatan, dan Perencanaan</label>
                                                    <input type="radio" class="form-check-input" id="pendataan" name="kegiatan" value="1">
                                                    @error('kegiatan')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-check">
                                                    <label for="seleksi" class="form-label">Seleksi Motivasi calon Klien</label>
                                                    <input type="radio" class="form-check-input" id="seleksi" name="kegiatan" value="2">
                                                    @error('kegiatan')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-check">
                                                    <label for="penyuluhan" class="form-label">Penyuluhan dan Bimbingan Sosial</label>
                                                    <input type="radio" class="form-check-input" id="penyuluhan" name="kegiatan" value="3">
                                                    @error('kegiatan')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-check">
                                                    <label for="bantuan" class="form-label">Bantuan Sosial dan Pendampingan</label>
                                                    <input type="radio" class="form-check-input" id="bantuan" name="kegiatan" value="4">
                                                    @error('kegiatan')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-check">
                                                    <label for="terminasi" class="form-label">Terminasi dan Tindak Lanju</label>
                                                    <input type="radio" class="form-check-input" id="terminasi" name="kegiatan" value="5">
                                                    @error('kegiatan')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label" for="kesos">kesos</label>
                                                    <input type="text" class="form-control" id="kesos" value="{{ old('kesos', $id->kesos)}}" name="kesos" placeholder="Masukan kesos" />
                                                </div>
                                                    @error('kesos')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                    <div class="mb-3">
                                                        <label class="form-label" for="non_kesos">Non_kesos</label>
                                                        <input type="text" class="form-control" id="non_kesos" value="{{ old('non_kesos', $id->non_kesos)}}" name="non_kesos" placeholder="Masukan non_kesos" />
                                                    </div>
                                                        @error('non_kesos')
                                                            <div class="alert alert-danger">{{ $message }}</div>
                                                        @enderror
                                                        <div class="mb-3">
                                                            <label class="form-label" for="identity_id" >Kode Identitas Lembaga</label>
                                                            <select class="form-control" id="identity_id" name="identity_id">
                                                                @foreach ($identities as $identity)
                                                                   <option value="{{ $identity->id }}">{{ $identity->nama }}</option>
                                                                @endforeach
                                                             </select>                                                           </div>
                                                            @error('identity_id')
                                                                <div class="alert alert-danger">{{ $message }}</div>
                                                            @enderror

                        <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
@endsection
