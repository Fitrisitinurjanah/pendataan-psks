@extends('layout.master')

@section('judul')
    Membuat Data Baru Taruna Siaga Bencana
@endsection

@section('content')
    <div class="card-body">
        <div class="row">
            <div class="demo-vertical-spacing demo-only-element col-md-5 ">
                <form action="/cadet" method="POST" enctype="multipart/form-data">
                    @csrf
                        <div class="mb-3">
                            <label class="form-label" for="kecamatan">Kecamatan</label>
                            <input type="text" class="form-control" id="kecamatan" name="kecamatan" placeholder="Masukan kecamatan" />
                        </div>
                            @error('kecamatan')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="kelurahan">Kelurahan</label>
                            <input type="text" class="form-control" id="kelurahan" name="kelurahan" placeholder="Masukan kelurahan" />
                        </div>
                            @error('kelurahan')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="nama">Nama</label>
                            <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan nama penyuluhan" />
                        </div>
                            @error('nama')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="alamat">Alamat (Jl./RT/RW)</label>
                            <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Masukan alamat penyuluhan" />
                        </div>
                            @error('alamat')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="col">
                            <label class="form-label">Jenis Kelamin</label>
                            <div class="form-check">
                                <label for="laki" class="form-label">Laki-Laki</label>
                                <input type="radio" class="form-check-input" id="laki" name="jenis_kelamin" value="1">
                                @error('jenis_kelamin')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="perempuan" class="form-label">Perempuan</label>
                                <input type="radio" class="form-check-input" id="perempuan" name="jenis_kelamin" value="2">
                                @error('jenis_kelamin')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="usia">usia </label>
                            <input type="number" class="form-control" id="usia" name="usia" placeholder="usia " />
                        </div>
                            @error('usia')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="col">
                            <label class="text-dark fw-semibold">Pendidikan tertinggi yang ditamatkan</label>
                            <div class="form-check">
                                <label for="belum" class="form-label">Tidak Tamat SD</label>
                                <input type="radio" class="form-check-input" id="belum" name="pendidikan" value="1">
                                @error('pendidikan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="sd" class="form-label">SD/MI/Sedrajat</label>
                                <input type="radio" class="form-check-input" id="sd" name="pendidikan" value="2">
                                @error('pendidikan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="smp" class="form-label">SLTP/MTS/Sedrejat</label>
                                <input type="radio" class="form-check-input" id="smp" name="pendidikan" value="3">
                                @error('pendidikan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="sma" class="form-label">SLTP/MA/Sedrajat</label>
                                <input type="radio" class="form-check-input" id="sma" name="pendidikan" value="4">
                                @error('pendidikan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="diploma" class="form-label">DiplomaI/II</label>
                                <input type="radio" class="form-check-input" id="diploma" name="pendidikan" value="5">
                                @error('pendidikan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="sarjanamuda" class="form-label">Diploma III/Sarjana Muda</label>
                                <input type="radio" class="form-check-input" id="sarjanamuda" name="pendidikan" value="6">
                                @error('pendidikan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="sarjana" class="form-label">DIV/S1</label>
                                <input type="radio" class="form-check-input" id="sarjana" name="pendidikan" value="7">
                                @error('pendidikan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="magister" class="form-label">S2/S3</label>
                                <input type="radio" class="form-check-input" id="magister" name="pendidikan" value="8">
                                @error('pendidikan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col">
                            <label class="text-dark fw-semibold">Pekerjaan</label>
                            <div class="form-check">
                                    <label for="pns" class="form-label">PNS Bukan Guru</label>
                                    <input type="radio" class="form-check-input" id="pns" name="pekerjaan" value="1">
                                    @error('pekerjaan')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                            </div>
                            <div class="form-check">
                                    <label for="guru" class="form-label">PNS GURU</label>
                                    <input type="radio" class="form-check-input" id="guru" name="pekerjaan" value="2">
                                    @error('pekerjaan')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                            </div>
                            <div class="form-check">
                                    <label for="wiraswasta" class="form-label">Wiraswasta</label>
                                    <input type="radio" class="form-check-input" id="wiraswasta" name="pekerjaan" value="3">
                                    @error('pekerjaan')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                            </div>
                            <div class="form-check">
                                    <label for="petani" class="form-label">Petani</label>
                                    <input type="radio" class="form-check-input" id="petani" name="pekerjaan" value="4">
                                    @error('pekerjaan')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                            </div>
                            <div class="form-check">
                                    <label for="pedagang" class="form-label">Pedagang</label>
                                    <input type="radio" class="form-check-input" id="pedagang" name="pekerjaan" value="5">
                                    @error('pekerjaan')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                            </div>
                            <div class="form-check">
                                    <label for="nelayan" class="form-label">Nelayan</label>
                                    <input type="radio" class="form-check-input" id="nelayan" name="pekerjaan" value="6">
                                    @error('pekerjaan')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                            </div>
                            <div class="form-check">
                                   <label for="buruh" class="form-label">Buruh</label>
                                   <input type="radio" class="form-check-input" id="buruh" name="pekerjaan" value="7">
                                    @error('pekerjaan')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                            </div>
                            <div class="form-check">
                                    <label for="pensiunan" class="form-label">Purnawirawan/Pensiunan</label>
                                    <input type="radio" class="form-check-input" id="pensiunan" name="pekerjaan" value="8">
                                    @error('pekerjaan')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                            </div>
                            <div class="form-check">
                                    <label for="jasa" class="form-label">Jasa</label>
                                    <input type="radio" class="form-check-input" id="jasa" name="pekerjaan" value="9">
                                    @error('pekerjaan')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                            </div>
                            <div class="form-check">
                                    <label for="lainnya" class="form-label">Lainnya</label>
                                    <input type="radio" class="form-check-input" id="lainnya" name="pekerjaan" value="10">
                                    @error('pekerjaan')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                            </div>
                        </div>
                        <div class="col">
                            <label class="text-dark fw-semibold">Kegiatan</label>
                            <div class="form-check">
                                <label for="pendataan" class="form-label">PENDATAAN, PENDEKATAN, DAN PERENCANAAN</label>
                                <input type="radio" class="form-check-input" id="pendataan" name="kegiatan" value="1">
                                @error('kegiatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="seleksi" class="form-label">Seleksi Motivasi calon Klien</label>
                                <input type="radio" class="form-check-input" id="seleksi" name="kegiatan" value="2">
                                @error('kegiatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="penyuluhan" class="form-label">Penyuluhan dan Bimbingan Sosial</label>
                                <input type="radio" class="form-check-input" id="penyuluhan" name="kegiatan" value="3">
                                @error('kegiatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="bantuan" class="form-label">Bantuan Sosial dan Pendampingan</label>
                                <input type="radio" class="form-check-input" id="bantuan" name="kegiatan" value="4">
                                @error('kegiatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="terminasi" class="form-label">Terminasi dan Tindak Lanjut</label>
                                <input type="radio" class="form-check-input" id="terminasi" name="kegiatan" value="5">
                                @error('kegiatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="identity_id">Kode Identitas Lembaga</label>
                            <select class="form-control" id="identity_id" name="identity_id">
                                @foreach ($identities as $identity)
                                   <option value="{{ $identity->id }}">{{ $identity->nama }}</option>
                                @endforeach
                             </select>
                        </div>
                            @error('identity_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="col">
                            <label class="text-dark fw-semibold">Pelatihan</label>
                            <div class="form-check">
                                <label for="sudah" class="form-label">Sudah</label>
                                <input type="radio" class="form-check-input" id="sudah" name="pelatihan" value="1">
                                @error('pelatihan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="belum" class="form-label">Belum</label>
                                <input type="radio" class="form-check-input" id="belum" name="pelatihan" value="2">
                                @error('pelatihan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Tambah</button>
                </form>
            </div>
        </div>
    </div>
@endsection
