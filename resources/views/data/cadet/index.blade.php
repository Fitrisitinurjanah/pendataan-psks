@extends('layout.master')
@section('judul')
<div class="row">
    <div class="col">
        Data Lembaga Taruna Siaga Bencana
        <div class="mt-3">
            <a href="{{ route('cadet.create') }}">
                <button class="btn btn-primary" type="button">Tambah Data</button>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <label for="datepicker_start">Tanggal Pendataan</label>
        <input type="date" class="form-control mt-1" name="tgl_surat" placeholder="Contoh. 2021-11-14" id="datepicker_start">
        <div class="d-grid gap-2 d-md-block mt-2">
            <button class="btn btn-primary" type="button">PDF</button>
            <a href="{{ route('cadet.export') }}"><button class="btn btn-primary" type="button">Excel</button></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kecamatan</th>
                    <th>Desa/Kelurahan</th>
                    <th>Nama</th>
                    <th>Alamat (Jl./RT/RW)</th>
                    <th>Jenis Kelamin</th>
                    <th>Usia</th>
                    <th>Pendidikan tertinggi yang ditamatkan</th>
                    <th>Pekerjaan Utama</th>
                    <th>Kegiatan Usaha Kesejahteraan Sosial (kode)</th>
                    <th>Pelatihan Penanggulangan Bencana</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($cadets as $key => $cadet)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $cadet->kecamatan }}</td>
                        <td>{{ $cadet->kelurahan }}</td>
                        <td><a href="/cadet/{{$cadet->id}}">{{ $cadet->nama }}</a></td>
                        <td>{{ $cadet->alamat }}</td>
                        <td>{{ $cadet->jenis_kelamin }}</td>
                        <td>{{ $cadet->usia }}</td>
                        <td>{{ $cadet->pendidikan }}</td>
                        <td>{{ $cadet->pekerjaan }}</td>
                        <td>{{ $cadet->kegiatan }}</td>
                        <td>{{ $cadet->pelatihan }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="11" align="center"> No Data Recorded </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection
