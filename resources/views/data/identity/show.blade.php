@extends('layout.master')
@section('judul')
    Detail Data Identitas Lembaga {{ $id->id }}
@endsection
@section('content')
    <div class="col-md-6 col-lg-4 order-2 mb-4">
        <div class="card h-100">
          <div class="card-header d-flex align-items-center justify-content-between">
            <h5 class="card-title m-0 me-2">Pemeriksaan</h5>
          </div>
          <div class="card-body">
            <ul class="p-0 m-0">
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">Nama Lembaga</small>
                    <h6 class="mb-0">{{ $id->nama }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">Alamat Lembaga</small>
                    <h6 class="mb-0">{{ $id->alamat }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">Nama Pimpinan</small>
                    <h6 class="mb-0">{{ $id->nama_pimpinan }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">Nama Responden</small>
                    <h6 class="mb-0">{{ $id->nama_responden }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">Jabatan Responden</small>
                    <h6 class="mb-0">{{ $id->jabatan }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">Cap Lembaga dan Tanda Tangan Responden</small>
                    <img src="/bukti/{{ $id->bukti }}" class="img-thumbnail" width="200px">
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">Wilayah Pendataan</small>
                    <h6 class="mb-0">{{ $id->place->kelurahan }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">Nama Petugas Pendata</small>
                    <h6 class="mb-0">{{ $id->user->name }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">Tanggal Pendataan</small>
                    <h6 class="mb-0">{{ $id->operator->tanggal }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">Nama Pengawas</small>
                    <h6 class="mb-0">{{ $id->coordinator->nama }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                    <form action="/identity/{{$id->id}}" method="post">
                        @csrf
                        <a href="/identity/{{$id->id}}/edit">
                            <button class="btn btn-primary" type="button">Ubah Data</button>
                        </a>
                        @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger">
                    </form>
              </li>
            </ul>
          </div>
        </div>
      </div>
@endsection
