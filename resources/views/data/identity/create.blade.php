
@extends('layout.master')

@section('judul')
    Membuat identitas lembaga baru
@endsection
@section('content')
    <div class="card-body">
        <div class="row">
            <div class="demo-vertical-spacing demo-only-element col-md-5 ">
                <form action="/identity" method="POST" enctype="multipart/form-data">
                    @csrf
                        <div class="mb-3">
                            <label class="form-label" for="nama">Nama Lembaga</label>
                            <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan nama lembaga" />
                        </div>
                            @error('nama')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="alamat">Alamat Lengkap Lembaga</label>
                            <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Masukan alamat lembaga" />
                        </div>
                            @error('alamat')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="rt">RT/RW/Dusun/Dll</label>
                            <input type="text" class="form-control" id="rt" name="rt" placeholder="Masukan RT/RW/Dusunn/Dll" />
                        </div>
                            @error('rt')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="nama_pimpinan">Nama Pimpinan Lembaga</label>
                            <input type="text" class="form-control" id="nama_pimpinan" name="nama_pimpinan" placeholder="Masukan nama pimpinan" />
                        </div>
                            @error('nama_pimpinan')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="nama_responden">Nama Responden</label>
                            <input type="text" class="form-control" id="nama_responden" name="nama_responden" placeholder="Masukan nama responden" />
                        </div>
                            @error('nama_responden')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="jabatan">Jabatan Responden</label>
                            <input type="text" class="form-control" id="jabatan" name="jabatan" placeholder="Masukan jabatan responden" />
                        </div>
                            @error('jabatan')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label for="bukti" class="form-label">Cap Lembaga dan Tanda Tangan Responden</label>
                            <input class="form-control" type="file" id="bukti" name="bukti" />
                        </div>
                            @error('bukti')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="place_id">Kode Pengenalan Tempat</label>
                            <select class="form-control" id="place_id" name="place_id">
                                @foreach ($places as $place)
                                   <option value="{{ $place->id }}">{{ $place->kelurahan }}</option>
                                @endforeach
                             </select>
                        </div>
                            @error('place_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="operator_id">Nama Pendata</label>
                            <select class="form-control" id="operator_id" name="operator_id">
                                @foreach ($operators as $operator)
                                   <option value="{{ $operator->id }}">{{ $operator->user->name }}</option>
                                @endforeach
                             </select>
                        </div>
                            @error('operator_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="coordinator_id">Nama Pengawas</label>
                            <select class="form-control" id="coordinator_id" name="coordinator_id">
                                @foreach ($coordinators as $coordinator)
                                   <option value="{{ $coordinator->id }}">{{ $coordinator->nama }}</option>
                                @endforeach
                             </select>
                        </div>
                            @error('coordinator_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <button type="submit" class="btn btn-primary">Tambah</button>
                </form>
            </div>
        </div>
    </div>
@endsection
