@extends('layout.master')
@section('judul')
<div class="row">
    <div class="col">
        Identitas Lembaga
        <div class="mt-3">
            <a href="{{ route('identity.create') }}">
                <button class="btn btn-primary" type="button">Tambah Data</button>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <label for="datepicker_start">Tanggal Pendataan</label>
        <input type="date" class="form-control mt-1" name="tgl_surat" placeholder="Contoh. 2021-11-14" id="datepicker_start">
        <div class="d-grid gap-2 d-md-block mt-2">
            <button class="btn btn-primary" type="button">PDF</button>
            <a href="{{ route('identity.export') }}"><button class="btn btn-primary" type="button"> Excel</button></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Lembaga </th>
                    <th>Alamat Lengkap Lembaga</th>
                    <th>RT/RW/Dusun/Dll</th>
                    <th>Nama Pimpinan Lembaga</th>
                    <th>Nama Responden</th>
                    <th>Jabatan Responden</th>
                    <th>Cap Lembaga dan Tanda Tangan Responden</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($identities as $key => $identity)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td><a href="/identity/{{$identity->id}}">{{ $identity->nama }}</a></td>
                        <td>{{ $identity->alamat }}</td>
                        <td>{{ $identity->rt }}</td>
                        <td>{{ $identity->nama_pimpinan }}</td>
                        <td>{{ $identity->nama_responden }}</td>
                        <td>{{ $identity->jabatan }}</td>
                        <td><img src="/bukti/{{ $identity->bukti }}" width="100px"></td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="8" align="center"> No Data Recorded </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection
