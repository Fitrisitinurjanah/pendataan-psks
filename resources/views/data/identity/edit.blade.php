
@extends('layout.master')

@section('judul')
    Perbaharui data identitas lembaga {{ $id->id }}
@endsection
@section('content')
    <div class="card-body">
        <div class="row">
            <div class="demo-vertical-spacing demo-only-element col-md-5 ">
                <form action="/identity/{{ $id->id }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                        <div class="mb-3">
                            <label class="form-label" for="nama">Nama Lembaga</label>
                            <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', $id->nama)}}" placeholder="Masukan nama lembaga" />
                        </div>
                            @error('nama')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="alamat">Alamat Lengkap Lembaga</label>
                            <input type="text" class="form-control" id="alamat" name="alamat" value="{{ old('alamat', $id->alamat)}}" placeholder="Masukan alamat lembaga" />
                        </div>
                            @error('alamat')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="rt">RT/RW/Dusun/Dll</label>
                            <input type="text" class="form-control" id="rt" name="rt" value="{{ old('rt', $id->rt)}}" placeholder="Masukan RT/RW/Dusunn/Dll" />
                        </div>
                            @error('rt')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="nama_pimpinan">Nama Pimpinan Lembaga</label>
                            <input type="text" class="form-control" id="nama_pimpinan" name="nama_pimpinan" value="{{ old('nama_pimpinan', $id->nama_pimpinan)}}" placeholder="Masukan nama pimpinan" />
                        </div>
                            @error('nama_pimpinan')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="nama_responden">Nama Responden</label>
                            <input type="text" class="form-control" id="nama_responden" name="nama_responden" value="{{ old('nama_responden', $id->nama_responden)}}" placeholder="Masukan nama responden" />
                        </div>
                            @error('nama_responden')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="jabatan">Jabatan Responden</label>
                            <input type="text" class="form-control" id="jabatan" name="jabatan" value="{{ old('jabatan', $id->jabatan)}}" placeholder="Masukan jabatan responden" />
                        </div>
                            @error('jabatan')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                       <div class="mb-3">
                            <label class="form-label" for="place_id">Kode Pengenalan Tempat</label>
                            <select class="form-control" id="place_id" name="place_id">
                                @foreach ($places as $place)
                                   <option value="{{ $place->id }}">{{ $place->kelurahan }}</option>
                                @endforeach
                             </select>
                        </div>
                            @error('place_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="operator_id">Nama Pendata</label>
                            <select class="form-control" id="operator_id" name="operator_id">
                                @foreach ($operators as $operator)
                                   <option value="{{ $operator->id }}">{{ $operator->user->name }}</option>
                                @endforeach
                             </select>
                        </div>
                            @error('operator_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="coordinator_id">Nama Pengawas</label>
                            <select class="form-control" id="coordinator_id" name="coordinator_id">
                                @foreach ($coordinators as $coordinator)
                                   <option value="{{ $coordinator->id }}">{{ $coordinator->nama }}</option>
                                @endforeach
                             </select>
                        </div>
                            @error('coordinator_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <button type="submit" class="btn btn-primary">Ubah</button>
                </form>
            </div>
        </div>
    </div>
@endsection
