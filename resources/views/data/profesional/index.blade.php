@extends('layout.master')
@section('judul')
<div class="row">
    <div class="col">
        Data Pekerja Sosial Profesional
        <div class="mt-3">
            <a href="{{ route('profesional.create') }}">
                <button class="btn btn-primary" type="button">Tambah Data</button>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <label for="datepicker_start">Tanggal Pendataan</label>
        <input type="date" class="form-control mt-1" name="tgl_surat" placeholder="Contoh. 2021-11-14" id="datepicker_start">
        <div class="d-grid gap-2 d-md-block mt-2">
            <button class="btn btn-primary" type="button">PDF</button>
            <a href="{{ route('profesional.export') }}"><button class="btn btn-primary" type="button"> Excel</button></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Kecamatan</th>
                    <th rowspan="2">Desa/Kelurahan</th>
                    <th rowspan="2">Nama Peksos</th>
                    <th rowspan="2">Alamat (Jl./RT/RW)</th>
                    <th rowspan="2">Jenis Kelamin</th>
                    <th rowspan="2">Usia</th>
                    <th rowspan="2">Pendidikan tertinggi yang ditamatkan</th>
                    <th rowspan="2">Pekerjaan Utama</th>
                    <th rowspan="2">Kegiatan Usaha Kesejahteraan Sosial</th>
                    <th colspan="3">Sertifikat</th>

                </tr>
                <tr>
                    <th>Nomor</th>
                    <th>Tanggal</th>
                    <th>Pejabat yang mengesahkan</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($profesionals as $key => $profesional)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $profesional->kecamatan }}</td>
                        <td>{{ $profesional->kelurahan }}</td>
                        <td><a href="/profesional/{{$profesional->id}}">{{ $profesional->nama }}</a></td>
                        <td>{{ $profesional->alamat }}</td>
                        <td>{{ $profesional->jenis_kelamin }}</td>
                        <td>{{ $profesional->usia }}</td>
                        <td>{{ $profesional->pendidikan }}</td>
                        <td>{{ $profesional->pekerjaan }}</td>
                        <td>{{ $profesional->kegiatan }}</td>
                        <td>{{ $profesional->nomor }}</td>
                        <td>{{ $profesional->tanggal }}</td>
                        <td>{{ $profesional->jabatan }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="13" align="center"> No Data Recorded </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection
