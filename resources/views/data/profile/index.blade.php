@extends('layout.master')
@section('judul')
Pengenalan Tempat
@endsection

@section('content')
<button type="button" class="btn my-btn-primary-awesome" data-toggle="modal" data-target="#exampleModal">
    Tambah Data
</button>
<h5>Filter & Report</h5>
<div class="row mb-3">
    <div class="col-md-3">
        <label for="datepicker_start">Tanggal Pendataan</label>
        <input type="date" class="form-control" name="tgl_surat" placeholder="Contoh. 2021-11-14" id="datepicker_start">
        <div class="d-grid gap-2 d-md-block mt-2">
            <button class="btn btn-primary" type="button">PDF</button>
            <button class="btn btn-primary" type="button"> Excel</button>
          </div>
    </div>

</div>
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>No </th>
                    <th>Provinsi</th>
                    <th>Kota</th>
                    <th>Kecamatan</th>
                    <th>Kelurahan</th>
                    <th>Jenis Kelamin</th>
                    <th>Status</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection
