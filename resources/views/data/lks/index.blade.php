@extends('layout.master')
@section('judul')
<div class="row">
    <div class="col">
        Data Lembaga Kesejahteraan Sosial (LKS)
        <div class="mt-3">
            <a href="{{ route('lks.create') }}">
                <button class="btn btn-primary" type="button">Tambah Data</button>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <label for="datepicker_start">Tanggal Pendataan</label>
        <input type="date" class="form-control mt-1" name="tgl_surat" placeholder="Contoh. 2021-11-14" id="datepicker_start">
        <div class="d-grid gap-2 d-md-block mt-2">
            <button class="btn btn-primary" type="button">PDF</button>
            <a href="{{ route('lks.export') }}"><button class="btn btn-primary" type="button"> Excel</button></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Kecamatan</th>
                    <th rowspan="2">Desa/Kelurahan</th>
                    <th rowspan="2">Nama Lembaga</th>
                    <th rowspan="2">Alamat</th>
                    <th rowspan="2">Tahun Berdiri</th>
                    <th rowspan="2">Status (kode)</th>
                    <th rowspan="2">Tipologi (kode)</th>
                    <th colspan="2">Jumlah Pengurus</th>
                    <th rowspan="2">Jumlah Anggota</th>
                    <th rowspan="2">Kegiatan Usaha Kesejahteraan Sosial (kode)</th>
                </tr>
                <tr>
                    <th>(L)</th>
                    <th>(P)</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($lkses as $key => $lks)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $lks->kecamatan }}</td>
                        <td>{{ $lks->kelurahan }}</td>
                        <td><a href="/lks/{{$lks->id}}">{{ $lks->nama }}</a></td>
                        <td>{{ $lks->alamat }}</td>
                        <td>{{ $lks->tahun }}</td>
                        <td>{{ $lks->status }}</td>
                        <td>{{ $lks->tipologi }}</td>
                        <td>{{ $lks->laki }}</td>
                        <td>{{ $lks->perempuan }}</td>
                        <td>{{ $lks->jumlah }}</td>
                        <td>{{ $lks->kegiatan }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="12" align="center"> No Data Recorded </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection
