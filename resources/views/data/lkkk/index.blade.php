@extends('layout.master')
@section('judul')
<div class="row">
    <div class="col">
        Data Lembaga Konsultasi Kesejahteraan Keluarga (LK3)
        <div class="mt-3">
            <a href="{{ route('lkkk.create') }}">
                <button class="btn btn-primary" type="button">Tambah Data</button>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <label for="datepicker_start">Tanggal Pendataan</label>
        <input type="date" class="form-control mt-1" name="tgl_surat" placeholder="Contoh. 2021-11-14" id="datepicker_start">
        <div class="d-grid gap-2 d-md-block mt-2">
            <button class="btn btn-primary" type="button">PDF</button>
            <a href="{{ route('lkkk.export') }}"><button class="btn btn-primary" type="button"> Excel</button></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Kecamatan</th>
                    <th rowspan="2">Desa/Kelurahan</th>
                    <th rowspan="2">Nama Lembaga</th>
                    <th rowspan="2">Alamat</th>
                    <th rowspan="2">Tahun Berdiri</th>
                    <th rowspan="2">Status (kode)</th>
                    <th rowspan="2">Tipologi (kode)</th>
                    <th colspan="2">Jumlah Pengurus</th>
                    <th rowspan="2">Jumlah Anggota</th>
                    <th rowspan="2">Kegiatan Usaha Kesejahteraan Sosial (kode)</th>
                </tr>
                <tr>
                    <th>(L)</th>
                    <th>(P)</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($lkkks as $key => $lkkk)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $lkkk->kecamatan }}</td>
                        <td>{{ $lkkk->kelurahan }}</td>
                        <td><a href="/lkkk/{{$lkkk->id}}">{{ $lkkk->nama }}</a></td>
                        <td>{{ $lkkk->alamat }}</td>
                        <td>{{ $lkkk->tahun }}</td>
                        <td>{{ $lkkk->status }}</td>
                        <td>{{ $lkkk->tipologi }}</td>
                        <td>{{ $lkkk->laki }}</td>
                        <td>{{ $lkkk->perempuan }}</td>
                        <td>{{ $lkkk->jumlah }}</td>
                        <td>{{ $lkkk->kegiatan }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="12" align="center"> No Data Recorded </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection
