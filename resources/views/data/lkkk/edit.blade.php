
@extends('layout.master')

@section('judul')
    Perbaharui data LK3
@endsection
@section('content')
    <div class="card-body">
        <div class="row">
            <div class="demo-vertical-spacing demo-only-element col-md-5 ">
                <form action="/lkkk/{{ $id->id }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                        <div class="mb-3">
                            <label class="form-label" for="kecamatan">Kecamatan</label>
                            <input type="text" class="form-control" id="kecamatan" name="kecamatan" value="{{ old('kecamatan', $id->kecamatan)}}" placeholder="Masukan kecamatan" />
                        </div>
                            @error('kecamatan')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="kelurahan">Kelurahan</label>
                            <input type="text" class="form-control" id="kelurahan" name="kelurahan" value="{{ old('kelurahan', $id->kelurahan)}}" placeholder="Masukan kelurahan" />
                        </div>
                            @error('kelurahan')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="nama">Nama Lembaga</label>
                            <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', $id->nama)}}" placeholder="Masukan nama lembaga" />
                        </div>
                            @error('nama')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="alamat">Alamat (Jl./RT/RW)</label>
                            <input type="text" class="form-control" id="alamat" name="alamat" value="{{ old('alamat', $id->alamat)}}" placeholder="Masukan alamat lembaga" />
                        </div>
                            @error('alamat')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="tahun">Tahun Berdiri</label>
                            <input type="text" class="form-control" id="tahun" name="tahun" value="{{ old('tahun', $id->tahun)}}" placeholder="Tahun Berdiri" />
                        </div>
                            @error('tahun')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="col-md-6">
                            <label class="text-dark fw-semibold">Status</label>
                            <div class="form-check">
                                <label for="badan_hukum" class="form-label">Badan Hukum</label>
                                <input type="radio" class="form-check-input" id="badan_hukum" name="status" value="1">
                                @error('status')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="tidak_badan" class="form-label">Tidak Berbadan Hukum</label>
                                <input type="radio" class="form-check-input" id="tidak_badan" name="status" value="2">
                                @error('status')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="text-dark fw-semibold">Tipologi</label>
                            <div class="form-check">
                                <label for="tipeA" class="form-label">Tipe A</label>
                                <input type="radio" class="form-check-input" id="tipeA" name="tipologi" value="1">
                                @error('tipologi')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="tipeB" class="form-label">Tipe B</label>
                                <input type="radio" class="form-check-input" id="tipeB" name="tipologi" value="2">
                                @error('tipologi')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="tipeC" class="form-label">Tipe C</label>
                                <input type="radio" class="form-check-input" id="tipeC" name="tipologi" value="3">
                                @error('tipologi')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="laki">Jumlah Pengurus laki-laki</label>
                            <input type="number" class="form-control" id="laki" name="laki" value="{{ old('laki', $id->laki)}}" placeholder="Jumlah Pengurus laki-laki" />
                        </div>
                            @error('laki')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="perempuan">Jumlah pengurus perempuan</label>
                            <input type="number" class="form-control" id="perempuan" name="perempuan" value="{{ old('perempuan', $id->perempuan)}}" placeholder="Masukan nama responden" />
                        </div>
                            @error('perempuan')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label class="form-label" for="jumlah">Jumlah anggota</label>
                            <input type="number" class="form-control" id="jumlah" name="jumlah" value="{{ old('jumlah', $id->jumlah)}}" placeholder="Jumlah seluruh anggota" />
                        </div>
                            @error('jumlah')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="col">
                            <label class="text-dark fw-semibold">Kegiatan Usaha Kesejahteraan Sosial</label>
                            <div class="form-check">
                                <label for="keagamaan" class="form-label">Keagamaan</label>
                                <input type="radio" class="form-check-input" id="keagamaan" name="kegiatan" value="1">
                                @error('kegiatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="penyantunan" class="form-label">Penyantunan PMKS (anak,lansia, cacat, fm)</label>
                                <input type="radio" class="form-check-input" id="penyantunan" name="kegiatan" value="2">
                                @error('kegiatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="pendidikan" class="form-label">Pendidikan (Olahraga & Kesenian)</label>
                                <input type="radio" class="form-check-input" id="pendidikan" name="kegiatan" value="3">
                                @error('kegiatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="uep" class="form-label">UEP (Usaha Ekonomi Produktif)</label>
                                <input type="radio" class="form-check-input" id="uep" name="kegiatan" value="4">
                                @error('kegiatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="kesehatan" class="form-label">Kesehatan</label>
                                <input type="radio" class="form-check-input" id="kesehatan" name="kegiatan" value="5">
                                @error('kegiatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="kematian" class="form-label">Kematian</label>
                                <input type="radio" class="form-check-input" id="kematian" name="kegiatan" value="6">
                                @error('kegiatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="lainnya" class="form-label">Lainnya</label>
                                <input type="radio" class="form-check-input" id="lainnya" name="kegiatan" value="7">
                                @error('kegiatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="identity_id">Kode Identitas Lembaga</label>
                            <select class="form-control" id="identity_id" name="identity_id">
                                @foreach ($identities as $identity)
                                   <option value="{{ $identity->id }}">{{ $identity->nama }}</option>
                                @endforeach
                             </select>                        </div>
                            @error('identity_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
@endsection
