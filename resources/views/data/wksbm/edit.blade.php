@extends('layout.master')

@section('judul')
    Perbaharui data WKSBM
@endsection
@section('content')
    <div class="card-body">
        <div class="row">
            <div class="demo-vertical-spacing demo-only-element col-md-5 ">
                <form action="/wksbm/{{ $id->id }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="mb-3">
                        <label class="form-label" for="kecamatan">Kecamatan</label>
                        <input type="text" class="form-control" id="kecamatan" name="kecamatan"
                            value="{{ old('kecamatan', $id->kecamatan) }}" placeholder="Masukan kecamatan" />
                    </div>
                    @error('kecamatan')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="mb-3">
                        <label class="form-label" for="kelurahan">Kelurahan</label>
                        <input type="text" class="form-control" id="kelurahan" name="kelurahan"
                            value="{{ old('kelurahan', $id->kelurahan) }}" placeholder="Masukan kelurahan" />
                    </div>
                    @error('kelurahan')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="mb-3">
                        <label class="form-label" for="nama">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama"
                            value="{{ old('nama', $id->nama) }}" placeholder="Masukan nama " />
                    </div>
                    @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="mb-3">
                        <label class="form-label" for="alamat">Alamat (Jl./RT/RW)</label>
                        <input type="text" class="form-control" id="alamat" name="alamat"
                            value="{{ old('alamat', $id->alamat) }}" placeholder="Masukan alamat lembaga" />
                    </div>
                    @error('alamat')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="mb-3">
                        <label class="form-label" for="tahun">Tahun berdiri</label>
                        <input type="number" class="form-control" id="tahun" name="tahun"
                            value="{{ old('tahun', $id->tahun) }}" placeholder="Masukan tahun lembaga" />
                    </div>
                    @error('tahun')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    {{-- unsur --}}
                    <label class="text-dark fw-semibold">Unsur yang bekerja sama</label>
                    <div class="form-check">
                        <label for="karta" class="form-label">Karang taruna</label>
                        <input type="radio" class="form-check-input" id="karta" name="unsur" value="1">
                        @error('unsur')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-check">
                        <label for="orsos" class="form-label">Orsos</label>
                        <input type="radio" class="form-check-input" id="orsos" name="unsur" value="2">
                        @error('unsur')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-check">
                        <label for="psm" class="form-label">PSM</label>
                        <input type="radio" class="form-check-input" id="psm" name="unsur" value="3">
                        @error('unsur')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-check">
                        <label for="tokoh" class="form-label">Tokoh Masyarakat</label>
                        <input type="radio" class="form-check-input" id="tokoh" name="unsur" value="4">
                        @error('unsur')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-check">
                        <label for="masyarakat" class="form-label">Kelompok masyarakat</label>
                        <input type="radio" class="form-check-input" id="masyarakat" name="unsur" value="5">
                        @error('unsur')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-check">
                        <label for="pengajian" class="form-label">Kelompok pengajian</label>
                        <input type="radio" class="form-check-input" id="pengajian" name="unsur" value="6">
                        @error('unsur')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-check">
                        <label for="lainnya" class="form-label">lainnya</label>
                        <input type="radio" class="form-check-input" id="lainnya" name="unsur" value="7">
                        @error('unsur')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    {{-- KEGIATAN --}}
                    <div class="col">
                        <label class="text-dark fw-semibold">Kegiatan yang dikerjasamakan</label>
                        <div class="form-check">
                            <label for="edukasi" class="form-label">Komunikasi informasi edukasi </label>
                            <input type="radio" class="form-check-input" id="edukasi" name="kegiatan" value="1">
                            @error('kegiatan')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-check">
                            <label for="mitra" class="form-label">Peningkatan Kemitraan</label>
                            <input type="radio" class="form-check-input" id="mitra" name="kegiatan" value="2">
                            @error('kegiatan')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-check">
                            <label for="lainnya" class="form-label">lainnya</label>
                            <input type="radio" class="form-check-input" id="lainnya" name="kegiatan" value="7">
                            @error('kegiatan')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="identity_id">Kode Identitas Lembaga</label>
                        <select class="form-control" id="identity_id" name="identity_id">
                            @foreach ($identities as $identity)
                               <option value="{{ $identity->id }}">{{ $identity->nama }}</option>
                            @endforeach
                         </select>
                    </div>
                    @error('identity_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
@endsection
