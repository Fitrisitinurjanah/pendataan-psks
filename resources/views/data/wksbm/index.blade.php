@extends('layout.master')
@section('judul')
<div class="row">
    <div class="col">
        Data Wahana Kesejahteraan Sosial Berbasis Masyarakat
        <div class="mt-3">
            <a href="{{ route('wksbm.create') }}">
                <button class="btn btn-primary" type="button">Tambah Data</button>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <label for="datepicker_start">Tanggal Pendataan</label>
        <input type="date" class="form-control mt-1" name="tgl_surat" placeholder="Contoh. 2021-11-14" id="datepicker_start">
        <div class="d-grid gap-2 d-md-block mt-2">
            <button class="btn btn-primary" type="button">PDF</button>
            <a href="{{ route('wksbm.export') }}"><button class="btn btn-primary" type="button"> Excel</button></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Kecamatan</th>
                    <th rowspan="2">Desa/Kelurahan</th>
                    <th rowspan="2">Nama </th>
                    <th rowspan="2">Alamat Sekertariat</th>
                    <th rowspan="2">Tahun Berdiri Jaringan</th>
                    <th rowspan="2">Unsur Yang Bekerja Sama</th>
                    <th rowspan="2">Kegiatan Yang Dikerjasamakan</th>
            </thead>
            <tbody>
                @forelse ($wksbms as $key => $wksbm)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $wksbm->kecamatan }}</td>
                        <td>{{ $wksbm->kelurahan }}</td>
                        <td><a href="/wksbm/{{$wksbm->id}}">{{ $wksbm->nama }}</a></td>
                        <td>{{ $wksbm->alamat }}</td>
                        <td>{{ $wksbm->tahun }}</td>
                        <td>{{ $wksbm->unsur }}</td>
                        <td>{{ $wksbm->kegiatan }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="12" align="center"> No Data Recorded </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection
