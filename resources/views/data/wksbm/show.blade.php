@extends('layout.master')
@section('judul')
    Detail Data WKSBM {{ $id->id }}
@endsection
@section('content')
    <div class="col-md-6 col-lg-4 order-2 mb-4">
        <div class="card h-100">
            <div class="card-header d-flex align-items-center justify-content-between">
                <h5 class="card-title m-0 me-2">wksbm</h5>
            </div>
            <div class="card-body">
                <ul class="p-0 m-0">
                    <li class="d-flex mb-4 pb-1">
                        <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                            <div class="me-2">
                                <small class="text-muted d-block mb-1">Nama </small>
                                <h6 class="mb-0">{{ $id->nama }}</h6>
                            </div>
                        </div>
                    </li>
                    <li class="d-flex mb-4 pb-1">
                        <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                            <div class="me-2">
                                <small class="text-muted d-block mb-1">Alamat</small>
                                <h6 class="mb-0">{{ $id->alamat }}</h6>
                            </div>
                        </div>
                    </li>
                    <li class="d-flex mb-4 pb-1">
                        <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                            <div class="me-2">
                                <small class="text-muted d-block mb-1">Kecamatan</small>
                                <h6 class="mb-0">{{ $id->kecamatan }}</h6>
                            </div>
                        </div>
                    </li>
                    <li class="d-flex mb-4 pb-1">
                        <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                            <div class="me-2">
                                <small class="text-muted d-block mb-1">Kelurahan</small>
                                <h6 class="mb-0">{{ $id->kelurahan }}</h6>
                            </div>
                        </div>
                    </li>
                    <li class="d-flex mb-4 pb-1">
                        <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                            <div class="me-2">
                                <small class="text-muted d-block mb-1">tahun</small>
                                <h6 class="mb-0">{{ $id->tahun }}</h6>
                            </div>
                        </div>
                    </li>
                    <li class="d-flex mb-4 pb-1">
                        <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                            <div class="me-2">
                                <small class="text-muted d-block mb-1">unsur</small>
                                <h6 class="mb-0">{{ $id->unsur }}</h6>
                            </div>
                        </div>
                    </li>
                    <li class="d-flex mb-4 pb-1">
                        <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                            <div class="me-2">
                                <small class="text-muted d-block mb-1">kegiatan</small>
                                <h6 class="mb-0">{{ $id->kegiatan }}</h6>
                            </div>
                        </div>
                    </li>
                    <li class="d-flex mb-4 pb-1">
                        <form action="/wksbm/{{ $id->id }}" method="post">
                            @csrf
                            <a href="/wksbm/{{ $id->id }}/edit">
                                <button class="btn btn-primary" type="button">Ubah Data</button>
                            </a>
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger">
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection
