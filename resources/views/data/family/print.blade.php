<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak</title>
</head>

<body>
    <div class="form-group">
        <p align="center"><b>Laporan Data Keluarga Pisioner</b></p>
        <table class="static" align="center" rules="all" border="1px" style="width:95%;">
            <thead>
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Kecamatan</th>
                    <th rowspan="2">Desa/Kelurahan</th>
                    <th rowspan="2">Nama </th>
                    <th rowspan="2">Alamat</th>
                    <th rowspan="2">Jenis Kelamin</th>
                    <th rowspan="2">Usia</th>
                    <th rowspan="2">Pendidikan</th>
                    <th rowspan="2">Pekerjaan</th>
                    <th rowspan="2">Kegiatan</th>
                    <th colspan="2">Pelatihan</th>
                </tr>
                <tr>
                    <th>Kesos</th>
                    <th>Non - Kesos</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($families as $key => $family)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $family->kecamatan }}</td>
                        <td>{{ $family->kelurahan }}</td>
                        <td><a href="/family/{{ $family->id }}">{{ $family->nama }}</a></td>
                        <td>{{ $family->alamat }}</td>
                        <td>{{ $family->jenis_kelamin }}</td>
                        <td>{{ $family->usia }}</td>
                        <td>{{ $family->pendidikan }}</td>
                        <td>{{ $family->pekerjaan }}</td>
                        <td>{{ $family->kegiatan }}</td>
                        <td>{{ $family->kesos }}</td>
                        <td>{{ $family->non_kesos }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="12" align="center"> No Data Recorded </td>
                    </tr>
                @endforelse
            </tbody>

        </table>
    </div>

</body>

</html>
