@extends('layout.master')
@section('judul')
    <div class="row">
        <div class="col">
            Data Keluarga Pioner
            <div class="mt-3">
                <a href="{{ route('family.create') }}">
                    <button class="btn btn-primary" type="button">Tambah Data</button>
                </a>

            </div>
        </div>
        <div class="col-md-3 mt-2">

    </div>
    <div class="col-md-3">
        <label for="datepicker_start">Tanggal Pendataan</label>
        <input type="date" class="form-control mt-1" name="tgl_surat" placeholder="Contoh. 2021-11-14" id="datepicker_start">
        <div class="d-grid gap-2 d-md-block mt-2">
            <a href="{{ route('family.print') }}" target="_blank" class="btn btn-primary">PDF</a>
            <a href="{{ route('family.export') }}"><button class="btn btn-primary" type="button"> Excel</button></a>
        </div>
    </div>
@endsection

@section('content')
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th rowspan="2">No</th>
                        <th rowspan="2">Kecamatan</th>
                        <th rowspan="2">Desa/Kelurahan</th>
                        <th rowspan="2">Nama </th>
                        <th rowspan="2">Alamat</th>
                        <th rowspan="2">Jenis Kelamin</th>
                        <th rowspan="2">Usia</th>
                        <th rowspan="2">Pendidikan</th>
                        <th rowspan="2">Pekerjaan</th>
                        <th rowspan="2">Kegiatan</th>
                        <th colspan="2">Pelatihan</th>
                    </tr>
                    <tr>
                        <th>Kesos</th>
                        <th>Non - Kesos</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($families as $key => $family)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $family->kecamatan }}</td>
                            <td>{{ $family->kelurahan }}</td>
                            <td><a href="/family/{{ $family->id }}">{{ $family->nama }}</a></td>
                            <td>{{ $family->alamat }}</td>
                            <td>{{ $family->jenis_kelamin }}</td>
                            <td>{{ $family->usia }}</td>
                            <td>{{ $family->pendidikan }}</td>
                            <td>{{ $family->pekerjaan }}</td>
                            <td>{{ $family->kegiatan }}</td>
                            <td>{{ $family->kesos }}</td>
                            <td>{{ $family->non_kesos }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="12" align="center"> No Data Recorded </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
