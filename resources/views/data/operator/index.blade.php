@extends('layout.master')
@section('judul')
<div class="row">
    <div class="col">
        Keterangan Petugas (operator)
        <div class="mt-3">
            <a href="{{ route('operator.create') }}">
                <button class="btn btn-primary" type="button">Tambah Data</button>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <label for="datepicker_start">Tanggal Pendataan</label>
        <input type="date" class="form-control mt-1" name="tgl_surat" placeholder="Contoh. 2021-11-14" id="datepicker_start">
        <div class="d-grid gap-2 d-md-block mt-2">
            <button class="btn btn-primary" type="button">PDF</button>
            <button class="btn btn-primary" type="button"> Excel</button>
        </div>
    </div>
</div>

@endsection

@section('content')
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
            <thead class>
                <tr>
                    <th>No</th>
                    <th>Nama Pengawas</th>
                    <th>Tanggal Pemeriksaan</th>
                    <th>Tanda Tangan Pengawas</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($operators as $key => $operator)
                    <tr>
                        <td>{{ $key + 1 }}</td>

                        <td>{{ $operator->user->name }}</td>
                        <td><a href="/operator/{{$operator->id}}">{{ $operator->tanggal }}</a></td>
                        <td>{{ $operator->ttd }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6"> No Data Recorded </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection
