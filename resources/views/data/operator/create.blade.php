@extends('layout.master')

@section('judul')
    Membuat Data Petugas (operator)
@endsection

@section('content')
    <div class="card-body">
        <div class="row">
            <div class="demo-vertical-spacing demo-only-element col-md-5 ">
                <form action="/operator" method="POST" enctype="multipart/form-data">
                    @csrf
                        <div class="mb-3">
                            <label class="form-label" for="nama">Nama Pendata</label>
                            <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan nama pendata" />
                        </div>
                            @error('nama')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label for="tanggal" class="col-form-label">Tanggal Pendataan</label>
                            <input class="form-control" type="date" value="2021-06-18" id="tanggal" name="tanggal" />
                        </div>
                            @error('tanggal')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label for="ttd" class="form-label">Tanda Tangan</label>
                            <input class="form-control" type="file" id="ttd" name="ttd" />
                        </div>
                            @error('ttd')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <button type="submit" class="btn btn-primary">Tambah</button>
                </form>
            </div>
        </div>
    </div>
@endsection
