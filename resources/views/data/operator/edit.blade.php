@extends('layout.master')

@section('judul')
    Perbaharui Data Petugas (operator) {{ $id->id }}
@endsection

@section('content')
    <div class="card-body">
        <div class="row">
            <div class="demo-vertical-spacing demo-only-element col-md-5 ">
                <form action="/operator/{{ $id->id }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                        
                            @error('nama')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <div class="mb-3">
                            <label for="tanggal" class="col-form-label">Tanggal Pemeriksaan</label>
                            <input class="form-control" type="date" value="{{ old('tanggal', $id->tanggal)}}" id="tanggal" name="tanggal" />
                        </div>
                            @error('tanggal')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        <button type="submit" class="btn btn-primary">Ubah</button>
                </form>
            </div>
        </div>
    </div>
@endsection
