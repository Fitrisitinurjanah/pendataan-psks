@extends('layout.master')
@section('judul')
<div class="row">
    <div class="col">
        Data WPKS
        <div class="mt-3">
            <a href="{{ route('wpks.create') }}">
                <button class="btn btn-primary" type="button">Tambah Data</button>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <label for="datepicker_start">Tanggal Pendataan</label>
        <input type="date" class="form-control mt-1" name="tgl_surat" placeholder="Contoh. 2021-11-14" id="datepicker_start">
        <div class="d-grid gap-2 d-md-block mt-2">
            <button class="btn btn-primary" type="button">PDF</button>
            <a href="{{ route('wpks.export') }}"><button class="btn btn-primary" type="button"> Excel</button></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Kecamatan</th>
                    <th rowspan="2">Desa/Kelurahan</th>
                    <th rowspan="2">Nama </th>
                    <th rowspan="2">Alamat</th>
                    <th rowspan="2">Usia</th>
                    <th rowspan="2">Pendidikan</th>
                    <th rowspan="2">Pekerjaan</th>
                    <th rowspan="2">Kegiatan</th>
                    <th colspan="2">Pelatihan</th>
                </tr>
                <tr>
                    <th>Kesos</th>
                    <th>Non - Kesos</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($wpkses as $key => $wpks)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $wpks->kecamatan }}</td>
                        <td>{{ $wpks->kelurahan }}</td>
                        <td><a href="/wpks/{{$wpks->id}}">{{ $wpks->nama }}</a></td>
                        <td>{{ $wpks->alamat }}</td>
                        <td>{{ $wpks->usia }}</td>
                        <td>{{ $wpks->pendidikan }}</td>
                        <td>{{ $wpks->pekerjaan }}</td>
                        <td>{{ $wpks->kegiatan }}</td>
                        <td>{{ $wpks->kesos }}</td>
                        <td>{{ $wpks->non_kesos }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="12" align="center"> No Data Recorded </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection
