@extends('layout.master')
@section('judul')
Data Dunia Usaha Yang Melakukan Usaha Kesejahteraan Masyarakat {{ $id->id }}
@endsection
@section('content')
    <div class="col-md-6 col-lg-4 order-2 mb-4">
        <div class="card h-100">
          <div class="card-header d-flex align-items-center justify-content-between">
            <h5 class="card-title m-0 me-2">World</h5>
          </div>
          <div class="card-body">
            <ul class="p-0 m-0">
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">Nama </small>
                    <h6 class="mb-0">{{ $id->nama }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">Alamat</small>
                    <h6 class="mb-0">{{ $id->alamat }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">kecamatan</small>
                    <h6 class="mb-0">{{ $id->kecamatan }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">Kelurahan</small>
                    <h6 class="mb-0">{{ $id->kelurahan }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">tahun</small>
                    <h6 class="mb-0">{{ $id->tahun }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">Jenis</small>
                    <h6 class="mb-0">{{ $id->jenis }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">organisasi</small>
                    <h6 class="mb-0">{{ $id->organisasi }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">perusahaan</small>
                    <h6 class="mb-0">{{ $id->perusahaan }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">Masyarakat</small>
                    <h6 class="mb-0">{{ $id->masyarakat }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">jumlah</small>
                    <h6 class="mb-0">{{ $id->jumlah }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">Sasaran Pelayanan</small>
                    <h6 class="mb-0">{{ $id->user_id }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                  <div class="me-2">
                    <small class="text-muted d-block mb-1">jaringan</small>
                    <h6 class="mb-0">{{ $id->jaringan }}</h6>
                  </div>
                </div>
              </li>
              <li class="d-flex mb-4 pb-1">
                    <form action="/world/{{$id->id}}" method="post">
                        @csrf
                        <a href="/world/{{$id->id}}/edit">
                            <button class="btn btn-primary" type="button">Ubah Data</button>
                        </a>
                        @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger">
                    </form>
              </li>
            </ul>
          </div>
        </div>
      </div>
@endsection
