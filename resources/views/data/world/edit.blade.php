
@extends('layout.master')

@section('judul')
    Perbaharui data Dunia Usaha Yang Melakukan Usaha Kesejahteraan Masyarakat
@endsection
@section('content')
    <div class="card-body">
        <div class="row">
            <div class="demo-vertical-spacing demo-only-element col-md-5 ">
                <form action="/world/{{ $id->id }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="mb-3">
                    <label clas="form-label" for="kecamatan">Kecamatan</label>
                    <input type="text" class="form-control" id="kecamatan" name="kecamatan" value="{{ old('kecamatan', $id->kecamatan) }}" placeholder="Masukan kecamatan" />
                </div>
                    @error('kecamatan')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                <div class="mb-3">
                    <label class="form-label" for="kelurahan">Kelurahan</label>
                    <input type="text" class="form-control" id="kelurahan" name="kelurahan" value="{{ old('kelurahan', $id->kelurahan) }}" placeholder="Masukan kelurahan" />
                </div>
                    @error('kelurahan')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                <div class="mb-3">
                    <label class="form-label" for="nama">Nama </label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', $id->nama) }}" placeholder="Masukan nama " />
                </div>
                    @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                <div class="mb-3">
                    <label class="form-label" for="alamat">Alamat (Jl./RT/RW)</label>
                    <input type="text" class="form-control" id="alamat" name="alamat" value="{{ old('alamat', $id->alamat) }}" placeholder="Masukan alamat penyuluhan" />
                </div>
                    @error('alamat')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                <div class="mb-3">
                    <label class="form-label" for="tahun">Tahun </label>
                    <input type="number" class="form-control" id="tahun" name="tahun" value="{{ old('tahun', $id->tahun) }}" placeholder="Masukan tahun penyuluhan" />
                </div>
                    @error('tahun')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="col">
                        <label class="text-dark fw-semibold">Jenis</label>
                        <div class="form-check">
                            <label for="anak" class="form-label">Penyantunan Anak</label>
                            <input type="radio" class="form-check-input" id="anak" name="jenis" value="1">
                            @error('jenis')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-check">
                            <label for="lanjut" class="form-label">Penyantunan Lanjut Usia</label>
                            <input type="radio" class="form-check-input" id="lanjut" name="jenis" value="2">
                            @error('jenis')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-check">
                            <label for="fakir" class="form-label">Penyantunan fakir-miskin</label>
                            <input type="radio" class="form-check-input" id="fakir" name="jenis" value="3">
                            @error('jenis')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-check">
                            <label for="cacat" class="form-label">Penyantunan cacat</label>
                            <input type="radio" class="form-check-input" id="cacat" name="jenis" value="4">
                            @error('jenis')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-check">
                            <label for="uep" class="form-label">UEP</label>
                            <input type="radio" class="form-check-input" id="uep" name="jenis" value="5">
                            @error('jenis')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-check">
                            <label for="lainnya" class="form-label">Lainnya</label>
                            <input type="radio" class="form-check-input" id="lainnya" name="jenis" value="6">
                            @error('jenis')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="col">
                            <label class="text-dark fw-semibold">organisasi</label>
                            <div class="form-check">
                                <label for="yayasan" class="form-label">Yayasan</label>
                                <input type="radio" class="form-check-input" id="yayasan" name="organisasi" value="1">
                                @error('organisasi')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="nonYayasan" class="form-label">Bekerja sama dengan masyarakat</label>
                                <input type="radio" class="form-check-input" id="nonYayasan" name="organisasi" value="2">
                                @error('organisasi')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label class="form-label" for="perusahaan">Jumlah dari Perusahaan </label>
                                <input type="number" class="form-control" id="perusahaan" value="{{ old('perusahaan', $id->perusahaan) }}" name="perusahaan" placeholder="Masukan perusahaan " />
                            </div>
                                @error('perusahaan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            <div class="mb-3">
                                <label class="form-label" for="masyarakat">Jumlah dar Masyarakat </label>
                                <input type="number" class="form-control" id="masyarakat" name="masyarakat" value="{{ old('masyarakat', $id->masyarakat) }}" placeholder="Masukan masyarakat " />
                            </div>
                                @error('masyarakat')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            <div class="mb-3">
                                <label class="form-label" for="jumlah">Jumlah </label>
                                <input type="number" class="form-control" id="jumlah" name="jumlah" value="{{ old('jumlah', $id->jumlah) }}" placeholder="Masukan jumlah " />
                            </div>
                                @error('jumlah')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror

                        <div class="col">
                            <label class="text-dark fw-semibold">Jaringan kerja</label>
                            <div class="form-check">
                                <label for="pemerintah" class="form-label">Lembaga Pemerintah</label>
                                <input type="radio" class="form-check-input" id="pemerintah" name="jaringan" value="1">
                                @error('jaringan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="nonPemerintah" class="form-label">Lembaga Non Pemerintah</label>
                                <input type="radio" class="form-check-input" id="nonPemerintah" name="jaringan" value="2">
                                @error('jaringan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="perguruan" class="form-label">Perguruan Tinggi</label>
                                <input type="radio" class="form-check-input" id="perguruan" name="jaringan" value="3">
                                @error('jaringan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="institusi" class="form-label">Institusi riset</label>
                                <input type="radio" class="form-check-input" id="institusi" name="jaringan" value="4">
                                @error('jaringan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="besar" class="form-label">Industri besar</label>
                                <input type="radio" class="form-check-input" id="besar" name="jaringan" value="5">
                                @error('jaringan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="menengah" class="form-label">Industri menengah</label>
                                <input type="radio" class="form-check-input" id="menengah" name="jaringan" value="6">
                                @error('jaringan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="kecil" class="form-label">Industri kecil</label>
                                <input type="radio" class="form-check-input" id="kecil" name="jaringan" value="7">
                                @error('jaringan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="koperasi" class="form-label">Koperasi</label>
                                <input type="radio" class="form-check-input" id="koperasi" name="jaringan" value="8">
                                @error('jaringan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="wirausaha" class="form-label">Wirausaha</label>
                                <input type="radio" class="form-check-input" id="wirausaha" name="jaringan" value="9">
                                @error('jaringan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                                        <div class="mb-3">
                                            <label class="form-label" for="identity_id">Kode Identitas Lembaga</label>
                                            <select class="form-control" id="identity_id" name="identity_id">
                                                @foreach ($identities as $identity)
                                                   <option value="{{ $identity->id }}">{{ $identity->nama }}</option>
                                                @endforeach
                                             </select>                                           </div>
                                            @error('identity_id')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror

                        <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
@endsection
