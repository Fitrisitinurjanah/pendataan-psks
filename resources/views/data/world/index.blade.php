@extends('layout.master')
@section('judul')
<div class="row">
    <div class="col">
        Data Dunia Usaha Yang Melakukan Usaha Kesejahteraan Masyarakat
        <div class="mt-3">
            <a href="{{ route('world.create') }}">
                <button class="btn btn-primary" type="button">Tambah Data</button>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <label for="datepicker_start">Tanggal Pendataan</label>
        <input type="date" class="form-control mt-1" name="tgl_surat" placeholder="Contoh. 2021-11-14" id="datepicker_start">
        <div class="d-grid gap-2 d-md-block mt-2">
            <button class="btn btn-primary" type="button">PDF</button>
            <a href="{{ route('world.export') }}"><button class="btn btn-primary" type="button"> Excel</button></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Nama </th>
                    <th rowspan="2">Kecamatan</th>
                    <th rowspan="2">Desa/Kelurahan</th>
                    <th rowspan="2">Alamat</th>
                    <th rowspan="2">Tahun Berdiri</th>
                    <th rowspan="2">Jenis UKS</th>
                    <th rowspan="2">Organisasi</th>
                    <th colspan="3">Sasaran Pelayanan</th>
                    <th rowspan="2">Jaringan kerja</th>
                </tr>
                <tr>
                    <th>Dalam Perusahaan</th>
                    <th>Masyarakat</th>
                    <th>Jumlah</th>
                </tr>


            </thead>
            <tbody>
                @forelse ($worlds as $key => $world)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $world->kecamatan }}</td>
                        <td>{{ $world->kelurahan }}</td>
                        <td><a href="/world/{{$world->id}}">{{ $world->nama }}</a></td>
                        <td>{{ $world->alamat }}</td>
                        <td>{{ $world->tahun }}</td>
                        <td>{{ $world->jenis }}</td>
                        <td>{{ $world->organisasi }}</td>
                        <td>{{ $world->perusahaan }}</td>
                        <td>{{ $world->masyarakat }}</td>
                        <td>{{ $world->jumlah }}</td>
                        <td>{{ $world->jaringan }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="12" align="center"> No Data Recorded </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection
