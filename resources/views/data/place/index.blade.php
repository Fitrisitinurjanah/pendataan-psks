@extends('layout.master')
@section('judul')
    <div class="row">
        <div class="col">
            Pengenalan Tempat
            <div class="mt-3">
                <a href="{{ route('place.create') }}">
                    <button class="btn btn-primary" type="button">Tambah Data</button>
                </a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="d-grid gap-2 d-md-block mt-2">
                <a href="{{ route('place.print') }}" target="_blank" class="btn btn-primary">PDF</a>
                <button class="btn btn-primary" type="button"> Excel</button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
                <thead class>
                    <tr>
                        <th>No</th>
                        <th>Provinsi</th>
                        <th>Kota</th>
                        <th>Kecamatan</th>
                        <th>Kelurahan</th>
                        <th>Status Daerah</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($places as $key => $place)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $place->provinsi }}</td>
                            <td>{{ $place->kota }}</td>
                            <td>{{ $place->kecamatan }}</td>
                            <td><a href="/place/{{ $place->id }}">{{ $place->kelurahan }}</a></td>
                            <td>{{ $place->status }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6" align="center"> No Data Recorded </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
