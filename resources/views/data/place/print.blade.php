<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak</title>
</head>

<body>
    <div class="form-group">
        <p align="center"><b>Laporan Data Keluarga Pisioner</b></p>
        <table class="static" align="center" rules="all" border="1px" style="width:95%;">
            <thead class>
                <tr>
                    <th>No</th>
                    <th>Provinsi</th>
                    <th>Kota</th>
                    <th>Kecamatan</th>
                    <th>Kelurahan</th>
                    <th>Status Daerah</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($places as $key => $place)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $place->provinsi }}</td>
                        <td>{{ $place->kota }}</td>
                        <td>{{ $place->kecamatan }}</td>
                        <td><a href="/place/{{ $place->id }}">{{ $place->kelurahan }}</a></td>
                        <td>{{ $place->status }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6" align="center"> No Data Recorded </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</body>

</html>
