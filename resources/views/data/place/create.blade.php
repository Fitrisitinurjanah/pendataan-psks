@extends('layout.master')

@section('judul')
    Membuat Data Pengenalan Tempat
@endsection

@section('content')
    <div class="card-body">
        <div class="row">
            <div class="demo-vertical-spacing demo-only-element col-md-5 ">
                <form action="/place" method="POST">
                    @csrf
                    <div class="row g-3">
                        <div class="col-md-6">
                            <label class="text-dark fw-semibold">Provinsi</label>
                            <div class="form-check">
                                <label for="provinsi" class="form-label">Provinsi Jawa Barat</label>
                                <input type="radio" class="form-check-input" id="provinsi" name="provinsi" value="02">
                                @error('provinsi')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="text-dark fw-semibold">Kota</label>
                            <div class="form-check">
                                <label for="kota" class="form-label">Kota Bandung</label>
                                <input type="radio" class="form-check-input" id="kota" name="kota" value="60">
                                @error('kota')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="text-dark fw-semibold">Kecamatan</label>
                            <div class="form-check">
                                <label for="kecamatan" class="form-label">Kecamatan Sukajadi</label>
                                <input type="radio" class="form-check-input" id="kecamatan" name="kecamatan" value="307">
                                @error('kecamatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="col-md-6">
                            <label class="text-dark fw-semibold">Kelurahan</label>
                            <div class="form-check">
                                <label for="kelurahanPasteur" class="form-label">Kelurahan Pasteur</label>
                                <input type="radio" class="form-check-input" id="kelurahanPasteur" name="kelurahan" value="001">
                                @error('kelurahan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="kelurahanCipedes" class="form-label">Kelurahan Cipedes</label>
                                <input type="radio" class="form-check-input" id="kelurahanCipedes" name="kelurahan" value="002">
                                @error('kelurahan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="kelurahanSukawarna" class="form-label">Kelurahan Sukawarna</label>
                                <input type="radio" class="form-check-input" id="kelurahanSukawarna" name="kelurahan" value="003">
                                @error('kecamatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="kelurahanSukagalih" class="form-label">Kelurahan Sukagalih</label>
                                <input type="radio" class="form-check-input" id="kelurahanSukagalih" name="kelurahan" value="004">
                                @error('kecamatan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="kelurahanSukabungah" class="form-label">Kelurahan Sukabungah</label>
                                <input type="radio" class="form-check-input" id="kelurahanSukabungah" name="kelurahan" value="005">
                                @error('kelurahan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="text-dark fw-semibold">Status Daerah</label>
                            <div class="form-check">
                                <label for="perkotaan" class="form-label">Perkotaan</label>
                                <input type="radio" class="form-check-input" id="perkotaan" name="status" value="1">
                                @error('status')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-check">
                                <label for="pedesaan" class="form-label">Pedesaan</label>
                                <input type="radio" class="form-check-input" id="pedesaan" name="status" value="2">
                                @error('status')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </form>
            </div>
        </div>
    </div>
@endsection
