@extends('layout.master')
@section('judul')
<div class="row">
    <div class="col">
        Data Penyuluhan
        <div class="mt-3">
            <a href="{{ route('extension.create') }}">
                <button class="btn btn-primary" type="button">Tambah Data</button>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <label for="datepicker_start">Tanggal Pendataan</label>
        <input type="date" class="form-control mt-1" name="tgl_surat" placeholder="Contoh. 2021-11-14" id="datepicker_start">
        <div class="d-grid gap-2 d-md-block mt-2">
            <button class="btn btn-primary" type="button">PDF</button>
            <a href="{{ route('extension.export') }}"><button class="btn btn-primary" type="button"> Excel</button></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Kecamatan</th>
                    <th rowspan="2">Desa/Kelurahan</th>
                    <th rowspan="2">Nama Lembaga</th>
                    <th rowspan="2">Alamat</th>
                    <th rowspan="2">Jenis Kelamin</th>
                    <th rowspan="2">Usia</th>
                    <th rowspan="2">Pendidikan</th>
                    <th rowspan="2">Pekerjaan</th>
                    <th rowspan="2">Kegiatan</th>
                    <th colspan="2">Pelatihan</th>

                </tr>
                <tr>
                    <th>Kesos</th>
                    <th>Non - Kesos</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($extensions as $key => $extension)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $extension->kecamatan }}</td>
                        <td>{{ $extension->kelurahan }}</td>
                        <td><a href="/extension/{{$extension->id}}">{{ $extension->nama }}</a></td>
                        <td>{{ $extension->alamat }}</td>
                        <td>{{ $extension->jenis_kelamin }}</td>
                        <td>{{ $extension->usia }}</td>
                        <td>{{ $extension->pendidikan }}</td>
                        <td>{{ $extension->pekerjaan }}</td>
                        <td>{{ $extension->kegiatan }}</td>
                        <td>{{ $extension->kesos }}</td>
                        <td>{{ $extension->non_kesos }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="12" align="center"> No Data Recorded </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection
