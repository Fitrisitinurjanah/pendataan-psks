<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak</title>
</head>

<body>
    <div class="form-group">
        <p align="center"><b>Laporan Data Keluarga Pisioner</b></p>
        <table class="static" align="center" rules="all" border="1px" style="width:95%;">
            thead class>
            <tr>
                <th>No</th>
                <th>Nama Pengawas</th>
                <th>Tanggal Pemeriksaan</th>
                <th>Tanda Tangan Pengawas</th>
            </tr>
            </thead>
            <tbody>
                @forelse ($coordinators as $key => $coordinator)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $coordinator->nama }}</td>
                        <td><a href="/coordinator/{{ $coordinator->id }}">{{ $coordinator->tanggal }}</a></td>
                        <td>{{ $coordinator->ttd }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6" align="center"> No Data Recorded </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</body>

</html>
