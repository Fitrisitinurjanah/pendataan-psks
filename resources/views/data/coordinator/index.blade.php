@extends('layout.master')
@section('judul')
    <div class="row">
        <div class="col">
            Keterangan Petugas (Koordinator)
            <div class="mt-3">
                <a href="{{ route('coordinator.create') }}">
                    <button class="btn btn-primary" type="button">Tambah Data</button>
                </a>
            </div>
        </div>
        <div class="col-md-3">

            <div class="d-grid gap-2 d-md-block mt-2">
                <a href="{{ route('coordinator.print') }}" target="_blank" class="btn btn-primary">PDF</a>
                <button class="btn btn-primary" type="button"> Excel</button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
                <thead class>
                    <tr>
                        <th>No</th>
                        <th>Nama Pengawas</th>
                        <th>Tanggal Pemeriksaan</th>
                        <th>Tanda Tangan Pengawas</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($coordinators as $key => $coordinator)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $coordinator->nama }}</td>
                            <td><a href="/coordinator/{{ $coordinator->id }}">{{ $coordinator->tanggal }}</a></td>
                            <td>{{ $coordinator->ttd }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6" align="center"> No Data Recorded </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
