<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo">
        <a href="/home" class="app-brand-link">
            <span><img src="{{ asset('img/logo.png') }}" width="30px" alt=""></span>
            <span class="app-brand-text demo menu-text fw-bolder ms-2">haur galur</span>
        </a>

        <a href="/home" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
            <i class="bx bx-chevron-left bx-sm align-middle"></i>
        </a>
    </div>

    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py-1">
        <!-- Dashboard -->
        <li class="menu-item">
            <a href="/home" class="menu-link">
                <i class="menu-icon tf-icons bx bx-home-circle"></i>
                <div data-i18n="Analytics">Dashboard</div>
            </a>
        </li>

        <!-- Layouts -->

        <!-- pages -->
        <li class="menu-header small text-uppercase">
            <span class="menu-header-text">Pages</span>
        </li>

        <!-- Instrumen Pendataan -->
        <li class="menu-item">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="menu-icon tf-icons bx bx-user-pin"></i>
                <div data-i18n="Account Settings">Petugas</div>
            </a>
            <ul class="menu-sub">
                <li class="menu-item">
                    <a href="/operator" class="menu-link">
                        <i class='menu-icon tf-icon bx bxs-user-detail'></i>
                        <div data-i18n="Connections">Pendata</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="/coordinator" class="menu-link">
                        <i class='menu-icon tf-icon bx bxs-user-voice'></i>
                        <div data-i18n="Connections">Pengawas</div>
                    </a>
                </li>
            </ul>
        </li>

        <!-- Instrumen Pendataan -->
        <li class="menu-item">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="menu-icon tf-icons bx bx-detail"></i>
                <div data-i18n="Account Settings">Instrumen Pendataan</div>
            </a>
            <ul class="menu-sub">
                <li class="menu-item">
                    <a href="/place" class="menu-link">
                        <div data-i18n="Account">Pengenalan Tempat</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="/identity" class="menu-link">
                        <div data-i18n="Account">Identitas Lembaga</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="/profesional" class="menu-link">
                        <div data-i18n="Notifications">Pekerja Sosial Profesional</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="/psm" class="menu-link">
                        <div data-i18n="Connections">Pekerja Sosial Masyarakat (PSM)</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="/wpks" class="menu-link">
                        <div data-i18n="Connections">Wanita Pemimpin Kesejahteraan Sosial(WPKS)</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="/extension" class="menu-link">
                        <div data-i18n="Account">Penyuluh Sosial</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="/cadet" class="menu-link">
                        <div data-i18n="Connections">Taruna Siaga Bencana</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="/youth" class="menu-link">
                        <div data-i18n="Connections">Karang Taruna</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="/wksbm" class="menu-link">
                        <div data-i18n="Connections">Wahana Kesejahteraan Sosial Berbasis Masyarakat</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="/lks" class="menu-link">
                        <div data-i18n="Connections">Lembaga Kesejahteraan Sosial</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="/lkkk" class="menu-link">
                        <div data-i18n="Connections">Lembaga Konsultasi Kesejahteraan Keluarga (LK3)</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="/family" class="menu-link">
                        <div data-i18n="Connections">Keluarga Pioner</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="/tksk" class="menu-link">
                        <div data-i18n="Connections">Tenaga Kesejahteraan Sosial Kecamatan (TKSK)</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="/world" class="menu-link">
                        <div data-i18n="Connections">Dunia Usaha Yang Melakukan Usaha Kesejahteraan Sosial (World)</div>
                    </a>
                </li>
            </ul>
        </li>



        <!-- Misc -->
        <li class="menu-header small text-uppercase"><span class="menu-header-text">Misc</span></li>
        <li class="menu-item">
            <a href="https://instagram.com/fitrisnurjannah" class="menu-link">
                <i class="menu-icon tf-icons bx bx-support"></i>
                <div data-i18n="Support">Developer I</div>
            </a>
        </li>
        <li class="menu-item">
            <a href="https://instagram.com/dikdikmusfar" class="menu-link">
                <i class="menu-icon tf-icons bx bx-support"></i>
                <div data-i18n="Support">Developer II</div>
            </a>
        </li>
        <li class="menu-item">
            <a href="https://www.instagram.com/karta_haurgalur" target="_blank" class="menu-link">
                <i class="menu-icon tf-icons bx bx-file"></i>
                <div data-i18n="Documentation">Special Thanks To :</div>
            </a>
        </li>
    </ul>
</aside>
