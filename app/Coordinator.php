<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordinator extends Model
{
    protected $table = 'coordinators';

    protected $guarded = [];

    public function user() {
        return $this->belongsTo('App\User','user_id');
    }

    public function identity(){
        return $this->hasMany('App\Identity');
    }
}
