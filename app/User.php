<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile(){
        return $this->hasOne('App\Profile');
    }

    public function coordinator(){
        return $this->hasMany('App\Coordinator');
    }

    public function operator(){
        return $this->hasMany('App\Operator');
    }

    public function place(){
        return $this->hasMany('App\Place');
    }

    public function identity(){
        return $this->hasMany('App\Identity');
    }

    public function wpks(){
        return $this->hasMany('App\Wpks');
    }

    public function extension(){
        return $this->hasMany('App\Extension');
    }

    public function profesional(){
        return $this->hasMany('App\Profesional');
    }

    public function psm(){
        return $this->hasMany('App\Psm');
    }

    public function cadet(){
        return $this->hasMany('App\Cadet');
    }

    public function youth(){
        return $this->hasMany('App\Youth');
    }

    public function family(){
        return $this->hasMany('App\Family');
    }

    public function world(){
        return $this->hasMany('App\World');
    }

    public function wksbm(){
        return $this->hasMany('App\Wksbm');
    }

    public function lks(){
        return $this->hasMany('App\Lks');
    }

    public function lkkk(){
        return $this->hasMany('App\Lkkk');
    }

    public function tksk(){
        return $this->hasMany('App\Tksk');
    }
}
