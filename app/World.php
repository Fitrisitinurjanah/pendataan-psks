<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class World extends Model
{
    protected $table = 'worlds';

    protected $guarded = [];

    public function identity() {
        return $this->belongsTo('App\Identity','identity_id');
    }

    public function user() {
        return $this->belongsTo('App\User','user_id');
    }
}
