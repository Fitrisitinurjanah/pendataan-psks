<?php

namespace App\Http\Controllers;

use App\Place;
use App\Identity;
use App\Coordinator;
use Illuminate\Http\Request;
use App\Exports\IdentitiesExport;
use App\Operator;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class IdentityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $identities = Identity::all();
        return view('data.identity.index', compact('identities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $places = Place::all();
        $coordinators = Coordinator::all();
        $operators = Operator::all();
        return view('data.identity.create', compact('places','coordinators','operators'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'rt' => 'required',
            'nama_pimpinan' => 'required',
            'nama_responden' => 'required',
            'jabatan' => 'required',
            'bukti' => 'required|mimes:jpeg,png,jpg,gif,svg,pdf',
            'place_id' => 'required',
            'coordinator_id' => 'required',
            'operator_id' => 'required'
        ]);
        $buktiName = $request->bukti->getClientOriginalName().'-'.time().'.'.$request->bukti->extension();
        $request->bukti->move(public_path('bukti'), $buktiName);


        $post = Identity::create([
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            'rt' => $request["rt"],
            'nama_pimpinan' => $request["nama_pimpinan"],
            "nama_responden" => $request["nama_responden"],
            'jabatan' => $request["jabatan"],
            'bukti' => $buktiName,
            "operator_id" => $request["operator_id"],
            'coordinator_id' => $request["coordinator_id"],
            'place_id' => $request["place_id"],
            "user_id" => Auth::id()
        ]);
        Alert::success('Berhasil', 'Berhasil membuat data');
        return redirect('/identity');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Identity  $identity
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Identity::find($id);

        return view('data.identity.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Identity  $identity
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Identity::find($id);
        $places = Place::all();
        $coordinators = Coordinator::all();
        $operators = Operator::all();
        return view('data.identity.edit', compact('id','places','coordinators','operators'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Identity  $identity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'rt' => 'required',
            'nama_pimpinan' => 'required',
            'nama_responden' => 'required',
            'jabatan' => 'required',
            'place_id' => 'required',
            'coordinator_id' => 'required',
            'operator_id' => 'required'
        ]);

        $update = Identity::where('id', $id)->update([
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            'rt' => $request["rt"],
            'nama_pimpinan' => $request["nama_pimpinan"],
            "nama_responden" => $request["nama_responden"],
            'jabatan' => $request["jabatan"],
            "operator_id" => $request["operator_id"],
            'place_id' => $request["place_id"],
            'coordinator_id' => $request["coordinator_id"],
        ]);
        Alert::success('Berhasil', 'Berhasil merubah data');
        return redirect('/identity');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Identity  $identity
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Identity::destroy($id);
        Alert::warning('Berhasil', 'Berhasil menghapus data');
        return redirect('/identity');
    }

    public function export()
    {
        return Excel::download(new IdentitiesExport, 'identities.xlsx');
    }
}
