<?php

namespace App\Http\Controllers;

use App\Youth;
use App\Identity;
use Illuminate\Http\Request;
use App\Exports\YouthsExport;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class YouthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $youths = Youth::all();
        return view('data.youth.index', compact('youths'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $identities = Identity::all();
        return view('data.youth.create', compact('identities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'tahun' => 'required|numeric',
            'klasifikasi' => 'required',
            'laki' => 'required|numeric',
            'perempuan' => 'required|numeric',
            'jumlah' => 'required|numeric',
            'kegiatan' => 'required',
            'identity_id' => 'required|numeric'
        ]);
        $post = Youth::create([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "tahun" => $request["tahun"],
            "klasifikasi" => $request["klasifikasi"],
            'laki' => $request["laki"],
            "perempuan" => $request["perempuan"],
            'jumlah' => $request["jumlah"],
            'kegiatan' => $request["kegiatan"],
            'identity_id' => $request["identity_id"],
            "user_id" => Auth::id()
        ]);
        Alert::success('Berhasil', 'Berhasil membuat data');
        return redirect('/youth');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Youth  $youth
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Youth::find($id);

        return view('data.youth.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Youth  $youth
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Youth::find($id);
        $identities = Identity::all();
        return view('data.youth.edit', compact('id','identities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Youth  $youth
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'tahun' => 'required|numeric',
            'klasifikasi' => 'required',
            'laki' => 'required|numeric',
            'perempuan' => 'required|numeric',
            'jumlah' => 'required|numeric',
            'kegiatan' => 'required',
            'identity_id' => 'required|numeric'
        ]);

        $update = Youth::where('id', $id)->update([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "tahun" => $request["tahun"],
            "klasifikasi" => $request["klasifikasi"],
            'laki' => $request["laki"],
            "perempuan" => $request["perempuan"],
            'jumlah' => $request["jumlah"],
            'kegiatan' => $request["kegiatan"],
            'identity_id' => $request["identity_id"]
        ]);
        Alert::success('Berhasil', 'Berhasil merubah data');
        return redirect('/youth');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Youth  $youth
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Youth::destroy($id);
        Alert::success('Berhasil', 'Berhasil menghapus data');
        return redirect('/youth');
    }

    public function export()
    {
        return Excel::download(new YouthsExport, 'extensions.xlsx');
    }
}
