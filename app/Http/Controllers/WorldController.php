<?php

namespace App\Http\Controllers;

use App\World;
use App\Identity;
use Illuminate\Http\Request;
use App\Exports\WorldsExport;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class WorldController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $worlds = World::all();
        return view('data.world.index', compact('worlds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $identities = Identity::all();
        return view('data.world.create', compact('identities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'tahun' => 'required|numeric',
            'jenis' => 'required|numeric',
            'organisasi' => 'required',
            'perusahaan' => 'required|numeric',
            'masyarakat' => 'required|numeric',
            'jumlah' => 'required|numeric',
            'jaringan' => 'required',
            'identity_id' => 'required|numeric'
        ]);
        $post = World::create([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "tahun" => $request["tahun"],
            "jenis" => $request["jenis"],
            'organisasi' => $request["organisasi"],
            'perusahaan' => $request["perusahaan"],
            "masyarakat" => $request["masyarakat"],
            'jumlah' => $request["jumlah"],
            'jaringan' => $request["jaringan"],
            'identity_id' => $request["identity_id"],
            "user_id" => Auth::id()
        ]);
        Alert::success('Berhasil', 'Berhasil membuat data');
        return redirect('/world');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\World  $world
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = World::find($id);

        return view('data.world.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\World  $world
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = World::find($id);
        $identities = Identity::all();
        return view('data.world.edit', compact('id','identities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\World  $world
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'tahun' => 'required|numeric',
            'jenis' => 'required|numeric',
            'organisasi' => 'required',
            'perusahaan' => 'required|numeric',
            'masyarakat' => 'required|numeric',
            'jumlah' => 'required|numeric',
            'jaringan' => 'required',
            'identity_id' => 'required|numeric'
        ]);
        $update = World::where('id', $id)->update([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "tahun" => $request["tahun"],
            "jenis" => $request["jenis"],
            'organisasi' => $request["organisasi"],
            'perusahaan' => $request["perusahaan"],
            "masyarakat" => $request["masyarakat"],
            'jumlah' => $request["jumlah"],
            'jaringan' => $request["jaringan"],
            'identity_id' => $request["identity_id"],
            "user_id" => Auth::id()
        ]);
        Alert::success('Berhasil', 'Berhasil merubah data');
        return redirect('/world');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\World  $world
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        World::destroy($id);
        Alert::warning('Berhasil', 'Berhasil menghapus data');
        return redirect('/world');
    }
    public function export()
    {
        return Excel::download(new WorldsExport, 'extensions.xlsx');
    }
}
