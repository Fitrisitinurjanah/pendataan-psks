<?php

namespace App\Http\Controllers;

use App\Tksk;
use App\Identity;
use App\Exports\TksksExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class TkskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tksks = Tksk::all();
        return view('data.tksk.index', compact('tksks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $identities = Identity::all();
        return view('data.tksk.create', compact('identities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'usia' => 'required|numeric',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'kegiatan' => 'required|numeric',
            'kesos' => 'required',
            'non_kesos' => 'required',
            'identity_id' => 'required|numeric'
        ]);
        $post = Tksk::create([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "usia" => $request["usia"],
            'pendidikan' => $request["pendidikan"],
            'pekerjaan' => $request["pekerjaan"],
            "kegiatan" => $request["kegiatan"],
            'kesos' => $request["kesos"],
            'non_kesos' => $request["non_kesos"],
            'identity_id' => $request["identity_id"],
            "user_id" => Auth::id()
        ]);
        Alert::success('Berhasil', 'Berhasil membuat data');
        return redirect('/tksk');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tksk  $tksk
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Tksk::find($id);

        return view('data.tksk.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tksk  $tksk
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Tksk::find($id);
        $identities = Identity::all();
        return view('data.tksk.edit', compact('id','identities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tksk  $tksk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'usia' => 'required|numeric',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'kegiatan' => 'required|numeric',
            'kesos' => 'required',
            'non_kesos' => 'required',
            'identity_id' => 'required|numeric'
        ]);

        $update = Tksk::where('id', $id)->update([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "usia" => $request["usia"],
            'pendidikan' => $request["pendidikan"],
            'pekerjaan' => $request["pekerjaan"],
            "kegiatan" => $request["kegiatan"],
            'kesos' => $request["kesos"],
            'non_kesos' => $request["non_kesos"],
            'identity_id' => $request["identity_id"]
        ]);
        Alert::success('Berhasil', 'Berhasil merubah data');
        return redirect('/tksk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tksk  $tksk
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tksk::destroy($id);
        Alert::warning('Berhasil', 'Berhasil menghapus data');
        return redirect('/tksk');
    }
    public function export()
    {
        return Excel::download(new TksksExport, 'extensions.xlsx');
    }
}
