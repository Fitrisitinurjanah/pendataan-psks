<?php

namespace App\Http\Controllers;

use App\Wpks;
use App\Identity;
use Illuminate\Http\Request;
use App\Exports\WpksesExport;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class WpksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wpkses = Wpks::all();
        return view('data.wpks.index', compact('wpkses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $identities = Identity::all();
        return view('data.wpks.create', compact('identities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'usia' => 'required|numeric',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'kegiatan' => 'required|numeric',
            'kesos' => 'required',
            'non_kesos' => 'required',
            'identity_id' => 'required|numeric'
        ]);
        $post = Wpks::create([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "usia" => $request["usia"],
            'pendidikan' => $request["pendidikan"],
            'pekerjaan' => $request["pekerjaan"],
            "kegiatan" => $request["kegiatan"],
            'kesos' => $request["kesos"],
            'non_kesos' => $request["non_kesos"],
            'identity_id' => $request["identity_id"],
            "user_id" => Auth::id()
        ]);
        Alert::success('Berhasil', 'Berhasil membuat data');
        return redirect('/wpks');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Wpks  $wpks
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Wpks::find($id);

        return view('data.wpks.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Wpks  $wpks
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Wpks::find($id);
        $identities = Identity::all();
        return view('data.wpks.edit', compact('id','identities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Wpks  $wpks
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'usia' => 'required|numeric',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'kegiatan' => 'required|numeric',
            'kesos' => 'required',
            'non_kesos' => 'required',
            'identity_id' => 'required|numeric'
        ]);

        $update = Wpks::where('id', $id)->update([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "usia" => $request["usia"],
            'pendidikan' => $request["pendidikan"],
            'pekerjaan' => $request["pekerjaan"],
            "kegiatan" => $request["kegiatan"],
            'kesos' => $request["kesos"],
            'non_kesos' => $request["non_kesos"],
            'identity_id' => $request["identity_id"]
        ]);
        Alert::success('Berhasil', 'Berhasil merubah data');
        return redirect('/wpks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Wpks  $wpks
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Wpks::destroy($id);
        Alert::warning('Berhasil', 'Berhasil menghapus data');
        return redirect('/wpks');
    }

    public function export()
    {
        return Excel::download(new WpksesExport, 'extensions.xlsx');
    }
}
