<?php

namespace App\Http\Controllers;

use App\Identity;
use App\Profesional;
use Illuminate\Http\Request;
use App\Exports\ProfesionalsExport;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class ProfesionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profesionals = Profesional::all();
        return view('data.profesional.index', compact('profesionals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $identities = Identity::all();
        return view('data.profesional.create', compact('identities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'usia' => 'required|numeric',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'kegiatan' => 'required',
            'nomor' => 'required',
            'tanggal' => 'required',
            'jabatan' => 'required',
            'identity_id' => 'required|numeric',
        ]);
        $post = Profesional::create([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "usia" => $request["usia"],
            'pendidikan' => $request["pendidikan"],
            'pekerjaan' => $request["pekerjaan"],
            "kegiatan" => $request["kegiatan"],
            'nomor' => $request["nomor"],
            'tanggal' => $request["tanggal"],
            'jabatan' => $request["jabatan"],
            'identity_id' => $request["identity_id"],
            "user_id" => Auth::id()
        ]);
        Alert::success('Berhasil', 'Berhasil membuat data');
        return redirect('/profesional');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profesional  $profesional
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Profesional::find($id);
        return view('data.profesional.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profesional  $profesional
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Profesional::find($id);
        $identities = Identity::all();
        return view('data.profesional.edit', compact('id','identities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profesional  $profesional
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'usia' => 'required|numeric',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'kegiatan' => 'required',
            'nomor' => 'required',
            'tanggal' => 'required',
            'jabatan' => 'required',
            'identity_id' => 'required|numeric'
        ]);

        $update = Profesional::where('id', $id)->update([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "usia" => $request["usia"],
            'pendidikan' => $request["pendidikan"],
            'pekerjaan' => $request["pekerjaan"],
            "kegiatan" => $request["kegiatan"],
            'nomor' => $request["nomor"],
            'tanggal' => $request["tanggal"],
            'jabatan' => $request["jabatan"],
            'identity_id' => $request["identity_id"]
        ]);
        Alert::success('Berhasil', 'Berhasil merubah data');
        return redirect('/profesional');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profesional  $profesional
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Profesional::destroy($id);
        Alert::warning('Berhasil', 'Berhasil menghapus data');
        return redirect('/profesional');
    }
    public function export()
    {
        return Excel::download(new ProfesionalsExport, 'extensions.xlsx');
    }
}
