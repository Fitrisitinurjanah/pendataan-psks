<?php

namespace App\Http\Controllers;

use App\Lks;
use App\Identity;
use App\Exports\LksesExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class LksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lkses = Lks::all();
        return view('data.lks.index', compact('lkses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $identities = Identity::all();
        return view('data.lks.create', compact('identities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'tahun' => 'required|numeric',
            'status' => 'required',
            'tipologi' => 'required',
            'laki' => 'required|numeric',
            'perempuan' => 'required|numeric',
            'jumlah' => 'required|numeric',
            'kegiatan' => 'required',
            'identity_id' => 'required|numeric',
        ]);
        $post = Lks::create([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "tahun" => $request["tahun"],
            "status" => $request["status"],
            'tipologi' => $request["tipologi"],
            'laki' => $request["laki"],
            "perempuan" => $request["perempuan"],
            'jumlah' => $request["jumlah"],
            'kegiatan' => $request["kegiatan"],
            'identity_id' => $request["identity_id"],
            "user_id" => Auth::id()
        ]);
        Alert::success('Berhasil', 'Berhasil membuat data');
        return redirect('/lks');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lks  $lks
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Lks::find($id);

        return view('data.lks.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lks  $lks
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Lks::find($id);
        $identities = Identity::all();
        return view('data.lks.edit', compact('id','identities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lks  $lks
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'tahun' => 'required|numeric',
            'status' => 'required',
            'tipologi' => 'required',
            'laki' => 'required|numeric',
            'perempuan' => 'required|numeric',
            'jumlah' => 'required|numeric',
            'kegiatan' => 'required',
            'identity_id' => 'required|numeric',
        ]);

        $update = Lks::where('id', $id)->update([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "tahun" => $request["tahun"],
            'tipologi' => $request["tipologi"],
            'laki' => $request["laki"],
            "perempuan" => $request["perempuan"],
            'jumlah' => $request["jumlah"],
            'kegiatan' => $request["kegiatan"],
            'identity_id' => $request["identity_id"],
        ]);
        Alert::success('Berhasil', 'Berhasil merubah data');
        return redirect('/lks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lks  $lks
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Lks::destroy($id);
        Alert::warning('Berhasil', 'Berhasil menghapus data');
        return redirect('/lks');
    }
    public function export()
    {
        return Excel::download(new LksesExport, 'extensions.xlsx');
    }
}
