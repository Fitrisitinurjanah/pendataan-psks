<?php

namespace App\Http\Controllers;

use App\Operator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class OperatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $operators = Operator::all();
        return view('data.operator.index', compact('operators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {
        return view('data.operator.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store (Request $request)
    {
        $request->validate([
            'tanggal' => 'required',
            'ttd' => 'required|mimes:jpeg,png,jpg,gif,svg,pdf'
        ]);
        $ttdName = $request->ttd->getClientOriginalName().'-'.time().'.'.$request->ttd->extension();
        $request->ttd->move(public_path('ttd'), $ttdName);

        $post = Operator::create([
            "tanggal" => $request["tanggal"],
            'ttd' => $ttdName,
            "user_id" => Auth::id()
        ]);
        Alert::success('Berhasil', 'Berhasil membuat data');
        return redirect('/operator');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Operator  $operator
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Operator::find($id);

        return view('data.operator.show',compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Operator  $operator
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Operator::find($id);
        return view('data.operator.edit', compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Operator  $operator
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tanggal' => 'required'
        ]);

        $update = Operator::where('id', $id)->update([
            "tanggal" => $request["tanggal"]
        ]);
        Alert::success('Berhasil', 'Berhasil merubah data');
        return redirect('/operator');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Operator  $operator
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Operator::destroy($id);
        Alert::warning('Berhasil', 'Berhasil membuat data');
        return redirect('/operator');
    }
}
