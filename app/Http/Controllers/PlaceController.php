<?php

namespace App\Http\Controllers;

use App\Place;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class PlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $places = Place::all();
        return view('data.place.index', compact('places'));
    }
    public function print()
    {
        $places = Place::all();
        $pdf = PDF::loadview('data.place.print',compact('places'));
        return $pdf->download('pendataan.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('data.place.create');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $request->validate([
            'provinsi' => 'required',
            'kota' => 'required',
            'kecamatan' => 'required',
            'kelurahan' => 'required|unique:places',
            'status' => 'required',
        ]);

        $post = Place::create([
            "provinsi" => $request["provinsi"],
            "kota" => $request["kota"],
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "status" => $request["status"],
            "user_id" => Auth::id()
        ]);
        Alert::success('Berhasil', 'Berhasil membuat data');
        return redirect('/place');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Place::find($id);

        return view('data.place.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Place  $place
     * @return \Illuminate\Http\Response
     */
    /** Tidak ada edit untuk Place */
    // public function edit($id)
    // {
    //     $id = Place::find($id);
    //     return view('data.place.edit', compact('id'));
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Place  $place
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     $request->validate([
    //         'provinsi' => 'required',
    //         'kota' => 'required',
    //         'kecamatan' => 'required',
    //         'kelurahan' => 'required',
    //         'status' => 'required',
    //         ]);
    //     $update = Place::where('id', $id)->update([
    //         "provinsi" => $request["provinsi"],
    //         "kota" => $request["kota"],
    //         "kecamatan" => $request["kecamatan"],
    //         "kelurahan" => $request["kelurahan"],
    //         "status" => $request["status"]
    //         ]);
    //     return redirect('/place');
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Place::destroy($id);
        Alert::warning('Berhasil', 'Berhasil menghapus data');
        return redirect('/place');
    }
}
