<?php

namespace App\Http\Controllers;

use App\Lkkk;
use App\Identity;
use App\Exports\LkksExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class LkkkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lkkks = Lkkk::all();
        return view('data.lkkk.index', compact('lkkks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $identities = Identity::all();
        return view('data.lkkk.create', compact('identities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'tahun' => 'required|numeric',
            'status' => 'required',
            'tipologi' => 'required',
            'laki' => 'required|numeric',
            'perempuan' => 'required|numeric',
            'jumlah' => 'required|numeric',
            'kegiatan' => 'required',
            'identity_id' => 'required|numeric',
        ]);
        $post = Lkkk::create([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "tahun" => $request["tahun"],
            "status" => $request["status"],
            'tipologi' => $request["tipologi"],
            'laki' => $request["laki"],
            "perempuan" => $request["perempuan"],
            'jumlah' => $request["jumlah"],
            'kegiatan' => $request["kegiatan"],
            'identity_id' => $request["identity_id"],
            "user_id" => Auth::id()
        ]);
        Alert::success('Berhasil', 'Berhasil membuat data');
        return redirect('/lkkk');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lkkk  $lkkk
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Lkkk::find($id);

        return view('data.lkkk.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lkkk  $lkkk
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $identities = Identity::all();
        $id = Lkkk::find($id);
        return view('data.lkkk.edit', compact('id','identities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lkkk  $lkkk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'tahun' => 'required|numeric',
            'status' => 'required',
            'tipologi' => 'required',
            'laki' => 'required|numeric',
            'perempuan' => 'required|numeric',
            'jumlah' => 'required|numeric',
            'kegiatan' => 'required',
            'identity_id' => 'required|numeric',
        ]);

        $update = Lkkk::where('id', $id)->update([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "tahun" => $request["tahun"],
            'tipologi' => $request["tipologi"],
            'laki' => $request["laki"],
            "perempuan" => $request["perempuan"],
            'jumlah' => $request["jumlah"],
            'kegiatan' => $request["kegiatan"],
            'identity_id' => $request["identity_id"],
        ]);
        Alert::success('Berhasil', 'Berhasil merubah data');
        return redirect('/lkkk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lkkk  $lkkk
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Lkkk::destroy($id);
        Alert::warning('Berhasil', 'Berhasil menghapus data');
        return redirect('/lkkk');
    }

    public function export()
    {
        return Excel::download(new LkksExport, 'lkkks.xlsx');
    }
}
