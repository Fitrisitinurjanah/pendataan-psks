<?php

namespace App\Http\Controllers;

use App\Coordinator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;
use RealRashid\SweetAlert\Facades\Alert;

class CoordinatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coordinators = Coordinator::all();
        return view('data.coordinator.index', compact('coordinators'));
    }
    public function print()
    {
        $coordinators = Coordinator::all();
        $pdf = PDF::loadview('data.coordinator.print',compact('coordinators'));
        return $pdf->download('pendataan.pdf');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('data.coordinator.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'tanggal' => 'required',
            'ttd' => 'required|mimes:jpeg,png,jpg,gif,svg,pdf'
        ]);
        $ttdName = $request->ttd->getClientOriginalName().'-'.time().'.'.$request->ttd->extension();
        $request->ttd->move(public_path('ttd'), $ttdName);

        $post = Coordinator::create([
            "nama" => $request["nama"],
            "tanggal" => $request["tanggal"],
            'ttd' => $ttdName,
            "user_id" => Auth::id()
        ]);
        Alert::success('Berhasil', 'Berhasil menambahkan data');
        return redirect('/coordinator');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Coordinator  $coordinator
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Coordinator::find($id);

        return view('data.coordinator.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Coordinator  $coordinator
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Coordinator::find($id);
        return view('data.coordinator.edit', compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coordinator  $coordinator
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'tanggal' => 'required'
        ]);

        $update = Coordinator::where('id', $id)->update([
            "nama" => $request["nama"],
            "tanggal" => $request["tanggal"]
        ]);
        Alert::success('Berhasil', 'Berhasil merubah data');
        return redirect('/coordinator');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coordinator  $coordinator
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Coordinator::destroy($id);
        Alert::warning('Perhatian', 'Berhasil menghapus data');
        return redirect('/coordinator');
    }
}
