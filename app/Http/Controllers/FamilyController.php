<?php

namespace App\Http\Controllers;

use App\Family;
use App\Identity;
use Illuminate\Http\Request;
use App\Exports\FamiliesExport;
use Illuminate\Support\Facades\Auth;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class FamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $families = Family::all();
        return view('data.family.index', compact('families'));
    }

    public function print()
    {
        $families = Family::all();
        $pdf = PDF::loadview('data.family.print',compact('families'));
        return $pdf->download('pendataan.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $identities = Identity::all();
        return view('data.family.create', compact('identities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'usia' => 'required|numeric',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'kegiatan' => 'required|numeric',
            'kesos' => 'required',
            'non_kesos' => 'required',
            'identity_id' => 'required|numeric'
        ]);
        $post = Family::create([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "usia" => $request["usia"],
            'pendidikan' => $request["pendidikan"],
            'pekerjaan' => $request["pekerjaan"],
            "kegiatan" => $request["kegiatan"],
            'kesos' => $request["kesos"],
            'non_kesos' => $request["non_kesos"],
            'identity_id' => $request["identity_id"],
            "user_id" => Auth::id()
        ]);
        Alert::success('Berhasil', 'Berhasil menambahkan data');
        return redirect('/family');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Family::find($id);

        return view('data.family.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Family::find($id);
        $identities = Identity::all();
        return view('data.family.edit', compact('id','identities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'usia' => 'required|numeric',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'kegiatan' => 'required|numeric',
            'kesos' => 'required',
            'non_kesos' => 'required',
            'identity_id' => 'required|numeric'
        ]);

        $update = Family::where('id', $id)->update([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "usia" => $request["usia"],
            'pendidikan' => $request["pendidikan"],
            'pekerjaan' => $request["pekerjaan"],
            "kegiatan" => $request["kegiatan"],
            'kesos' => $request["kesos"],
            'non_kesos' => $request["non_kesos"],
            'identity_id' => $request["identity_id"]
        ]);
        Alert::success('Berhasil', 'Berhasil merubah data');
        return redirect('/family');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */



    public function destroy($id)
    {
        Family::destroy($id);
        Alert::warning('Berhasil', 'Berhasil menghapus data');
        return redirect('/family');
    }

    public function export()
    {
        return Excel::download(new FamiliesExport, 'families.xlsx');
    }
}
