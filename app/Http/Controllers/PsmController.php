<?php

namespace App\Http\Controllers;

use App\Psm;
use App\Identity;
use App\Exports\PsmsExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class PsmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $psms = Psm::all();
        return view('data.psm.index', compact('psms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $identities = Identity::all();
        return view('data.psm.create', compact('identities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'usia' => 'required|numeric',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'kegiatan' => 'required|numeric',
            'kesos' => 'required',
            'non_kesos' => 'required',
            'identity_id' => 'required|numeric'
        ]);
        $post = Psm::create([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "usia" => $request["usia"],
            'pendidikan' => $request["pendidikan"],
            'pekerjaan' => $request["pekerjaan"],
            "kegiatan" => $request["kegiatan"],
            'kesos' => $request["kesos"],
            'non_kesos' => $request["non_kesos"],
            'identity_id' => $request["identity_id"],
            "user_id" => Auth::id()
        ]);
        Alert::success('Berhasil', 'Berhasil membuat data');
        return redirect('/psm');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Psm  $psm
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Psm::find($id);

        return view('data.psm.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Psm  $psm
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Psm::find($id);
        $identities = Identity::all();
        return view('data.psm.edit', compact('id','identities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Psm  $psm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'usia' => 'required|numeric',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'kegiatan' => 'required|numeric',
            'kesos' => 'required',
            'non_kesos' => 'required',
            'identity_id' => 'required|numeric'
        ]);

        $update = Psm::where('id', $id)->update([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "usia" => $request["usia"],
            'pendidikan' => $request["pendidikan"],
            'pekerjaan' => $request["pekerjaan"],
            "kegiatan" => $request["kegiatan"],
            'kesos' => $request["kesos"],
            'non_kesos' => $request["non_kesos"],
            'identity_id' => $request["identity_id"]
        ]);
        Alert::success('Berhasil', 'Berhasil merubah data');
        return redirect('/psm');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Psm  $psm
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Psm::destroy($id);
        Alert::warning('Berhasil', 'Berhasil menghapus data');
        return redirect('/psm');
    }
    public function export()
    {
        return Excel::download(new PsmsExport, 'extensions.xlsx');
    }
}
