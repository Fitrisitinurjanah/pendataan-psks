<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use App\Cadet;
use App\Identity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Exports\CadetsExport;
use Maatwebsite\Excel\Facades\Excel;

class CadetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cadets = Cadet::all();
        return view('data.cadet.index', compact('cadets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $identities = Identity::all();
        return view('data.cadet.create', compact('identities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'usia' => 'required|numeric',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'kegiatan' => 'required',
            'pelatihan' => 'required',
            'identity_id' => 'required|numeric',
        ]);
        $post = Cadet::create([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "usia" => $request["usia"],
            'pendidikan' => $request["pendidikan"],
            'pekerjaan' => $request["pekerjaan"],
            "kegiatan" => $request["kegiatan"],
            'pelatihan' => $request["pelatihan"],
            'identity_id' => $request["identity_id"],
            "user_id" => Auth::id()
        ]);
        Alert::success('Berhasil', 'Berhasil menambahkan data');
        return redirect('/cadet');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cadet  $cadet
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Cadet::find($id);

        return view('data.cadet.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cadet  $cadet
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Cadet::find($id);
        $identities = Identity::all();
        return view('data.cadet.edit', compact('id','identities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cadet  $cadet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'usia' => 'required|numeric',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'kegiatan' => 'required',
            'pelatihan' => 'required',
            'identity_id' => 'required|numeric',
        ]);
        $update = Cadet::where('id', $id)->update([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "usia" => $request["usia"],
            'pendidikan' => $request["pendidikan"],
            'pekerjaan' => $request["pekerjaan"],
            "kegiatan" => $request["kegiatan"],
            'pelatihan' => $request["pelatihan"],
            'identity_id' => $request["identity_id"]
        ]);
        Alert::success('Berhasil', 'Berhasil merubah data');
        return redirect('/cadet');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cadet  $cadet
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cadet::destroy($id);
        Alert::warning('Perhatian', 'Data telah dihapus!');
        return redirect('/cadet');
    }

    public function export()
    {
        return Excel::download(new CadetsExport, 'cadets.xlsx');
    }

}
