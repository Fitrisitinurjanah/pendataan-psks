<?php

namespace App\Http\Controllers;

use App\Identity;
use App\Extension;
use Illuminate\Http\Request;
use App\Exports\ExtensionsExport;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class ExtensionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $extensions = Extension::all();
        return view('data.extension.index', compact('extensions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $identities = Identity::all();
        return view('data.extension.create', compact('identities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'usia' => 'required|numeric',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'kegiatan' => 'required',
            'kesos' => 'required',
            'non_kesos' => 'required',
            'identity_id' => 'required|numeric',
        ]);
        $post = Extension::create([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "usia" => $request["usia"],
            'pendidikan' => $request["pendidikan"],
            'pekerjaan' => $request["pekerjaan"],
            "kegiatan" => $request["kegiatan"],
            'kesos' => $request["kesos"],
            'non_kesos' => $request["non_kesos"],
            'identity_id' => $request["identity_id"],
            "user_id" => Auth::id()
        ]);
        Alert::success('Berhasil', 'Berhasil menambahkan data');
        return redirect('/extension');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Extension  $extension
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = extension::find($id);

        return view('data.extension.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Extension  $extension
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Extension::find($id);
        $identities = Identity::all();
        return view('data.extension.edit', compact('id','identities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Extension  $extension
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'usia' => 'required|numeric',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'kegiatan' => 'required',
            'kesos' => 'required',
            'non_kesos' => 'required',
            'identity_id' => 'required|numeric',
        ]);
        $update = Extension::where('id', $id)->update([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "usia" => $request["usia"],
            'pendidikan' => $request["pendidikan"],
            'pekerjaan' => $request["pekerjaan"],
            "kegiatan" => $request["kegiatan"],
            'kesos' => $request["kesos"],
            'non_kesos' => $request["non_kesos"],
            'identity_id' => $request["identity_id"]
        ]);
        Alert::success('Berhasil', 'Berhasil merubah data');
        return redirect('/extension');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Extension  $extension
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        extension::destroy($id);
        Alert::warning('Perhatian', 'Berhasil menghapus data');
        return redirect('/extension');
    }

    public function export()
    {
        return Excel::download(new ExtensionsExport, 'extensions.xlsx');
    }
}
