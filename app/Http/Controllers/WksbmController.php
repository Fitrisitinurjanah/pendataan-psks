<?php

namespace App\Http\Controllers;

use App\Wksbm;
use App\Identity;
use Illuminate\Http\Request;
use App\Exports\WksbmsExport;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class WksbmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wksbms = Wksbm::all();
        return view('data.wksbm.index', compact('wksbms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $identities = Identity::all();
        return view('data.wksbm.create', compact('identities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'tahun' => 'required',
            'unsur' => 'required',
            'kegiatan' => 'required',
            'identity_id' => 'required|numeric'
        ]);
        $post = Wksbm::create([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "tahun" => $request["tahun"],
            "unsur" => $request["unsur"],
            'kegiatan' => $request["kegiatan"],
            'identity_id' => $request["identity_id"],
            "user_id" => Auth::id()
        ]);
        Alert::success('Berhasil', 'Berhasil membuat data');
        return redirect('/wksbm');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Wksbm  $wksbm
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Wksbm::find($id);

        return view('data.wksbm.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Wksbm  $wksbm
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Wksbm::find($id);
        $identities = Identity::all();
        return view('data.wksbm.edit', compact('id','identities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Wksbm  $wksbm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'tahun' => 'required',
            'unsur' => 'required|numeric',
            'kegiatan' => 'required',
            'identity_id' => 'required|numeric'
        ]);
        $update = Wksbm::where('id', $id)->update([
            "kecamatan" => $request["kecamatan"],
            "kelurahan" => $request["kelurahan"],
            "nama" => $request["nama"],
            'alamat' => $request["alamat"],
            "tahun" => $request["tahun"],
            "unsur" => $request["unsur"],
            'kegiatan' => $request["kegiatan"],
            'identity_id' => $request["identity_id"],
            "user_id" => Auth::id()
        ]);
        Alert::success('Berhasil', 'Berhasil merubah data');
        return redirect('/wksbm');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Wksbm  $wksbm
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Wksbm::destroy($id);
        Alert::Warning('Perhatian', 'Berhasil menghapus data');
        return redirect('/wksbm');
    }
    public function export()
    {
        return Excel::download(new WksbmsExport, 'extensions.xlsx');
    }
}
