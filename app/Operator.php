<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operator extends Model
{
    protected $table = 'operators';

    protected $guarded = [];

    public function user() {
        return $this->belongsTo('App\User','user_id');
    }

    public function identity(){
        return $this->hasMany('App\Identity');
    }
}
