<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Identity extends Model
{
    protected $table = 'identities';

    protected $guarded = [];

    public function user() {
        return $this->belongsTo('App\User','user_id');
    }

    public function place() {
        return $this->belongsTo('App\Place','place_id');
    }

    public function coordinator() {
        return $this->belongsTo('App\Coordinator','coordinator_id');
    }

    public function operator() {
        return $this->belongsTo('App\Operator','operator_id');
    }

    public function wpks(){
        return $this->hasMany('App\Wpks');
    }

    public function extension(){
        return $this->hasMany('App\Extension');
    }

    public function profesional(){
        return $this->hasMany('App\Profesional');
    }

    public function psm(){
        return $this->hasMany('App\Psm');
    }

    public function cadet(){
        return $this->hasMany('App\Cadet');
    }

    public function youth(){
        return $this->hasMany('App\Youth');
    }

    public function family(){
        return $this->hasMany('App\Family');
    }

    public function world(){
        return $this->hasMany('App\World');
    }

    public function wksbm(){
        return $this->hasMany('App\Wksbm');
    }

    public function lks(){
        return $this->hasMany('App\Lks');
    }

    public function lkkk(){
        return $this->hasMany('App\Lkkk');
    }

    public function tksk(){
        return $this->hasMany('App\Tksk');
    }
}
