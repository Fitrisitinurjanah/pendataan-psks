<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $table = 'places';

    protected $guarded = [];

    public function user() {
        return $this->belongsTo('App\User','user_id');
    }

    public function identity(){
        return $this->hasMany('App\Identity');
    }
}
