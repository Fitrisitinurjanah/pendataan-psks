<?php

namespace App\Exports;

use App\Wksbm;
use Maatwebsite\Excel\Concerns\FromCollection;

class WksbmsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Wksbm::all();
    }
}
