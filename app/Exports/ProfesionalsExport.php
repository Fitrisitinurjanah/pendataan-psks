<?php

namespace App\Exports;

use App\Profesional;
use Maatwebsite\Excel\Concerns\FromCollection;

class ProfesionalsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Profesional::all();
    }
}
