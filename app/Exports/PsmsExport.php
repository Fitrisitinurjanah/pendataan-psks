<?php

namespace App\Exports;

use App\Psm;
use Maatwebsite\Excel\Concerns\FromCollection;

class PsmsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Psm::all();
    }
}
