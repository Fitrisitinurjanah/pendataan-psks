<?php

namespace App\Exports;

use App\Identity;
use Maatwebsite\Excel\Concerns\FromCollection;

class IdentitiesExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Identity::all();
    }
}
