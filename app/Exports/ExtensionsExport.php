<?php

namespace App\Exports;

use App\Extension;
use Maatwebsite\Excel\Concerns\FromCollection;

class ExtensionsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Extension::all();
    }
}
