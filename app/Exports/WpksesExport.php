<?php

namespace App\Exports;

use App\Wpks;
use Maatwebsite\Excel\Concerns\FromCollection;

class WpksesExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Wpks::all();
    }
}
