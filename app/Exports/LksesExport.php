<?php

namespace App\Exports;

use App\Lks;
use Maatwebsite\Excel\Concerns\FromCollection;

class LksesExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Lks::all();
    }
}
