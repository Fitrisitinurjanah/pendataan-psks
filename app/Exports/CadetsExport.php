<?php

namespace App\Exports;

use App\Cadet;
use Maatwebsite\Excel\Concerns\FromCollection;

class CadetsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Cadet::all();
    }
}
