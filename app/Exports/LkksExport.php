<?php

namespace App\Exports;

use App\Lkkk;
use Maatwebsite\Excel\Concerns\FromCollection;

class LkksExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Lkkk::all();
    }
}
