<?php

namespace App\Exports;

use App\World;
use Maatwebsite\Excel\Concerns\FromCollection;

class WorldsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return World::all();
    }
}
