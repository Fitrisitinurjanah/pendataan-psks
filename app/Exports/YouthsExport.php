<?php

namespace App\Exports;

use App\Youth;
use Maatwebsite\Excel\Concerns\FromCollection;

class YouthsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Youth::all();
    }
}
