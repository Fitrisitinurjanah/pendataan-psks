<?php

namespace App\Exports;

use App\Tksk;
use Maatwebsite\Excel\Concerns\FromCollection;

class TksksExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Tksk::all();
    }
}
